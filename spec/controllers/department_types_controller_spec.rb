require 'rails_helper'

RSpec.describe DepartmentTypesController, type: :controller do
  include LookupSpec

  let(:user) { create(:sate_auth_user) }
  let!(:application_module) { create(:sate_auth_application_module, code: "CRMT") }
  let(:header) { { 'Authorization' => token_generator(user.id) } }

  let(:valid_attributes) {
    {
        code: FFaker::Name.name,
        name: FFaker::Name.name
    }
  }

  let(:invalid_attributes) {
    {
        code: '',
        name: FFaker::Name.name
    }
  }

  describe "GET #index" do
    it 'gets all department types' do
      get_index
    end
  end

  describe "POST #create" do
    context 'with valid params' do
      it 'creates a new department type' do
        post_valid
      end

      it 'should return a success message' do
        post_message
      end
    end

    context 'with invalid params' do
      it 'should return an error message' do
        post_invalid
      end
    end
  end

  describe "PUT #update" do
    context 'with valid params' do
      let(:new_attributes) {
        {
            code: FFaker::Name.name
        }
      }

      it 'updates the requested department' do
        put_valid
      end

      it 'should return a success message' do
        put_message
      end
    end

    context 'with invalid params' do
      it 'should return an error message' do
        put_invalid
      end
    end
  end

end
