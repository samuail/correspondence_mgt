require 'rails_helper'

RSpec.describe RoyaltiesController, type: :controller do

  let(:user) { create(:sate_auth_user) }
  let!(:application_module) { create(:sate_auth_application_module, code: "CRMT") }
  let!(:manuscript) { create(:sate_crmgt_manuscript) }
  let!(:author) { create(:sate_crmgt_author) }

  let(:header) { { 'Authorization' => token_generator(user.id) } }

  let(:valid_attributes) {
    {
      manuscript_id: manuscript.id,
      author_id: author.id,
      reference_no: FFaker::Name.name,
      amount: FFaker.numerify("#.##"),
      remark: FFaker::Name.name,
      payment_date: FFaker::Time.datetime,
      key_in_by_id: user.id,
      application_module_id: application_module.id
    }
  }

  describe "GET #index" do
    before :each do
      Sate::Crmgt::Royalty.create valid_attributes
      @royalty_get = Sate::Crmgt::Royalty.where application_module_id: application_module.id,
                                                author_id: author.id
      request.headers.merge!(header)
      get :index, params: { author_id: author.id }
    end

    it "returns a success response" do
      expect(response).to be_successful
    end

    it "assigns royalties as @royalties" do
      expect(assigns(:royalties)).to match_array(@royalty_get)
    end
  end

  describe "POST #create" do
    manuscript_id = nil
    author_id = nil
    reference_no = nil
    amount = nil
    remark = nil
    let!(:manuscript_id) { manuscript_id }
    let!(:author_id) { author_id }
    let!(:reference_no) { reference_no }
    let!(:amount) { amount }
    let!(:remark) { remark }

    before :each do
      create :sate_crmgt_royalty, manuscript_id: manuscript.id,
             author_id: author.id, reference_no: "ABC-123-2022",
             amount: 30000, remark: "Sample Remark",
             payment_date: FFaker::Time.datetime,
             key_in_by_id: user.id,
             application_module_id: application_module.id
      request.headers.merge!(header)
      post :create, params: { :royalty => { manuscript_id: manuscript_id,
                                            author_id: author_id,
                                            reference_no: reference_no,
                                            amount: amount,
                                            remark: remark } }
      @decoded_response = JSON(@response.body)
    end

    context "with valid params" do
      let!(:manuscript_id) { manuscript_id = manuscript.id }
      let!(:author_id) { author_id = author.id }
      let!(:reference_no) { reference_no = FFaker::Name.name }
      let!(:amount) { amount = FFaker.numerify("#.##") }
      let!(:remark) { remark = FFaker::Name.name }
      it "creates a new Royalty" do
        expect(Sate::Crmgt::Royalty.count).to eq 2
      end

      it "returns a success message" do
        expect(@response).to be_successful
        expect(@decoded_response["message"]).to eq "Royalty Saved Successfully!"
      end
    end

    context "with invalid params" do
      context "Blank Attributes" do
        context "Manuscript" do
          let!(:manuscript_id) { manuscript_id = nil }
          let!(:author_id) { author_id = author.id }
          let!(:reference_no) { reference_no = FFaker::Name.name }
          let!(:amount) { amount = FFaker.numerify("#.##") }
          let!(:remark) { remark = FFaker::Name.name }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Royalty Manuscript must exist", "Royalty Manuscript can't be blank"]
          end
        end
        context "Author" do
          let!(:manuscript_id) { manuscript_id = manuscript.id }
          let!(:author_id) { author_id = nil }
          let!(:reference_no) { reference_no = FFaker::Name.name }
          let!(:amount) { amount = FFaker.numerify("#.##") }
          let!(:remark) { remark = FFaker::Name.name }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Royalty Author must exist", "Royalty Author can't be blank"]
          end
        end
        context "Reference No" do
          let!(:manuscript_id) { manuscript_id = manuscript.id }
          let!(:author_id) { author_id = author.id }
          let!(:reference_no) { reference_no = nil }
          let!(:amount) { amount = FFaker.numerify("#.##") }
          let!(:remark) { remark = FFaker::Name.name }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Royalty Reference no can't be blank"]
          end
        end
        context "Amount" do
          let!(:manuscript_id) { manuscript_id = manuscript.id }
          let!(:author_id) { author_id = author.id }
          let!(:reference_no) { reference_no = FFaker::Name.name }
          let!(:amount) { amount = nil }
          let!(:remark) { remark = FFaker::Name.name }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Royalty Amount can't be blank"]
          end
        end
      end
    end
  end

  describe "PUT #update" do
    manuscript_id = nil
    author_id = nil
    reference_no = nil
    amount = nil
    remark = nil
    let!(:manuscript_id) { manuscript_id }
    let!(:author_id) { author_id }
    let!(:reference_no) { reference_no }
    let!(:amount) { amount }
    let!(:remark) { remark }

    before :each do
      @update = create :sate_crmgt_royalty, manuscript_id: manuscript.id,
                       author_id: author.id, reference_no: "ABC-123-2022",
                       amount: 30000, remark: "Sample Remark",
                       payment_date: FFaker::Time.datetime,
                       key_in_by_id: user.id,
                       application_module_id: application_module.id
      request.headers.merge!(header)
      put :update, params: { id: @update.to_param,
                             :royalty => { manuscript_id: manuscript_id,
                                           author_id: author_id,
                                           reference_no: reference_no,
                                           amount: amount,
                                           remark: remark } }
      @decoded_response = JSON(@response.body)
    end

    context "with valid params" do
      let!(:manuscript_id) { manuscript_id = manuscript.id }
      let!(:author_id) { author_id = author.id }
      let!(:reference_no) { reference_no = FFaker::Name.name }
      let!(:amount) { amount = FFaker.numerify("#.##") }
      let!(:remark) { remark = FFaker::Name.name }

      it "Updates the requested Royalty" do
        expect(@update.reference_no).to_not eq attributes_for(:sate_crmgt_royalty)[:reference_no]
      end

      it "returns a success message" do
        expect(@response).to be_successful
        expect(@decoded_response["message"]).to eq "Royalty Updated Successfully!"
      end
    end

    context "with invalid params" do
      context "Blank Attributes" do
        context "Manuscript" do
          let!(:manuscript_id) { manuscript_id = nil }
          let!(:author_id) { author_id = author.id }
          let!(:reference_no) { reference_no = FFaker::Name.name }
          let!(:amount) { amount = FFaker.numerify("#.##") }
          let!(:remark) { remark = FFaker::Name.name }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Royalty Manuscript must exist", "Royalty Manuscript can't be blank"]
          end
        end
        context "Author" do
          let!(:manuscript_id) { manuscript_id = manuscript.id }
          let!(:author_id) { author_id = nil }
          let!(:reference_no) { reference_no = FFaker::Name.name }
          let!(:amount) { amount = FFaker.numerify("#.##") }
          let!(:remark) { remark = FFaker::Name.name }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Royalty Author must exist", "Royalty Author can't be blank"]
          end
        end
        context "Reference No" do
          let!(:manuscript_id) { manuscript_id = manuscript.id }
          let!(:author_id) { author_id = author.id }
          let!(:reference_no) { reference_no = nil }
          let!(:amount) { amount = FFaker.numerify("#.##") }
          let!(:remark) { remark = FFaker::Name.name }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Royalty Reference no can't be blank"]
          end
        end
        context "Amount" do
          let!(:manuscript_id) { manuscript_id = manuscript.id }
          let!(:author_id) { author_id = author.id }
          let!(:reference_no) { reference_no = FFaker::Name.name }
          let!(:amount) { amount = nil }
          let!(:remark) { remark = FFaker::Name.name }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Royalty Amount can't be blank"]
          end
        end
      end
    end
  end

end
