require 'rails_helper'

# require 'controllers/access_controller_spec_context'

RSpec.describe AccessController, type: :controller do

  before :each do
    @user = build_stubbed(:sate_auth_user)
    @user_inactive = build_stubbed(:sate_auth_user)
    app_module = create(:sate_auth_application_module)

    Sate::Auth::User.create first_name: @user.first_name,
                            last_name: @user.last_name,
                            email: @user.email,
                            password: @user.password,
                            application_module_id: app_module.id
    Sate::Auth::User.create first_name: @user_inactive.first_name,
                            last_name: @user_inactive.last_name,
                            email: @user_inactive.email,
                            password: @user_inactive.password,
                            active: false,
                            application_module_id: app_module.id
  end

  describe "POST #login" do

    email = nil
    password = nil
    let!(:email) { email }
    let!(:password) { password }

    before :each do
      post :login, params: { email: email,
                             password: password }
      @decoded_response = JSON(@response.body)
    end

    context "with valid params" do

      let!(:email) { email = @user.email }
      let!(:password) { password = @user.password }

      it "login the user to the application" do
        expect(@decoded_response["data"]["user"]["first_name"]).to eq @user.first_name
      end

      it "returns a success and success message" do
        expect(@response).to be_successful
        expect(@decoded_response["success"]).to eq true
        expect(@decoded_response["message"]).to eq "Login Successful"
      end
    end

    context "with invalid params" do
      context "with invalid user password" do

        let!(:email) { email = @user.email }
        let!(:password) { password = @user.password + "1" }

        it "returns error message -- Invalid Credentials." do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"][0]).to eq "Invalid Credentials."
        end
      end

      context "with inactive user" do
        let!(:email) { email = @user_inactive.email }
        let!(:password) { password = @user_inactive.password }

        it "returns error message -- Inactive Credentials." do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"][0]).to eq "Inactive Credentials."
        end
      end

      context "with non existent user" do
        let!(:email) { email = @user.email + "1" }
        let!(:password) { password = @user.password }

        it "returns error message -- Credentials doesn't Exist." do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"][0]).to eq "Credentials doesn't Exist."
        end
      end
    end
  end

  describe "GET #logout" do

    context "with valid authentication token" do
      before :each do
        post :login, params: { email: @user.email,
                               password: @user.password }
        @login_response = JSON(@response.body)

        token = @login_response["data"]["access_token"]

        header = {'Authorization' => "Bearer #{token}"}

        request.headers.merge!(header)

        get :logout

        @decoded_response = JSON(@response.body)
      end

      it "returns success" do
        expect(@decoded_response["success"]).to eq true
      end

      it "returns success message -- Logout Successful" do
        expect(@decoded_response["message"]).to eq "Logout Successful"
      end
    end

    context "with no authentication token" do
      before :each do
        get :logout

        @decoded_response = JSON(@response.body)
      end

      it "returns error message -- Missing token" do
        expect(@decoded_response["errors"][0]).to eq "Missing token"
      end
    end

    context "with expired authentication token" do

      before :each do
        post :login, params: { email: @user.email,
                               password: @user.password }
        @login_response = JSON(@response.body)

        header = { 'Authorization' => expired_token_generator(@user.id) }

        request.headers.merge!(header)

        get :logout

        @decoded_response = JSON(@response.body)
      end

      it "returns error message -- Signature has expired" do
        expect(@decoded_response["errors"][0]).to eq "Signature has expired"
      end
    end

    context "with invalid authentication token" do

      before :each do
        post :login, params: { email: @user.email,
                               password: @user.password }
        @login_response = JSON(@response.body)

        header = { 'Authorization' => token_generator(5) }

        request.headers.merge!(header)

        get :logout

        @decoded_response = JSON(@response.body)
      end

      it "returns error message -- Invalid token" do
        expect(@decoded_response["errors"][0]).to eq "Invalid token Couldn't find Sate::Auth::User with 'id'=5"
      end
    end

    context "with fake authentication token" do

      before :each do
        post :login, params: { email: @user.email,
                               password: @user.password }
        @login_response = JSON(@response.body)

        header = { 'Authorization' => 'foobar' }

        request.headers.merge!(header)

        get :logout

        @decoded_response = JSON(@response.body)
      end

      it "returns error message -- Decode Error" do
        expect(@decoded_response["errors"][0]).to eq "Not enough or too many segments"
      end
    end
  end

end
