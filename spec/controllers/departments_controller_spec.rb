require 'rails_helper'

RSpec.describe DepartmentsController, type: :controller do
  let(:user) { create(:sate_auth_user) }
  let!(:application_module) { create(:sate_auth_application_module, code: "CRMT") }
  let!(:department_type) { create(:sate_crmgt_department_type) }
  let(:header) { { 'Authorization' => token_generator(user.id) } }

  let(:valid_attributes) {
    {
        code: FFaker::Name.name,
        description: FFaker::Name.name,
        department_type_id: department_type.id,
        application_module_id: application_module.id
    }
  }

  let(:invalid_attributes) {
    {
        code: '',
        description: FFaker::Name.name,
        department_type_id: department_type.id,
        application_module_id: application_module.id
    }
  }
  describe "GET #index" do
    before :each do
      Sate::Crmgt::Department.create valid_attributes
      @department_get = Sate::Crmgt::Department.where application_module_id: application_module.id
      request.headers.merge!(header)
      get :index
    end

    it "returns a success response" do
      expect(response).to be_successful
    end

    it "assigns all departments as @departments" do
      expect(assigns(:departments)).to eq(@department_get)
    end

  end

  describe "GET #get_department_by_type for Center" do
    before :each do
      parent_department = create(:sate_crmgt_department, description: "Main Campus", parent_id: nil, application_module_id: application_module.id)
      create(:sate_crmgt_department, parent_id: parent_department.id, application_module_id: application_module.id)

      other_department = create(:sate_crmgt_department, parent_id: parent_department.id, application_module_id: application_module.id)
      user_role = create(:sate_auth_user_role)
      Sate::Crmgt::UserDepartment.create user_id: user.id, user_role_id: user_role.id, department_id: other_department.id, application_module_id: application_module.id

      ids = Sate::Crmgt::Department.center
      @department_get = Sate::Crmgt::Department.where(application_module_id: application_module.id, id: ids).where.not(id: other_department.id).order(:description)

      request.headers.merge!(header)
      get :get_department_by_type, params: { department_type: "Center" }
    end

    it "returns a success response" do
      expect(response).to be_successful
    end

    it "assigns all departments as @departments" do
      expect(assigns(:departments)).to eq(@department_get)
    end
  end

  describe "GET #get_department_by_type for College" do
    before :each do
      parent_department = create(:sate_crmgt_department, description: "Main Campus", parent_id: nil, application_module_id: application_module.id)
      create(:sate_crmgt_department, parent_id: parent_department.id, application_module_id: application_module.id)
      create(:sate_crmgt_department, parent_id: nil, application_module_id: application_module.id)

      other_department = create(:sate_crmgt_department, parent_id: parent_department.id, application_module_id: application_module.id)
      user_role = create(:sate_auth_user_role)
      Sate::Crmgt::UserDepartment.create user_id: user.id, user_role_id: user_role.id, department_id: other_department.id, application_module_id: application_module.id

      ids = Sate::Crmgt::Department.college
      @department_get = Sate::Crmgt::Department.where(application_module_id: application_module.id, id: ids).where.not(id: other_department.id).order(:description)

      request.headers.merge!(header)
      get :get_department_by_type, params: { department_type: "College" }
    end

    it "returns a success response" do
      expect(response).to be_successful
    end

    it "assigns all departments as @departments" do
      expect(assigns(:departments)).to eq(@department_get)
    end
  end

  describe "GET #get_department_by_type for Internal Outgoing" do
    before :each do
      parent_department = create(:sate_crmgt_department, description: "Main Campus", parent_id: nil, application_module_id: application_module.id)
      create(:sate_crmgt_department, parent_id: parent_department.id, application_module_id: application_module.id)
      create(:sate_crmgt_department, parent_id: nil, application_module_id: application_module.id)

      other_department = create(:sate_crmgt_department, parent_id: parent_department.id, application_module_id: application_module.id)
      user_role = create(:sate_auth_user_role)
      Sate::Crmgt::UserDepartment.create user_id: user.id, user_role_id: user_role.id, department_id: other_department.id, application_module_id: application_module.id

      @department_get = Sate::Crmgt::Department.where(application_module_id: application_module.id)
                                               .where.not(id: other_department.id)
                                               .where.not(description: "Main Campus")
                                               .order(:description)

      request.headers.merge!(header)
      get :get_department_by_type, params: { department_type: "Outgoing Internal" }
    end

    it "returns a success response" do
      expect(response).to be_successful
    end

    it "assigns all departments as @departments" do
      expect(assigns(:departments)).to eq(@department_get)
    end
  end

  describe "GET #fetch_departments" do
    before :each do
      parent_department = create(:sate_crmgt_department, parent_id: nil, application_module_id: application_module.id)
      create(:sate_crmgt_department, parent_id: parent_department.id, application_module_id: application_module.id)
      request.headers.merge!(header)
      get :fetch_departments
    end

    it "returns a success response" do
      expect(response).to be_successful
    end

    it "assigns all departments as @departments" do
      expect(assigns(:departments)).to eq(JSON.parse(response.body)["data"])
    end
  end

  describe "POST #create" do
    code = nil
    description = nil
    let!(:code) { code }
    let!(:description) { description }

    before :each do
      create :sate_crmgt_department, code: "MAC",
             description: "Main Campus",
             department_type_id: department_type.id,
             application_module_id: application_module.id
      request.headers.merge!(header)
      post :create, params: { department: { 'code' => code,
                                            'description' => description,
                                            'department_type_id' => department_type.id } }
      @decoded_response = JSON(@response.body)
    end
    context "with valid params" do
      let!(:code) { code = "CBE" }
      let!(:description) { description = "College of BE" }

      it "creates a new Department" do
        expect(Sate::Crmgt::Department.count).to eq 2
      end

      it "returns a success message" do
        expect(@response).to be_successful
        expect(@decoded_response["message"]).to eq "Department Saved Successfully!"
      end
    end
    context "with invalid params" do
      context "Blank Department Code" do
        let!(:code) { code = nil }
        let!(:description) { description = "College of BE" }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["Department Code can't be blank"]
        end
      end

      context "Blank Department Description" do
        let!(:code) { code = "CBE" }
        let!(:description) { description = nil }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["Department Description can't be blank"]
        end
      end

      context "Duplicate Department code and description" do
        let!(:code) { code = "MAC" }
        let!(:description) { description = "Main Campus" }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["Department Code has already been taken", "Department Description has already been taken"]
        end
      end
    end
  end

  describe "PUT #update" do
    code = nil
    description = nil
    let!(:code) { code }
    let!(:description) { description }

    before :each do
      @department_update = create :sate_crmgt_department, code: "MAC",
                                  description: "Main Campus",
                                  department_type_id: department_type.id,
                                  application_module_id: application_module.id
      create :sate_crmgt_department, code: "CBE",
             description: "College of BE",
             department_type_id: department_type.id,
             application_module_id: application_module.id

      request.headers.merge!(header)
      put :update, params: { id: @department_update.to_param,
                             department: { 'code' => code,
                                           'description' => description } }
      @decoded_response = JSON(@response.body)
    end

    context "with valid params" do
      let!(:code) { code = "CHS" }
      let!(:description) { description = "College of HS" }

      it "updates the requested Department" do
        expect(@department_update.code).to_not eq attributes_for(:sate_crmgt_department)[:code]
      end

      it "returns a success message" do
        expect(@response).to be_successful
        expect(@decoded_response["message"]).to eq "Department Updated Successfully!"
      end
    end

    context "with invalid params" do
      context "Blank Department Code" do
        let!(:code) { code = nil }
        let!(:description) { description = "College of HS" }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["Department Code can't be blank"]
        end
      end

      context "Blank Department Description" do
        let!(:code) { code = "CHS" }
        let!(:description) { description = nil }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["Department Description can't be blank"]
        end
      end

      context "Duplicate Department Code and Description" do
        let!(:code) { code = "CBE" }
        let!(:description) { description = "College of BE" }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["Department Code has already been taken", "Department Description has already been taken"]
        end
      end
    end
  end

  describe "GET #get_users" do
    before :each do
      user_2 = create(:sate_auth_user)
      user_role = create(:sate_auth_user_role)
      department = Sate::Crmgt::Department.create valid_attributes
      Sate::Crmgt::UserDepartment.create user_id: user.id, user_role_id: user_role.id, department_id: department.id, application_module_id: application_module.id
      Sate::Crmgt::UserDepartment.create user_id: user_2.id, user_role_id: user_role.id, department_id: department.id, application_module_id: application_module.id
      @user_get = Sate::Auth::User.where id: user_2.id
      request.headers.merge!(header)
      get :get_users, params: {dept_id: department.id}
    end

    it "returns a success response" do
      expect(response).to be_successful
    end

    it "assigns all users within a department as @users" do
      expect(assigns(:users)).to eq(@user_get)
    end
  end

  describe "GET #get_users_with_the_role" do
    before :each do
      user_1 = create(:sate_auth_user)
      user_2 = create(:sate_auth_user)
      user_role = create(:sate_auth_user_role)
      department = Sate::Crmgt::Department.create valid_attributes
      Sate::Crmgt::UserDepartment.create user_id: user_1.id,
                                         user_role_id: user_role.id,
                                         department_id: department.id,
                                         application_module_id: application_module.id
      Sate::Crmgt::UserDepartment.create user_id: user_2.id,
                                         user_role_id: user_role.id,
                                         department_id: department.id,
                                         application_module_id: application_module.id
      # @user_get = Sate::Auth::User.where id: user_2.id

      role = Sate::Auth::UserRole.find user_role.id
      role_users = role.users

      user_departments = Sate::Crmgt::UserDepartment.where department_id: department.id

      @users_get = []

      user_departments.each do |user_department|
        if user_department.user_id != user.id
          @users_get << role_users.detect { |role_user| role_user[:id] == user_department.user_id }
        end
      end

      request.headers.merge!(header)

      get :get_users_with_the_role, params: {role_id: user_role.id,  department_id: department.id}
    end

    it "returns a success response" do
      expect(response).to be_successful
    end

    it "assigns all users within a department as @users" do
      expect(assigns(:users)).to eq(@users_get)
    end
  end

end
