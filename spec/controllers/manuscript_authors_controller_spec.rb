require 'rails_helper'

RSpec.describe ManuscriptAuthorsController, type: :controller do
  let(:user) { create(:sate_auth_user) }
  let!(:application_module) { create(:sate_auth_application_module, code: "CRMT") }
  let!(:manuscript) { create(:sate_crmgt_manuscript) }
  let!(:author) { create(:sate_crmgt_author) }
  let!(:another_author) { create(:sate_crmgt_author) }

  let(:header) { { 'Authorization' => token_generator(user.id) } }

  let(:valid_attributes) {
    {
      manuscript_id: manuscript.id,
      author_id: author.id,
      application_module_id: application_module.id
    }
  }

  describe "GET #index" do
    before :each do
      Sate::Crmgt::ManuscriptAuthor.create valid_attributes
      @manuscript_author_get = Sate::Crmgt::ManuscriptAuthor.where application_module_id: application_module.id,
                                                                   manuscript_id: manuscript.id
      request.headers.merge!(header)
      get :index, params: { manuscript_id: manuscript.id }
    end

    it "returns a success response" do
      expect(response).to be_successful
    end

    it "assigns manuscript author as @manuscript_author" do
      expect(assigns(:manuscript_authors)).to match_array(@manuscript_author_get)
    end
  end

  describe "POST #create" do
    manuscript_id = nil
    author_id = nil
    let!(:manuscript_id) { manuscript_id }
    let!(:author_id) { author_id }

    before :each do
      create :sate_crmgt_manuscript_author, manuscript_id: manuscript.id,
             author_id: author.id,
             application_module_id: application_module.id

      request.headers.merge!(header)
      post :create, params: { :manuscript_author => { manuscript_id: manuscript_id,
                                                      author_id: author_id } }
      @decoded_response = JSON(@response.body)
    end

    context "with valid params" do
      let!(:manuscript_id) { manuscript_id = manuscript.id }
      let!(:author_id) { author_id = create(:sate_crmgt_author).id }
      it "creates a new ManuscriptAuthor" do
        expect(Sate::Crmgt::ManuscriptAuthor.count).to eq 2
      end

      it "returns a success message" do
        expect(@response).to be_successful
        expect(@decoded_response["message"]).to eq "Manuscript Author Saved Successfully!"
      end
    end

    context "with invalid params" do
      context "Blank Attributes" do
        context "Manuscript" do
          let!(:manuscript_id) { manuscript_id = nil }
          let!(:author_id) { author_id = create(:sate_crmgt_author).id }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Manuscript Author Manuscript must exist", "Manuscript Author Manuscript can't be blank"]
          end
        end
        context "Author" do
          let!(:manuscript_id) { manuscript_id = manuscript.id }
          let!(:author_id) { author_id = nil }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Manuscript Author Author must exist", "Manuscript Author Author can't be blank"]
          end
        end
      end

      context "Duplicate Data" do
        let!(:manuscript_id) { manuscript_id = manuscript.id }
        let!(:author_id) { author_id = author.id }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["Manuscript Author Manuscript has already been taken", "Manuscript Author Author has already been taken", "Manuscript Author Application module has already been taken"]
        end
      end
    end
  end

  describe "PUT #update" do
    manuscript_id = nil
    author_id = nil
    let!(:manuscript_id) { manuscript_id }
    let!(:author_id) { author_id }

    before :each do
      @update = create :sate_crmgt_manuscript_author, manuscript_id: manuscript.id,
                       author_id: author.id,
                       application_module_id: application_module.id

      create :sate_crmgt_manuscript_author, manuscript_id: manuscript.id,
             author_id: another_author.id,
             application_module_id: application_module.id

      request.headers.merge!(header)
      put :update, params: { id: @update.to_param,
                             :manuscript_author => { manuscript_id: manuscript_id,
                                                     author_id: author_id } }
      @decoded_response = JSON(@response.body)
    end

    context "with valid params" do
      let!(:manuscript_id) { manuscript_id = manuscript.id }
      let!(:author_id) { author_id = create(:sate_crmgt_author).id }
      it "Updates the requested Manuscript Author" do
        expect(@update.author_id).to_not eq attributes_for(:sate_crmgt_manuscript_author)[:author_id]
      end

      it "returns a success message" do
        expect(@response).to be_successful
        expect(@decoded_response["message"]).to eq "Manuscript Author Updated Successfully!"
      end
    end

    context "with invalid params" do
      context "Blank Attributes" do
        context "Manuscript" do
          let!(:manuscript_id) { manuscript_id = nil }
          let!(:author_id) { author_id = create(:sate_crmgt_author).id }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Manuscript Author Manuscript must exist", "Manuscript Author Manuscript can't be blank"]
          end
        end
        context "Author" do
          let!(:manuscript_id) { manuscript_id = manuscript.id }
          let!(:author_id) { author_id = nil }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Manuscript Author Author must exist", "Manuscript Author Author can't be blank"]
          end
        end
      end

      context "Duplicate Data" do
        let!(:manuscript_id) { manuscript_id = manuscript.id }
        let!(:author_id) { author_id = another_author.id }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["Manuscript Author Manuscript has already been taken", "Manuscript Author Author has already been taken", "Manuscript Author Application module has already been taken"]
        end
      end
    end
  end

end
