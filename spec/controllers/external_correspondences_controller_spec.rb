require 'rails_helper'

RSpec.describe ExternalCorrespondencesController, type: :controller do
  let(:user) { create(:sate_auth_user) }
  let!(:application_module) { create(:sate_auth_application_module, code: "CRMT") }
  let!(:source_incoming) { create(:sate_crmgt_organization) }
  let!(:destination_incoming) { create(:sate_crmgt_department) }
  let!(:another_destination_incoming) { create(:sate_crmgt_department) }
  let!(:source_outgoing) { create(:sate_crmgt_department) }
  let!(:destination_outgoing) { create(:sate_crmgt_organization) }
  let!(:user_role) { create(:sate_auth_user_role) }

  let(:header) { { 'Authorization' => token_generator(user.id) } }

  let(:valid_incoming_attributes) {
    {
        reference_no: FFaker::Name.name,
        letter_date: FFaker::Time.date,
        subject: FFaker::Name.name,
        search_keywords: [FFaker::Name.name, FFaker::Name.name],
        source_id: source_incoming.id,
        destination_id: destination_incoming.id,
        received_date: FFaker::Time.date,
        application_module_id: application_module.id,
        key_in_by_id: user.id
    }
  }

  let(:valid_incoming_with_cc_attributes) {
    {
      reference_no: FFaker::Name.name,
      letter_date: FFaker::Time.date,
      subject: FFaker::Name.name,
      search_keywords: [FFaker::Name.name, FFaker::Name.name],
      source_id: source_incoming.id,
      destination_id: another_destination_incoming.id,
      received_date: FFaker::Time.date,
      application_module_id: application_module.id,
      key_in_by_id: user.id,
      has_cc: true
    }
  }

  let(:valid_outgoing_attributes) {
    {
        reference_no: FFaker::Name.name,
        letter_date: FFaker::Time.date,
        subject: FFaker::Name.name,
        search_keywords: [FFaker::Name.name, FFaker::Name.name],
        source_id: source_outgoing.id,
        destination_id: destination_outgoing.id,
        received_date: FFaker::Time.date,
        application_module_id: application_module.id,
        key_in_by_id: user.id
    }
  }

  describe "GET #index for Incoming Requests" do
    before :each do
      Sate::Crmgt::ExternalIncomingCorrespondence.create valid_incoming_attributes
      create(:sate_crmgt_user_department, user_id: user.id, department_id: destination_incoming.id)
      dest_dep = Sate::Crmgt::UserDepartment.find_by_user_id user.id
      @in_ext_get = Sate::Crmgt::ExternalIncomingCorrespondence.where application_module_id: application_module.id,
                                                                      destination_id: dest_dep.department_id
      request.headers.merge!(header)
      get :index, params: { request_type: "incoming" }
    end
    it "returns a success response" do
      expect(response).to be_successful
    end

    it "assigns internal correspondence of the current user as @external_correspondence" do
      expect(assigns(:external_correspondences3)).to eq(@in_ext_get)
    end
  end

  describe "GET #index for Incoming Requests with Cc" do
    before :each do
      Sate::Crmgt::ExternalIncomingCorrespondence.create valid_incoming_attributes
      correspondence = Sate::Crmgt::ExternalIncomingCorrespondence.create valid_incoming_with_cc_attributes

      create(:sate_crmgt_user_department, user_id: user.id, department_id: destination_incoming.id)

      Sate::Crmgt::CorrespondenceCarbonCopy.create correspondence: correspondence,
                                                   destination_id: destination_incoming.id,
                                                   application_module_id: application_module.id

      correspondence1 = Sate::Crmgt::ExternalIncomingCorrespondence.where application_module_id: application_module.id,
                                                                          destination_id: destination_incoming.id

      correspondence_ccs = Sate::Crmgt::CorrespondenceCarbonCopy.where(application_module_id: application_module.id,
                                                                       correspondence_type: 'Sate::Crmgt::ExternalIncomingCorrespondence',
                                                                       destination_id: destination_incoming.id)
                                                                .select("correspondence_id").map(&:correspondence_id)

      correspondence2 = Sate::Crmgt::ExternalIncomingCorrespondence.where id: correspondence_ccs,
                                                                  application_module_id: application_module.id

      @in_ext_get = correspondence1.all + correspondence2.all

      request.headers.merge!(header)

      get :index, params: { request_type: "incoming" }
    end
    it "returns a success response" do
      expect(response).to be_successful
    end

    it "assigns internal correspondence of the current user as @external_correspondence" do
      expect(assigns(:external_correspondences3)).to eq(@in_ext_get)
    end
  end

  describe "GET #index for Incoming Requests with Only Cc" do
    before :each do
      correspondence = Sate::Crmgt::ExternalIncomingCorrespondence.create valid_incoming_with_cc_attributes

      create(:sate_crmgt_user_department, user_id: user.id, department_id: destination_incoming.id)

      Sate::Crmgt::CorrespondenceCarbonCopy.create correspondence: correspondence,
                                                   destination_id: destination_incoming.id,
                                                   application_module_id: application_module.id

      correspondence_ccs = Sate::Crmgt::CorrespondenceCarbonCopy.where(application_module_id: application_module.id,
                                                                       correspondence_type: 'Sate::Crmgt::ExternalIncomingCorrespondence',
                                                                       destination_id: destination_incoming.id)
                                                                .select("correspondence_id").map(&:correspondence_id)

      correspondence1 = Sate::Crmgt::ExternalIncomingCorrespondence.where application_module_id: application_module.id,
                                                                          destination_id: destination_incoming.id

      correspondence2 = Sate::Crmgt::ExternalIncomingCorrespondence.where id: correspondence_ccs,
                                                                          application_module_id: application_module.id

      @in_ext_get = correspondence1.all + correspondence2.all

      request.headers.merge!(header)

      get :index, params: { request_type: "incoming" }
    end
    it "returns a success response" do
      expect(response).to be_successful
    end

    it "assigns internal correspondence of the current user as @external_correspondence" do
      expect(assigns(:external_correspondences3)).to eq(@in_ext_get)
    end
  end

  describe "GET #index for Outgoing Requests" do
    before :each do
      Sate::Crmgt::ExternalOutgoingCorrespondence.create valid_outgoing_attributes
      create(:sate_crmgt_user_department, user_id: user.id, department_id: source_outgoing.id)
      source_dep = Sate::Crmgt::UserDepartment.find_by_user_id user.id
      @out_ext_get = Sate::Crmgt::ExternalOutgoingCorrespondence.where application_module_id: application_module.id,
                                                                      source_id: source_dep.department_id
      request.headers.merge!(header)
      get :index, params: { request_type: "outgoing" }
    end
    it "returns a success response" do
      expect(response).to be_successful
    end

    it "assigns internal correspondence of the current user as @external_correspondence" do
      expect(assigns(:external_correspondences3)).to eq(@out_ext_get)
    end
  end

  describe "GET #get_assigned for External Incoming Correspondence" do
    before :each do
      correspondence = Sate::Crmgt::ExternalIncomingCorrespondence.create valid_incoming_attributes
      from_user = create(:sate_auth_user)
      create(:sate_crmgt_correspondence_assignment, from_user_id: from_user.id,
             to_user_id: user.id,
             correspondence_id: correspondence.id,
             correspondence_type: 'Sate::Crmgt::ExternalIncomingCorrespondence',
             order: 1,
             status: "Assigned",
             application_module_id: application_module.id)
      create(:sate_crmgt_user_department, user_id: user.id, department_id: destination_incoming.id)

      assigned = Sate::Crmgt::CorrespondenceAssignment.where(application_module_id: application_module.id,
                                                             to_user_id: user.id,
                                                             correspondence_type: 'Sate::Crmgt::ExternalIncomingCorrespondence')
                                                      .select("correspondence_id").map(&:correspondence_id)

      @external_incoming_get = Sate::Crmgt::ExternalIncomingCorrespondence.where id: assigned,
                                                                                  application_module_id: application_module.id
      request.headers.merge!(header)

      get :get_assigned
    end

    it "returns a success response" do
      expect(response).to be_successful
    end

    it "assigns external incoming correspondence Assigned to the current user as @external_correspondences" do
      expect(assigns(:external_correspondences)).to eq(@external_incoming_get)
    end
  end

  describe "POST #create for Incoming Requests" do
    reference_no = nil
    letter_date = nil
    subject = nil
    search_keywords = []
    source_id = nil
    destination_id = nil
    let!(:reference_no) { reference_no }
    let!(:letter_date) { letter_date }
    let!(:subject) { subject }
    let!(:search_keywords) { search_keywords }
    let!(:source_id) { source_id }
    let!(:destination_id) { destination_id }

    before :each do
      create :sate_crmgt_external_incoming_correspondence, reference_no: "ABC/0001/13",
             letter_date: FFaker::Time.date, subject: "Letter Subject",
             search_keywords: ["Word 1", "Word 2"],
             source_id: source_incoming.id, destination_id: destination_incoming.id,
             received_date: FFaker::Time.date,
             application_module_id: application_module.id

      create :sate_crmgt_user_department, user_id: user.id, department_id: destination_incoming.id
      request.headers.merge! header
      post :create, params: { request_type: "incoming",
                              external_correspondence: {
                                  reference_no: reference_no,
                                  letter_date: letter_date,
                                  subject: subject,
                                  search_keywords: search_keywords,
                                  source_id: source_id,
                                  destination_id: destination_id
                              }}
      @decoded_response = JSON(@response.body)
    end

    context "with valid params" do
      let!(:reference_no) { reference_no = FFaker::Name.name }
      let!(:letter_date ){ letter_date = FFaker::Time.date }
      let!(:subject) { subject = FFaker::Name.name }
      let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
      let!(:source_id) { source_id = source_incoming.id }
      let!(:destination_id) { destination_id = destination_incoming.id }

      it "creates a new Incoming External Correspondence" do
        expect(Sate::Crmgt::ExternalIncomingCorrespondence.count).to eq 2
      end

      it "returns a success message" do
        expect(@response).to be_successful
        expect(@decoded_response["message"]).to eq "External Correspondence Saved Successfully!"
      end
    end

    context "with invalid params" do
      context "Blank attributes" do
        context "Reference No" do
          let!(:reference_no) { reference_no = nil }
          let!(:letter_date ){ letter_date = FFaker::Time.date }
          let!(:subject) { subject = FFaker::Name.name }
          let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
          let!(:source_id) { source_id = source_incoming.id }
          let!(:destination_id) { destination_id = destination_incoming.id }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["External Correspondence Reference no can't be blank"]
          end
        end
        context "Letter Date" do
          let!(:reference_no) { reference_no = FFaker::Name.name }
          let!(:letter_date ){ letter_date = nil }
          let!(:subject) { subject = FFaker::Name.name }
          let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
          let!(:source_id) { source_id = source_incoming.id }
          let!(:destination_id) { destination_id = destination_incoming.id }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["External Correspondence Letter date can't be blank"]
          end
        end
        context "Letter Subject" do
          let!(:reference_no) { reference_no = FFaker::Name.name }
          let!(:letter_date ){ letter_date = FFaker::Time.date }
          let!(:subject) { subject = nil }
          let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
          let!(:source_id) { source_id = source_incoming.id }
          let!(:destination_id) { destination_id = destination_incoming.id }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["External Correspondence Subject can't be blank"]
          end
        end
        context "Letter Source Department" do
          let!(:reference_no) { reference_no = FFaker::Name.name }
          let!(:letter_date ){ letter_date = FFaker::Time.date }
          let!(:subject) { subject = FFaker::Name.name }
          let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
          let!(:source_id) { source_id = nil }
          let!(:destination_id) { destination_id = destination_incoming.id }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["External Correspondence Source must exist", "External Correspondence Source can't be blank"]
          end
        end
      end

      context "Duplicate Data" do
        context "Reference No" do
          let!(:reference_no) { reference_no = "ABC/0001/13" }
          let!(:letter_date ){ letter_date = FFaker::Time.date }
          let!(:subject) { subject = FFaker::Name.name }
          let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
          let!(:source_id) { source_id = source_incoming.id }
          let!(:destination_id) { destination_id = destination_incoming.id }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["External Correspondence Reference no has already been taken"]
          end
        end
        context "All Fields" do
          let!(:reference_no) { reference_no = "ABC/0001/13" }
          let!(:letter_date ){ letter_date = Date.strptime("09/28/2020", "%m/%d/%Y") }
          let!(:subject) { subject = "Letter Subject" }
          let!(:search_keywords) { search_keywords = ["Word 1", "Word 2"] }
          let!(:source_id) { source_id = source_incoming.id }
          let!(:destination_id) { destination_id = destination_incoming.id }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["External Correspondence Reference no has already been taken"]
          end
        end
      end
    end
  end

  describe "POST #create for Outgoing Requests" do
    reference_no = nil
    letter_date = nil
    subject = nil
    search_keywords = []
    source_id = nil
    destination_id = nil
    let!(:reference_no) { reference_no }
    let!(:letter_date) { letter_date }
    let!(:subject) { subject }
    let!(:search_keywords) { search_keywords }
    let!(:source_id) { source_id }
    let!(:destination_id) { destination_id }

    before :each do
      create :sate_crmgt_external_outgoing_correspondence, reference_no: "ABC/0001/13",
             letter_date: FFaker::Time.date, subject: "Letter Subject",
             search_keywords: ["Word 1", "Word 2"],
             source_id: source_outgoing.id, destination_id: destination_outgoing.id,
             received_date: FFaker::Time.date,
             application_module_id: application_module.id

      create :sate_crmgt_user_department, user_id: user.id, department_id: source_outgoing.id
      request.headers.merge! header
      post :create, params: { request_type: "outgoing",
                              external_correspondence: {
                                  reference_no: reference_no,
                                  letter_date: letter_date,
                                  subject: subject,
                                  search_keywords: search_keywords,
                                  source_id: source_id,
                                  destination_id: destination_id
                              }}
      @decoded_response = JSON(@response.body)
    end

    context "with valid params" do
      let!(:reference_no) { reference_no = FFaker::Name.name }
      let!(:letter_date ){ letter_date = FFaker::Time.date }
      let!(:subject) { subject = FFaker::Name.name }
      let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
      let!(:source_id) { source_id = source_outgoing.id }
      let!(:destination_id) { destination_id = destination_outgoing.id }

      it "creates a new Outgoing External Correspondence" do
        expect(Sate::Crmgt::ExternalOutgoingCorrespondence.count).to eq 2
      end

      it "returns a success message" do
        expect(@response).to be_successful
        expect(@decoded_response["message"]).to eq "External Correspondence Saved Successfully!"
      end
    end

    context "with invalid params" do
      context "Blank attributes" do
        context "Reference No" do
          let!(:reference_no) { reference_no = nil }
          let!(:letter_date ){ letter_date = FFaker::Time.date }
          let!(:subject) { subject = FFaker::Name.name }
          let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
          let!(:source_id) { source_id = source_outgoing.id }
          let!(:destination_id) { destination_id = destination_outgoing.id }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["External Correspondence Reference no can't be blank"]
          end
        end
        context "Letter Date" do
          let!(:reference_no) { reference_no = FFaker::Name.name }
          let!(:letter_date ){ letter_date = nil }
          let!(:subject) { subject = FFaker::Name.name }
          let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
          let!(:source_id) { source_id = source_outgoing.id }
          let!(:destination_id) { destination_id = destination_outgoing.id }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["External Correspondence Letter date can't be blank"]
          end
        end
        context "Letter Subject" do
          let!(:reference_no) { reference_no = FFaker::Name.name }
          let!(:letter_date ){ letter_date = FFaker::Time.date }
          let!(:subject) { subject = nil }
          let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
          let!(:source_id) { source_id = source_outgoing.id }
          let!(:destination_id) { destination_id = destination_outgoing.id }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["External Correspondence Subject can't be blank"]
          end
        end
        context "Letter Destination Department" do
          let!(:reference_no) { reference_no = FFaker::Name.name }
          let!(:letter_date ){ letter_date = FFaker::Time.date }
          let!(:subject) { subject = FFaker::Name.name }
          let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
          let!(:source_id) { source_id = source_outgoing.id }
          let!(:destination_id) { destination_id = nil }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["External Correspondence Destination must exist", "External Correspondence Destination can't be blank"]
          end
        end
      end

      context "Duplicate Data" do
        context "Reference No" do
          let!(:reference_no) { reference_no = "ABC/0001/13" }
          let!(:letter_date ){ letter_date = FFaker::Time.date }
          let!(:subject) { subject = FFaker::Name.name }
          let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
          let!(:source_id) { source_id = source_outgoing.id }
          let!(:destination_id) { destination_id = destination_outgoing.id }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["External Correspondence Reference no has already been taken"]
          end
        end
        context "All Fields" do
          let!(:reference_no) { reference_no = "ABC/0001/13" }
          let!(:letter_date ){ letter_date = Date.strptime("09/28/2020", "%m/%d/%Y") }
          let!(:subject) { subject = "Letter Subject" }
          let!(:search_keywords) { search_keywords = ["Word 1", "Word 2"] }
          let!(:source_id) { source_id = source_outgoing.id }
          let!(:destination_id) { destination_id = destination_outgoing.id }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["External Correspondence Reference no has already been taken"]
          end
        end
      end
    end
  end

  describe "PUT #update for Incoming Requests" do
    reference_no = nil
    letter_date = nil
    subject = nil
    search_keywords = []
    source_id = nil
    destination_id = nil
    let!(:reference_no) { reference_no }
    let!(:letter_date) { letter_date }
    let!(:subject) { subject }
    let!(:search_keywords) { search_keywords }
    let!(:source_id) { source_id }
    let!(:destination_id) { destination_id }

    before :each do
      @update = create :sate_crmgt_external_incoming_correspondence,
                       reference_no: "ABC/0001/13",
                       letter_date: FFaker::Time.date, subject: "Letter Subject",
                       search_keywords: ["Word 1", "Word 2"],
                       source_id: source_incoming.id, destination_id: destination_incoming.id,
                       received_date: FFaker::Time.date,
                       application_module_id: application_module.id

      create :sate_crmgt_user_department, user_id: user.id, department_id: destination_incoming.id
      request.headers.merge! header
      put :update, params: { request_type: "incoming",
                             id: @update.to_param,
                             external_correspondence: {
                                 reference_no: reference_no,
                                 letter_date: letter_date,
                                 subject: subject,
                                 search_keywords: search_keywords,
                                 source_id: source_id,
                                 destination_id: destination_id
                              }}
      @decoded_response = JSON(@response.body)
    end

    context "with valid params" do
      let!(:reference_no) { reference_no = FFaker::Name.name }
      let!(:letter_date ){ letter_date = FFaker::Time.date }
      let!(:subject) { subject = FFaker::Name.name }
      let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
      let!(:source_id) { source_id = create(:sate_crmgt_organization).id }
      let!(:destination_id) { destination_id = destination_incoming.id }

      it "Updates the requested Incoming External Correspondence" do
        expect(@update.source_id).to_not eq attributes_for(:sate_crmgt_external_incoming_correspondence)[:source_id]
      end

      it "returns a success message" do
        expect(@response).to be_successful
        expect(@decoded_response["message"]).to eq "External Correspondence Updated Successfully!"
      end
    end

    context "with invalid params" do
      context "Blank attributes" do
        context "Reference No" do
          let!(:reference_no) { reference_no = nil }
          let!(:letter_date ){ letter_date = FFaker::Time.date }
          let!(:subject) { subject = FFaker::Name.name }
          let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
          let!(:source_id) { source_id = source_incoming.id }
          let!(:destination_id) { destination_id = destination_incoming.id }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["External Correspondence Reference no can't be blank"]
          end
        end
        context "Letter Date" do
          let!(:reference_no) { reference_no = FFaker::Name.name }
          let!(:letter_date ){ letter_date = nil }
          let!(:subject) { subject = FFaker::Name.name }
          let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
          let!(:source_id) { source_id = source_incoming.id }
          let!(:destination_id) { destination_id = destination_incoming.id }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["External Correspondence Letter date can't be blank"]
          end
        end
        context "Letter Subject" do
          let!(:reference_no) { reference_no = FFaker::Name.name }
          let!(:letter_date ){ letter_date = FFaker::Time.date }
          let!(:subject) { subject = nil }
          let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
          let!(:source_id) { source_id = source_incoming.id }
          let!(:destination_id) { destination_id = destination_incoming.id }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["External Correspondence Subject can't be blank"]
          end
        end
        context "Letter Source Department" do
          let!(:reference_no) { reference_no = FFaker::Name.name }
          let!(:letter_date ){ letter_date = FFaker::Time.date }
          let!(:subject) { subject = FFaker::Name.name }
          let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
          let!(:source_id) { source_id = nil }
          let!(:destination_id) { destination_id = destination_incoming.id }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["External Correspondence Source must exist", "External Correspondence Source can't be blank"]
          end
        end
      end
    end
  end

  describe "PUT #update for Outgoing Requests" do
    reference_no = nil
    letter_date = nil
    subject = nil
    search_keywords = []
    source_id = nil
    destination_id = nil
    let!(:reference_no) { reference_no }
    let!(:letter_date) { letter_date }
    let!(:subject) { subject }
    let!(:search_keywords) { search_keywords }
    let!(:source_id) { source_id }
    let!(:destination_id) { destination_id }

    before :each do
      @update = create :sate_crmgt_external_outgoing_correspondence,
                       reference_no: "ABC/0001/13",
                       letter_date: FFaker::Time.date, subject: "Letter Subject",
                       search_keywords: ["Word 1", "Word 2"],
                       source_id: source_outgoing.id, destination_id: destination_outgoing.id,
                       received_date: FFaker::Time.date,
                       application_module_id: application_module.id

      create :sate_crmgt_user_department, user_id: user.id, department_id: source_outgoing.id
      request.headers.merge! header
      put :update, params: { request_type: "outgoing",
                             id: @update.to_param,
                             external_correspondence: {
                                 reference_no: reference_no,
                                 letter_date: letter_date,
                                 subject: subject,
                                 search_keywords: search_keywords,
                                 source_id: source_id,
                                 destination_id: destination_id
                             }}
      @decoded_response = JSON(@response.body)
    end

    context "with valid params" do
      let!(:reference_no) { reference_no = FFaker::Name.name }
      let!(:letter_date ){ letter_date = FFaker::Time.date }
      let!(:subject) { subject = FFaker::Name.name }
      let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
      let!(:source_id) { source_id = source_outgoing.id }
      let!(:destination_id) { destination_id = create(:sate_crmgt_organization).id }

      it "Updates the requested Incoming External Correspondence" do
        expect(@update.source_id).to_not eq attributes_for(:sate_crmgt_external_outgoing_correspondence)[:destination_id]
      end

      it "returns a success message" do
        expect(@response).to be_successful
        expect(@decoded_response["message"]).to eq "External Correspondence Updated Successfully!"
      end
    end

    context "with invalid params" do
      context "Blank attributes" do
        context "Reference No" do
          let!(:reference_no) { reference_no = nil }
          let!(:letter_date ){ letter_date = FFaker::Time.date }
          let!(:subject) { subject = FFaker::Name.name }
          let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
          let!(:source_id) { source_id = source_outgoing.id }
          let!(:destination_id) { destination_id = destination_outgoing.id }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["External Correspondence Reference no can't be blank"]
          end
        end
        context "Letter Date" do
          let!(:reference_no) { reference_no = FFaker::Name.name }
          let!(:letter_date ){ letter_date = nil }
          let!(:subject) { subject = FFaker::Name.name }
          let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
          let!(:source_id) { source_id = source_outgoing.id }
          let!(:destination_id) { destination_id = destination_outgoing.id }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["External Correspondence Letter date can't be blank"]
          end
        end
        context "Letter Subject" do
          let!(:reference_no) { reference_no = FFaker::Name.name }
          let!(:letter_date ){ letter_date = FFaker::Time.date }
          let!(:subject) { subject = nil }
          let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
          let!(:source_id) { source_id = source_outgoing.id }
          let!(:destination_id) { destination_id = destination_outgoing.id }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["External Correspondence Subject can't be blank"]
          end
        end
        context "Letter Destination Department" do
          let!(:reference_no) { reference_no = FFaker::Name.name }
          let!(:letter_date ){ letter_date = FFaker::Time.date }
          let!(:subject) { subject = FFaker::Name.name }
          let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
          let!(:source_id) { source_id = source_outgoing.id }
          let!(:destination_id) { destination_id = nil }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["External Correspondence Destination must exist", "External Correspondence Destination can't be blank"]
          end
        end
      end
    end
  end

  describe "PUT #update_status for Acceptance" do
    before :each do
      @update = create :sate_crmgt_external_incoming_correspondence, reference_no: "ABC/0001/13",
                       letter_date: FFaker::Time.date, subject: "Letter Subject",
                       search_keywords: ["Word 1", "Word 2"],
                       source_id: source_incoming.id, destination_id: destination_incoming.id,
                       received_date: FFaker::Time.date,
                       application_module_id: application_module.id

      create :sate_crmgt_user_department, user_id: user.id, department_id: destination_incoming.id

      request.headers.merge!(header)
      put :update_status, params: { id: @update.to_param,
                                    request_type: "incoming",
                                    status: "Accepted" }
      @decoded_response = JSON(@response.body)
    end
    it "Updates the requested External Incoming Correspondence" do
      expect(@update.status).to_not eq attributes_for(:sate_crmgt_external_incoming_correspondence)[:status]
      expect(@update.received_date).to_not eq attributes_for(:sate_crmgt_external_incoming_correspondence)[:received_date]
    end

    it "returns a success message" do
      expect(@response).to be_successful
      expect(@decoded_response["message"]).to eq "External Correspondence Status Updated Successfully!"
    end
  end

  describe "PUT #update_status for Rejection" do
    before :each do
      @update = create :sate_crmgt_external_incoming_correspondence, reference_no: "ABC/0001/13",
                       letter_date: FFaker::Time.date, subject: "Letter Subject",
                       search_keywords: ["Word 1", "Word 2"],
                       source_id: source_incoming.id, destination_id: destination_incoming.id,
                       received_date: FFaker::Time.date,
                       application_module_id: application_module.id

      create :sate_crmgt_user_department, user_id: user.id, department_id: destination_incoming.id

      request.headers.merge!(header)
      put :update_status, params: { id: @update.to_param,
                                    request_type: "incoming",
                                    status: "Rejected" }
      @decoded_response = JSON(@response.body)
    end
    it "Updates the requested External Incoming Correspondence" do
      expect(@update.status).to_not eq attributes_for(:sate_crmgt_external_incoming_correspondence)[:status]
      expect(@update.received_date).to_not eq attributes_for(:sate_crmgt_external_incoming_correspondence)[:received_date]
    end

    it "returns a success message" do
      expect(@response).to be_successful
      expect(@decoded_response["message"]).to eq "External Correspondence Status Updated Successfully!"
    end
  end

  describe "POST #upload for Incoming Requests" do
    type = nil
    file = nil
    let!(:type) { type }
    let!(:file) { file }
    before :each do
      @incoming = Sate::Crmgt::ExternalIncomingCorrespondence.create valid_incoming_attributes

      request.headers.merge!(header)

      post :upload, params: { id: @incoming.to_param,
                              request_type: "incoming",
                              type: type,
                              file: file }
      @decoded_response = JSON(@response.body)
    end

    context "Main Image Upload" do
      context "with valid params" do
        let!(:type) { type = "Main" }
        let!(:file) {
          main = "/home/samuel/Desktop/changes.png"
          file = Rack::Test::UploadedFile.new(main, "image/png") }

        it "should upload the file" do
          expect(ActiveStorage::Attachment.count).to eq(1)
        end

        it "returns a success message" do
          expect(@response).to be_successful
          expect(@decoded_response["message"]).to eq "Image Uploaded Successfully!"
        end
      end

      context "with invalid params" do
        context "with Big File Size" do
          let!(:type) { type = "Main" }
          let!(:file) {
            main = "/home/samuel/Desktop/ancient-town-lake-china.jpg"
            file = Rack::Test::UploadedFile.new(main, "image/jpg") }

          it "should not upload the file" do
            expect(ActiveStorage::Attachment.count).to eq(0)
          end

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"][0]).to eq "External Correspondence Main images is too big"
          end
        end

        context "with non image file" do
          let!(:type) { type = "Main" }
          let!(:file) {
            main = "/home/samuel/Desktop/registration_notice.txt"
            file = Rack::Test::UploadedFile.new(main, "text/plain") }

          it "should not upload the file" do
            expect(ActiveStorage::Attachment.count).to eq(0)
          end

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"][0]).to eq "External Correspondence Main images must be an image"
          end
        end
      end
    end

    context "Attachment Image Upload" do
      context "with valid params" do
        let!(:type) { type = "Attachment" }
        let!(:file) {
          main = "/home/samuel/Desktop/changes.png"
          file = Rack::Test::UploadedFile.new(main, "image/png") }

        it "should upload the file" do
          expect(ActiveStorage::Attachment.count).to eq(1)
        end

        it "returns a success message" do
          expect(@response).to be_successful
          expect(@decoded_response["message"]).to eq "Image Uploaded Successfully!"
        end
      end

      context "with invalid params" do
        context "with Big File Size" do
          let!(:type) { type = "Attachment" }
          let!(:file) {
            main = "/home/samuel/Desktop/ancient-town-lake-china.jpg"
            file = Rack::Test::UploadedFile.new(main, "image/jpg") }

          it "should not upload the file" do
            expect(ActiveStorage::Attachment.count).to eq(0)
          end

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"][0]).to eq "External Correspondence Attachment images is too big"
          end
        end

        context "with non image file" do
          let!(:type) { type = "Attachment" }
          let!(:file) {
            main = "/home/samuel/Desktop/registration_notice.txt"
            file = Rack::Test::UploadedFile.new(main, "text/plain") }

          it "should not upload the file" do
            expect(ActiveStorage::Attachment.count).to eq(0)
          end

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"][0]).to eq "External Correspondence Attachment images must be an image"
          end
        end
      end
    end
  end

  describe "POST #upload for Outgoing Requests" do
    type = nil
    file = nil
    let!(:type) { type }
    let!(:file) { file }
    before :each do
      @outgoing = Sate::Crmgt::ExternalOutgoingCorrespondence.create valid_outgoing_attributes

      request.headers.merge!(header)

      post :upload, params: { id: @outgoing.to_param,
                              request_type: "outgoing",
                              type: type,
                              file: file }
      @decoded_response = JSON(@response.body)
    end

    context "Main Image Upload" do
      context "with valid params" do
        let!(:type) { type = "Main" }
        let!(:file) {
          main = "/home/samuel/Desktop/changes.png"
          file = Rack::Test::UploadedFile.new(main, "image/png") }

        it "should upload the file" do
          expect(ActiveStorage::Attachment.count).to eq(1)
        end

        it "returns a success message" do
          expect(@response).to be_successful
          expect(@decoded_response["message"]).to eq "Image Uploaded Successfully!"
        end
      end

      context "with invalid params" do
        context "with Big File Size" do
          let!(:type) { type = "Main" }
          let!(:file) {
            main = "/home/samuel/Desktop/ancient-town-lake-china.jpg"
            file = Rack::Test::UploadedFile.new(main, "image/jpg") }

          it "should not upload the file" do
            expect(ActiveStorage::Attachment.count).to eq(0)
          end

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"][0]).to eq "External Correspondence Main images is too big"
          end
        end

        context "with non image file" do
          let!(:type) { type = "Main" }
          let!(:file) {
            main = "/home/samuel/Desktop/registration_notice.txt"
            file = Rack::Test::UploadedFile.new(main, "text/plain") }

          it "should not upload the file" do
            expect(ActiveStorage::Attachment.count).to eq(0)
          end

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"][0]).to eq "External Correspondence Main images must be an image"
          end
        end
      end
    end

    context "Attachment Image Upload" do
      context "with valid params" do
        let!(:type) { type = "Attachment" }
        let!(:file) {
          main = "/home/samuel/Desktop/changes.png"
          file = Rack::Test::UploadedFile.new(main, "image/png") }

        it "should upload the file" do
          expect(ActiveStorage::Attachment.count).to eq(1)
        end

        it "returns a success message" do
          expect(@response).to be_successful
          expect(@decoded_response["message"]).to eq "Image Uploaded Successfully!"
        end
      end

      context "with invalid params" do
        context "with Big File Size" do
          let!(:type) { type = "Attachment" }
          let!(:file) {
            main = "/home/samuel/Desktop/ancient-town-lake-china.jpg"
            file = Rack::Test::UploadedFile.new(main, "image/jpg") }

          it "should not upload the file" do
            expect(ActiveStorage::Attachment.count).to eq(0)
          end

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"][0]).to eq "External Correspondence Attachment images is too big"
          end
        end

        context "with non image file" do
          let!(:type) { type = "Attachment" }
          let!(:file) {
            main = "/home/samuel/Desktop/registration_notice.txt"
            file = Rack::Test::UploadedFile.new(main, "text/plain") }

          it "should not upload the file" do
            expect(ActiveStorage::Attachment.count).to eq(0)
          end

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"][0]).to eq "External Correspondence Attachment images must be an image"
          end
        end
      end
    end
  end

  describe "POST #change_uploads for Incoming Requests" do
    type = nil
    file = nil
    let!(:type) { type }
    let!(:file) { file }
    before :each do
      @incoming = Sate::Crmgt::ExternalIncomingCorrespondence.create valid_incoming_attributes

      main = '/home/samuel/Desktop/sate_auth_changes.png'
      old_file = Rack::Test::UploadedFile.new(main, "image/png")
      @incoming.main_images.attach(old_file)

      request.headers.merge!(header)

      post :change_uploads, params: { id: @incoming.to_param,
                                      request_type: "incoming",
                                      existing_image: @incoming.main_images.blobs[0].id,
                                      type: type,
                                      file: file }
      @decoded_response = JSON(@response.body)
    end

    context "Main Image Upload" do
      context "with valid params" do
        let!(:type) { type = "Main" }
        let!(:file) {
          main = "/home/samuel/Desktop/changes.png"
          file = Rack::Test::UploadedFile.new(main, "image/png") }

        it "should upload the file" do
          expect(ActiveStorage::Attachment.count).to eq(1)
        end

        it "returns a success message" do
          expect(@response).to be_successful
          expect(@decoded_response["message"]).to eq "Image Uploaded Successfully!"
        end
      end

      context "with invalid params" do
        context "with Big File Size" do
          let!(:type) { type = "Main" }
          let!(:file) {
            main = "/home/samuel/Desktop/ancient-town-lake-china.jpg"
            file = Rack::Test::UploadedFile.new(main, "image/jpg") }

          it "should not upload the file" do
            expect(ActiveStorage::Attachment.count).to eq(1)
          end

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"][0]).to eq "External Correspondence Main images is too big"
          end
        end

        context "with non image file" do
          let!(:type) { type = "Main" }
          let!(:file) {
            main = "/home/samuel/Desktop/registration_notice.txt"
            file = Rack::Test::UploadedFile.new(main, "text/plain") }

          it "should not upload the file" do
            expect(ActiveStorage::Attachment.count).to eq(1)
          end

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"][0]).to eq "External Correspondence Main images must be an image"
          end
        end
      end
    end

    context "Attachment Image Upload" do
      context "with valid params" do
        let!(:type) { type = "Attachment" }
        let!(:file) {
          main = "/home/samuel/Desktop/changes.png"
          file = Rack::Test::UploadedFile.new(main, "image/png") }

        it "should upload the file" do
          expect(ActiveStorage::Attachment.count).to eq(1)
        end

        it "returns a success message" do
          expect(@response).to be_successful
          expect(@decoded_response["message"]).to eq "Image Uploaded Successfully!"
        end
      end

      context "with invalid params" do
        context "with Big File Size" do
          let!(:type) { type = "Attachment" }
          let!(:file) {
            main = "/home/samuel/Desktop/ancient-town-lake-china.jpg"
            file = Rack::Test::UploadedFile.new(main, "image/jpg") }

          it "should not upload the file" do
            expect(ActiveStorage::Attachment.count).to eq(1)
          end

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"][0]).to eq "External Correspondence Attachment images is too big"
          end
        end

        context "with non image file" do
          let!(:type) { type = "Attachment" }
          let!(:file) {
            main = "/home/samuel/Desktop/registration_notice.txt"
            file = Rack::Test::UploadedFile.new(main, "text/plain") }

          it "should not upload the file" do
            expect(ActiveStorage::Attachment.count).to eq(1)
          end

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"][0]).to eq "External Correspondence Attachment images must be an image"
          end
        end
      end
    end
  end

  describe "POST #change_uploads for Outgoing Requests" do
    type = nil
    file = nil
    let!(:type) { type }
    let!(:file) { file }
    before :each do
      @outgoing = Sate::Crmgt::ExternalOutgoingCorrespondence.create valid_outgoing_attributes

      main = '/home/samuel/Desktop/sate_auth_changes.png'
      old_file = Rack::Test::UploadedFile.new(main, "image/png")
      @outgoing.main_images.attach(old_file)

      request.headers.merge!(header)

      post :change_uploads, params: { id: @outgoing.to_param,
                                      request_type: "outgoing",
                                      existing_image: @outgoing.main_images.blobs[0].id,
                                      type: type,
                                      file: file }
      @decoded_response = JSON(@response.body)
    end

    context "Main Image Upload" do
      context "with valid params" do
        let!(:type) { type = "Main" }
        let!(:file) {
          main = "/home/samuel/Desktop/changes.png"
          file = Rack::Test::UploadedFile.new(main, "image/png") }

        it "should upload the file" do
          expect(ActiveStorage::Attachment.count).to eq(1)
        end

        it "returns a success message" do
          expect(@response).to be_successful
          expect(@decoded_response["message"]).to eq "Image Uploaded Successfully!"
        end
      end

      context "with invalid params" do
        context "with Big File Size" do
          let!(:type) { type = "Main" }
          let!(:file) {
            main = "/home/samuel/Desktop/ancient-town-lake-china.jpg"
            file = Rack::Test::UploadedFile.new(main, "image/jpg") }

          it "should not upload the file" do
            expect(ActiveStorage::Attachment.count).to eq(1)
          end

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"][0]).to eq "External Correspondence Main images is too big"
          end
        end

        context "with non image file" do
          let!(:type) { type = "Main" }
          let!(:file) {
            main = "/home/samuel/Desktop/registration_notice.txt"
            file = Rack::Test::UploadedFile.new(main, "text/plain") }

          it "should not upload the file" do
            expect(ActiveStorage::Attachment.count).to eq(1)
          end

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"][0]).to eq "External Correspondence Main images must be an image"
          end
        end
      end
    end

    context "Attachment Image Upload" do
      context "with valid params" do
        let!(:type) { type = "Attachment" }
        let!(:file) {
          main = "/home/samuel/Desktop/changes.png"
          file = Rack::Test::UploadedFile.new(main, "image/png") }

        it "should upload the file" do
          expect(ActiveStorage::Attachment.count).to eq(1)
        end

        it "returns a success message" do
          expect(@response).to be_successful
          expect(@decoded_response["message"]).to eq "Image Uploaded Successfully!"
        end
      end

      context "with invalid params" do
        context "with Big File Size" do
          let!(:type) { type = "Attachment" }
          let!(:file) {
            main = "/home/samuel/Desktop/ancient-town-lake-china.jpg"
            file = Rack::Test::UploadedFile.new(main, "image/jpg") }

          it "should not upload the file" do
            expect(ActiveStorage::Attachment.count).to eq(1)
          end

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"][0]).to eq "External Correspondence Attachment images is too big"
          end
        end

        context "with non image file" do
          let!(:type) { type = "Attachment" }
          let!(:file) {
            main = "/home/samuel/Desktop/registration_notice.txt"
            file = Rack::Test::UploadedFile.new(main, "text/plain") }

          it "should not upload the file" do
            expect(ActiveStorage::Attachment.count).to eq(1)
          end

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"][0]).to eq "External Correspondence Attachment images must be an image"
          end
        end
      end
    end
  end

  describe "GET #get_images for Incoming Requests" do
    before :each do
      @incoming = Sate::Crmgt::ExternalIncomingCorrespondence.create valid_incoming_attributes

      request.headers.merge!(header)

      main = '/home/samuel/Desktop/sate_auth_changes.png'
      attachment = '/home/samuel/Desktop/changes.png'
      file = Rack::Test::UploadedFile.new(main, "image/png")
      file_att = Rack::Test::UploadedFile.new(attachment, "image/png")
      file_att2 = Rack::Test::UploadedFile.new(attachment, "image/png")
      @incoming.main_images.attach(file)
      @incoming.attachment_images.attach(file_att)
      @incoming.attachment_images.attach(file_att2)

      get :get_images, params: { id: @incoming.to_param,
                                 request_type: "incoming" }
      @decoded_response = JSON(@response.body)
    end

    it "returns a success response" do
      expect(response).to be_successful
    end

    it "assigns one main image to the correspondence" do
      expect(@decoded_response["main_images"].count).to eq(1)
    end

    it "assigns one attachment image to the correspondence" do
      expect(@decoded_response["attachment_images"].count).to eq(2)
    end
  end

  describe "GET #get_images for Outgoing Requests" do
    before :each do
      @outgoing = Sate::Crmgt::ExternalOutgoingCorrespondence.create valid_outgoing_attributes

      request.headers.merge!(header)

      main = '/home/samuel/Desktop/sate_auth_changes.png'
      attachment = '/home/samuel/Desktop/changes.png'
      file = Rack::Test::UploadedFile.new(main, "image/png")
      file_att = Rack::Test::UploadedFile.new(attachment, "image/png")
      file_att2 = Rack::Test::UploadedFile.new(attachment, "image/png")
      @outgoing.main_images.attach(file)
      @outgoing.attachment_images.attach(file_att)
      @outgoing.attachment_images.attach(file_att2)

      get :get_images, params: { id: @outgoing.to_param,
                                 request_type: "outgoing" }
      @decoded_response = JSON(@response.body)
    end

    it "returns a success response" do
      expect(response).to be_successful
    end

    it "assigns one main image to the correspondence" do
      expect(@decoded_response["main_images"].count).to eq(1)
    end

    it "assigns one attachment image to the correspondence" do
      expect(@decoded_response["attachment_images"].count).to eq(2)
    end
  end

end
