require 'rails_helper'

RSpec.describe AuthorsController, type: :controller do
  let(:user) { create(:sate_auth_user) }
  let!(:application_module) { create(:sate_auth_application_module, code: "CRMT") }
  let(:header) { { 'Authorization' => token_generator(user.id) } }

  let(:valid_attributes) {
    {
      first_name: FFaker::Name.name,
      father_name: FFaker::Name.name,
      grand_father_name: FFaker::Name.name,
      email: FFaker::Internet.email[0..49],
      telephone: FFaker::Name.name,
      application_module_id: application_module.id
    }
  }

  let(:invalid_attributes) {
    {
      first_name: nil,
      father_name: FFaker::Name.name,
      grand_father_name: FFaker::Name.name,
      email: FFaker::Internet.email[0..49],
      telephone: FFaker::Name.name,
      application_module_id: application_module.id
    }
  }

  describe "GET #index" do
    before :each do
      Sate::Crmgt::Author.create valid_attributes
      @author_get = Sate::Crmgt::Author.where application_module_id: application_module.id
      request.headers.merge!(header)
      get :index
    end

    it "returns a success response" do
      expect(response).to be_successful
    end

    it "assigns all authors as @authors" do
      expect(assigns(:authors)).to eq(@author_get)
    end
  end

  describe "POST #create" do
    first_name = nil
    father_name = nil
    grand_father_name = nil
    email = nil
    telephone = nil
    let!(:first_name) { first_name }
    let!(:father_name) { father_name }
    let!(:grand_father_name) { grand_father_name }
    let!(:email) { email }
    let!(:telephone) { telephone }

    before :each do
      create :sate_crmgt_author, first_name: "Author1",
             father_name: "Father1",
             grand_father_name: "G Father1",
             email: "Author1.Father1@example.com",
             telephone: "0911111111",
             application_module_id: application_module.id
      request.headers.merge!(header)
      post :create, params: { author: { 'first_name' => first_name,
                                        'father_name' => father_name,
                                        'grand_father_name' => grand_father_name,
                                        'email' => email,
                                        'telephone' => telephone } }
      @decoded_response = JSON(@response.body)
    end
    context "with valid params" do
      let!(:first_name) { first_name = "Author2" }
      let!(:father_name) { father_name = "Father2" }
      let!(:grand_father_name) { grand_father_name = "G Father2" }
      let!(:email) { email = "Author2.Father2@example.com" }
      let!(:telephone) { telephone = "0911111111" }

      it "creates a new Author" do
        expect(Sate::Crmgt::Author.count).to eq 2
      end

      it "returns a success message" do
        expect(@response).to be_successful
        expect(@decoded_response["message"]).to eq "Author Saved Successfully!"
      end
    end
    context "with invalid params" do
      context "Blank Author First Name" do
        let!(:first_name) { first_name = nil }
        let!(:father_name) { father_name = "Father2" }
        let!(:grand_father_name) { grand_father_name = "G Father2" }
        let!(:email) { email = "Author2.Father2@example.com" }
        let!(:telephone) { telephone = "0911111111" }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["Author First name can't be blank"]
        end
      end

      context "Blank Author Father Name" do
        let!(:first_name) { first_name = "Author2" }
        let!(:father_name) { father_name = nil }
        let!(:grand_father_name) { grand_father_name = "G Father2" }
        let!(:email) { email = "Author2.Father2@example.com" }
        let!(:telephone) { telephone = "0911111111" }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["Author Father name can't be blank"]
        end
      end

      context "Blank Author Grand Father Name" do
        let!(:first_name) { first_name = "Author2" }
        let!(:father_name) { father_name = "Father2" }
        let!(:grand_father_name) { grand_father_name = nil }
        let!(:email) { email = "Author2.Father2@example.com" }
        let!(:telephone) { telephone = "0911111111" }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["Author Grand father name can't be blank"]
        end
      end

      context "Blank Author Email" do
        let!(:first_name) { first_name = "Author2" }
        let!(:father_name) { father_name = "Father2" }
        let!(:grand_father_name) { grand_father_name = "G Father2" }
        let!(:email) { email = nil }
        let!(:telephone) { telephone = "0911111111" }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["Author Email can't be blank", "Author Email is invalid"]
        end
      end

      context "Invalid Author Email" do
        let!(:first_name) { first_name = "Author2" }
        let!(:father_name) { father_name = "Father2" }
        let!(:grand_father_name) { grand_father_name = "G Father2" }
        let!(:email) { email = "Author2.Father2@example" }
        let!(:telephone) { telephone = "0911111111" }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["Author Email is invalid"]
        end
      end

      context "Blank Author Telephone" do
        let!(:first_name) { first_name = "Author2" }
        let!(:father_name) { father_name = "Father2" }
        let!(:grand_father_name) { grand_father_name = "G Father2" }
        let!(:email) { email = "Author2.Father2@example.com" }
        let!(:telephone) { telephone = nil }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["Author Telephone can't be blank"]
        end
      end

      context "Duplicate First Name, Father Name and Grand Father Name" do
        let!(:first_name) { first_name = "Author1" }
        let!(:father_name) { father_name = "Father1" }
        let!(:grand_father_name) { grand_father_name = "G Father1" }
        let!(:email) { email = "Author1.Father1@example.com" }
        let!(:telephone) { telephone = "0911111111" }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["Author First name has already been taken", "Author Father name has already been taken", "Author Grand father name has already been taken"]
        end
      end
    end
  end

  describe "PUT #update" do
    first_name = nil
    father_name = nil
    grand_father_name = nil
    email = nil
    telephone = nil
    let!(:first_name) { first_name }
    let!(:father_name) { father_name }
    let!(:grand_father_name) { grand_father_name }
    let!(:email) { email }
    let!(:telephone) { telephone }

    before :each do
      @author_update = create :sate_crmgt_author, first_name: "Author1", father_name: "Father1",
                              grand_father_name: "G Father1", email: "Author1.Father1@example.com",
                              telephone: "0911111111", application_module_id: application_module.id

      create :sate_crmgt_author, first_name: "Author2",
             father_name: "Father2",
             grand_father_name: "G Father2",
             email: "Author2.Father2@example.com",
             telephone: "0911111111",
             application_module_id: application_module.id

      request.headers.merge!(header)
      put :update, params: { id: @author_update.to_param,
                             author: { 'first_name' => first_name,
                                       'father_name' => father_name,
                                       'grand_father_name' => grand_father_name,
                                       'email' => email,
                                       'telephone' => telephone } }
      @decoded_response = JSON(@response.body)
    end

    context "with valid params" do
      let!(:first_name) { first_name = "Author3" }
      let!(:father_name) { father_name = "Father3" }
      let!(:grand_father_name) { grand_father_name = "G Father3" }
      let!(:email) { email = "Author3.Father2@example.com" }
      let!(:telephone) { telephone = "0911111111" }

      it "updates the requested Author" do
        expect(@author_update.first_name).to_not eq attributes_for(:sate_crmgt_author)[:first_name]
      end

      it "returns a success message" do
        expect(@response).to be_successful
        expect(@decoded_response["message"]).to eq "Author Updated Successfully!"
      end
    end

    context "with invalid params" do
      context "Blank Author First Name" do
        let!(:first_name) { first_name = nil }
        let!(:father_name) { father_name = "Father2" }
        let!(:grand_father_name) { grand_father_name = "G Father2" }
        let!(:email) { email = "Author2.Father2@example.com" }
        let!(:telephone) { telephone = "0911111111" }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["Author First name can't be blank"]
        end
      end

      context "Blank Author Father Name" do
        let!(:first_name) { first_name = "Author2" }
        let!(:father_name) { father_name = nil }
        let!(:grand_father_name) { grand_father_name = "G Father2" }
        let!(:email) { email = "Author2.Father2@example.com" }
        let!(:telephone) { telephone = "0911111111" }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["Author Father name can't be blank"]
        end
      end

      context "Blank Author Grand Father Name" do
        let!(:first_name) { first_name = "Author2" }
        let!(:father_name) { father_name = "Father2" }
        let!(:grand_father_name) { grand_father_name = nil }
        let!(:email) { email = "Author2.Father2@example.com" }
        let!(:telephone) { telephone = "0911111111" }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["Author Grand father name can't be blank"]
        end
      end

      context "Blank Author Email" do
        let!(:first_name) { first_name = "Author3" }
        let!(:father_name) { father_name = "Father3" }
        let!(:grand_father_name) { grand_father_name = "G Father3" }
        let!(:email) { email = nil }
        let!(:telephone) { telephone = "0911111111" }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["Author Email can't be blank", "Author Email is invalid"]
        end
      end

      context "Invalid Author Email" do
        let!(:first_name) { first_name = "Author3" }
        let!(:father_name) { father_name = "Father3" }
        let!(:grand_father_name) { grand_father_name = "G Father2" }
        let!(:email) { email = "Author2.Father2@example" }
        let!(:telephone) { telephone = "0911111111" }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["Author Email is invalid"]
        end
      end

      context "Blank Author Telephone" do
        let!(:first_name) { first_name = "Author3" }
        let!(:father_name) { father_name = "Father2" }
        let!(:grand_father_name) { grand_father_name = "G Father2" }
        let!(:email) { email = "Author2.Father2@example.com" }
        let!(:telephone) { telephone = nil }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["Author Telephone can't be blank"]
        end
      end

      context "Duplicate First Name, Father Name and Grand Father Name" do
        let!(:first_name) { first_name = "Author2" }
        let!(:father_name) { father_name = "Father2" }
        let!(:grand_father_name) { grand_father_name = "G Father2" }
        let!(:email) { email = "Author1.Father1@example.com" }
        let!(:telephone) { telephone = "0911111111" }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["Author First name has already been taken", "Author Father name has already been taken", "Author Grand father name has already been taken"]
        end
      end
    end

  end

end
