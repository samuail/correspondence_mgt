require 'rails_helper'

RSpec.describe OrganizationsController, type: :controller do
  let(:user) { create(:sate_auth_user) }
  let!(:application_module) { create(:sate_auth_application_module, code: "CRMT") }
  let!(:organization_type) { create(:sate_crmgt_organization_type) }
  let(:header) { { 'Authorization' => token_generator(user.id) } }

  let(:valid_attributes) {
    {
        code: FFaker::Name.name,
        description: FFaker::Name.name,
        organization_type_id: organization_type.id,
        application_module_id: application_module.id
    }
  }

  let(:invalid_attributes) {
    {
        code: '',
        description: FFaker::Name.name,
        organization_type_id: organization_type.id,
        application_module_id: application_module.id
    }
  }

  describe "GET #index" do
    before :each do
      Sate::Crmgt::Organization.create valid_attributes
      @organization_get = Sate::Crmgt::Organization.where application_module_id: application_module.id
      request.headers.merge!(header)
      get :index
    end

    it "returns a success response" do
      expect(response).to be_successful
    end

    it "assigns all organizations as @organizations" do
      expect(assigns(:organizations)).to eq(@organization_get)
    end
  end

  describe "POST #create" do
    code = nil
    description = nil
    let!(:code) { code }
    let!(:description) { description }

    before :each do
      create :sate_crmgt_organization, code: "GOV",
             description: "Government Body",
             organization_type_id: organization_type.id,
             application_module_id: application_module.id
      request.headers.merge!(header)
      post :create, params: { organization: { 'code' => code,
                                              'description' => description,
                                              'organization_type_id' => organization_type.id } }
      @decoded_response = JSON(@response.body)
    end
    context "with valid params" do
      let!(:code) { code = "NGO" }
      let!(:description) { description = "Non Government" }

      it "creates a new Organization" do
        expect(Sate::Crmgt::Organization.count).to eq 2
      end

      it "returns a success message" do
        expect(@response).to be_successful
        expect(@decoded_response["message"]).to eq "Organization Saved Successfully!"
      end
    end
    context "with invalid params" do
      context "Blank Organization Code" do
        let!(:code) { code = nil }
        let!(:description) { description = "Non Government" }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["Organization Code can't be blank"]
        end
      end

      context "Blank Organization Description" do
        let!(:code) { code = "CBE" }
        let!(:description) { description = nil }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["Organization Description can't be blank"]
        end
      end

      context "Duplicate Organization code and description" do
        let!(:code) { code = "GOV" }
        let!(:description) { description = "Government Body" }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["Organization Code has already been taken", "Organization Description has already been taken"]
        end
      end
    end
  end

  describe "PUT #update" do
    code = nil
    description = nil
    let!(:code) { code }
    let!(:description) { description }

    before :each do
      @organization_update = create :sate_crmgt_organization, code: "GOV",
                                    description: "Government Body",
                                    organization_type_id: organization_type.id,
                                    application_module_id: application_module.id
      create :sate_crmgt_organization, code: "NGO",
             description: "Non Government",
             organization_type_id: organization_type.id,
             application_module_id: application_module.id

      request.headers.merge!(header)
      put :update, params: { id: @organization_update.to_param,
                             organization: { 'code' => code,
                                             'description' => description } }
      @decoded_response = JSON(@response.body)
    end

    context "with valid params" do
      let!(:code) { code = "PLC" }
      let!(:description) { description = "Private LC" }

      it "updates the requested Organization" do
        expect(@organization_update.code).to_not eq attributes_for(:sate_crmgt_organization)[:code]
      end

      it "returns a success message" do
        expect(@response).to be_successful
        expect(@decoded_response["message"]).to eq "Organization Updated Successfully!"
      end
    end

    context "with invalid params" do
      context "Blank Organization Code" do
        let!(:code) { code = nil }
        let!(:description) { description = "Private LC" }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["Organization Code can't be blank"]
        end
      end

      context "Blank Organization Description" do
        let!(:code) { code = "PLC" }
        let!(:description) { description = nil }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["Organization Description can't be blank"]
        end
      end

      context "Duplicate Organization Code and Description" do
        let!(:code) { code = "NGO" }
        let!(:description) { description = "Non Government" }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["Organization Code has already been taken", "Organization Description has already been taken"]
        end
      end
    end
  end

end
