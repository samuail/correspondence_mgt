require 'rails_helper'

RSpec.describe ExternalAssessorsController, type: :controller do

  let(:user) { create(:sate_auth_user) }
  let!(:application_module) { create(:sate_auth_application_module, code: "CRMT") }
  let(:header) { { 'Authorization' => token_generator(user.id) } }
  let(:valid_attributes) {
    {
      title: FFaker::Name.name,
      first_name: FFaker::Name.name,
      father_name: FFaker::Name.name,
      grand_father_name: FFaker::Name.name,
      email: FFaker::Internet.email[0..49],
      telephone: FFaker::Name.name,
      area_of_expertise: FFaker::Name.name,
      institution: FFaker::Name.name,
      bank_name: FFaker::Name.name,
      bank_acc: FFaker::Name.name,
      tin: FFaker::Name.name,
      application_module_id: application_module.id
    }
  }

  describe "GET #index" do
    before :each do
      Sate::Crmgt::ExternalAssessor.create valid_attributes
      @assessor_get = Sate::Crmgt::ExternalAssessor.where application_module_id: application_module.id
      request.headers.merge!(header)
      get :index
    end

    it "returns a success response" do
      expect(response).to be_successful
    end

    it "assigns all external_assessors as @external_assessors" do
      expect(assigns(:external_assessors)).to eq(@assessor_get)
    end
  end

  describe "POST #create" do
    title = nil
    first_name = nil
    father_name = nil
    grand_father_name = nil
    email = nil
    telephone = nil
    area_of_expertise = nil
    institution = nil
    bank_name = nil
    bank_acc = nil
    tin = nil
    let!(:title) { title }
    let!(:first_name) { first_name }
    let!(:father_name) { father_name }
    let!(:grand_father_name) { grand_father_name }
    let!(:email) { email }
    let!(:telephone) { telephone }
    let!(:area_of_expertise) { area_of_expertise }
    let!(:institution) { institution }
    let!(:bank_name) { bank_name }
    let!(:bank_acc) { bank_acc }
    let!(:tin) { tin }

    before :each do
      create :sate_crmgt_external_assessor, title: "Prof.",
             first_name: "Assessor1",
             father_name: "Father1",
             grand_father_name: "G Father1",
             email: "Assessor1.Father1@example.com",
             telephone: "0911111111",
             area_of_expertise: FFaker::Name.name,
             institution: FFaker::Name.name,
             bank_name: FFaker::Name.name,
             bank_acc: FFaker::Name.name,
             tin: FFaker::Name.name,
             application_module_id: application_module.id
      request.headers.merge!(header)
      post :create, params: { external_assessor: { 'title' => title,
                                                   'first_name' => first_name,
                                                   'father_name' => father_name,
                                                   'grand_father_name' => grand_father_name,
                                                   'email' => email,
                                                   'telephone' => telephone,
                                                   'area_of_expertise' => area_of_expertise,
                                                   'institution' => institution,
                                                   'bank_name' => bank_name,
                                                   'bank_acc' => bank_acc,
                                                   'tin' => tin } }
      @decoded_response = JSON(@response.body)
    end
    context "with valid params" do
      let!(:title) { title = "Prof." }
      let!(:first_name) { first_name = "Assessor2" }
      let!(:father_name) { father_name = "Father2" }
      let!(:grand_father_name) { grand_father_name = "G Father2" }
      let!(:email) { email = "Assessor2.Father2@example.com" }
      let!(:telephone) { telephone = "0911111111" }
      let!(:area_of_expertise) { area_of_expertise = FFaker::Name.name }
      let!(:institution) { institution = FFaker::Name.name }
      let!(:bank_name) { bank_name = FFaker::Name.name }
      let!(:bank_acc) { bank_acc = FFaker::Name.name }
      let!(:tin) { tin = FFaker::Name.name }

      it "creates a new Assessor" do
        expect(Sate::Crmgt::ExternalAssessor.count).to eq 2
      end

      it "returns a success message" do
        expect(@response).to be_successful
        expect(@decoded_response["message"]).to eq "Assessor Saved Successfully!"
      end
    end
    context "with invalid params" do
      context "Blank Assessor First Name" do
        let!(:title) { title = "Prof." }
        let!(:first_name) { first_name = nil }
        let!(:father_name) { father_name = "Father2" }
        let!(:grand_father_name) { grand_father_name = "G Father2" }
        let!(:email) { email = "Assessor2.Father2@example.com" }
        let!(:telephone) { telephone = "0911111111" }
        let!(:area_of_expertise) { area_of_expertise = FFaker::Name.name }
        let!(:institution) { institution = FFaker::Name.name }
        let!(:bank_name) { bank_name = FFaker::Name.name }
        let!(:bank_acc) { bank_acc = FFaker::Name.name }
        let!(:tin) { tin = FFaker::Name.name }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["External Assessor First name can't be blank"]
        end
      end

      context "Blank Assessor Father Name" do
        let!(:title) { title = "Prof." }
        let!(:first_name) { first_name = "Assessor2" }
        let!(:father_name) { father_name = nil }
        let!(:grand_father_name) { grand_father_name = "G Father2" }
        let!(:email) { email = "Assessor2.Father2@example.com" }
        let!(:telephone) { telephone = "0911111111" }
        let!(:area_of_expertise) { area_of_expertise = FFaker::Name.name }
        let!(:institution) { institution = FFaker::Name.name }
        let!(:bank_name) { bank_name = FFaker::Name.name }
        let!(:bank_acc) { bank_acc = FFaker::Name.name }
        let!(:tin) { tin = FFaker::Name.name }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["External Assessor Father name can't be blank"]
        end
      end

      context "Blank Assessor Grand Father Name" do
        let!(:title) { title = "Prof." }
        let!(:first_name) { first_name = "Assessor2" }
        let!(:father_name) { father_name = "Father2" }
        let!(:grand_father_name) { grand_father_name = nil }
        let!(:email) { email = "Assessor2.Father2@example.com" }
        let!(:telephone) { telephone = "0911111111" }
        let!(:area_of_expertise) { area_of_expertise = FFaker::Name.name }
        let!(:institution) { institution = FFaker::Name.name }
        let!(:bank_name) { bank_name = FFaker::Name.name }
        let!(:bank_acc) { bank_acc = FFaker::Name.name }
        let!(:tin) { tin = FFaker::Name.name }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["External Assessor Grand father name can't be blank"]
        end
      end

      context "Blank Assessor Email" do
        let!(:title) { title = "Prof." }
        let!(:first_name) { first_name = "Assessor2" }
        let!(:father_name) { father_name = "Father2" }
        let!(:grand_father_name) { grand_father_name = "G Father2" }
        let!(:email) { email = nil }
        let!(:telephone) { telephone = "0911111111" }
        let!(:area_of_expertise) { area_of_expertise = FFaker::Name.name }
        let!(:institution) { institution = FFaker::Name.name }
        let!(:bank_name) { bank_name = FFaker::Name.name }
        let!(:bank_acc) { bank_acc = FFaker::Name.name }
        let!(:tin) { tin = FFaker::Name.name }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["External Assessor Email can't be blank", "External Assessor Email is invalid"]
        end
      end

      context "Invalid Assessor Email" do
        let!(:title) { title = "Prof." }
        let!(:first_name) { first_name = "Assessor2" }
        let!(:father_name) { father_name = "Father2" }
        let!(:grand_father_name) { grand_father_name = "G Father2" }
        let!(:email) { email = "Assessor2.Father2@example" }
        let!(:telephone) { telephone = "0911111111" }
        let!(:area_of_expertise) { area_of_expertise = FFaker::Name.name }
        let!(:institution) { institution = FFaker::Name.name }
        let!(:bank_name) { bank_name = FFaker::Name.name }
        let!(:bank_acc) { bank_acc = FFaker::Name.name }
        let!(:tin) { tin = FFaker::Name.name }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["External Assessor Email is invalid"]
        end
      end

      context "Blank Assessor Telephone" do
        let!(:title) { title = "Prof." }
        let!(:first_name) { first_name = "Assessor2" }
        let!(:father_name) { father_name = "Father2" }
        let!(:grand_father_name) { grand_father_name = "G Father2" }
        let!(:email) { email = "Assessor2.Father2@example.com" }
        let!(:telephone) { telephone = nil }
        let!(:area_of_expertise) { area_of_expertise = FFaker::Name.name }
        let!(:institution) { institution = FFaker::Name.name }
        let!(:bank_name) { bank_name = FFaker::Name.name }
        let!(:bank_acc) { bank_acc = FFaker::Name.name }
        let!(:tin) { tin = FFaker::Name.name }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["External Assessor Telephone can't be blank"]
        end
      end

      context "Blank Assessor Area of Expertise" do
        let!(:title) { title = "Prof." }
        let!(:first_name) { first_name = "Assessor2" }
        let!(:father_name) { father_name = "Father2" }
        let!(:grand_father_name) { grand_father_name = "G Father2" }
        let!(:email) { email = "Assessor2.Father2@example.com" }
        let!(:telephone) { telephone = "0911111111" }
        let!(:area_of_expertise) { area_of_expertise = nil }
        let!(:institution) { institution = FFaker::Name.name }
        let!(:bank_name) { bank_name = FFaker::Name.name }
        let!(:bank_acc) { bank_acc = FFaker::Name.name }
        let!(:tin) { tin = FFaker::Name.name }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["External Assessor Area of expertise can't be blank"]
        end
      end

      context "Blank Assessor Bank Name" do
        let!(:title) { title = "Prof." }
        let!(:first_name) { first_name = "Assessor2" }
        let!(:father_name) { father_name = "Father2" }
        let!(:grand_father_name) { grand_father_name = "G Father2" }
        let!(:email) { email = "Assessor2.Father2@example.com" }
        let!(:telephone) { telephone = "0911111111" }
        let!(:area_of_expertise) { area_of_expertise = FFaker::Name.name }
        let!(:institution) { institution = FFaker::Name.name }
        let!(:bank_name) { bank_name = nil }
        let!(:bank_acc) { bank_acc = FFaker::Name.name }
        let!(:tin) { tin = FFaker::Name.name }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["External Assessor Bank name can't be blank"]
        end
      end

      context "Blank Assessor Bank Account" do
        let!(:title) { title = "Prof." }
        let!(:first_name) { first_name = "Assessor2" }
        let!(:father_name) { father_name = "Father2" }
        let!(:grand_father_name) { grand_father_name = "G Father2" }
        let!(:email) { email = "Assessor2.Father2@example.com" }
        let!(:telephone) { telephone = "0911111111" }
        let!(:area_of_expertise) { area_of_expertise = FFaker::Name.name }
        let!(:institution) { institution = FFaker::Name.name }
        let!(:bank_name) { bank_name = FFaker::Name.name }
        let!(:bank_acc) { bank_acc = nil }
        let!(:tin) { tin = FFaker::Name.name }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["External Assessor Bank acc can't be blank"]
        end
      end

      context "Blank Assessor TIN" do
        let!(:title) { title = "Prof." }
        let!(:first_name) { first_name = "Assessor2" }
        let!(:father_name) { father_name = "Father2" }
        let!(:grand_father_name) { grand_father_name = "G Father2" }
        let!(:email) { email = "Assessor2.Father2@example.com" }
        let!(:telephone) { telephone = "0911111111" }
        let!(:area_of_expertise) { area_of_expertise = FFaker::Name.name }
        let!(:institution) { institution = FFaker::Name.name }
        let!(:bank_name) { bank_name = FFaker::Name.name }
        let!(:bank_acc) { bank_acc = FFaker::Name.name }
        let!(:tin) { tin = nil }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["External Assessor Tin can't be blank"]
        end
      end

      context "Duplicate First Name, Father Name and Grand Father Name" do
        let!(:title) { title = "Prof." }
        let!(:first_name) { first_name = "Assessor1" }
        let!(:father_name) { father_name = "Father1" }
        let!(:grand_father_name) { grand_father_name = "G Father1" }
        let!(:email) { email = "Assessor1.Father1@example.com" }
        let!(:telephone) { telephone = "0911111111" }
        let!(:area_of_expertise) { area_of_expertise = FFaker::Name.name }
        let!(:institution) { institution = FFaker::Name.name }
        let!(:bank_name) { bank_name = FFaker::Name.name }
        let!(:bank_acc) { bank_acc = FFaker::Name.name }
        let!(:tin) { tin = FFaker::Name.name }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["External Assessor First name has already been taken", "External Assessor Father name has already been taken", "External Assessor Grand father name has already been taken"]
        end
      end
    end
  end

  describe "PUT #update" do
    title = nil
    first_name = nil
    father_name = nil
    grand_father_name = nil
    email = nil
    telephone = nil
    area_of_expertise = nil
    institution = nil
    bank_name = nil
    bank_acc = nil
    tin = nil
    let!(:title) { title }
    let!(:first_name) { first_name }
    let!(:father_name) { father_name }
    let!(:grand_father_name) { grand_father_name }
    let!(:email) { email }
    let!(:telephone) { telephone }
    let!(:area_of_expertise) { area_of_expertise }
    let!(:institution) { institution }
    let!(:bank_name) { bank_name }
    let!(:bank_acc) { bank_acc }
    let!(:tin) { tin }

    before :each do
      @assessor_update = create :sate_crmgt_external_assessor, title: "Prof.", first_name: "Assessor1",
                              father_name: "Father1", grand_father_name: "G Father1",
                              email: "Assessor1.Father1@example.com", telephone: "0911111111",
                              area_of_expertise: FFaker::Name.name, institution: FFaker::Name.name,
                              bank_name: FFaker::Name.name, bank_acc: FFaker::Name.name,
                              tin: FFaker::Name.name, application_module_id: application_module.id

      create :sate_crmgt_external_assessor, title: "Prof.", first_name: "Assessor2",
             father_name: "Father2", grand_father_name: "G Father2",
             email: "Assessor2.Father2@example.com", telephone: "0911111111",
             area_of_expertise: FFaker::Name.name, institution: FFaker::Name.name,
             bank_name: FFaker::Name.name, bank_acc: FFaker::Name.name,
             tin: FFaker::Name.name, application_module_id: application_module.id

      request.headers.merge!(header)
      put :update, params: { id: @assessor_update.to_param,
                             external_assessor: { 'title' => title,
                                                  'first_name' => first_name,
                                                  'father_name' => father_name,
                                                  'grand_father_name' => grand_father_name,
                                                  'email' => email,
                                                  'telephone' => telephone,
                                                  'area_of_expertise' => area_of_expertise,
                                                  'institution' => institution,
                                                  'bank_name' => bank_name,
                                                  'bank_acc' => bank_acc,
                                                  'tin' => tin } }
      @decoded_response = JSON(@response.body)
    end

    context "with valid params" do
      let!(:title) { title = "Prof." }
      let!(:first_name) { first_name = "Assessor3" }
      let!(:father_name) { father_name = "Father3" }
      let!(:grand_father_name) { grand_father_name = "G Father3" }
      let!(:email) { email = "Assessor3.Father2@example.com" }
      let!(:telephone) { telephone = "0911111111" }
      let!(:area_of_expertise) { area_of_expertise = FFaker::Name.name }
      let!(:institution) { institution = FFaker::Name.name }
      let!(:bank_name) { bank_name = FFaker::Name.name }
      let!(:bank_acc) { bank_acc = FFaker::Name.name }
      let!(:tin) { tin = FFaker::Name.name }

      it "updates the requested Assessor" do
        expect(@assessor_update.first_name).to_not eq attributes_for(:sate_crmgt_external_assessor)[:first_name]
      end

      it "returns a success message" do
        expect(@response).to be_successful
        expect(@decoded_response["message"]).to eq "Assessor Updated Successfully!"
      end
    end

    context "with invalid params" do
      context "Blank Assessor First Name" do
        let!(:title) { title = "Prof." }
        let!(:first_name) { first_name = nil }
        let!(:father_name) { father_name = "Father2" }
        let!(:grand_father_name) { grand_father_name = "G Father2" }
        let!(:email) { email = "Assessor2.Father2@example.com" }
        let!(:telephone) { telephone = "0911111111" }
        let!(:area_of_expertise) { area_of_expertise = FFaker::Name.name }
        let!(:institution) { institution = FFaker::Name.name }
        let!(:bank_name) { bank_name = FFaker::Name.name }
        let!(:bank_acc) { bank_acc = FFaker::Name.name }
        let!(:tin) { tin = FFaker::Name.name }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["External Assessor First name can't be blank"]
        end
      end

      context "Blank Assessor Father Name" do
        let!(:title) { title = "Prof." }
        let!(:first_name) { first_name = "Assessor2" }
        let!(:father_name) { father_name = nil }
        let!(:grand_father_name) { grand_father_name = "G Father2" }
        let!(:email) { email = "Assessor2.Father2@example.com" }
        let!(:telephone) { telephone = "0911111111" }
        let!(:area_of_expertise) { area_of_expertise = FFaker::Name.name }
        let!(:institution) { institution = FFaker::Name.name }
        let!(:bank_name) { bank_name = FFaker::Name.name }
        let!(:bank_acc) { bank_acc = FFaker::Name.name }
        let!(:tin) { tin = FFaker::Name.name }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["External Assessor Father name can't be blank"]
        end
      end

      context "Blank Assessor Grand Father Name" do
        let!(:title) { title = "Prof." }
        let!(:first_name) { first_name = "Assessor2" }
        let!(:father_name) { father_name = "Father2" }
        let!(:grand_father_name) { grand_father_name = nil }
        let!(:email) { email = "Assessor2.Father2@example.com" }
        let!(:telephone) { telephone = "0911111111" }
        let!(:area_of_expertise) { area_of_expertise = FFaker::Name.name }
        let!(:institution) { institution = FFaker::Name.name }
        let!(:bank_name) { bank_name = FFaker::Name.name }
        let!(:bank_acc) { bank_acc = FFaker::Name.name }
        let!(:tin) { tin = FFaker::Name.name }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["External Assessor Grand father name can't be blank"]
        end
      end

      context "Blank Assessor Email" do
        let!(:title) { title = "Prof." }
        let!(:first_name) { first_name = "Assessor3" }
        let!(:father_name) { father_name = "Father3" }
        let!(:grand_father_name) { grand_father_name = "G Father3" }
        let!(:email) { email = nil }
        let!(:telephone) { telephone = "0911111111" }
        let!(:area_of_expertise) { area_of_expertise = FFaker::Name.name }
        let!(:institution) { institution = FFaker::Name.name }
        let!(:bank_name) { bank_name = FFaker::Name.name }
        let!(:bank_acc) { bank_acc = FFaker::Name.name }
        let!(:tin) { tin = FFaker::Name.name }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["External Assessor Email can't be blank", "External Assessor Email is invalid"]
        end
      end

      context "Invalid Assessor Email" do
        let!(:title) { title = "Prof." }
        let!(:first_name) { first_name = "Assessor3" }
        let!(:father_name) { father_name = "Father3" }
        let!(:grand_father_name) { grand_father_name = "G Father2" }
        let!(:email) { email = "Assessor2.Father2@example" }
        let!(:telephone) { telephone = "0911111111" }
        let!(:area_of_expertise) { area_of_expertise = FFaker::Name.name }
        let!(:institution) { institution = FFaker::Name.name }
        let!(:bank_name) { bank_name = FFaker::Name.name }
        let!(:bank_acc) { bank_acc = FFaker::Name.name }
        let!(:tin) { tin = FFaker::Name.name }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["External Assessor Email is invalid"]
        end
      end

      context "Blank Assessor Telephone" do
        let!(:title) { title = "Prof." }
        let!(:first_name) { first_name = "Assessor3" }
        let!(:father_name) { father_name = "Father2" }
        let!(:grand_father_name) { grand_father_name = "G Father2" }
        let!(:email) { email = "Assessor2.Father2@example.com" }
        let!(:telephone) { telephone = nil }
        let!(:area_of_expertise) { area_of_expertise = FFaker::Name.name }
        let!(:institution) { institution = FFaker::Name.name }
        let!(:bank_name) { bank_name = FFaker::Name.name }
        let!(:bank_acc) { bank_acc = FFaker::Name.name }
        let!(:tin) { tin = FFaker::Name.name }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["External Assessor Telephone can't be blank"]
        end
      end

      context "Blank Assessor Area of Expertise" do
        let!(:title) { title = "Prof." }
        let!(:first_name) { first_name = "Assessor3" }
        let!(:father_name) { father_name = "Father3" }
        let!(:grand_father_name) { grand_father_name = "G Father3" }
        let!(:email) { email = "Assessor3.Father3@example.com" }
        let!(:telephone) { telephone = "0911111111" }
        let!(:area_of_expertise) { area_of_expertise = nil }
        let!(:institution) { institution = FFaker::Name.name }
        let!(:bank_name) { bank_name = FFaker::Name.name }
        let!(:bank_acc) { bank_acc = FFaker::Name.name }
        let!(:tin) { tin = FFaker::Name.name }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["External Assessor Area of expertise can't be blank"]
        end
      end

      context "Blank Assessor Bank Name" do
        let!(:title) { title = "Prof." }
        let!(:first_name) { first_name = "Assessor3" }
        let!(:father_name) { father_name = "Father3" }
        let!(:grand_father_name) { grand_father_name = "G Father3" }
        let!(:email) { email = "Assessor3.Father3@example.com" }
        let!(:telephone) { telephone = "0911111111" }
        let!(:area_of_expertise) { area_of_expertise = FFaker::Name.name }
        let!(:institution) { institution = FFaker::Name.name }
        let!(:bank_name) { bank_name = nil }
        let!(:bank_acc) { bank_acc = FFaker::Name.name }
        let!(:tin) { tin = FFaker::Name.name }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["External Assessor Bank name can't be blank"]
        end
      end

      context "Blank Assessor Bank Account" do
        let!(:title) { title = "Prof." }
        let!(:first_name) { first_name = "Assessor3" }
        let!(:father_name) { father_name = "Father3" }
        let!(:grand_father_name) { grand_father_name = "G Father3" }
        let!(:email) { email = "Assessor3.Father3@example.com" }
        let!(:telephone) { telephone = "0911111111" }
        let!(:area_of_expertise) { area_of_expertise = FFaker::Name.name }
        let!(:institution) { institution = FFaker::Name.name }
        let!(:bank_name) { bank_name = FFaker::Name.name }
        let!(:bank_acc) { bank_acc = nil }
        let!(:tin) { tin = FFaker::Name.name }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["External Assessor Bank acc can't be blank"]
        end
      end

      context "Blank Assessor TIN" do
        let!(:title) { title = "Prof." }
        let!(:first_name) { first_name = "Assessor3" }
        let!(:father_name) { father_name = "Father3" }
        let!(:grand_father_name) { grand_father_name = "G Father3" }
        let!(:email) { email = "Assessor3.Father3@example.com" }
        let!(:telephone) { telephone = "0911111111" }
        let!(:area_of_expertise) { area_of_expertise = FFaker::Name.name }
        let!(:institution) { institution = FFaker::Name.name }
        let!(:bank_name) { bank_name = FFaker::Name.name }
        let!(:bank_acc) { bank_acc = FFaker::Name.name }
        let!(:tin) { tin = nil }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["External Assessor Tin can't be blank"]
        end
      end

      context "Duplicate First Name, Father Name and Grand Father Name" do
        let!(:title) { title = "Prof." }
        let!(:first_name) { first_name = "Assessor2" }
        let!(:father_name) { father_name = "Father2" }
        let!(:grand_father_name) { grand_father_name = "G Father2" }
        let!(:email) { email = "Assessor2.Father2@example.com" }
        let!(:telephone) { telephone = "0911111111" }
        let!(:area_of_expertise) { area_of_expertise = FFaker::Name.name }
        let!(:institution) { institution = FFaker::Name.name }
        let!(:bank_name) { bank_name = FFaker::Name.name }
        let!(:bank_acc) { bank_acc = FFaker::Name.name }
        let!(:tin) { tin = FFaker::Name.name }

        it "returns an error message" do
          # byebug
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["External Assessor First name has already been taken", "External Assessor Father name has already been taken", "External Assessor Grand father name has already been taken"]
        end
      end
    end
  end

end
