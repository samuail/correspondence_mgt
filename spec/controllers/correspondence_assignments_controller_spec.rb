require 'rails_helper'

RSpec.describe CorrespondenceAssignmentsController, type: :controller do
  let(:user) { create(:sate_auth_user) }
  let(:to_user) { create(:sate_auth_user) }
  let(:to_other_user) { create(:sate_auth_user) }
  let(:to_another_user) { create(:sate_auth_user) }
  let(:internal) { create(:sate_crmgt_internal_correspondence) }
  let(:other_internal) { create(:sate_crmgt_internal_correspondence) }
  let(:external) { create(:sate_crmgt_external_incoming_correspondence) }
  let(:other_external) { create(:sate_crmgt_external_incoming_correspondence) }
  let!(:application_module) { create(:sate_auth_application_module, code: "CRMT") }

  let(:header) { { 'Authorization' => token_generator(user.id) } }

  let(:valid_internal_attributes) {
    {
        from_user_id: user.id,
        to_user_id: to_user.id,
        correspondence: internal,
        order: 1,
        status: FFaker::Name.name,
        assigned_date: FFaker::Time.date,
        received_date: FFaker::Time.date,
        application_module_id: application_module.id
    }
  }

  let(:valid_external_attributes) {
    {
        from_user_id: user.id,
        to_user_id: to_user.id,
        correspondence: external,
        order: 1,
        status: FFaker::Name.name,
        assigned_date: FFaker::Time.date,
        received_date: FFaker::Time.date,
        application_module_id: application_module.id
    }
  }

  describe "GET #index for Internal Correspondence Assignment" do
    before :each do
      Sate::Crmgt::CorrespondenceAssignment.create valid_internal_attributes

      @internal_assignment_get = Sate::Crmgt::CorrespondenceAssignment.where application_module_id: application_module.id,
                                                                             correspondence: internal
      request.headers.merge!(header)

      get :index, params: { correspondence_id: internal.id }
    end

    it "returns a success response" do
      expect(response).to be_successful
    end

    it "assigns internal correspondence assignment of the current user as @correspondence_assignments" do
      expect(assigns(:@correspondence_assignments)).to eq(@internal_incoming_get)
    end
  end

  describe "GET #index for External Incoming Correspondence Assignment" do
    before :each do
      Sate::Crmgt::CorrespondenceAssignment.create valid_external_attributes

      @internal_assignment_get = Sate::Crmgt::CorrespondenceAssignment.where application_module_id: application_module.id,
                                                                             correspondence: external
      request.headers.merge!(header)

      get :index, params: { correspondence_id: external.id, request_type: "incoming" }
    end

    it "returns a success response" do
      expect(response).to be_successful
    end

    it "assigns external correspondence assignment of the current user as @correspondence_assignments" do
      expect(assigns(:@correspondence_assignments)).to eq(@internal_incoming_get)
    end
  end

  describe "POST #create for Internal Correspondence Assignment" do
    to_user_id = nil
    correspondence_id = nil
    let!(:to_user_id) { to_user_id }
    let!(:correspondence_id) { correspondence_id }

    before :each do
      create :sate_crmgt_correspondence_assignment, from_user_id: user.id,
                                                    to_user_id: to_other_user.id,
                                                    correspondence: other_internal,
                                                    application_module_id: application_module.id

      request.headers.merge!(header)
      post :create, params: { :correspondence_assignment => { to_user_id: to_user_id,
                                                              correspondence_id: correspondence_id } }
      @decoded_response = JSON(@response.body)
    end

    context "with valid params" do
      let!(:to_user_id) { to_user_id = to_user.id }
      let!(:correspondence_id) { correspondence_id = internal.id }

      it "creates a new Internal Correspondence Assignment" do
        expect(Sate::Crmgt::CorrespondenceAssignment.count).to eq 2
      end

      it "returns a success message" do
        expect(@response).to be_successful
        expect(@decoded_response["message"]).to eq "Correspondence Assignment Saved Successfully!"
      end
    end

    context "with invalid params" do
      context "Blank attributes" do
        context "To User" do
          let!(:to_user_id) { to_user_id = nil }
          let!(:correspondence_id) { correspondence_id = internal.id }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Correspondence Assignment To user must exist", "Correspondence Assignment To user can't be blank"]
          end
        end
        context "Correspondence" do
          let!(:to_user_id) { to_user_id = to_user.id }
          let!(:correspondence_id) { correspondence_id = nil }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Correspondence Assignment Correspondence must exist", "Correspondence Assignment Correspondence can't be blank"]
          end
        end
      end

      context "Duplicate Data" do
        let!(:to_user_id) { to_user_id = to_other_user.id }
        let!(:correspondence_id) { correspondence_id = other_internal.id }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["Correspondence Assignment From user has already been taken", "Correspondence Assignment To user has already been taken", "Correspondence Assignment Correspondence has already been taken", "Correspondence Assignment Correspondence type has already been taken", "Correspondence Assignment Application module has already been taken"]
        end
      end
    end

  end

  describe "POST #create for External Incoming Correspondence Assignment" do
    to_user_id = nil
    correspondence_id = nil
    let!(:to_user_id) { to_user_id }
    let!(:correspondence_id) { correspondence_id }

    before :each do
      create :sate_crmgt_correspondence_assignment, from_user_id: user.id,
                                                    to_user_id: to_other_user.id,
                                                    correspondence: other_external,
                                                    application_module_id: application_module.id

      request.headers.merge!(header)
      post :create, params: { request_type: "incoming",
                              :correspondence_assignment => { to_user_id: to_user_id,
                                                              correspondence_id: correspondence_id } }
      @decoded_response = JSON(@response.body)
    end

    context "with valid params" do
      let!(:to_user_id) { to_user_id = to_user.id }
      let!(:correspondence_id) { correspondence_id = external.id }

      it "creates a new Internal Correspondence Assignment" do
        expect(Sate::Crmgt::CorrespondenceAssignment.count).to eq 2
      end

      it "returns a success message" do
        expect(@response).to be_successful
        expect(@decoded_response["message"]).to eq "Correspondence Assignment Saved Successfully!"
      end
    end

    context "with invalid params" do
      context "Blank attributes" do
        context "To User" do
          let!(:to_user_id) { to_user_id = nil }
          let!(:correspondence_id) { correspondence_id = external.id }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Correspondence Assignment To user must exist", "Correspondence Assignment To user can't be blank"]
          end
        end
        context "Correspondence" do
          let!(:to_user_id) { to_user_id = to_user.id }
          let!(:correspondence_id) { correspondence_id = nil }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Correspondence Assignment Correspondence must exist", "Correspondence Assignment Correspondence can't be blank"]
          end
        end
      end

      context "Duplicate Data" do
        let!(:to_user_id) { to_user_id = to_other_user.id }
        let!(:correspondence_id) { correspondence_id = other_external.id }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["Correspondence Assignment From user has already been taken", "Correspondence Assignment To user has already been taken", "Correspondence Assignment Correspondence has already been taken", "Correspondence Assignment Correspondence type has already been taken", "Correspondence Assignment Application module has already been taken"]
        end
      end
    end

  end

  describe "PUT #update to Assign for another user" do
    to_user_id = nil
    let!(:to_user_id) { to_user_id }

    before :each do
      create :sate_crmgt_correspondence_assignment, from_user_id: user.id,
                                                    to_user_id: to_another_user.id,
                                                    correspondence: internal,
                                                    application_module_id: application_module.id
      @update = create :sate_crmgt_correspondence_assignment, from_user_id: user.id,
                                                              to_user_id: to_user.id,
                                                              correspondence: internal,
                                                              application_module_id: application_module.id

      request.headers.merge!(header)
      put :update, params: { id: @update.to_param,
                             :correspondence_assignment => { to_user_id: to_user_id } }
      @decoded_response = JSON(@response.body)
    end

    context "with valid params" do
      let!(:to_user_id) { to_user_id = to_other_user.id }

      it "Updates the requested Internal Correspondence Assignment" do
        expect(@update.to_user_id).to_not eq attributes_for(:sate_crmgt_correspondence_assignment)[:to_user_id]
      end

      it "returns a success message" do
        expect(@response).to be_successful
        expect(@decoded_response["message"]).to eq "Correspondence Assignment Updated Successfully!"
      end
    end

    context "with invalid params" do
      context "Blank To User attributes" do
        let!(:to_user_id) { to_user_id = nil }
        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["Correspondence Assignment To user must exist", "Correspondence Assignment To user can't be blank"]
        end
      end

      context "Duplicate Data" do
        let!(:to_user_id) { to_user_id = to_another_user.id }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["Correspondence Assignment From user has already been taken", "Correspondence Assignment To user has already been taken", "Correspondence Assignment Correspondence has already been taken", "Correspondence Assignment Correspondence type has already been taken", "Correspondence Assignment Order has already been taken", "Correspondence Assignment Application module has already been taken"]
        end
      end
    end

  end

  describe "PUT #update to Received by assigned user" do
    status = nil
    received_date = nil
    let!(:status) { status }
    let!(:received_date) { received_date }

    before :each do
      @update = create :sate_crmgt_correspondence_assignment, from_user_id: user.id,
                       to_user_id: to_user.id,
                       correspondence: internal,
                       status: "Assigned",
                       application_module_id: application_module.id

      request.headers.merge!(header)
      put :update, params: { id: @update.to_param,
                             :correspondence_assignment => { status: status,
                                                             received_date: received_date } }
      @decoded_response = JSON(@response.body)
    end

    context "with valid params" do
      let!(:status) { status = "Received" }
      let!(:received_date) { received_date = Date.today }

      it "Updates the requested Internal Correspondence Assignment" do
        expect(@update.status).to_not eq attributes_for(:sate_crmgt_correspondence_assignment)[:status]
        expect(@update.received_date).to_not eq attributes_for(:sate_crmgt_correspondence_assignment)[:received_date]
      end

      it "returns a success message" do
        expect(@response).to be_successful
        expect(@decoded_response["message"]).to eq "Correspondence Assignment Updated Successfully!"
      end
    end

    context "with invalid params" do
      context "Blank attributes" do
        context "Status" do
          let!(:status) { status = nil }
          let!(:received_date) { received_date = Date.today }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Correspondence Assignment Status can't be blank"]
          end
        end

        context "Received Date" do
          let!(:status) { status = "Received" }
          let!(:received_date) { received_date = nil }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Correspondence Assignment Received date can't be blank"]
          end
        end
      end
    end
  end
end
