require 'rails_helper'

RSpec.describe InternalCorrespondencesController, type: :controller do
  let(:user) { create(:sate_auth_user) }
  let!(:application_module) { create(:sate_auth_application_module, code: "CRMT") }
  let!(:center_department) { create(:sate_crmgt_department, description: "Main Campus") }
  let!(:source) { create(:sate_crmgt_department, parent_id: center_department.id) }
  let!(:source_center) { create(:sate_crmgt_department, parent_id: center_department.id) }
  let!(:source_college) { create(:sate_crmgt_department) }
  let!(:destination) { create(:sate_crmgt_department) }
  let!(:destination_center) { create(:sate_crmgt_department, parent_id: center_department.id) }
  let!(:other_department) { create(:sate_crmgt_department, parent_id: center_department.id) }
  let!(:another_department) { create(:sate_crmgt_department) }
  let!(:user_role) { create(:sate_auth_user_role) }

  let(:header) { { 'Authorization' => token_generator(user.id) } }

  let(:valid_attributes) {
    {
        reference_no: FFaker::Name.name,
        letter_date: FFaker::Time.date,
        subject: FFaker::Name.name,
        search_keywords: [FFaker::Name.name, FFaker::Name.name],
        source_id: source.id,
        destination_id: destination.id,
        received_date: FFaker::Time.date,
        application_module_id: application_module.id,
        key_in_by_id: user.id
    }
  }

  let(:valid_attributes_for_center) {
    {
      reference_no: FFaker::Name.name,
      letter_date: FFaker::Time.date,
      subject: FFaker::Name.name,
      search_keywords: [FFaker::Name.name, FFaker::Name.name],
      source_id: source_center.id,
      destination_id: destination_center.id,
      received_date: FFaker::Time.date,
      application_module_id: application_module.id,
      key_in_by_id: user.id
    }
  }

  let(:valid_attributes_for_college) {
    {
      reference_no: FFaker::Name.name,
      letter_date: FFaker::Time.date,
      subject: FFaker::Name.name,
      search_keywords: [FFaker::Name.name, FFaker::Name.name],
      source_id: source_college.id,
      destination_id: destination.id,
      received_date: FFaker::Time.date,
      application_module_id: application_module.id,
      key_in_by_id: user.id
    }
  }

  let(:valid_attributes_center_cc) {
    {
      reference_no: FFaker::Name.name,
      letter_date: FFaker::Time.date,
      subject: FFaker::Name.name,
      search_keywords: [FFaker::Name.name, FFaker::Name.name],
      source_id: source_center.id,
      destination_id: destination_center.id,
      received_date: FFaker::Time.date,
      application_module_id: application_module.id,
      key_in_by_id: user.id,
      has_cc: true
    }
  }

  let(:valid_attributes_college_cc) {
    {
      reference_no: FFaker::Name.name,
      letter_date: FFaker::Time.date,
      subject: FFaker::Name.name,
      search_keywords: [FFaker::Name.name, FFaker::Name.name],
      source_id: source.id,
      destination_id: another_department.id,
      received_date: FFaker::Time.date,
      application_module_id: application_module.id,
      key_in_by_id: user.id,
      has_cc: true
    }
  }

  describe "GET #index for Incoming Requests from Center" do
    before :each do
      Sate::Crmgt::InternalCorrespondence.create valid_attributes_for_center
      create(:sate_crmgt_user_department, user_id: user.id, department_id: destination_center.id)
      source_departments = Sate::Crmgt::Department.center
      dest_dep = Sate::Crmgt::UserDepartment.find_by_user_id user.id
      @internal_incoming_get = Sate::Crmgt::InternalCorrespondence.where application_module_id: application_module.id,
                                                                         destination_id: dest_dep.department_id,
                                                                         source_id: source_departments
      request.headers.merge!(header)
      get :index, params: { request_type: "incoming", department_type: "center" }
    end

    it "returns a success response" do
      expect(response).to be_successful
    end

    it "assigns internal correspondence of the current user as @internal_correspondence" do
      expect(assigns(:internal_correspondences3)).to eq(@internal_incoming_get)
    end
  end

  describe "GET #index for Incoming Requests from College" do
    before :each do
      Sate::Crmgt::InternalCorrespondence.create valid_attributes_for_college
      create(:sate_crmgt_user_department, user_id: user.id, department_id: destination.id)
      source_departments = Sate::Crmgt::Department.college
      dest_dep = Sate::Crmgt::UserDepartment.find_by_user_id user.id
      @internal_incoming_get = Sate::Crmgt::InternalCorrespondence.where application_module_id: application_module.id,
                                                                         destination_id: dest_dep.department_id,
                                                                         source_id: source_departments
      request.headers.merge!(header)
      get :index, params: { request_type: "incoming", department_type: "college" }
    end

    it "returns a success response" do
      expect(response).to be_successful
    end

    it "assigns internal correspondence of the current user as @internal_correspondence" do
      expect(assigns(:internal_correspondences3)).to eq(@internal_incoming_get)
    end
  end

  describe "GET #index for Outgoing Requests" do
    before :each do
      Sate::Crmgt::InternalCorrespondence.create valid_attributes
      create(:sate_crmgt_user_department, user_id: user.id, department_id: source.id)
      source_dep = Sate::Crmgt::UserDepartment.find_by_user_id user.id
      @internal_outgoing_get = Sate::Crmgt::InternalCorrespondence.where application_module_id: application_module.id,
                                                                         source_id: source_dep.department_id
      request.headers.merge!(header)
      get :index, params: { request_type: "outgoing" }
    end

    it "returns a success response" do
      expect(response).to be_successful
    end

    it "assigns internal correspondence of the current user as @internal_correspondence" do
      expect(assigns(:internal_correspondences3)).to eq(@internal_outgoing_get)
    end
  end

  describe "GET #index for Incoming Requests with Cc from Center" do
    before :each do
      create(:sate_crmgt_user_department, user_id: user.id, department_id: other_department.id)
      source_departments = Sate::Crmgt::Department.center

      Sate::Crmgt::InternalCorrespondence.create valid_attributes_for_center
      correspondence = Sate::Crmgt::InternalCorrespondence.create valid_attributes_center_cc

      Sate::Crmgt::CorrespondenceCarbonCopy.create correspondence: correspondence,
                                                   destination_id: other_department.id,
                                                   application_module_id: application_module.id

      correspondence1 = Sate::Crmgt::InternalCorrespondence.where application_module_id: application_module.id,
                                                                  destination_id: destination.id,
                                                                  source_id: source_departments

      ids = Sate::Crmgt::CorrespondenceCarbonCopy.where(application_module_id: application_module.id,
                                                      correspondence_type: 'Sate::Crmgt::InternalCorrespondence',
                                                      destination_id: other_department.id)
                                               .select("correspondence_id").map(&:correspondence_id)

      correspondence2 = Sate::Crmgt::InternalCorrespondence.where id: ids,
                                                                  application_module_id: application_module.id,
                                                                  source_id: source_departments

      @internal_incoming_get = correspondence2.all + correspondence1.all

      request.headers.merge!(header)

      get :index, params: { request_type: "incoming", department_type: "center" }
    end

    it "returns a success response" do
      expect(response).to be_successful
    end

    it "assigns internal correspondence of the current user as @internal_correspondence" do
      expect(assigns(:internal_correspondences3)).to eq(@internal_incoming_get)
    end
  end

  describe "GET #index for Incoming Requests with Cc from College" do
    before :each do
      create(:sate_crmgt_user_department, user_id: user.id, department_id: destination.id)
      source_departments = Sate::Crmgt::Department.college

      Sate::Crmgt::InternalCorrespondence.create valid_attributes_for_college
      correspondence = Sate::Crmgt::InternalCorrespondence.create valid_attributes_college_cc

      Sate::Crmgt::CorrespondenceCarbonCopy.create correspondence: correspondence,
                                                   destination_id: destination.id,
                                                   application_module_id: application_module.id

      correspondence1 = Sate::Crmgt::InternalCorrespondence.where application_module_id: application_module.id,
                                                                  destination_id: destination.id,
                                                                  source_id: source_departments

      ids = Sate::Crmgt::CorrespondenceCarbonCopy.where(application_module_id: application_module.id,
                                                        correspondence_type: 'Sate::Crmgt::InternalCorrespondence',
                                                        destination_id: another_department.id)
                                                 .select("correspondence_id").map(&:correspondence_id)

      correspondence2 = Sate::Crmgt::InternalCorrespondence.where id: ids,
                                                                  application_module_id: application_module.id,
                                                                  source_id: source_departments

      @internal_incoming_get = correspondence2.all + correspondence1.all

      request.headers.merge!(header)
      get :index, params: { request_type: "incoming", department_type: "college" }
    end

    it "returns a success response" do
      expect(response).to be_successful
    end

    it "assigns internal correspondence of the current user as @internal_correspondence" do
      expect(assigns(:internal_correspondences3)).to eq(@internal_incoming_get)
    end
  end

  describe "GET #index for Incoming Requests with only Cc" do
    before :each do
      create(:sate_crmgt_user_department, user_id: user.id, department_id: other_department.id)
      source_departments = Sate::Crmgt::Department.center

      correspondence = Sate::Crmgt::InternalCorrespondence.create valid_attributes_center_cc
      Sate::Crmgt::CorrespondenceCarbonCopy.create correspondence: correspondence,
                                                   destination_id: other_department.id,
                                                   application_module_id: application_module.id
      ids = Sate::Crmgt::CorrespondenceCarbonCopy.where(application_module_id: application_module.id,
                                                        correspondence_type: 'Sate::Crmgt::InternalCorrespondence',
                                                        destination_id: other_department.id)
                                                 .select("correspondence_id").map(&:correspondence_id)
      correspondence1 = Sate::Crmgt::InternalCorrespondence.where application_module_id: application_module.id,
                                                                  destination_id: destination.id,
                                                                  source_id: source_departments
      correspondence2 = Sate::Crmgt::InternalCorrespondence.where id: ids,
                                                                  application_module_id: application_module.id,
                                                                  source_id: source_departments

      @internal_incoming_get = correspondence2.all + correspondence1.all

      request.headers.merge!(header)

      get :index, params: { request_type: "incoming", department_type: "center" }
    end

    it "returns a success response" do
      expect(response).to be_successful
    end

    it "assigns internal correspondence of the current user as @internal_correspondence" do
      expect(assigns(:internal_correspondences3)).to eq(@internal_incoming_get)
    end
  end

  describe "GET #get_assigned under Center department" do
    before :each do
      correspondence = Sate::Crmgt::InternalCorrespondence.create valid_attributes_for_center
      from_user = create(:sate_auth_user)
      source.parent_id = center_department.id
      source.save
      source_departments = Sate::Crmgt::Department.center
      create(:sate_crmgt_correspondence_assignment, from_user_id: from_user.id,
                                                    to_user_id: user.id,
                                                    correspondence_id: correspondence.id,
                                                    correspondence_type: 'Sate::Crmgt::InternalCorrespondence',
                                                    order: 1,
                                                    status: "Assigned",
                                                    application_module_id: application_module.id)
      create(:sate_crmgt_user_department, user_id: user.id, department_id: destination_center.id)

      assigned = Sate::Crmgt::CorrespondenceAssignment.where(application_module_id: application_module.id,
                                                             to_user_id: user.id,
                                                             correspondence_type: 'Sate::Crmgt::InternalCorrespondence')
                                                      .select("correspondence_id").map(&:correspondence_id)
      @internal_incoming_get = Sate::Crmgt::InternalCorrespondence.where id: assigned,
                                                                         application_module_id: application_module.id,
                                                                         source_id: source_departments
      request.headers.merge!(header)
      get :get_assigned, params: { department_type: "center" }
    end

    it "returns a success response" do
      expect(response).to be_successful
    end

    it "assigns internal correspondence Assigned to the current user as @internal_correspondence" do
      expect(assigns(:internal_correspondences)).to eq(@internal_incoming_get)
    end
  end

  describe "GET #get_assigned under College department" do
    before :each do
      correspondence = Sate::Crmgt::InternalCorrespondence.create valid_attributes_for_college
      from_user = create(:sate_auth_user)
      source.parent_id = create(:sate_crmgt_department).id
      source.save
      source_departments = Sate::Crmgt::Department.college
      create(:sate_crmgt_correspondence_assignment, from_user_id: from_user.id,
             to_user_id: user.id,
             correspondence_id: correspondence.id,
             correspondence_type: 'Sate::Crmgt::InternalCorrespondence',
             order: 1,
             status: "Assigned",
             application_module_id: application_module.id)
      create(:sate_crmgt_user_department, user_id: user.id, department_id: destination.id)

      assigned = Sate::Crmgt::CorrespondenceAssignment.where(application_module_id: application_module.id,
                                                             to_user_id: user.id,
                                                             correspondence_type: 'Sate::Crmgt::InternalCorrespondence')
                                                      .select("correspondence_id").map(&:correspondence_id)
      @internal_incoming_get = Sate::Crmgt::InternalCorrespondence.where id: assigned,
                                                                         application_module_id: application_module.id,
                                                                         source_id: source_departments
      request.headers.merge!(header)
      get :get_assigned, params: { department_type: "college" }
    end

    it "returns a success response" do
      expect(response).to be_successful
    end

    it "assigns internal correspondence Assigned to the current user as @internal_correspondence" do
      expect(assigns(:internal_correspondences)).to eq(@internal_incoming_get)
    end
  end

  describe "POST #create for Incoming Requests" do
    reference_no = nil
    letter_date = nil
    subject = nil
    search_keywords = []
    source_id = nil
    destination_id = nil
    let!(:reference_no) { reference_no }
    let!(:letter_date) { letter_date }
    let!(:subject) { subject }
    let!(:search_keywords) { search_keywords }
    let!(:source_id) { source_id }
    let!(:destination_id) { destination_id }

    before :each do
      create :sate_crmgt_internal_correspondence, reference_no: "ABC/0001/13",
             letter_date: FFaker::Time.date, subject: "Letter Subject",
             search_keywords: ["Word 1", "Word 2"],
             source_id: source.id, destination_id: destination.id,
             received_date: FFaker::Time.date,
             application_module_id: application_module.id

      create :sate_crmgt_user_department, user_id: user.id, department_id: destination.id

      request.headers.merge!(header)
      post :create, params: { request_type: "incoming",
                              :internal_correspondence => { reference_no: reference_no,
                                                            letter_date: letter_date,
                                                            subject: subject,
                                                            search_keywords: search_keywords,
                                                            source_id: source_id,
                                                            destination_id: destination_id } }
      @decoded_response = JSON(@response.body)
    end
    context "with valid params" do
      let!(:reference_no) { reference_no = FFaker::Name.name }
      let!(:letter_date ){ letter_date = FFaker::Time.date }
      let!(:subject) { subject = FFaker::Name.name }
      let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
      let!(:source_id) { source_id = source.id }
      let!(:destination_id) { destination_id = nil }

      it "creates a new Internal Correspondence" do
        expect(Sate::Crmgt::InternalCorrespondence.count).to eq 2
      end

      it "returns a success message" do
        expect(@response).to be_successful
        expect(@decoded_response["message"]).to eq "Internal Correspondence Saved Successfully!"
      end
    end

    context "with invalid params" do
      context "Blank attributes" do
        context "Reference No" do
          let!(:reference_no) { reference_no = nil }
          let!(:letter_date ){ letter_date = FFaker::Time.date }
          let!(:subject) { subject = FFaker::Name.name }
          let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
          let!(:source_id) { source_id = source.id }
          let!(:destination_id) { destination_id = nil }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Internal Correspondence Reference no can't be blank"]
          end
        end
        context "Letter Date" do
          let!(:reference_no) { reference_no = FFaker::Name.name }
          let!(:letter_date ){ letter_date = nil }
          let!(:subject) { subject = FFaker::Name.name }
          let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
          let!(:source_id) { source_id = source.id }
          let!(:destination_id) { destination_id = nil }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Internal Correspondence Letter date can't be blank"]
          end
        end
        context "Letter Subject" do
          let!(:reference_no) { reference_no = FFaker::Name.name }
          let!(:letter_date ){ letter_date = FFaker::Time.date }
          let!(:subject) { subject = nil }
          let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
          let!(:source_id) { source_id = source.id }
          let!(:destination_id) { destination_id = nil }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Internal Correspondence Subject can't be blank"]
          end
        end
        context "Letter Source Department" do
          let!(:reference_no) { reference_no = FFaker::Name.name }
          let!(:letter_date ){ letter_date = FFaker::Time.date }
          let!(:subject) { subject = FFaker::Name.name }
          let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
          let!(:source_id) { source_id = nil }
          let!(:destination_id) { destination_id = nil }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Internal Correspondence Source must exist", "Internal Correspondence Source can't be blank"]
          end
        end
      end

      context "Duplicate Data" do
        context "Reference No" do
          let!(:reference_no) { reference_no = "ABC/0001/13" }
          let!(:letter_date ){ letter_date = FFaker::Time.date }
          let!(:subject) { subject = FFaker::Name.name }
          let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
          let!(:source_id) { source_id = source.id }
          let!(:destination_id) { destination_id = nil }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Internal Correspondence Reference no has already been taken"]
          end
        end
        context "All Fields" do
          let!(:reference_no) { reference_no = "ABC/0001/13" }
          let!(:letter_date ){ letter_date = Date.strptime("09/28/2020", "%m/%d/%Y") }
          let!(:subject) { subject = "Letter Subject" }
          let!(:search_keywords) { search_keywords = ["Word 1", "Word 2"] }
          let!(:source_id) { source_id = source.id }
          let!(:destination_id) { destination_id = nil }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Internal Correspondence Reference no has already been taken"]
          end
        end
      end
    end

  end

  describe "POST #create for Outgoing Requests" do
    reference_no = nil
    letter_date = nil
    subject = nil
    search_keywords = []
    source_id = nil
    destination_id = nil
    let!(:reference_no) { reference_no }
    let!(:letter_date) { letter_date }
    let!(:subject) { subject }
    let!(:search_keywords) { search_keywords }
    let!(:source_id) { source_id }
    let!(:destination_id) { destination_id }

    before :each do
      create :sate_crmgt_internal_correspondence, reference_no: "ABC/0001/13",
             letter_date: FFaker::Time.date, subject: "Letter Subject",
             search_keywords: ["Word 1", "Word 2"],
             source_id: source.id, destination_id: destination.id,
             received_date: FFaker::Time.date,
             application_module_id: application_module.id

      create :sate_crmgt_user_department, user_id: user.id, department_id: source.id

      request.headers.merge!(header)
      post :create, params: { request_type: "outgoing",
                              :internal_correspondence => { reference_no: reference_no,
                                                            letter_date: letter_date,
                                                            subject: subject,
                                                            search_keywords: search_keywords,
                                                            source_id: source_id,
                                                            destination_id: destination_id } }
      @decoded_response = JSON(@response.body)
    end
    context "with valid params" do
      let!(:reference_no) { reference_no = FFaker::Name.name }
      let!(:letter_date ){ letter_date = FFaker::Time.date }
      let!(:subject) { subject = FFaker::Name.name }
      let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
      let!(:source_id) { source_id = nil }
      let!(:destination_id) { destination_id = destination.id }

      it "creates a new Internal Correspondence" do
        expect(Sate::Crmgt::InternalCorrespondence.count).to eq 2
      end

      it "returns a success message" do
        expect(@response).to be_successful
        expect(@decoded_response["message"]).to eq "Internal Correspondence Saved Successfully!"
      end
    end

    context "with invalid params" do
      context "Blank attributes" do
        context "Reference No" do
          let!(:reference_no) { reference_no = nil }
          let!(:letter_date ){ letter_date = FFaker::Time.date }
          let!(:subject) { subject = FFaker::Name.name }
          let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
          let!(:source_id) { source_id = nil }
          let!(:destination_id) { destination_id = destination.id }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Internal Correspondence Reference no can't be blank"]
          end
        end
        context "Letter Date" do
          let!(:reference_no) { reference_no = FFaker::Name.name }
          let!(:letter_date ){ letter_date = nil }
          let!(:subject) { subject = FFaker::Name.name }
          let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
          let!(:source_id) { source_id = nil }
          let!(:destination_id) { destination_id = destination.id }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Internal Correspondence Letter date can't be blank"]
          end
        end
        context "Letter Subject" do
          let!(:reference_no) { reference_no = FFaker::Name.name }
          let!(:letter_date ){ letter_date = FFaker::Time.date }
          let!(:subject) { subject = nil }
          let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
          let!(:source_id) { source_id = nil }
          let!(:destination_id) { destination_id = destination.id }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Internal Correspondence Subject can't be blank"]
          end
        end
        context "Letter Destination Department" do
          let!(:reference_no) { reference_no = FFaker::Name.name }
          let!(:letter_date ){ letter_date = FFaker::Time.date }
          let!(:subject) { subject = FFaker::Name.name }
          let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
          let!(:source_id) { source_id = nil }
          let!(:destination_id) { destination_id = nil }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Internal Correspondence Destination must exist", "Internal Correspondence Destination can't be blank"]
          end
        end
      end

      context "Duplicate Data" do
        context "Reference No" do
          let!(:reference_no) { reference_no = "ABC/0001/13" }
          let!(:letter_date ){ letter_date = FFaker::Time.date }
          let!(:subject) { subject = FFaker::Name.name }
          let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
          let!(:source_id) { source_id = nil }
          let!(:destination_id) { destination_id = destination.id }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Internal Correspondence Reference no has already been taken"]
          end
        end
        context "All Fields" do
          let!(:reference_no) { reference_no = "ABC/0001/13" }
          let!(:letter_date ){ letter_date = Date.strptime("09/28/2020", "%m/%d/%Y") }
          let!(:subject) { subject = "Letter Subject" }
          let!(:search_keywords) { search_keywords = ["Word 1", "Word 2"] }
          let!(:source_id) { source_id = nil }
          let!(:destination_id) { destination_id = destination.id }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Internal Correspondence Reference no has already been taken"]
          end
        end
      end
    end

  end

  describe "PUT #update" do
    reference_no = nil
    letter_date = nil
    subject = nil
    search_keywords = []
    source_id = nil
    let!(:reference_no) { reference_no }
    let!(:letter_date) { letter_date }
    let!(:subject) { subject }
    let!(:search_keywords) { search_keywords }
    let!(:source_id) { source_id }

    before :each do
      @update = create :sate_crmgt_internal_correspondence, reference_no: "ABC/0001/13",
                       letter_date: FFaker::Time.date, subject: "Letter Subject",
                       search_keywords: ["Word 1", "Word 2"],
                       source_id: source.id, destination_id: destination.id,
                       received_date: FFaker::Time.date,
                       application_module_id: application_module.id

      create :sate_crmgt_user_department, user_id: user.id, department_id: destination.id

      request.headers.merge!(header)
      put :update, params: { id: @update.to_param,
                             internal_correspondence: { reference_no: reference_no,
                                                        letter_date: letter_date,
                                                        subject: subject,
                                                        search_keywords: search_keywords,
                                                        source_id: source_id } }
      @decoded_response = JSON(@response.body)
    end

    context "with valid params" do
      let!(:reference_no) { reference_no = FFaker::Name.name }
      let!(:letter_date ){ letter_date = FFaker::Time.date }
      let!(:subject) { subject = FFaker::Name.name }
      let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
      let!(:source_id) { source_id = create(:sate_crmgt_department).id }

      it "Updates the requested Internal Correspondence" do
        expect(@update.source_id).to_not eq attributes_for(:sate_crmgt_internal_correspondence)[:source_id]
      end

      it "returns a success message" do
        expect(@response).to be_successful
        expect(@decoded_response["message"]).to eq "Internal Correspondence Updated Successfully!"
      end
    end

    context "with invalid params" do
      context "Blank attributes" do
        context "Reference No" do
          let!(:reference_no) { reference_no = nil }
          let!(:letter_date ){ letter_date = FFaker::Time.date }
          let!(:subject) { subject = FFaker::Name.name }
          let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
          let!(:source_id) { source_id = source.id }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Internal Correspondence Reference no can't be blank"]
          end
        end
        context "Letter Date" do
          let!(:reference_no) { reference_no = FFaker::Name.name }
          let!(:letter_date ){ letter_date = nil }
          let!(:subject) { subject = FFaker::Name.name }
          let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
          let!(:source_id) { source_id = source.id }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Internal Correspondence Letter date can't be blank"]
          end
        end
        context "Letter Subject" do
          let!(:reference_no) { reference_no = FFaker::Name.name }
          let!(:letter_date ){ letter_date = FFaker::Time.date }
          let!(:subject) { subject = nil }
          let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
          let!(:source_id) { source_id = source.id }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Internal Correspondence Subject can't be blank"]
          end
        end
        context "Letter Source Department" do
          let!(:reference_no) { reference_no = FFaker::Name.name }
          let!(:letter_date ){ letter_date = FFaker::Time.date }
          let!(:subject) { subject = FFaker::Name.name }
          let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
          let!(:source_id) { source_id = nil }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Internal Correspondence Source must exist", "Internal Correspondence Source can't be blank"]
          end
        end
      end
    end
  end

  describe "PUT #update_status for Acceptance" do
    before :each do
      @update = create :sate_crmgt_internal_correspondence, reference_no: "ABC/0001/13",
                       letter_date: FFaker::Time.date, subject: "Letter Subject",
                       search_keywords: ["Word 1", "Word 2"],
                       source_id: source.id, destination_id: destination.id,
                       received_date: FFaker::Time.date,
                       application_module_id: application_module.id

      create :sate_crmgt_user_department, user_id: user.id, department_id: destination.id

      request.headers.merge!(header)
      put :update_status, params: { id: @update.to_param,
                                    status: "Accepted" }
      @decoded_response = JSON(@response.body)
    end
    it "Updates the requested Internal Correspondence" do
      expect(@update.status).to_not eq attributes_for(:sate_crmgt_internal_correspondence)[:status]
      expect(@update.received_date).to_not eq attributes_for(:sate_crmgt_internal_correspondence)[:received_date]
    end

    it "returns a success message" do
      expect(@response).to be_successful
      expect(@decoded_response["message"]).to eq "Internal Correspondence Status Updated Successfully!"
    end
  end

  describe "PUT #update_status for Rejection" do
    before :each do
      @update = create :sate_crmgt_internal_correspondence, reference_no: "ABC/0001/13",
                       letter_date: FFaker::Time.date, subject: "Letter Subject",
                       search_keywords: ["Word 1", "Word 2"],
                       source_id: source.id, destination_id: destination.id,
                       received_date: FFaker::Time.date,
                       application_module_id: application_module.id

      create :sate_crmgt_user_department, user_id: user.id, department_id: destination.id

      request.headers.merge!(header)
      put :update_status, params: { id: @update.to_param,
                                    status: "Rejected" }
      @decoded_response = JSON(@response.body)
    end
    it "Updates the requested Internal Correspondence" do
      expect(@update.status).to_not eq attributes_for(:sate_crmgt_internal_correspondence)[:status]
      expect(@update.received_date).to_not eq attributes_for(:sate_crmgt_internal_correspondence)[:received_date]
    end

    it "returns a success message" do
      expect(@response).to be_successful
      expect(@decoded_response["message"]).to eq "Internal Correspondence Status Updated Successfully!"
    end
  end

  describe "POST #upload" do
    type = nil
    file = nil
    let!(:type) { type }
    let!(:file) { file }
    before :each do
      Sate::Crmgt::InternalCorrespondence.create valid_attributes
      create(:sate_crmgt_user_department, user_id: user.id, department_id: destination.id)
      dest_dep = Sate::Crmgt::UserDepartment.find_by_user_id user.id
      @internal_incoming = Sate::Crmgt::InternalCorrespondence.where application_module_id: application_module.id,
                                                                     destination_id: dest_dep.department_id
      request.headers.merge!(header)

      @path_to_file = '/home/samuel/Desktop'

      post :upload, params: { id: @internal_incoming.first.to_param,
                              type: type,
                              file: file }
      @decoded_response = JSON(@response.body)
    end

    context "Main Image Upload" do
      context "with valid params" do
        let!(:type) { type = "Main" }
        let!(:file) {
          main = "/home/samuel/Desktop/changes.png"
          file = Rack::Test::UploadedFile.new(main, "image/png") }

        it "should upload the file" do
          expect(ActiveStorage::Attachment.count).to eq(1)
        end

        it "returns a success message" do
          expect(@response).to be_successful
          expect(@decoded_response["message"]).to eq "Image Uploaded Successfully!"
        end
      end

      context "with invalid params" do
        context "with Big File Size" do
          let!(:type) { type = "Main" }
          let!(:file) {
            main = "/home/samuel/Desktop/ancient-town-lake-china.jpg"
            file = Rack::Test::UploadedFile.new(main, "image/jpg") }

          it "should not upload the file" do
            expect(ActiveStorage::Attachment.count).to eq(0)
          end

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"][0]).to eq "Internal Correspondence Main images is too big"
          end
        end

        context "with non image file" do
          let!(:type) { type = "Main" }
          let!(:file) {
            main = "/home/samuel/Desktop/registration_notice.txt"
            file = Rack::Test::UploadedFile.new(main, "text/plain") }

          it "should not upload the file" do
            expect(ActiveStorage::Attachment.count).to eq(0)
          end

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"][0]).to eq "Internal Correspondence Main images must be an image"
          end
        end
      end
    end

    context "Attachment Image Upload" do
      context "with valid params" do
        let!(:type) { type = "Attachment" }
        let!(:file) {
          main = "/home/samuel/Desktop/changes.png"
          file = Rack::Test::UploadedFile.new(main, "image/png") }

        it "should upload the file" do
          expect(ActiveStorage::Attachment.count).to eq(1)
        end

        it "returns a success message" do
          expect(@response).to be_successful
          expect(@decoded_response["message"]).to eq "Image Uploaded Successfully!"
        end
      end

      context "with invalid params" do
        context "with Big File Size" do
          let!(:type) { type = "Attachment" }
          let!(:file) {
            main = "/home/samuel/Desktop/ancient-town-lake-china.jpg"
            file = Rack::Test::UploadedFile.new(main, "image/jpg") }

          it "should not upload the file" do
            expect(ActiveStorage::Attachment.count).to eq(0)
          end

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"][0]).to eq "Internal Correspondence Attachment images is too big"
          end
        end

        context "with non image file" do
          let!(:type) { type = "Attachment" }
          let!(:file) {
            main = "/home/samuel/Desktop/registration_notice.txt"
            file = Rack::Test::UploadedFile.new(main, "text/plain") }

          it "should not upload the file" do
            expect(ActiveStorage::Attachment.count).to eq(0)
          end

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"][0]).to eq "Internal Correspondence Attachment images must be an image"
          end
        end
      end
    end
  end

  describe "POST #change_uploads" do
    type = nil
    file = nil
    let!(:type) { type }
    let!(:file) { file }
    before :each do
      Sate::Crmgt::InternalCorrespondence.create valid_attributes
      create(:sate_crmgt_user_department, user_id: user.id, department_id: destination.id)
      dest_dep = Sate::Crmgt::UserDepartment.find_by_user_id user.id
      @internal_incoming = Sate::Crmgt::InternalCorrespondence.where application_module_id: application_module.id,
                                                                     destination_id: dest_dep.department_id
      request.headers.merge!(header)

      main = '/home/samuel/Desktop/sate_auth_changes.png'
      old_file = Rack::Test::UploadedFile.new(main, "image/png")
      @internal_incoming.first.main_images.attach(old_file)

      post :change_uploads, params: { id: @internal_incoming.first.to_param,
                                      existing_image: @internal_incoming.first.main_images.blobs[0].id,
                                      type: type,
                                      file: file }
      @decoded_response = JSON(@response.body)
    end

    context "Main Image Upload" do
      context "with valid params" do
        let!(:type) { type = "Main" }
        let!(:file) {
          main = "/home/samuel/Desktop/changes.png"
          file = Rack::Test::UploadedFile.new(main, "image/png") }

        it "should upload the file" do
          expect(ActiveStorage::Attachment.count).to eq(1)
        end

        it "returns a success message" do
          expect(@response).to be_successful
          expect(@decoded_response["message"]).to eq "Image Uploaded Successfully!"
        end
      end

      context "with invalid params" do
        context "with Big File Size" do
          let!(:type) { type = "Main" }
          let!(:file) {
            main = "/home/samuel/Desktop/ancient-town-lake-china.jpg"
            file = Rack::Test::UploadedFile.new(main, "image/jpg") }

          it "should not upload the file" do
            expect(ActiveStorage::Attachment.count).to eq(1)
          end

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"][0]).to eq "Internal Correspondence Main images is too big"
          end
        end

        context "with non image file" do
          let!(:type) { type = "Main" }
          let!(:file) {
            main = "/home/samuel/Desktop/registration_notice.txt"
            file = Rack::Test::UploadedFile.new(main, "text/plain") }

          it "should not upload the file" do
            expect(ActiveStorage::Attachment.count).to eq(1)
          end

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"][0]).to eq "Internal Correspondence Main images must be an image"
          end
        end
      end
    end

    context "Attachment Image Upload" do
      context "with valid params" do
        let!(:type) { type = "Attachment" }
        let!(:file) {
          main = "/home/samuel/Desktop/changes.png"
          file = Rack::Test::UploadedFile.new(main, "image/png") }

        it "should upload the file" do
          expect(ActiveStorage::Attachment.count).to eq(1)
        end

        it "returns a success message" do
          expect(@response).to be_successful
          expect(@decoded_response["message"]).to eq "Image Uploaded Successfully!"
        end
      end

      context "with invalid params" do
        context "with Big File Size" do
          let!(:type) { type = "Attachment" }
          let!(:file) {
            main = "/home/samuel/Desktop/ancient-town-lake-china.jpg"
            file = Rack::Test::UploadedFile.new(main, "image/jpg") }

          it "should not upload the file" do
            expect(ActiveStorage::Attachment.count).to eq(1)
          end

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"][0]).to eq "Internal Correspondence Attachment images is too big"
          end
        end

        context "with non image file" do
          let!(:type) { type = "Attachment" }
          let!(:file) {
            main = "/home/samuel/Desktop/registration_notice.txt"
            file = Rack::Test::UploadedFile.new(main, "text/plain") }

          it "should not upload the file" do
            expect(ActiveStorage::Attachment.count).to eq(1)
          end

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"][0]).to eq "Internal Correspondence Attachment images must be an image"
          end
        end
      end
    end
  end

  describe "GET #get_images" do
    before :each do
      Sate::Crmgt::InternalCorrespondence.create valid_attributes
      create(:sate_crmgt_user_department, user_id: user.id, department_id: destination.id)
      dest_dep = Sate::Crmgt::UserDepartment.find_by_user_id user.id
      @internal_incoming = Sate::Crmgt::InternalCorrespondence.where application_module_id: application_module.id,
                                                                     destination_id: dest_dep.department_id
      request.headers.merge!(header)

      main = '/home/samuel/Desktop/sate_auth_changes.png'
      attachment = '/home/samuel/Desktop/changes.png'
      file = Rack::Test::UploadedFile.new(main, "image/png")
      file_att = Rack::Test::UploadedFile.new(attachment, "image/png")
      file_att2 = Rack::Test::UploadedFile.new(attachment, "image/png")
      @internal_incoming.first.main_images.attach(file)
      @internal_incoming.first.attachment_images.attach(file_att)
      @internal_incoming.first.attachment_images.attach(file_att2)

      get :get_images, params: { id: @internal_incoming.first.to_param }
      @decoded_response = JSON(@response.body)
    end

    it "returns a success response" do
      expect(response).to be_successful
    end

    it "assigns one main image to the correspondence" do
      expect(@decoded_response["main_images"].count).to eq(1)
    end

    it "assigns one attachment image to the correspondence" do
      expect(@decoded_response["attachment_images"].count).to eq(2)
    end
  end
end
