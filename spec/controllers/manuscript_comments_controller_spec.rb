require 'rails_helper'

RSpec.describe ManuscriptCommentsController, type: :controller do
  let(:user) { create(:sate_auth_user) }
  let(:manuscript) { create(:sate_crmgt_manuscript) }
  let!(:application_module) { create(:sate_auth_application_module, code: "CRMT") }

  let(:header) { { 'Authorization' => token_generator(user.id) } }

  let(:valid_attributes) {
    {
      commented_by_id: user.id,
      manuscript: manuscript,
      content: FFaker::Name.name,
      comment_date: FFaker::Time.datetime,
      application_module_id: application_module.id
    }
  }

  describe "GET #index" do
    before :each do
      Sate::Crmgt::ManuscriptComment.create valid_attributes

      @manuscript_comment_get = Sate::Crmgt::ManuscriptComment.where application_module_id: application_module.id,
                                                                     manuscript: manuscript
      request.headers.merge!(header)

      get :index, params: { manuscript_id: manuscript.id }
    end

    it "returns a success response" do
      expect(response).to be_successful
    end

    it "assigns manuscript comment of the current user as @manuscript_comment_get" do
      expect(assigns(:manuscript_comments)).to eq(@manuscript_comment_get)
    end
  end

  describe "POST #create" do
    manuscript_id = nil
    content = nil
    let!(:manuscript_id) { manuscript_id }
    let!(:content) { content }

    before :each do
      request.headers.merge!(header)
      post :create, params: { :manuscript_comment => { manuscript_id: manuscript_id,
                                                       content: content } }
      @decoded_response = JSON(@response.body)
    end

    context "with valid params" do
      let!(:manuscript_id) { manuscript_id = manuscript.id }
      let!(:content) { content = "For your action" }

      it "creates a new Manuscript Comment" do
        expect(Sate::Crmgt::ManuscriptComment.count).to eq 1
      end

      it "returns a success message" do
        expect(@response).to be_successful
        expect(@decoded_response["message"]).to eq "Manuscript Comment Saved Successfully!"
      end
    end

    context "with invalid params" do
      context "Blank Manuscript attributes" do
        let!(:manuscript_id) { manuscript_id = nil }
        let!(:content) { content = "For your action" }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["Manuscript Comment Manuscript must exist", "Manuscript Comment Manuscript can't be blank"]
        end
      end
    end
  end

  describe "PUT #update" do
    content = nil
    let!(:content) { content }

    before :each do
      @update = create :sate_crmgt_manuscript_comment, commented_by_id: user.id,
                       content: "For your action",
                       manuscript: manuscript,
                       comment_date: DateTime.now,
                       application_module_id: application_module.id

      request.headers.merge!(header)
      put :update, params: { id: @update.to_param,
                             :manuscript_comment => { content: content } }
      @decoded_response = JSON(@response.body)
    end

    context "with valid params" do
      let!(:content) { content = "For your Information" }

      it "Updates the requested Manuscript Content" do
        expect(@update.content).to_not eq attributes_for(:sate_crmgt_manuscript_comment)[:content]
      end

      it "returns a success message" do
        expect(@response).to be_successful
        expect(@decoded_response["message"]).to eq "Manuscript Comment Updated Successfully!"
      end
    end
  end

end
