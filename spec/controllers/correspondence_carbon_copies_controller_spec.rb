require 'rails_helper'

RSpec.describe CorrespondenceCarbonCopiesController, type: :controller do
  let(:user) { create(:sate_auth_user) }
  let(:internal) { create(:sate_crmgt_internal_correspondence) }
  let(:external) { create(:sate_crmgt_external_incoming_correspondence) }
  let(:department) { create(:sate_crmgt_department) }
  let(:other_department) { create(:sate_crmgt_department) }
  let(:another_department) { create(:sate_crmgt_department) }
  let!(:application_module) { create(:sate_auth_application_module, code: "CRMT") }

  let(:header) { { 'Authorization' => token_generator(user.id) } }

  let(:valid_internal_attributes) {
    {
      correspondence: internal,
      destination_id: department.id,
      application_module_id: application_module.id
    }
  }

  let(:valid_external_attributes) {
    {
      correspondence: external,
      destination_id: department.id,
      application_module_id: application_module.id
    }
  }

  describe "GET #index for Internal Correspondences" do
    before :each do
      Sate::Crmgt::CorrespondenceCarbonCopy.create valid_internal_attributes

      @correspondence_carbon_copy_get = Sate::Crmgt::CorrespondenceCarbonCopy.where application_module_id: application_module.id,
                                                                                    correspondence: internal
      request.headers.merge!(header)

      get :index, params: { correspondence_id: internal.id }
    end

    it "returns a success response" do
      expect(response).to be_successful
    end

    it "returns correspondence carbon copy of the internal correspondence as @correspondence_carbon_copy_get" do
      expect(assigns(:correspondence_carbon_copies)).to eq(@correspondence_carbon_copy_get)
    end
  end

  describe "GET #index for External Correspondences" do
    before :each do
      Sate::Crmgt::CorrespondenceCarbonCopy.create valid_external_attributes

      @correspondence_carbon_copy_get = Sate::Crmgt::CorrespondenceCarbonCopy.where application_module_id: application_module.id,
                                                                                    correspondence: external
      request.headers.merge!(header)

      get :index, params: { correspondence_id: external.id, request_type: "incoming" }
    end

    it "returns a success response" do
      expect(response).to be_successful
    end

    it "returns correspondence carbon copy of the internal correspondence as @correspondence_carbon_copy_get" do
      expect(assigns(:correspondence_carbon_copies)).to eq(@correspondence_carbon_copy_get)
    end
  end

  describe "POST #create for Internal Correspondence Carbon Copy" do
    destination_id = nil
    correspondence_id = nil
    let!(:destination_id) { destination_id }
    let!(:correspondence_id) { correspondence_id }

    before :each do
      create :sate_crmgt_correspondence_carbon_copy, destination_id: other_department.id,
             correspondence: internal,
             application_module_id: application_module.id

      request.headers.merge!(header)
      post :create, params: { :correspondence_carbon_copy => { destination_id: destination_id,
                                                              correspondence_id: correspondence_id } }
      @decoded_response = JSON(@response.body)
    end

    context "with valid params" do
      let!(:destination_id) { destination_id = department.id }
      let!(:correspondence_id) { correspondence_id = internal.id }

      it "creates a new Internal Correspondence Carbon Copy" do
        expect(Sate::Crmgt::CorrespondenceCarbonCopy.count).to eq 2
      end

      it "returns a success message" do
        expect(@response).to be_successful
        expect(@decoded_response["message"]).to eq "Correspondence Cc Saved Successfully!"
      end
    end

    context "with invalid params" do
      context "Blank attributes" do
        context "Destination Department" do
          let!(:destination_id) { destination_id = nil }
          let!(:correspondence_id) { correspondence_id = internal.id }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Correspondence Cc Destination must exist", "Correspondence Cc Destination can't be blank"]
          end
        end
        context "Correspondence" do
          let!(:destination_id) { destination_id = department.id }
          let!(:correspondence_id) { correspondence_id = nil }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Correspondence Cc Correspondence must exist", "Correspondence Cc Correspondence can't be blank"]
          end
        end
      end

      context "Duplicate Data" do
        let!(:destination_id) { destination_id = other_department.id }
        let!(:correspondence_id) { correspondence_id = internal.id }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["Correspondence Cc Correspondence has already been taken", "Correspondence Cc Correspondence type has already been taken", "Correspondence Cc Destination has already been taken", "Correspondence Cc Application module has already been taken"]
        end
      end
    end
  end

  describe "POST #create for External Incoming Correspondence Carbon Copy" do
    destination_id = nil
    correspondence_id = nil
    let!(:destination_id) { destination_id }
    let!(:correspondence_id) { correspondence_id }

    before :each do
      create :sate_crmgt_correspondence_carbon_copy, destination_id: other_department.id,
             correspondence: external,
             application_module_id: application_module.id

      request.headers.merge!(header)
      post :create, params: { request_type: "incoming",
                              :correspondence_carbon_copy => { destination_id: destination_id,
                                                               correspondence_id: correspondence_id } }
      @decoded_response = JSON(@response.body)
    end

    context "with valid params" do
      let!(:destination_id) { destination_id = department.id }
      let!(:correspondence_id) { correspondence_id = external.id }

      it "creates a new Internal Correspondence Carbon Copy" do
        expect(Sate::Crmgt::CorrespondenceCarbonCopy.count).to eq 2
      end

      it "returns a success message" do
        expect(@response).to be_successful
        expect(@decoded_response["message"]).to eq "Correspondence Cc Saved Successfully!"
      end
    end

    context "with invalid params" do
      context "Blank attributes" do
        context "Destination Department" do
          let!(:destination_id) { destination_id = nil }
          let!(:correspondence_id) { correspondence_id = external.id }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Correspondence Cc Destination must exist", "Correspondence Cc Destination can't be blank"]
          end
        end
        context "Correspondence" do
          let!(:destination_id) { destination_id = department.id }
          let!(:correspondence_id) { correspondence_id = nil }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Correspondence Cc Correspondence must exist", "Correspondence Cc Correspondence can't be blank"]
          end
        end
      end

      context "Duplicate Data" do
        let!(:destination_id) { destination_id = other_department.id }
        let!(:correspondence_id) { correspondence_id = external.id }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["Correspondence Cc Correspondence has already been taken", "Correspondence Cc Correspondence type has already been taken", "Correspondence Cc Destination has already been taken", "Correspondence Cc Application module has already been taken"]
        end
      end
    end
  end

  describe "PUT #update" do
    destination_id = nil
    correspondence_id = nil
    let!(:destination_id) { destination_id }
    let!(:correspondence_id) { correspondence_id }

    before :each do
      create :sate_crmgt_correspondence_carbon_copy, destination_id: other_department.id,
             correspondence: internal,
             application_module_id: application_module.id

      @update = create :sate_crmgt_correspondence_carbon_copy, destination_id: department.id,
                       correspondence: internal,
                       application_module_id: application_module.id

      request.headers.merge!(header)
      put :update, params: { id: @update.to_param,
                             :correspondence_carbon_copy => { destination_id: destination_id,
                                                              correspondence_id: correspondence_id } }
      @decoded_response = JSON(@response.body)
    end

    context "with valid params" do
      let!(:destination_id) { destination_id = another_department.id }
      let!(:correspondence_id) { correspondence_id = internal.id }

      it "creates a new Internal Correspondence Carbon Copy" do
        expect(Sate::Crmgt::CorrespondenceCarbonCopy.count).to eq 2
      end

      it "Updates the requested Internal Correspondence Carbon Copy" do
        expect(@update.destination_id).to_not eq attributes_for(:sate_crmgt_correspondence_assignment)[:destination_id]
      end

      it "returns a success message" do
        expect(@response).to be_successful
        expect(@decoded_response["message"]).to eq "Correspondence Cc Updated Successfully!"
      end
    end

    context "with invalid params" do
      context "Blank attributes" do
        context "Destination Department" do
          let!(:destination_id) { destination_id = nil }
          let!(:correspondence_id) { correspondence_id = internal.id }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Correspondence Cc Destination must exist", "Correspondence Cc Destination can't be blank"]
          end
        end
      end

      context "Duplicate Data" do
        let!(:destination_id) { destination_id = other_department.id }
        let!(:correspondence_id) { correspondence_id = internal.id }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["Correspondence Cc Correspondence has already been taken", "Correspondence Cc Correspondence type has already been taken", "Correspondence Cc Destination has already been taken", "Correspondence Cc Application module has already been taken"]
        end
      end
    end
  end

end
