require 'rails_helper'

RSpec.describe UserDepartmentsController, type: :controller do
  let(:user) { create(:sate_auth_user) }
  let!(:application_module) { create(:sate_auth_application_module, code: "CRMT") }
  let!(:department) { create(:sate_crmgt_department) }
  let!(:user_role) { create(:sate_auth_user_role) }
  let(:header) { { 'Authorization' => token_generator(user.id) } }

  let(:valid_attributes) {
    {
        user_id: user.id,
        user_role_id: user_role.id,
        department_id: department.id,
        application_module_id: application_module.id
    }
  }

  let(:invalid_attributes) {
    {
        user_id: user.id,
        user_role_id: nil,
        department_id: department.id,
        application_module_id: application_module.id
    }
  }

  describe "GET #index" do
    before :each do
      Sate::Crmgt::UserDepartment.create valid_attributes
      @user_department_role = Sate::Crmgt::UserDepartment.where application_module_id: application_module.id,
                                                                user_id: user.id
      request.headers.merge!(header)
      get :index, params: { user_id: user.id }
    end

    it "returns a success response" do
      expect(response).to be_successful
    end

    it "assigns department and role of a user as @user_department" do
      # byebug
      expect(assigns(:user_department)).to eq(@user_department_role.first)
    end
  end

  describe "POST #create" do
    user_id = nil
    department_id = nil
    user_role_id = nil
    let!(:user_id) { user_id }
    let!(:department_id) { department_id }
    let!(:user_role_id) { user_role_id }

    before :each do
      create :sate_crmgt_user_department, user_id: user.id,
             department_id: department.id,
             user_role_id: user_role.id,
             application_module_id: application_module.id

      request.headers.merge!(header)
      post :create, params: { user_department: { 'user_id' => user_id,
                                                 'department_id' => department_id,
                                                 'user_role_id' => user_role_id } }
      @decoded_response = JSON(@response.body)
    end
    context "with valid params" do
      let!(:user_id) { user_id = create(:sate_auth_user).id }
      let!(:department_id) { department_id = create(:sate_crmgt_department).id }
      let!(:user_role_id) { user_role_id = create(:sate_auth_user_role).id }

      it "creates a new User Department Role" do
        expect(Sate::Crmgt::UserDepartment.count).to eq 2
      end

      it "returns a success message" do
        expect(@response).to be_successful
        expect(@decoded_response["message"]).to eq "Department and Role Assigned Successfully!"
      end
    end
    context "with invalid params" do
      context "Blank User" do
        let!(:user_id) { user_id = nil }
        let!(:department_id) { department_id = department.id }
        let!(:user_role_id) { user_role_id = user_role.id }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["User Department Role User must exist", "User Department Role User can't be blank"]
        end
      end

      context "Blank Department" do
        let!(:user_id) { user_id = create(:sate_auth_user).id }
        let!(:department_id) { department_id = nil }
        let!(:user_role_id) { user_role_id = user_role.id }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["User Department Role Department must exist", "User Department Role Department can't be blank"]
        end
      end

      context "Blank Role" do
        let!(:user_id) { user_id = create(:sate_auth_user).id }
        let!(:department_id) { department_id = department.id }
        let!(:user_role_id) { user_role_id = nil }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["User Department Role User role must exist", "User Department Role User role can't be blank"]
        end
      end

      context "Duplicate User, Department, and Role" do
        let!(:user_id) { user_id = user.id }
        let!(:department_id) { department_id = department.id }
        let!(:user_role_id) { user_role_id = user_role.id }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["User Department Role User has already been taken", "User Department Role Department has already been taken", "User Department Role User role has already been taken", "User Department Role Application module has already been taken"]
        end
      end
    end
  end

  describe "PUT #update" do
    user_id = nil
    department_id = nil
    user_role_id = nil
    let!(:user_id) { user_id }
    let!(:department_id) { department_id }
    let!(:user_role_id) { user_role_id }

    before :each do
      @user_department_update = create :sate_crmgt_user_department, user_id: user.id,
                                       user_role_id: user_role.id,
                                       department_id: department.id,
                                       application_module_id: application_module.id

      request.headers.merge!(header)
      put :update, params: { id: @user_department_update.to_param,
                             user_department: { 'user_id' => user_id,
                                                'department_id' => department_id,
                                                'user_role_id' => user_role_id } }
      @decoded_response = JSON(@response.body)
    end

    context "with valid params" do
      let!(:user_id) { user_id = user.id }
      let!(:department_id) { department_id = create(:sate_crmgt_department).id }
      let!(:user_role_id) { user_role_id = user_role.id }

      it "updates the requested Department" do
        expect(@user_department_update.department_id).to_not eq attributes_for(:sate_crmgt_user_department)[:department_id]
      end

      it "returns a success message" do
        expect(@response).to be_successful
        expect(@decoded_response["message"]).to eq "Department and Role Updated Successfully!"
      end
    end

    context "with invalid params" do
      context "Blank User" do
        let!(:user_id) { user_id = nil }
        let!(:department_id) { department_id = department.id }
        let!(:user_role_id) { user_role_id = user_role.id }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq  ["User Department Role User must exist", "User Department Role User can't be blank"]
        end
      end

      context "Blank Department" do
        let!(:user_id) { user_id = user.id }
        let!(:department_id) { department_id = nil }
        let!(:user_role_id) { user_role_id = user_role.id }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["User Department Role Department must exist", "User Department Role Department can't be blank"]
        end
      end

      context "Blank Role" do
        let!(:user_id) { user_id = user.id }
        let!(:department_id) { department_id = department.id }
        let!(:user_role_id) { user_role_id = nil }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["User Department Role User role must exist", "User Department Role User role can't be blank"]
        end
      end
    end
  end

end
