require 'rails_helper'
RSpec.describe ManuscriptsController, type: :controller do
  let(:user) { create(:sate_auth_user) }
  let!(:application_module) { create(:sate_auth_application_module, code: "CRMT") }
  let!(:destination) { create(:sate_crmgt_department) }
  let!(:user_role) { create(:sate_auth_user_role) }

  let(:header) { { 'Authorization' => token_generator(user.id) } }

  let(:valid_attributes) {
    {
      reference_no: FFaker::Name.name,
      title: FFaker::Name.name,
      search_keywords: [FFaker::Name.name, FFaker::Name.name],
      destination_id: destination.id,
      received_date: FFaker::Time.datetime,
      application_module_id: application_module.id,
      key_in_by_id: user.id
    }
  }

  describe "GET #index" do
    before :each do
      Sate::Crmgt::Manuscript.create valid_attributes
      dept_role = create(:sate_crmgt_department_user_role, department_id: destination.id, user_role_id: user_role.id)
      create(:sate_crmgt_user_department_role, user_id: user.id, department_user_role_id: dept_role.id)
      @manuscript_get = Sate::Crmgt::Manuscript.where application_module_id: application_module.id,
                                                      destination_id: destination.id
      request.headers.merge!(header)
      get :index
    end

    it "returns a success response" do
      expect(response).to be_successful
    end

    it "assigns manuscript as @manuscript" do
      expect(assigns(:manuscripts)).to eq(@manuscript_get)
    end
  end

  describe "POST #create" do
    reference_no = nil
    title = nil
    search_keywords = []
    let!(:reference_no) { reference_no }
    let!(:title) { title }
    let!(:search_keywords) { search_keywords }

    before :each do
      create :sate_crmgt_manuscript, reference_no: "ABC/0001/13",
             received_date: FFaker::Time.date, title: "Manuscript Title",
             search_keywords: ["Word 1", "Word 2"],
             destination_id: destination.id, key_in_by_id: user.id,
             application_module_id: application_module.id

      dept_role = create(:sate_crmgt_department_user_role, department_id: destination.id, user_role_id: user_role.id)
      create(:sate_crmgt_user_department_role, user_id: user.id, department_user_role_id: dept_role.id)

      request.headers.merge!(header)
      post :create, params: { :manuscript => { reference_no: reference_no,
                                               title: title,
                                               search_keywords: search_keywords } }
      @decoded_response = JSON(@response.body)
    end

    context "with valid params" do
      let!(:reference_no) { reference_no = FFaker::Name.name }
      let!(:title) { title = FFaker::Name.name }
      let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
      it "creates a new Manuscript" do
        expect(Sate::Crmgt::Manuscript.count).to eq 2
      end

      it "returns a success message" do
        expect(@response).to be_successful
        expect(@decoded_response["message"]).to eq "Manuscript Saved Successfully!"
      end
    end

    context "with invalid params" do
      context "Blank attributes" do
        context "Manuscript Reference No" do
          let!(:reference_no) { reference_no = nil }
          let!(:title) { title = FFaker::Name.name }
          let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Manuscript Reference no can't be blank"]
          end
        end
        context "Manuscript Title" do
          let!(:reference_no) { reference_no = FFaker::Name.name }
          let!(:title) { title = nil }
          let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Manuscript Title can't be blank"]
          end
        end
      end

      context "Duplicate Data" do
        context "Manuscript Reference No" do
          let!(:reference_no) { reference_no = "ABC/0001/13" }
          let!(:title) { title = FFaker::Name.name }
          let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Manuscript Reference no has already been taken"]
          end
        end
        context "Manuscript Title" do
          let!(:reference_no) { reference_no = FFaker::Name.name }
          let!(:title) { title = "Manuscript Title" }
          let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Manuscript Title has already been taken"]
          end
        end
        context "All Fields" do
          let!(:reference_no) { reference_no = "ABC/0001/13" }
          let!(:title) { title = "Manuscript Title" }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Manuscript Reference no has already been taken", "Manuscript Title has already been taken", "Manuscript Destination has already been taken", "Manuscript Key in by has already been taken", "Manuscript Application module has already been taken"]
          end
        end
      end
    end
  end

  describe "PUT #update" do
    title = nil
    search_keywords = []
    let!(:title) { title }
    let!(:search_keywords) { search_keywords }

    before :each do
      @update = create :sate_crmgt_manuscript, reference_no: "ABC/0001/13",
                       received_date: FFaker::Time.date, title: "Manuscript Title",
                       search_keywords: ["Word 1", "Word 2"],
                       destination_id: destination.id, key_in_by_id: user.id,
                       application_module_id: application_module.id

      create :sate_crmgt_manuscript, reference_no: "ABC/0002/13",
             received_date: FFaker::Time.date, title: "Manuscript Second Title",
             search_keywords: ["Word 1", "Word 2"],
             destination_id: destination.id, key_in_by_id: user.id,
             application_module_id: application_module.id

      dept_role = create(:sate_crmgt_department_user_role, department_id: destination.id, user_role_id: user_role.id)
      create(:sate_crmgt_user_department_role, user_id: user.id, department_user_role_id: dept_role.id)

      request.headers.merge!(header)
      put :update, params: { id: @update.to_param,
                             manuscript: { title: title,
                                           search_keywords: search_keywords } }
      @decoded_response = JSON(@response.body)
    end

    context "with valid params" do
      let!(:title) { title = FFaker::Name.name }
      let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }
      it "Updates the requested Manuscript" do
        expect(@update.title).to_not eq attributes_for(:sate_crmgt_manuscript)[:title]
        expect(@update.search_keywords).to_not eq attributes_for(:sate_crmgt_manuscript)[:search_keywords]
      end

      it "returns a success message" do
        expect(@response).to be_successful
        expect(@decoded_response["message"]).to eq "Manuscript Updated Successfully!"
      end
    end

    context "with invalid params" do
      context "Blank attributes" do
        context "Manuscript Title" do
          let!(:title) { title = nil }
          let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Manuscript Title can't be blank"]
          end
        end
      end

      context "Duplicate Data" do
        context "Manuscript Title" do
          let!(:title) { title = "Manuscript Second Title" }
          let!(:search_keywords) { search_keywords = [FFaker::Name.name, FFaker::Name.name] }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Manuscript Title has already been taken"]
          end
        end
      end
    end
  end
end
