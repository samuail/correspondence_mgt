require 'rails_helper'

RSpec.describe UserMenuActionsController, type: :controller do
  let(:user) { create(:sate_auth_user) }
  let!(:application_module) { create(:sate_auth_application_module, code: "CRMT") }
  let(:header) { { 'Authorization' => token_generator(user.id) } }

  describe "GET #get_menu_action" do
    before :each do
      @parent = Sate::Auth::Menu.create text: "Correspondence", parent_id: nil, application_module_id: application_module.id
      @menu1 = Sate::Auth::Menu.create text: "Incoming", parent_id: @parent.id, application_module_id: application_module.id
      @menu2 = Sate::Auth::Menu.create text: "Outgoing", parent_id: @parent.id, application_module_id: application_module.id
      @action1 = Sate::Crmgt::Action.create code: "ADD", name: "Add", application_module_id: application_module.id
      @action2 = Sate::Crmgt::Action.create code: "EDT", name: "Edit", application_module_id: application_module.id
      Sate::Crmgt::MenuAction.create menu_id: @menu1.id, action_id: @action1.id, application_module_id: application_module.id
      Sate::Crmgt::MenuAction.create menu_id: @menu1.id, action_id: @action2.id, application_module_id: application_module.id
      Sate::Crmgt::MenuAction.create menu_id: @menu2.id, action_id: @action1.id, application_module_id: application_module.id
      Sate::Crmgt::MenuAction.create menu_id: @menu2.id, action_id: @action2.id, application_module_id: application_module.id

      @parent1 = Sate::Auth::Menu.create text: "Business Settings", parent_id: nil, application_module_id: application_module.id
      @menu21 = Sate::Auth::Menu.create text: "Users", parent_id: @parent1.id, application_module_id: application_module.id
      @menu22 = Sate::Auth::Menu.create text: "Menus", parent_id: @parent1.id, application_module_id: application_module.id
      Sate::Crmgt::MenuAction.create menu_id: @menu21.id, action_id: @action1.id, application_module_id: application_module.id
      Sate::Crmgt::MenuAction.create menu_id: @menu21.id, action_id: @action2.id, application_module_id: application_module.id
      Sate::Crmgt::MenuAction.create menu_id: @menu22.id, action_id: @action1.id, application_module_id: application_module.id
      Sate::Crmgt::MenuAction.create menu_id: @menu22.id, action_id: @action2.id, application_module_id: application_module.id

      @menu_actions = Sate::Crmgt::MenuAction.where menu_id: [@menu1.id, @menu2.id, @menu21.id, @menu22.id],
                                                   application_module_id: application_module.id

      request.headers.merge!(header)
      get :get_menu_action, params: { menu_names: [@menu1.text, @menu2.text, @menu21.text, @menu22.text] }

      @decoded_response = JSON(@response.body)
    end

    it 'fetch actions assigned to a menu' do
      expect(assigns(:menu_actions)).to eq(@menu_actions)
    end

    it "returns a success response" do
      expect(response).to be_successful
    end

    it "return tree of menu actions" do
      expect(@decoded_response["data"][0]["parent_text"]).to eq @parent.text
      expect(@decoded_response["data"][0]["menus"][0]["menu_text"]).to eq @menu1.text
      expect(@decoded_response["data"][0]["menus"][0]["actions"][0]["action_name"]).to eq @action1.name
    end

  end

  describe "GET #get_assigned_menu_action" do
    before :each do
      @new_user = Sate::Auth::User.create first_name: "Sample", last_name: "User",
                                      email: "sample@abc.com", password: "123456",
                                      application_module_id: application_module.id
      @parent = Sate::Auth::Menu.create text: "Correspondence", parent_id: nil, application_module_id: application_module.id
      @menu1 = Sate::Auth::Menu.create text: "Incoming", parent_id: @parent.id, application_module_id: application_module.id
      @menu2 = Sate::Auth::Menu.create text: "Outgoing", parent_id: @parent.id, application_module_id: application_module.id
      @action1 = Sate::Crmgt::Action.create code: "ADD", name: "Add", application_module_id: application_module.id
      @action2 = Sate::Crmgt::Action.create code: "EDT", name: "Edit", application_module_id: application_module.id

      @new_user.menus << @parent
      @new_user.menus << @parent.children

      @incoming_add = Sate::Crmgt::MenuAction.create menu_id: @menu1.id, action_id: @action1.id, application_module_id: application_module.id
      @incoming_edit = Sate::Crmgt::MenuAction.create menu_id: @menu1.id, action_id: @action2.id, application_module_id: application_module.id
      @outgoing_add = Sate::Crmgt::MenuAction.create menu_id: @menu2.id, action_id: @action1.id, application_module_id: application_module.id
      @outgoing_edit = Sate::Crmgt::MenuAction.create menu_id: @menu2.id, action_id: @action2.id, application_module_id: application_module.id

      Sate::Crmgt::UserMenuAction.create user_id: @new_user.id, menu_action_id: @incoming_add.id, application_module_id: application_module.id
      Sate::Crmgt::UserMenuAction.create user_id: @new_user.id, menu_action_id: @incoming_edit.id, application_module_id: application_module.id
      Sate::Crmgt::UserMenuAction.create user_id: @new_user.id, menu_action_id: @outgoing_add.id, application_module_id: application_module.id
      Sate::Crmgt::UserMenuAction.create user_id: @new_user.id, menu_action_id: @outgoing_edit.id, application_module_id: application_module.id

      @user_menu_actions = Sate::Crmgt::UserMenuAction.where user_id: @new_user.id,
                                                             application_module_id: application_module.id

      request.headers.merge!(header)
      get :get_assigned_menu_action, params: { user_id: @new_user.id }

      @decoded_response = JSON(@response.body)
    end

    it 'fetch actions assigned to a menu' do
      expect(assigns(:menu_actions)).to eq(@user_menu_actions)
    end

    it "returns a success response" do
      expect(response).to be_successful
    end

    it "return tree of menu actions" do
      expect(@decoded_response["data"][0]["parent_text"]).to eq @parent.text
      expect(@decoded_response["data"][0]["menus"][0]["menu_text"]).to eq @menu1.text
      expect(@decoded_response["data"][0]["menus"][0]["actions"][0]["action_name"]).to eq @action1.name
    end
  end

  describe "POST #assign_menu_action" do
    before :each do
      @new_user = Sate::Auth::User.create first_name: "Sample", last_name: "User",
                                          email: "sample@abc.com", password: "123456",
                                          application_module_id: application_module.id
      @parent = Sate::Auth::Menu.create text: "Correspondence", parent_id: nil, application_module_id: application_module.id
      @menu1 = Sate::Auth::Menu.create text: "Incoming", parent_id: @parent.id, application_module_id: application_module.id
      @menu2 = Sate::Auth::Menu.create text: "Outgoing", parent_id: @parent.id, application_module_id: application_module.id
      @action1 = Sate::Crmgt::Action.create code: "ADD", name: "Add", application_module_id: application_module.id
      @action2 = Sate::Crmgt::Action.create code: "EDT", name: "Edit", application_module_id: application_module.id

      @new_user.menus << @parent
      @new_user.menus << @parent.children

      @incoming_add = Sate::Crmgt::MenuAction.create menu_id: @menu1.id, action_id: @action1.id, application_module_id: application_module.id
      @incoming_edit = Sate::Crmgt::MenuAction.create menu_id: @menu1.id, action_id: @action2.id, application_module_id: application_module.id
      @outgoing_add = Sate::Crmgt::MenuAction.create menu_id: @menu2.id, action_id: @action1.id, application_module_id: application_module.id
      @outgoing_edit = Sate::Crmgt::MenuAction.create menu_id: @menu2.id, action_id: @action2.id, application_module_id: application_module.id

      request.headers.merge!(header)
      post :assign_menu_action, params: { user_id: @new_user.id, menu_action_ids: [@incoming_add.id, @incoming_edit.id,
                                                                                   @outgoing_add.id, @outgoing_edit.id] }

      @decoded_response = JSON(@response.body)
    end

    it 'assigns menu actions to the User' do
      count = Sate::Crmgt::UserMenuAction.where(user_id: @new_user.id).count
      expect(count).to eq 4
    end

    it "returns a success response" do
      expect(response).to be_successful
    end

    it "return a success message" do
      expect(@decoded_response["message"]).to eq "Menu Action Assigned Successfully!"
      expect(@decoded_response["data"][0]["menu_action_id"]).to eq @incoming_add.id
      expect(@decoded_response["data"][1]["menu_action_id"]).to eq @incoming_edit.id
      expect(@decoded_response["data"][2]["menu_action_id"]).to eq @outgoing_add.id
      expect(@decoded_response["data"][3]["menu_action_id"]).to eq @outgoing_edit.id
    end
  end

end
