require 'rails_helper'

RSpec.describe ManuscriptAssignmentsController, type: :controller do
  let(:user) { create(:sate_auth_user) }
  let(:to_user) { create(:sate_auth_user) }
  let(:to_author) { create(:sate_crmgt_author) }
  let(:to_assessor) { create(:sate_crmgt_external_assessor) }
  let(:manuscript) { create(:sate_crmgt_manuscript) }
  let!(:application_module) { create(:sate_auth_application_module, code: "CRMT") }

  let(:header) { { 'Authorization' => token_generator(user.id) } }

  let(:valid_internal_attributes) {
    {
      stage: "Preliminary Assessment",
      from_user_id:  user.id,
      assignee_id: to_user.id,
      assignee_type: 'Sate::Auth::User',
      manuscript: manuscript,
      status: FFaker::Name.name,
      assigned_date: FFaker::Time.datetime,
      received_date: FFaker::Time.datetime,
      application_module_id:  application_module.id
    }
  }

  let(:valid_author_attributes) {
    {
      stage: "Preliminary Assessment",
      from_user_id:  user.id,
      assignee_id: to_author.id,
      assignee_type: 'Sate::Crmgt::Author',
      manuscript: manuscript,
      status: FFaker::Name.name,
      assigned_date: FFaker::Time.datetime,
      received_date: FFaker::Time.datetime,
      application_module_id:  application_module.id
    }
  }

  let(:valid_assessor_attributes) {
    {
      stage: "Preliminary Assessment",
      from_user_id:  user.id,
      assignee_id: to_assessor.id,
      assignee_type: 'Sate::Crmgt::ExternalAssessor',
      manuscript: manuscript,
      status: FFaker::Name.name,
      assigned_date: FFaker::Time.datetime,
      received_date: FFaker::Time.datetime,
      application_module_id:  application_module.id
    }
  }

  describe "GET #index" do
    before :each do
      Sate::Crmgt::ManuscriptAssignment.create valid_internal_attributes
      Sate::Crmgt::ManuscriptAssignment.create valid_author_attributes
      Sate::Crmgt::ManuscriptAssignment.create valid_assessor_attributes
      @manuscript_assignment_get = Sate::Crmgt::ManuscriptAssignment.where(application_module_id: application_module.id,
                                                                           manuscript: manuscript).order(:assigned_date)
      request.headers.merge!(header)

      get :index, params: { manuscript_id: manuscript.id }
    end
    it "returns a success response" do
      expect(response).to be_successful
    end

    it "assigns manuscript assignment as @manuscript_assignments" do
      expect(assigns(:manuscript_assignments)).to eq(@manuscript_assignment_get)
    end
  end

  describe "POST #create" do
    stage = nil
    assignee_id = nil
    assignee_type = nil
    manuscript_id = nil
    let!(:stage) { stage }
    let!(:assignee_id) { assignee_id }
    let!(:assignee_type) { assignee_type }
    let!(:manuscript_id) { manuscript_id }

    before :each do
      Sate::Crmgt::ManuscriptAssignment.create valid_internal_attributes

      request.headers.merge!(header)
      post :create , params: { assignee_type: assignee_type,
                               :manuscript_assignment => { stage: stage,
                                                           assignee_id: assignee_id,
                                                           manuscript_id: manuscript_id } }
      @decoded_response = JSON(@response.body)
    end

    context "with valid params" do
      let!(:stage) {  stage = "Editorial" }
      let!(:assignee_id) { assignee_id = to_user.id }
      let!(:assignee_type) { assignee_type = "User" }
      let!(:manuscript_id) { manuscript_id = manuscript.id }

      it "creates a new Manuscript Assignment for Internal User" do
        expect(Sate::Crmgt::ManuscriptAssignment.count).to eq 2
      end

      it "returns a success message" do
        expect(@response).to be_successful
        expect(@decoded_response["message"]).to eq "Manuscript Assignment Saved Successfully!"
      end
    end

    context "with invalid params" do
      context "Blank Attributes" do
        context "Stage" do
          let!(:stage) {  stage = nil }
          let!(:assignee_id) { assignee_id = to_user.id }
          let!(:assignee_type) { assignee_type = "User" }
          let!(:manuscript_id) { manuscript_id = manuscript.id }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Manuscript Assignment Stage can't be blank"]
          end
        end
        context "Manuscript" do
          let!(:stage) {  stage = "Editorial" }
          let!(:assignee_id) { assignee_id = to_user.id }
          let!(:assignee_type) { assignee_type = "User" }
          let!(:manuscript_id) { manuscript_id = nil }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Manuscript Assignment Manuscript must exist", "Manuscript Assignment Manuscript can't be blank"]
          end
        end
      end
    end
  end

  describe "PUT #update" do
    stage = nil
    assignee_id = nil
    assignee_type = nil
    manuscript_id = nil
    let!(:stage) { stage }
    let!(:assignee_id) { assignee_id }
    let!(:assignee_type) { assignee_type }
    let!(:manuscript_id) { manuscript_id }

    before :each do
      @update = Sate::Crmgt::ManuscriptAssignment.create valid_internal_attributes

      request.headers.merge!(header)
      put :update , params: { id: @update.id,
                              assignee_type: assignee_type,
                              :manuscript_assignment => { stage: stage,
                                                          assignee_id: assignee_id,
                                                          manuscript_id: manuscript_id } }
      @decoded_response = JSON(@response.body)
    end

    context "with valid params" do
      let!(:stage) {  stage = "Editorial" }
      let!(:assignee_id) { assignee_id = to_user.id }
      let!(:assignee_type) { assignee_type = "User" }
      let!(:manuscript_id) { manuscript_id = manuscript.id }

      it "Updates the requested Manuscript Assignment" do
        expect(@update.stage).to_not eq attributes_for(:sate_crmgt_manuscript_assignment)[:stage]
      end

      it "returns a success message" do
        expect(@response).to be_successful
        expect(@decoded_response["message"]).to eq "Manuscript Assignment Updated Successfully!"
      end
    end

    context "with invalid params" do
      context "Blank Attributes" do
        context "Stage" do
          let!(:stage) {  stage = nil }
          let!(:assignee_id) { assignee_id = to_user.id }
          let!(:assignee_type) { assignee_type = "User" }
          let!(:manuscript_id) { manuscript_id = manuscript.id }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Manuscript Assignment Stage can't be blank"]
          end
        end
        context "Manuscript" do
          let!(:stage) {  stage = "Editorial" }
          let!(:assignee_id) { assignee_id = to_user.id }
          let!(:assignee_type) { assignee_type = "User" }
          let!(:manuscript_id) { manuscript_id = nil }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Manuscript Assignment Manuscript must exist", "Manuscript Assignment Manuscript can't be blank"]
          end
        end
      end
    end
  end

end
