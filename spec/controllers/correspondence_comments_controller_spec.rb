require 'rails_helper'

RSpec.describe CorrespondenceCommentsController, type: :controller do
  let(:user) { create(:sate_auth_user) }
  let(:internal) { create(:sate_crmgt_internal_correspondence) }
  let(:external) { create(:sate_crmgt_external_incoming_correspondence) }
  let!(:application_module) { create(:sate_auth_application_module, code: "CRMT") }

  let(:header) { { 'Authorization' => token_generator(user.id) } }

  let(:valid_internal_attributes) {
    {
        commented_by_id: user.id,
        correspondence: internal,
        order: 1,
        content: FFaker::Name.name,
        comment_date: FFaker::Time.datetime,
        application_module_id: application_module.id
    }
  }

  let(:valid_external_attributes) {
    {
        commented_by_id: user.id,
        correspondence: external,
        order: 1,
        content: FFaker::Name.name,
        comment_date: FFaker::Time.datetime,
        application_module_id: application_module.id
    }
  }

  describe "GET #index for Internal Correspondence Comment" do
    before :each do
      Sate::Crmgt::CorrespondenceComment.create valid_internal_attributes

      @internal_comment_get = Sate::Crmgt::CorrespondenceComment.where(application_module_id: application_module.id,
                                                                       correspondence: internal).order(:order)
      request.headers.merge!(header)

      get :index, params: { correspondence_id: internal.id }
    end

    it "returns a success response" do
      expect(response).to be_successful
    end

    it "assigns internal correspondence comment of the current user as @correspondence_comments" do
      expect(assigns(:correspondence_comments)).to eq(@internal_comment_get)
    end
  end

  describe "GET #index for External Incoming Correspondence Comment" do
    before :each do
      Sate::Crmgt::CorrespondenceComment.create valid_external_attributes

      @external_comment_get = Sate::Crmgt::CorrespondenceComment.where(application_module_id: application_module.id,
                                                                       correspondence: external).order(:order)
      request.headers.merge!(header)

      get :index, params: { correspondence_id: external.id, request_type: "incoming" }
    end

    it "returns a success response" do
      expect(response).to be_successful
    end

    it "assigns internal correspondence comment of the current user as @correspondence_comments" do
      expect(assigns(:correspondence_comments)).to eq(@external_comment_get)
    end
  end

  describe "POST #create for Internal Correspondence Comment" do
    correspondence_id = nil
    content = nil
    let!(:correspondence_id) { correspondence_id }
    let!(:content) { content }

    before :each do
      request.headers.merge!(header)
      post :create, params: { :correspondence_comment => { correspondence_id: correspondence_id,
                                                           content: content } }
      @decoded_response = JSON(@response.body)
    end

    context "with valid params" do
      let!(:correspondence_id) { correspondence_id = internal.id }
      let!(:content) { content = "For your action" }

      it "creates a new Internal Correspondence Comment" do
        expect(Sate::Crmgt::CorrespondenceComment.count).to eq 1
      end

      it "returns a success message" do
        expect(@response).to be_successful
        expect(@decoded_response["message"]).to eq "Correspondence Comment Saved Successfully!"
      end
    end

    context "with invalid params" do
      context "Blank attributes" do
        context "Correspondence" do
          let!(:correspondence_id) { correspondence_id = nil }
          let!(:content) { content = "For your action" }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Correspondence Comment Correspondence must exist", "Correspondence Comment Correspondence can't be blank"]
          end
        end

        context "To User" do
          let!(:correspondence_id) { correspondence_id = internal.id }
          let!(:content) { content = nil }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Correspondence Comment Content can't be blank"]
          end
        end
      end
    end

  end

  describe "POST #create for External Incoming Correspondence Comment" do
    correspondence_id = nil
    content = nil
    let!(:correspondence_id) { correspondence_id }
    let!(:content) { content }

    before :each do
      request.headers.merge!(header)
      post :create, params: { request_type: "incoming",
                              :correspondence_comment => { correspondence_id: correspondence_id,
                                                           content: content } }
      @decoded_response = JSON(@response.body)
    end

    context "with valid params" do
      let!(:correspondence_id) { correspondence_id = external.id }
      let!(:content) { content = "For your action" }

      it "creates a new Internal Correspondence Comment" do
        expect(Sate::Crmgt::CorrespondenceComment.count).to eq 1
      end

      it "returns a success message" do
        expect(@response).to be_successful
        expect(@decoded_response["message"]).to eq "Correspondence Comment Saved Successfully!"
      end
    end

    context "with invalid params" do
      context "Blank attributes" do
        context "Correspondence" do
          let!(:correspondence_id) { correspondence_id = nil }
          let!(:content) { content = "For your action" }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Correspondence Comment Correspondence must exist", "Correspondence Comment Correspondence can't be blank"]
          end
        end

        context "To User" do
          let!(:correspondence_id) { correspondence_id = external.id }
          let!(:content) { content = nil }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Correspondence Comment Content can't be blank"]
          end
        end
      end
    end

  end

  describe "POST #create for Adding another Comment" do
    correspondence_id = nil
    content = nil
    let!(:correspondence_id) { correspondence_id }
    let!(:content) { content }

    before :each do
      create(:sate_crmgt_correspondence_comment, correspondence_id: internal.id,
                                                  correspondence_type: 'Sate::Crmgt::InternalCorrespondence',
                                                  order: 1)
      request.headers.merge!(header)
      post :create, params: { :correspondence_comment => { correspondence_id: correspondence_id,
                                                           content: content } }
      @decoded_response = JSON(@response.body)
    end

    context "with valid params" do
      let!(:correspondence_id) { correspondence_id = internal.id }
      let!(:content) { content = "For your action" }

      it "creates a new Internal Correspondence Comment" do
        expect(Sate::Crmgt::CorrespondenceComment.count).to eq 2
      end

      it "returns a success message" do
        expect(@response).to be_successful
        expect(@decoded_response["message"]).to eq "Correspondence Comment Saved Successfully!"
        expect(@decoded_response["data"]["order"]).to eq 2
      end
    end

    context "with invalid params" do
      context "Blank attributes" do
        context "Correspondence" do
          let!(:correspondence_id) { correspondence_id = nil }
          let!(:content) { content = "For your action" }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Correspondence Comment Correspondence must exist", "Correspondence Comment Correspondence can't be blank"]
          end
        end

        context "To User" do
          let!(:correspondence_id) { correspondence_id = internal.id }
          let!(:content) { content = nil }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Correspondence Comment Content can't be blank"]
          end
        end
      end
    end

  end

  describe "PUT #update Internal Correspondence Comment" do
    content = nil
    let!(:content) { content }

    before :each do
      @update = create :sate_crmgt_correspondence_comment, commented_by_id: user.id,
                       content: "For your action",
                       correspondence: internal,
                       application_module_id: application_module.id

      request.headers.merge!(header)
      put :update, params: { id: @update.to_param,
                             :correspondence_comment => { content: content } }
      @decoded_response = JSON(@response.body)
    end

    context "with valid params" do
      let!(:content) { content = "For your Information" }

      it "Updates the requested Internal Correspondence Assignment" do
        expect(@update.content).to_not eq attributes_for(:sate_crmgt_correspondence_comment)[:content]
      end

      it "returns a success message" do
        expect(@response).to be_successful
        expect(@decoded_response["message"]).to eq "Correspondence Comment Updated Successfully!"
      end
    end

    context "with invalid params" do
      context "Blank Content attributes" do
        let!(:content) { content = nil }
        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["Correspondence Comment Content can't be blank"]
        end
      end
    end

  end

  describe "PUT #update External Incoming Correspondence Comment" do
    content = nil
    let!(:content) { content }

    before :each do
      @update = create :sate_crmgt_correspondence_comment, commented_by_id: user.id,
                       content: "For your action",
                       correspondence: external,
                       application_module_id: application_module.id

      request.headers.merge!(header)
      put :update, params: { id: @update.to_param,
                             :correspondence_comment => { content: content } }
      @decoded_response = JSON(@response.body)
    end

    context "with valid params" do
      let!(:content) { content = "For your Information" }

      it "Updates the requested Internal Correspondence Assignment" do
        expect(@update.content).to_not eq attributes_for(:sate_crmgt_correspondence_comment)[:content]
      end

      it "returns a success message" do
        expect(@response).to be_successful
        expect(@decoded_response["message"]).to eq "Correspondence Comment Updated Successfully!"
      end
    end

    context "with invalid params" do
      context "Blank Content attributes" do
        let!(:content) { content = nil }
        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["Correspondence Comment Content can't be blank"]
        end
      end
    end

  end
end
