require "rails_helper"

RSpec.describe DepartmentsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/crmgt/business_settings/department").to route_to("departments#index")
    end

    it "routes to #fetch_departments" do
      expect(:get => "/crmgt/business_settings/department_tree").to route_to("departments#fetch_departments")
    end

    it "routes to #get_department_by_type" do
      expect(:get => "/crmgt/business_settings/department_by_type").to route_to("departments#get_department_by_type")
    end

    it "routes to #create" do
      expect(:post => "/crmgt/business_settings/department").to route_to("departments#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/crmgt/business_settings/department").to route_to("departments#update")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/crmgt/business_settings/department").to route_to("departments#update")
    end

    it "routes to #get_users" do
      expect(:get => "/crmgt/business_settings/department/1/users").to route_to("departments#get_users", :dept_id => "1")
    end

  end
end
