require "rails_helper"

RSpec.describe ExternalCorrespondencesController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/crmgt/correspondence/external").to route_to("external_correspondences#index")
    end

    it "routes to #get_assigned" do
      expect(:get => "/crmgt/correspondence/external/assigned").to route_to("external_correspondences#get_assigned")
    end

    it "routes to #create" do
      expect(:post => "/crmgt/correspondence/external").to route_to("external_correspondences#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/crmgt/correspondence/external").to route_to("external_correspondences#update")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/crmgt/correspondence/external").to route_to("external_correspondences#update")
    end

    it "routes to #update_status via PUT" do
      expect(:put => "/crmgt/correspondence/external/update_status").to route_to("external_correspondences#update_status")
    end

    it "routes to #upload" do
      expect(:post => "/crmgt/correspondence/external/upload").to route_to("external_correspondences#upload")
    end

    it "routes to #change_uploads" do
      expect(:post => "/crmgt/correspondence/external/change_uploads").to route_to("external_correspondences#change_uploads")
    end

    it "routes to #get_images" do
      expect(:get => "/crmgt/correspondence/external/get_images").to route_to("external_correspondences#get_images")
    end
  end
end
