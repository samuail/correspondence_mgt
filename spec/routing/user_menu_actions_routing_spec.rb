require "rails_helper"

RSpec.describe UserMenuActionsController, type: :routing do
  describe "routing" do
    it "routes to #get_menu_action" do
      expect(:get => "/crmgt/auth/menu/actions").to route_to("user_menu_actions#get_menu_action")
    end

    it "routes to #get_assigned_menu_action" do
      expect(:get => "/crmgt/auth/user/menu/actions").to route_to("user_menu_actions#get_assigned_menu_action")
    end


    it "routes to #assign_menu_action" do
      expect(:post => "/crmgt/auth/user/menu/actions").to route_to("user_menu_actions#assign_menu_action")
    end

  end
end
