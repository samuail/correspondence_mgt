require "rails_helper"

RSpec.describe RoyaltiesController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/crmgt/business_settings/author/royalty").to route_to("royalties#index")
    end

    it "routes to #create" do
      expect(:post => "/crmgt/business_settings/author/royalty").to route_to("royalties#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/crmgt/business_settings/author/royalty").to route_to("royalties#update")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/crmgt/business_settings/author/royalty").to route_to("royalties#update")
    end
  end
end
