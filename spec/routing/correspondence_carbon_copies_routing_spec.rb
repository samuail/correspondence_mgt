require "rails_helper"

RSpec.describe CorrespondenceCarbonCopiesController, type: :routing do
  describe "Internal Correspondence Carbon Copy" do
    it "routes to #index" do
      expect(:get => "/crmgt/correspondence/internal/ccs").to route_to("correspondence_carbon_copies#index")
    end

    it "routes to #create" do
      expect(:post => "/crmgt/correspondence/internal/ccs").to route_to("correspondence_carbon_copies#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/crmgt/correspondence/internal/ccs").to route_to("correspondence_carbon_copies#update")
    end
  end

  describe "External Incoming Correspondence Carbon Copy" do
    it "routes to #index" do
      expect(:get => "/crmgt/correspondence/external/ccs").to route_to("correspondence_carbon_copies#index")
    end

    it "routes to #create" do
      expect(:post => "/crmgt/correspondence/external/ccs").to route_to("correspondence_carbon_copies#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/crmgt/correspondence/external/ccs").to route_to("correspondence_carbon_copies#update")
    end
  end
end
