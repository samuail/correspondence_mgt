require "rails_helper"

RSpec.describe ManuscriptAuthorsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/crmgt/manuscript/authors").to route_to("manuscript_authors#index")
    end

    it "routes to #create" do
      expect(:post => "/crmgt/manuscript/authors").to route_to("manuscript_authors#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/crmgt/manuscript/authors").to route_to("manuscript_authors#update")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/crmgt/manuscript/authors").to route_to("manuscript_authors#update")
    end
  end
end
