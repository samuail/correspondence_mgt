require "rails_helper"

RSpec.describe InternalCorrespondencesController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/crmgt/correspondence/internal").to route_to("internal_correspondences#index")
    end

    it "routes to #get_assigned" do
      expect(:get => "/crmgt/correspondence/internal/assigned").to route_to("internal_correspondences#get_assigned")
    end

    it "routes to #create" do
      expect(:post => "/crmgt/correspondence/internal").to route_to("internal_correspondences#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/crmgt/correspondence/internal").to route_to("internal_correspondences#update")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/crmgt/correspondence/internal").to route_to("internal_correspondences#update")
    end

    it "routes to #update_status via PUT" do
      expect(:put => "/crmgt/correspondence/internal/update_status").to route_to("internal_correspondences#update_status")
    end

    it "routes to #upload" do
      expect(:post => "/crmgt/correspondence/internal/upload").to route_to("internal_correspondences#upload")
    end

    it "routes to #change_uploads" do
      expect(:post => "/crmgt/correspondence/internal/change_uploads").to route_to("internal_correspondences#change_uploads")
    end

    it "routes to #get_images" do
      expect(:get => "/crmgt/correspondence/internal/get_images").to route_to("internal_correspondences#get_images")
    end
  end
end
