require "rails_helper"

RSpec.describe OrganizationsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/crmgt/business_settings/organization").to route_to("organizations#index")
    end

    it "routes to #create" do
      expect(:post => "/crmgt/business_settings/organization").to route_to("organizations#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/crmgt/business_settings/organization").to route_to("organizations#update")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/crmgt/business_settings/organization").to route_to("organizations#update")
    end

  end
end
