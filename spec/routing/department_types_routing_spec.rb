require "rails_helper"

RSpec.describe DepartmentTypesController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/crmgt/business_settings/department_type").to route_to("department_types#index")
    end

    it "routes to #create" do
      expect(:post => "/crmgt/business_settings/department_type").to route_to("department_types#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/crmgt/business_settings/department_type").to route_to("department_types#update")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/crmgt/business_settings/department_type").to route_to("department_types#update")
    end

  end
end
