require "rails_helper"

RSpec.describe ManuscriptCommentsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/crmgt/manuscript/comments").to route_to("manuscript_comments#index")
    end

    it "routes to #create" do
      expect(:post => "/crmgt/manuscript/comments").to route_to("manuscript_comments#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/crmgt/manuscript/comments").to route_to("manuscript_comments#update")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/crmgt/manuscript/comments").to route_to("manuscript_comments#update")
    end

  end
end
