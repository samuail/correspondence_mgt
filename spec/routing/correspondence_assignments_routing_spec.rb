require "rails_helper"

RSpec.describe CorrespondenceAssignmentsController, type: :routing do
  describe "Internal Correspondence Routing" do
    it "routes to #index" do
      expect(:get => "/crmgt/correspondence/internal/assignment").to route_to("correspondence_assignments#index")
    end

    it "routes to #create" do
      expect(:post => "/crmgt/correspondence/internal/assignment").to route_to("correspondence_assignments#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/crmgt/correspondence/internal/assignment").to route_to("correspondence_assignments#update")
    end
  end

  describe "External Incoming Correspondence Routing" do
    it "routes to #index" do
      expect(:get => "/crmgt/correspondence/external/assignment").to route_to("correspondence_assignments#index")
    end

    it "routes to #create" do
      expect(:post => "/crmgt/correspondence/external/assignment").to route_to("correspondence_assignments#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/crmgt/correspondence/external/assignment").to route_to("correspondence_assignments#update")
    end
  end
end
