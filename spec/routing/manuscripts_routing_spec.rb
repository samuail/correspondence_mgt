require "rails_helper"

RSpec.describe ManuscriptsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/crmgt/manuscript").to route_to("manuscripts#index")
    end

    it "routes to #create" do
      expect(:post => "/crmgt/manuscript").to route_to("manuscripts#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/crmgt/manuscript").to route_to("manuscripts#update")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/crmgt/manuscript").to route_to("manuscripts#update")
    end
  end
end
