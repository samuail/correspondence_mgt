require "rails_helper"

RSpec.describe UserDepartmentsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/crmgt/auth/user/1/department_role").to route_to("user_departments#index", :user_id => "1")
    end

    it "routes to #create" do
      expect(:post => "/crmgt/auth/user/department_role").to route_to("user_departments#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/crmgt/auth/user/department_role").to route_to("user_departments#update")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/crmgt/auth/user/department_role").to route_to("user_departments#update")
    end
  end
end
