require "rails_helper"

RSpec.describe ManuscriptAssignmentsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/crmgt/manuscript/assignments").to route_to("manuscript_assignments#index")
    end

    it "routes to #create" do
      expect(:post => "/crmgt/manuscript/assignments").to route_to("manuscript_assignments#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/crmgt/manuscript/assignments").to route_to("manuscript_assignments#update")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/crmgt/manuscript/assignments").to route_to("manuscript_assignments#update")
    end
  end
end
