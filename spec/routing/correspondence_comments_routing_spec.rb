require "rails_helper"

RSpec.describe CorrespondenceCommentsController, type: :routing do
  describe "Internal Correspondence Routing" do
    it "routes to #index" do
      expect(:get => "/crmgt/correspondence/internal/comments").to route_to("correspondence_comments#index")
    end

    it "routes to #create" do
      expect(:post => "/crmgt/correspondence/internal/comments").to route_to("correspondence_comments#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/crmgt/correspondence/internal/comments").to route_to("correspondence_comments#update")
    end
  end

  describe "External Incoming Correspondence Routing" do
    it "routes to #index" do
      expect(:get => "/crmgt/correspondence/external/comments").to route_to("correspondence_comments#index")
    end

    it "routes to #create" do
      expect(:post => "/crmgt/correspondence/external/comments").to route_to("correspondence_comments#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/crmgt/correspondence/external/comments").to route_to("correspondence_comments#update")
    end
  end
end
