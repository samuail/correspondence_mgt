require "rails_helper"

RSpec.describe AuthorsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/crmgt/business_settings/author").to route_to("authors#index")
    end

    it "routes to #create" do
      expect(:post => "/crmgt/business_settings/author").to route_to("authors#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/crmgt/business_settings/author").to route_to("authors#update")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/crmgt/business_settings/author").to route_to("authors#update")
    end
  end
end
