require "rails_helper"

RSpec.describe ExternalAssessorsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/crmgt/business_settings/external_assessor").to route_to("external_assessors#index")
    end

    it "routes to #create" do
      expect(:post => "/crmgt/business_settings/external_assessor").to route_to("external_assessors#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/crmgt/business_settings/external_assessor").to route_to("external_assessors#update")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/crmgt/business_settings/external_assessor").to route_to("external_assessors#update")
    end
  end
end
