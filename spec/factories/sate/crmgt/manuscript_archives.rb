FactoryBot.define do
  factory :sate_crmgt_manuscript_archive, class: 'Sate::Crmgt::ManuscriptArchive' do
    manuscript { create(:sate_crmgt_manuscript) }
    remark { FFaker::Name.name }
    no_pages { 1 }
    no_copies { 1 }
    has_archive_att { false }
    archived_date { FFaker::Time.datetime }
    archived_by_id { create(:sate_auth_user).id }
    application_module { create(:sate_auth_application_module) }
  end
end
