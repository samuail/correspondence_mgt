FactoryBot.define do
  factory :sate_crmgt_organization, class: 'Sate::Crmgt::Organization' do
    code { FFaker::Name.name }
    description { FFaker::Name.name }
    organization_type { create (:sate_crmgt_organization_type) }
    application_module { create (:sate_auth_application_module) }
  end
end
