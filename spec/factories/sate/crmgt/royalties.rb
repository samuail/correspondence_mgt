FactoryBot.define do
  factory :sate_crmgt_royalty, class: 'Sate::Crmgt::Royalty' do
    manuscript { create(:sate_crmgt_manuscript) }
    author { create(:sate_crmgt_author) }
    reference_no { FFaker::Name.name }
    amount { FFaker.numerify("#.##")}
    remark { FFaker::Name.name }
    payment_date { FFaker::Time.datetime }
    key_in_by_id { create(:sate_auth_user).id }
    application_module { create(:sate_auth_application_module) }
  end
end
