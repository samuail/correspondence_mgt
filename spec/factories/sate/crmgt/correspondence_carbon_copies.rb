FactoryBot.define do
  factory :sate_crmgt_correspondence_carbon_copy, class: 'Sate::Crmgt::CorrespondenceCarbonCopy' do
    correspondence { nil }
    destination_id { create(:sate_crmgt_department).id }
    application_module { create(:sate_auth_application_module) }

    trait :for_internal_correspondence do
      before :create do |correspondence_carbon_copy|
        internal = create(:sate_crmgt_internal_correspondence)
        correspondence_carbon_copy.correspondence = internal
      end
    end

    trait :for_external_incoming_correspondence do
      before :create do |correspondence_carbon_copy|
        external = create(:sate_crmgt_external_incoming_correspondence)
        correspondence_carbon_copy.correspondence = external
      end
    end
  end
end