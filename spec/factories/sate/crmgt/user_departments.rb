FactoryBot.define do
  factory :sate_crmgt_user_department, class: 'Sate::Crmgt::UserDepartment' do
    user { create(:sate_auth_user) }
    department { create(:sate_crmgt_department) }
    user_role { create(:sate_auth_user_role) }
    application_module { create(:sate_auth_application_module) }
  end
end
