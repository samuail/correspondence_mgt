FactoryBot.define do
  factory :sate_crmgt_manuscript_author, class: 'Sate::Crmgt::ManuscriptAuthor' do
    manuscript { create(:sate_crmgt_manuscript) }
    author { create(:sate_crmgt_author) }
    application_module { create(:sate_auth_application_module) }
  end
end
