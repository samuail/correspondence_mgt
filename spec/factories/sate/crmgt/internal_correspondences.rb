FactoryBot.define do
  factory :sate_crmgt_internal_correspondence, class: 'Sate::Crmgt::InternalCorrespondence' do
    reference_no { FFaker::Name.name }
    letter_date { FFaker::Time.date }
    subject { FFaker::Name.name }
    search_keywords { [FFaker::Name.name, FFaker::Name.name] }
    source_id { create(:sate_crmgt_department).id }
    destination_id { create(:sate_crmgt_department).id }
    received_date { FFaker::Time.date }
    status { FFaker::Name.name }
    application_module { create(:sate_auth_application_module) }
    key_in_by_id { create(:sate_auth_user).id }

    trait :with_main_images do
      after :create do |internal_correspondence|
        @path_to_file = '/home/samuel/Desktop'
        main = @path_to_file + "/changes.png"
        file = Rack::Test::UploadedFile.new(main, 'image/png')
        internal_correspondence.main_images.attach(file)
      end
    end

    trait :with_invalid_main_images do
      after :create do |internal_correspondence|
        @path_to_file = '/home/samuel/Desktop'
        main = @path_to_file + "/registration_notice.txt"
        file = Rack::Test::UploadedFile.new(main, 'text/plain')
        internal_correspondence.main_images.attach(file)
      end
    end

    trait :with_big_size_main_images do
      after :create do |internal_correspondence|
        @path_to_file = '/home/samuel/Desktop'
        main = @path_to_file + "/ancient-town-lake-china.jpg"
        file = Rack::Test::UploadedFile.new(main, 'image/jpg')
        internal_correspondence.main_images.attach(file)
      end
    end

    trait :with_attachment_images do
      after :create do |internal_correspondence|
        @path_to_file = '/home/samuel/Desktop'
        main = @path_to_file + "/changes.png"
        file = Rack::Test::UploadedFile.new(main, 'image/png')
        internal_correspondence.attachment_images.attach(file)
      end
    end

    trait :with_invalid_attachment_images do
      after :create do |internal_correspondence|
        @path_to_file = '/home/samuel/Desktop'
        main = @path_to_file + "/registration_notice.txt"
        file = Rack::Test::UploadedFile.new(main, 'text/plain')
        internal_correspondence.attachment_images.attach(file)
      end
    end

    trait :with_big_size_attachment_images do
      after :create do |internal_correspondence|
        @path_to_file = '/home/samuel/Desktop'
        main = @path_to_file + "/ancient-town-lake-china.jpg"
        file = Rack::Test::UploadedFile.new(main, 'image/jpg')
        internal_correspondence.attachment_images.attach(file)
      end
    end
  end
end
