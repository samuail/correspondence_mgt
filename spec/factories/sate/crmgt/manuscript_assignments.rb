FactoryBot.define do
  factory :sate_crmgt_manuscript_assignment, class: 'Sate::Crmgt::ManuscriptAssignment' do
    stage { "Preliminary Assessment" }
    from_user_id { create(:sate_auth_user).id }
    assignee { nil }
    manuscript { create(:sate_crmgt_manuscript) }
    status { FFaker::Name.name }
    assigned_date { FFaker::Time.datetime }
    received_date { FFaker::Time.datetime }
    application_module { create (:sate_auth_application_module) }

    trait :for_internal_user do
      before :create do |manuscript_assignment|
        internal = create(:sate_auth_user)
        manuscript_assignment.assignee_id = internal.id
        manuscript_assignment.assignee_type = 'Sate::Auth::User'
      end
    end

    trait :for_author do
      before :create do |manuscript_assignment|
        author = create(:sate_crmgt_author)
        manuscript_assignment.assignee_id = author.id
        manuscript_assignment.assignee_type = 'Sate::Crmgt::Author'
      end
    end

    trait :for_external_assessor do
      before :create do |manuscript_assignment|
        assessor = create(:sate_crmgt_external_assessor)
        manuscript_assignment.assignee_id = assessor.id
        manuscript_assignment.assignee_type = 'Sate::Crmgt::ExternalAssessor'
      end
    end
  end
end
