FactoryBot.define do
  factory :sate_crmgt_department, class: 'Sate::Crmgt::Department' do
    code { FFaker::Name.name }
    description { FFaker::Name.name }
    department_type { create (:sate_crmgt_department_type) }
    parent_id { nil }
    application_module { create (:sate_auth_application_module) }
  end
end
