FactoryBot.define do
  factory :sate_crmgt_correspondence_comment, class: 'Sate::Crmgt::CorrespondenceComment' do
    commented_by_id { create(:sate_auth_user).id }
    correspondence { nil }
    content { FFaker::Name.name }
    order { 1 }
    comment_date { FFaker::Time.datetime }
    application_module { create (:sate_auth_application_module) }

    trait :for_internal_correspondence do
      before :create do |correspondence_comment|
        internal = create(:sate_crmgt_internal_correspondence)
        correspondence_comment.correspondence = internal
      end
    end

    trait :for_external_incoming_correspondence do
      before :create do |correspondence_comment|
        external = create(:sate_crmgt_external_incoming_correspondence)
        correspondence_comment.correspondence = external
      end
    end
  end
end
