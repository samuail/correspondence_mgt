FactoryBot.define do
  factory :sate_crmgt_correspondence_assignment, class: 'Sate::Crmgt::CorrespondenceAssignment' do
    from_user_id { create(:sate_auth_user).id }
    to_user_id { create(:sate_auth_user).id }
    correspondence { nil }
    order { 1 }
    status { FFaker::Name.name }
    assigned_date { FFaker::Time.date }
    received_date { FFaker::Time.date }
    application_module { create (:sate_auth_application_module) }

    trait :for_internal_correspondence do
      before :create do |correspondence_assignment|
        internal = create(:sate_crmgt_internal_correspondence)
        correspondence_assignment.correspondence = internal
      end
    end

    trait :for_external_incoming_correspondence do
      before :create do |correspondence_assignment|
        external = create(:sate_crmgt_external_incoming_correspondence)
        correspondence_assignment.correspondence = external
      end
    end
  end
end