FactoryBot.define do
  factory :sate_crmgt_user_menu_action, class: 'Sate::Crmgt::UserMenuAction' do
    user { create(:sate_auth_user) }
    menu_action { create(:sate_crmgt_menu_action) }
    active { true }
    application_module { create(:sate_auth_application_module) }
  end
end
