FactoryBot.define do
  factory :sate_crmgt_department_type, parent: :sate_crmgt_lookup, class: 'Sate::Crmgt::DepartmentType' do
    type { 'Sate::Crmgt::DepartmentType' }
  end
end
