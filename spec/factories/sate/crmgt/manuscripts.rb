FactoryBot.define do
  factory :sate_crmgt_manuscript, class: 'Sate::Crmgt::Manuscript' do
    reference_no { FFaker::Name.name }
    received_date { FFaker::Time.datetime }
    title { FFaker::Name.name }
    search_keywords { [FFaker::Name.name, FFaker::Name.name] }
    destination_id { create(:sate_crmgt_department).id }
    key_in_by_id { create(:sate_auth_user).id }
    status { "New" }
    application_module { create(:sate_auth_application_module) }
  end
end
