FactoryBot.define do
  factory :sate_crmgt_external_assessor, class: 'Sate::Crmgt::ExternalAssessor' do
    title { FFaker::Name.name[0..20] }
    first_name { FFaker::Name.name[0..20] }
    father_name { FFaker::Name.name[0..20] }
    grand_father_name { FFaker::Name.name[0..20] }
    email { FFaker::Internet.email[0..49] }
    telephone { FFaker::Name.name[0..20] }
    area_of_expertise { FFaker::Name.name[0..20] }
    institution { FFaker::Name.name[0..20] }
    tin { FFaker::Name.name[0..20] }
    bank_acc { FFaker::Name.name[0..20] }
    bank_name { FFaker::Name.name[0..20] }
    application_module { create (:sate_auth_application_module) }
  end
end
