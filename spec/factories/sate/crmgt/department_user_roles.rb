FactoryBot.define do
  factory :sate_crmgt_department_user_role, class: 'Sate::Crmgt::DepartmentUserRole' do
    department { create(:sate_crmgt_department) }
    user_role { create(:sate_auth_user_role) }
    is_head { false }
    application_module { create(:sate_auth_application_module) }
  end
end
