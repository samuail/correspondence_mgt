FactoryBot.define do
  factory :sate_crmgt_user_department_role, class: 'Sate::Crmgt::UserDepartmentRole' do
    user { create(:sate_auth_user) }
    department_user_role { create(:sate_crmgt_department_user_role) }
    application_module { create(:sate_auth_application_module) }
  end
end
