FactoryBot.define do
  factory :sate_crmgt_author_contact, class: 'Sate::Crmgt::AuthorContact' do
    author { create(:sate_crmgt_author) }
    first_name { FFaker::Name.name[0..20] }
    father_name { FFaker::Name.name[0..20] }
    email { FFaker::Internet.email[0..49] }
    telephone { FFaker::Name.name[0..20] }
    application_module { create (:sate_auth_application_module) }
  end
end
