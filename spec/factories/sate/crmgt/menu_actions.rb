FactoryBot.define do
  factory :sate_crmgt_menu_action, class: 'Sate::Crmgt::MenuAction' do
    menu { create(:sate_auth_menu) }
    action { create(:sate_crmgt_action) }
    application_module { create(:sate_auth_application_module) }
  end
end
