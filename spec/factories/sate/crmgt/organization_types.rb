FactoryBot.define do
  factory :sate_crmgt_organization_type, parent: :sate_crmgt_lookup, class: 'Sate::Crmgt::OrganizationType' do
    type { 'Sate::Crmgt::OrganizationType' }
  end
end
