FactoryBot.define do
  factory :sate_crmgt_action, parent: :sate_crmgt_lookup, class: 'Sate::Crmgt::Action' do
    type { 'Sate::Crmgt::Action' }
  end
end
