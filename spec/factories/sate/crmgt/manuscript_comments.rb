FactoryBot.define do
  factory :sate_crmgt_manuscript_comment, class: 'Sate::Crmgt::ManuscriptComment' do
    commented_by_id { create(:sate_auth_user).id }
    manuscript { create(:sate_crmgt_manuscript) }
    content { FFaker::Name.name }
    comment_date { FFaker::Time.datetime }
    application_module { create (:sate_auth_application_module) }
  end
end
