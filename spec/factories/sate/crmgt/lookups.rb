FactoryBot.define do
  factory :sate_crmgt_lookup, class: 'Sate::Crmgt::Lookup' do
    code { FFaker::Name.name }
    name { FFaker::Name.name }
    type { "Lookup" }
    application_module { create (:sate_auth_application_module) }
  end
end
