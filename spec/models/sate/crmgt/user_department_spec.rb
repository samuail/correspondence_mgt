require 'rails_helper'

RSpec.describe Sate::Crmgt::UserDepartment, type: :model do
  it 'has a valid factory' do
    expect(create(:sate_crmgt_user_department)).to be_valid
  end

  it 'is invalid with no user, department, user role, and application module' do
    expect(build(:sate_crmgt_user_department, user_id: nil)).not_to be_valid
    expect(build(:sate_crmgt_user_department, department_id: nil)).not_to be_valid
    expect(build(:sate_crmgt_user_department, user_role_id: nil)).not_to be_valid
    expect(build(:sate_crmgt_user_department, application_module_id: nil)).not_to be_valid
  end

  it 'is invalid with duplicate user department information' do
    user_department = create(:sate_crmgt_user_department)
    expect(build(:sate_crmgt_user_department, user_id: user_department.user_id,
                 department_id: user_department.department_id,
                 user_role_id: user_department.user_role_id,
                 application_module_id: user_department.application_module_id
           )).not_to be_valid
  end
end
