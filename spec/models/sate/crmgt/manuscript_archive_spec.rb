require 'rails_helper'

RSpec.describe Sate::Crmgt::ManuscriptArchive, type: :model do
  it 'has a valid factory' do
    expect(create(:sate_crmgt_manuscript_archive)).to be_valid
  end

  it 'is invalid with no manuscript, number of copies, archived by, and application module' do
    expect(build(:sate_crmgt_manuscript_archive, manuscript_id: nil)).not_to be_valid
    expect(build(:sate_crmgt_manuscript_archive, no_copies: nil)).not_to be_valid
    expect(build(:sate_crmgt_manuscript_archive, archived_by_id: nil)).not_to be_valid
    expect(build(:sate_crmgt_manuscript_archive, application_module_id: nil)).not_to be_valid
  end

  it 'is invalid with duplicate Manuscript Archive information' do
    manuscript_archive = create(:sate_crmgt_manuscript_archive)
    expect(build(:sate_crmgt_manuscript_archive, manuscript: manuscript_archive.manuscript,
                 remark: manuscript_archive.remark, no_copies: manuscript_archive.no_copies,
                 archived_by_id: manuscript_archive.archived_by_id,
                 application_module_id: manuscript_archive.application_module_id
           )).not_to be_valid
  end
end
