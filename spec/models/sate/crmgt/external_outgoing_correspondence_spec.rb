require 'rails_helper'

RSpec.describe Sate::Crmgt::ExternalOutgoingCorrespondence, type: :model do
  it 'has a valid factory' do
    expect(create(:sate_crmgt_external_outgoing_correspondence)).to be_valid
  end

  it 'is invalid with no reference no, letter date, subject, source, destination, received date, and application module' do
    expect(build(:sate_crmgt_external_outgoing_correspondence, reference_no: nil)).not_to be_valid
    expect(build(:sate_crmgt_external_outgoing_correspondence, letter_date: nil)).not_to be_valid
    expect(build(:sate_crmgt_external_outgoing_correspondence, subject: nil)).not_to be_valid
    expect(build(:sate_crmgt_external_outgoing_correspondence, source_id: nil)).not_to be_valid
    expect(build(:sate_crmgt_external_outgoing_correspondence, destination_id: nil)).not_to be_valid
    expect(build(:sate_crmgt_external_outgoing_correspondence, received_date: nil)).not_to be_valid
    expect(build(:sate_crmgt_external_outgoing_correspondence, application_module_id: nil)).not_to be_valid
  end

  it 'is invalid with duplication reference no' do
    external_outgoing = create(:sate_crmgt_external_outgoing_correspondence)
    expect(build(:sate_crmgt_external_outgoing_correspondence,
                 reference_no: external_outgoing.reference_no)).not_to be_valid
  end

  it 'is invalid with duplicate correspondence information' do
    external_outgoing = create(:sate_crmgt_external_outgoing_correspondence)
    expect(build(:sate_crmgt_external_outgoing_correspondence, reference_no: external_outgoing.reference_no,
                 letter_date: external_outgoing.letter_date, subject: external_outgoing.subject,
                 source_id: external_outgoing.source_id, destination_id: external_outgoing.destination_id,
                 received_date: external_outgoing.received_date, application_module_id: external_outgoing.application_module_id
           )).not_to be_valid
  end

  it 'main image is attached' do
    create(:sate_crmgt_external_outgoing_correspondence, :with_main_images)
    correspondence = Sate::Crmgt::ExternalOutgoingCorrespondence.last
    expect(correspondence.main_images).to be_attached
  end

  it 'main image is invalid with non image file type' do
    create(:sate_crmgt_external_outgoing_correspondence, :with_invalid_main_images)
    correspondence = Sate::Crmgt::ExternalOutgoingCorrespondence.last
    expect(correspondence.main_images).not_to be_attached
  end

  it 'main image is invalid with big size file' do
    create(:sate_crmgt_external_outgoing_correspondence, :with_big_size_main_images)
    correspondence = Sate::Crmgt::ExternalOutgoingCorrespondence.last
    expect(correspondence.main_images).not_to be_attached
  end

  it 'attachment image is attached' do
    create(:sate_crmgt_external_outgoing_correspondence, :with_attachment_images)
    correspondence = Sate::Crmgt::ExternalOutgoingCorrespondence.last
    expect(correspondence.attachment_images).to be_attached
  end

  it 'attachment image is invalid with non image file type' do
    create(:sate_crmgt_external_outgoing_correspondence, :with_invalid_attachment_images)
    correspondence = Sate::Crmgt::ExternalOutgoingCorrespondence.last
    expect(correspondence.attachment_images).not_to be_attached
  end

  it 'attachment image is invalid with big size file' do
    create(:sate_crmgt_external_outgoing_correspondence, :with_big_size_attachment_images)
    correspondence = Sate::Crmgt::ExternalOutgoingCorrespondence.last
    expect(correspondence.attachment_images).not_to be_attached
  end
end
