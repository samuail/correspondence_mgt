require 'rails_helper'

RSpec.describe Sate::Crmgt::ExternalAssessor, type: :model do
  it 'has a valid factory' do
    expect(create(:sate_crmgt_external_assessor)).to be_valid
  end

  it 'is invalid with no First Name' do
    expect(build(:sate_crmgt_external_assessor, first_name: nil)).not_to be_valid
  end

  it 'is invalid with no Father Name' do
    expect(build(:sate_crmgt_external_assessor, father_name: nil)).not_to be_valid
  end

  it 'is invalid with no Grand Father Name' do
    expect(build(:sate_crmgt_external_assessor, grand_father_name: nil)).not_to be_valid
  end

  it 'is invalid with no Email' do
    expect(build(:sate_crmgt_external_assessor, email: nil)).not_to be_valid
  end

  it 'is invalid with no Telephone' do
    expect(build(:sate_crmgt_external_assessor, telephone: nil)).not_to be_valid
  end

  it 'is invalid with no Area of Expertise' do
    expect(build(:sate_crmgt_external_assessor, area_of_expertise: nil)).not_to be_valid
  end

  it 'is invalid with no Tin' do
    expect(build(:sate_crmgt_external_assessor, tin: nil)).not_to be_valid
  end

  it 'is invalid with no Bank Name' do
    expect(build(:sate_crmgt_external_assessor, bank_name: nil)).not_to be_valid
  end

  it 'is invalid with no Bank A/C' do
    expect(build(:sate_crmgt_external_assessor, bank_acc: nil)).not_to be_valid
  end

  it 'is invalid with duplicate Full Name and Application Module' do
    assessor = create(:sate_crmgt_external_assessor)
    expect(build(:sate_crmgt_external_assessor, first_name: assessor.first_name,
                 father_name: assessor.father_name,
                 grand_father_name: assessor.grand_father_name,
                 email: assessor.email,
                 telephone: assessor.telephone,
                 tin: assessor.tin,
                 bank_name: assessor.bank_name,
                 bank_acc: assessor.bank_acc,
                 application_module_id: assessor.application_module_id)).not_to be_valid
  end

  context "validations" do
    subject { build(:sate_crmgt_external_assessor) }
    it { should allow_value('first.last@example.com').for(:email) }
    it { should_not allow_value('first.lastexample.com', 'first.last@example',
                                'first.last@example.', '@example.com').for(:email)}
    it "should downcase email before saving" do
      user_stub = build_stubbed(:sate_crmgt_external_assessor)
      user = Sate::Crmgt::ExternalAssessor.create first_name: user_stub.first_name,
                                                  father_name: user_stub.father_name,
                                                  grand_father_name: user_stub.grand_father_name,
                                                  email: user_stub.email.upcase,
                                                  telephone: user_stub.telephone,
                                                  tin: user_stub.tin,
                                                  bank_name: user_stub.bank_name,
                                                  bank_acc: user_stub.bank_acc,
                                                  application_module_id: user_stub.application_module_id
      expect(user.email).not_to eq user_stub.email
    end
  end
end
