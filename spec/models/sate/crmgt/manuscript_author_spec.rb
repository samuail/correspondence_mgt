require 'rails_helper'

RSpec.describe Sate::Crmgt::ManuscriptAuthor, type: :model do
  it 'has a valid factory' do
    expect(create(:sate_crmgt_manuscript_author)).to be_valid
  end

  it 'is invalid with no manuscript, author, and application module' do
    expect(build(:sate_crmgt_manuscript_author, manuscript_id: nil)).not_to be_valid
    expect(build(:sate_crmgt_manuscript_author, author_id: nil)).not_to be_valid
    expect(build(:sate_crmgt_manuscript_author, application_module_id: nil)).not_to be_valid
  end

  it 'is invalid with duplicate Manuscript Author information' do
    manuscript_author = create(:sate_crmgt_manuscript_author)
    expect(build(:sate_crmgt_manuscript_author, manuscript_id: manuscript_author.manuscript_id,
                 author_id: manuscript_author.author_id,
                 application_module_id: manuscript_author.application_module_id
           )).not_to be_valid
  end
end
