require 'rails_helper'

RSpec.describe Sate::Crmgt::Lookup, type: :model do
  it 'has a valid factory' do
    expect(create(:sate_crmgt_lookup)).to be_valid
  end

  it 'is invalid with no name' do
    expect(build(:sate_crmgt_lookup, :name => nil)).not_to be_valid
  end

  it 'is invalid with no type' do
    expect(build(:sate_crmgt_lookup, type: nil)).not_to be_valid
  end

  it 'is invalid with duplicate code, name, type and application module' do
    lookup = create(:sate_crmgt_lookup)
    expect(build(:sate_crmgt_lookup, code: lookup.code,
                 name: lookup.name, type: lookup.type,
                 application_module_id: lookup.application_module_id)).not_to be_valid
  end
end
