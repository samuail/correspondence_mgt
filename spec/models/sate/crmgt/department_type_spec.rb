require 'rails_helper'

RSpec.describe Sate::Crmgt::DepartmentType, type: :model do
  it 'has a valid factory' do
    expect(create(:sate_crmgt_department_type)).to be_valid
  end
end
