require 'rails_helper'

RSpec.describe Sate::Crmgt::Author, type: :model do
  it 'has a valid factory' do
    expect(create(:sate_crmgt_author)).to be_valid
  end

  it 'is invalid with no First Name' do
    expect(build(:sate_crmgt_author, first_name: nil)).not_to be_valid
  end

  it 'is invalid with no Father Name' do
    expect(build(:sate_crmgt_author, father_name: nil)).not_to be_valid
  end

  it 'is invalid with no Grand Father Name' do
    expect(build(:sate_crmgt_author, grand_father_name: nil)).not_to be_valid
  end

  it 'is invalid with no Email' do
    expect(build(:sate_crmgt_author, email: nil)).not_to be_valid
  end

  it 'is invalid with no Telephone' do
    expect(build(:sate_crmgt_author, telephone: nil)).not_to be_valid
  end

  it 'is invalid with duplicate Full Name and Application Module' do
    author = create(:sate_crmgt_author)
    expect(build(:sate_crmgt_author, first_name: author.first_name,
                 father_name: author.father_name,
                 grand_father_name: author.grand_father_name,
                 application_module_id: author.application_module_id)).not_to be_valid
  end

  context "validations" do
    subject { build(:sate_crmgt_author) }
    it { should allow_value('first.last@example.com').for(:email) }
    it { should_not allow_value('first.lastexample.com', 'first.last@example',
                                'first.last@example.', '@example.com').for(:email)}
    it "should downcase email before saving" do
      user_stub = build_stubbed(:sate_crmgt_author)
      user = Sate::Crmgt::Author.create first_name: user_stub.first_name,
                                        father_name: user_stub.father_name,
                                        grand_father_name: user_stub.grand_father_name,
                                        email: user_stub.email.upcase,
                                        telephone: user_stub.telephone,
                                        application_module_id: user_stub.application_module_id
      expect(user.email).not_to eq user_stub.email
    end
  end
end
