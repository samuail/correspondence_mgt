require 'rails_helper'

RSpec.describe Sate::Crmgt::ManuscriptAssignment, type: :model do
  it 'has a valid factory for Internal User' do
    expect(create(:sate_crmgt_manuscript_assignment, :for_internal_user)).to be_valid
  end

  it 'has a valid factory for Author' do
    expect(create(:sate_crmgt_manuscript_assignment, :for_author)).to be_valid
  end

  it 'has a valid factory for External Assessor' do
    expect(create(:sate_crmgt_manuscript_assignment, :for_external_assessor)).to be_valid
  end

  it 'is invalid with no satge, from user, assignee, manuscript, assigned date, received date, and application module' do
    expect(build(:sate_crmgt_manuscript_assignment, from_user_id: nil)).not_to be_valid
    expect(build(:sate_crmgt_manuscript_assignment, stage: nil)).not_to be_valid
    expect(build(:sate_crmgt_manuscript_assignment, assignee: nil)).not_to be_valid
    expect(build(:sate_crmgt_manuscript_assignment, manuscript: nil)).not_to be_valid
    expect(build(:sate_crmgt_manuscript_assignment, assigned_date: nil)).not_to be_valid
    expect(build(:sate_crmgt_manuscript_assignment, received_date: nil)).not_to be_valid
    expect(build(:sate_crmgt_manuscript_assignment, application_module_id: nil)).not_to be_valid
  end

  it 'is invalid with duplicate manuscript assignment for internal user information' do
    internal = create(:sate_crmgt_manuscript_assignment, :for_internal_user)
    expect(build(:sate_crmgt_manuscript_assignment, stage: internal.stage, from_user_id: internal.from_user_id,
                 assignee_id: internal.assignee_id, assignee_type: internal.assignee_type,
                 manuscript: internal.manuscript, assigned_date: internal.assigned_date, received_date: internal.received_date,
                 application_module_id: internal.application_module_id
           )).not_to be_valid
  end

  it 'is invalid with duplicate manuscript assignment for author information' do
    author = create(:sate_crmgt_manuscript_assignment, :for_author)
    expect(build(:sate_crmgt_manuscript_assignment, stage: author.stage, from_user_id: author.from_user_id,
                 assignee_id: author.assignee_id, assignee_type: author.assignee_type,
                 manuscript: author.manuscript, assigned_date: author.assigned_date, received_date: author.received_date,
                 application_module_id: author.application_module_id
           )).not_to be_valid
  end

  it 'is invalid with duplicate manuscript assignment for external assessor information' do
    external = create(:sate_crmgt_manuscript_assignment, :for_external_assessor)
    expect(build(:sate_crmgt_manuscript_assignment, stage: external.stage, from_user_id: external.from_user_id,
                 assignee_id: external.assignee_id, assignee_type: external.assignee_type,
                 manuscript: external.manuscript, assigned_date: external.assigned_date, received_date: external.received_date,
                 application_module_id: external.application_module_id
           )).not_to be_valid
  end
end
