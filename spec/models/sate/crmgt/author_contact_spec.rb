require 'rails_helper'

RSpec.describe Sate::Crmgt::AuthorContact, type: :model do
  it 'has a valid factory' do
    expect(create(:sate_crmgt_author_contact)).to be_valid
  end

  it 'is invalid with no Author' do
    expect(build(:sate_crmgt_author_contact, author: nil)).not_to be_valid
  end

  it 'is invalid with no First Name' do
    expect(build(:sate_crmgt_author_contact, first_name: nil)).not_to be_valid
  end

  it 'is invalid with no Father Name' do
    expect(build(:sate_crmgt_author_contact, father_name: nil)).not_to be_valid
  end

  it 'is invalid with no Email' do
    expect(build(:sate_crmgt_author_contact, email: nil)).not_to be_valid
  end

  it 'is invalid with no Telephone' do
    expect(build(:sate_crmgt_author_contact, telephone: nil)).not_to be_valid
  end

  it 'is invalid with duplicate Full Name and Application Module' do
    author_contact = create(:sate_crmgt_author_contact)
    expect(build(:sate_crmgt_author_contact, author_id: author_contact.author_id,
                 first_name: author_contact.first_name,
                 father_name: author_contact.father_name,
                 email: author_contact.email,
                 telephone: author_contact.telephone,
                 application_module_id: author_contact.application_module_id)).not_to be_valid
  end

  context "validations" do
    subject { build(:sate_crmgt_author_contact) }
    it { should allow_value('first.last@example.com').for(:email) }
    it { should_not allow_value('first.lastexample.com', 'first.last@example',
                                'first.last@example.', '@example.com').for(:email)}
    it "should downcase email before saving" do
      user_stub = build_stubbed(:sate_crmgt_author_contact)
      user = Sate::Crmgt::AuthorContact.create author_id: user_stub.author_id,
                                               first_name: user_stub.first_name,
                                               father_name: user_stub.father_name,
                                               email: user_stub.email.upcase,
                                               telephone: user_stub.telephone,
                                               application_module_id: user_stub.application_module_id
      expect(user.email).not_to eq user_stub.email
    end
  end
end
