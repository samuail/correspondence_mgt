require 'rails_helper'

RSpec.describe Sate::Crmgt::MenuAction, type: :model do
  it 'has a valid factory' do
    expect(create(:sate_crmgt_menu_action)).to be_valid
  end

  it 'is invalid with no Menu' do
    expect(build(:sate_crmgt_menu_action, menu_id: nil)).not_to be_valid
  end

  it 'is invalid with no Action' do
    expect(build(:sate_crmgt_menu_action, action_id: nil)).not_to be_valid
  end

  it 'is invalid with duplicate menu, action, and application module' do
    menu_action = create(:sate_crmgt_menu_action)
    expect(build(:sate_crmgt_menu_action, menu_id: menu_action.menu_id,
                 action_id: menu_action.action_id,
                 application_module_id: menu_action.application_module_id)).not_to be_valid
  end
end
