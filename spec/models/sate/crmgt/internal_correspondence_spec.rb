require 'rails_helper'

RSpec.describe Sate::Crmgt::InternalCorrespondence, type: :model do
  it 'has a valid factory' do
    expect(create(:sate_crmgt_internal_correspondence)).to be_valid
  end

  it 'is invalid with no reference no, letter date, subject, source, destination, received date, and application module' do
    expect(build(:sate_crmgt_internal_correspondence, reference_no: nil)).not_to be_valid
    expect(build(:sate_crmgt_internal_correspondence, letter_date: nil)).not_to be_valid
    expect(build(:sate_crmgt_internal_correspondence, subject: nil)).not_to be_valid
    expect(build(:sate_crmgt_internal_correspondence, source_id: nil)).not_to be_valid
    expect(build(:sate_crmgt_internal_correspondence, destination_id: nil)).not_to be_valid
    expect(build(:sate_crmgt_internal_correspondence, received_date: nil)).not_to be_valid
    expect(build(:sate_crmgt_internal_correspondence, application_module_id: nil)).not_to be_valid
  end

  it 'is invalid with duplication reference no' do
    internal = create(:sate_crmgt_internal_correspondence)
    expect(build(:sate_crmgt_internal_correspondence,
                 reference_no: internal.reference_no)).not_to be_valid
  end

  it 'is invalid with duplicate correspondence information' do
    internal = create(:sate_crmgt_internal_correspondence)
    expect(build(:sate_crmgt_internal_correspondence, reference_no: internal.reference_no,
                 letter_date: internal.letter_date, subject: internal.subject,
                 source_id: internal.source_id, destination_id: internal.destination_id,
                 received_date: internal.received_date, application_module_id: internal.application_module_id
           )).not_to be_valid
  end

  it 'main image is attached' do
    create(:sate_crmgt_internal_correspondence, :with_main_images)
    correspondence = Sate::Crmgt::InternalCorrespondence.last
    expect(correspondence.main_images).to be_attached
  end

  it 'main image is invalid with non image file type' do
    create(:sate_crmgt_internal_correspondence, :with_invalid_main_images)
    correspondence = Sate::Crmgt::InternalCorrespondence.last
    expect(correspondence.main_images).not_to be_attached
  end

  it 'main image is invalid with big size file' do
    create(:sate_crmgt_internal_correspondence, :with_big_size_main_images)
    correspondence = Sate::Crmgt::InternalCorrespondence.last
    expect(correspondence.main_images).not_to be_attached
  end

  it 'attachment image is attached' do
    create(:sate_crmgt_internal_correspondence, :with_attachment_images)
    correspondence = Sate::Crmgt::InternalCorrespondence.last
    expect(correspondence.attachment_images).to be_attached
  end

  it 'attachment image is invalid with non image file type' do
    create(:sate_crmgt_internal_correspondence, :with_invalid_attachment_images)
    correspondence = Sate::Crmgt::InternalCorrespondence.last
    expect(correspondence.attachment_images).not_to be_attached
  end

  it 'attachment image is invalid with big size file' do
    create(:sate_crmgt_internal_correspondence, :with_big_size_attachment_images)
    correspondence = Sate::Crmgt::InternalCorrespondence.last
    expect(correspondence.attachment_images).not_to be_attached
  end
end
