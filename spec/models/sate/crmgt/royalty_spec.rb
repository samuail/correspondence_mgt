require 'rails_helper'

RSpec.describe Sate::Crmgt::Royalty, type: :model do
  it 'has a valid factory' do
    expect(create(:sate_crmgt_royalty)).to be_valid
  end

  it 'is invalid with no manuscript, author, reference no, amount, and application module' do
    expect(build(:sate_crmgt_royalty, manuscript_id: nil)).not_to be_valid
    expect(build(:sate_crmgt_royalty, author_id: nil)).not_to be_valid
    expect(build(:sate_crmgt_royalty, reference_no: nil)).not_to be_valid
    expect(build(:sate_crmgt_royalty, amount: nil)).not_to be_valid
    expect(build(:sate_crmgt_royalty, application_module_id: nil)).not_to be_valid
  end

  it 'is invalid with duplicate Royalty information' do
    royalty = create(:sate_crmgt_royalty)
    expect(build(:sate_crmgt_royalty, manuscript_id: royalty.manuscript_id,
                 author_id: royalty.author_id,
                 reference_no: royalty.reference_no,
                 amount: royalty.amount,
                 remark: royalty.remark,
                 payment_date: royalty.payment_date,
                 application_module_id: royalty.application_module_id
           )).not_to be_valid
  end
end
