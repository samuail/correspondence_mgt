require 'rails_helper'

RSpec.describe Sate::Crmgt::UserMenuAction, type: :model do
  it 'has a valid factory' do
    expect(create(:sate_crmgt_user_menu_action)).to be_valid
  end

  it 'is invalid with no User' do
    expect(build(:sate_crmgt_user_menu_action, user_id: nil)).not_to be_valid
  end

  it 'is invalid with no Menu Action' do
    expect(build(:sate_crmgt_user_menu_action, menu_action_id: nil)).not_to be_valid
  end

  it 'is invalid with duplicate menu, action, and application module' do
    user_menu_action = create(:sate_crmgt_user_menu_action)
    expect(build(:sate_crmgt_user_menu_action, user_id: user_menu_action.user_id,
                 menu_action_id: user_menu_action.menu_action_id,
                 application_module_id: user_menu_action.application_module_id)).not_to be_valid
  end
end
