require 'rails_helper'

RSpec.describe Sate::Crmgt::Department, type: :model do
  it 'has a valid factory' do
    expect(create(:sate_crmgt_department)).to be_valid
  end

  it 'is invalid with no code' do
    expect(build(:sate_crmgt_department, code: nil)).not_to be_valid
  end

  it 'is invalid with no description' do
    expect(build(:sate_crmgt_department, description: nil)).not_to be_valid
  end

  it 'is invalid with duplicate code, description, department type and application module' do
    department = create(:sate_crmgt_department)
    expect(build(:sate_crmgt_department, code: department.code,
                 description: department.description,
                 department_type_id: department.department_type_id,
                 application_module_id: department.application_module_id)).not_to be_valid
  end

  it 'returns descendants under center departments' do
    center_department = create(:sate_crmgt_department, description: "Main Campus")
    department = create(:sate_crmgt_department, parent_id: center_department.id)

    expect(Sate::Crmgt::Department.center).to eq [department.id]
  end

  it 'returns descendants under branch departments' do
    center_department = create(:sate_crmgt_department, description: "Main Campus")
    create(:sate_crmgt_department, parent_id: center_department.id)
    department = create(:sate_crmgt_department)

    expect(Sate::Crmgt::Department.college).to eq [department.id]
  end
end
