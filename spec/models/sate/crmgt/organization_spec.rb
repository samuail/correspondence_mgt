require 'rails_helper'

RSpec.describe Sate::Crmgt::Organization, type: :model do
  it 'has a valid factory' do
    expect(create(:sate_crmgt_organization)).to be_valid
  end

  it 'is invalid with no code' do
    expect(build(:sate_crmgt_organization, code: nil)).not_to be_valid
  end

  it 'is invalid with no description' do
    expect(build(:sate_crmgt_organization, description: nil)).not_to be_valid
  end

  it 'is invalid with duplicate code, description, organization type and application module' do
    organization = create(:sate_crmgt_organization)
    expect(build(:sate_crmgt_organization, code: organization.code,
                 description: organization.description,
                 organization_type_id: organization.organization_type_id,
                 application_module_id: organization.application_module_id)).not_to be_valid
  end
end
