require 'rails_helper'

RSpec.describe Sate::Crmgt::CorrespondenceComment, type: :model do
  it 'has a valid factory for Internal Correspondence' do
    expect(create(:sate_crmgt_correspondence_comment, :for_internal_correspondence)).to be_valid
  end

  it 'has a valid factory for External Incoming Correspondence' do
    expect(create(:sate_crmgt_correspondence_comment, :for_external_incoming_correspondence)).to be_valid
  end

  it 'is invalid with no commented by user, correspondence, content, order, comment date, and application module' do
    expect(build(:sate_crmgt_correspondence_comment, commented_by_id: nil)).not_to be_valid
    expect(build(:sate_crmgt_correspondence_comment, correspondence: nil)).not_to be_valid
    expect(build(:sate_crmgt_correspondence_comment, content: nil)).not_to be_valid
    expect(build(:sate_crmgt_correspondence_comment, order: nil)).not_to be_valid
    expect(build(:sate_crmgt_correspondence_comment, comment_date: nil)).not_to be_valid
    expect(build(:sate_crmgt_correspondence_comment, application_module_id: nil)).not_to be_valid
  end

  it 'is invalid with duplicate internal correspondence assignment information' do
    internal = create(:sate_crmgt_correspondence_comment, :for_internal_correspondence)
    expect(build(:sate_crmgt_correspondence_comment, commented_by_id: internal.commented_by_id,
                 content: internal.content, correspondence: internal.correspondence,
                 order: internal.order, comment_date: internal.comment_date,
                 application_module_id: internal.application_module_id
           )).not_to be_valid
  end

  it 'is invalid with duplicate external incoming correspondence assignment information' do
    external = create(:sate_crmgt_correspondence_comment, :for_external_incoming_correspondence)
    expect(build(:sate_crmgt_correspondence_comment, commented_by_id: external.commented_by_id,
                 content: external.content, correspondence: external.correspondence,
                 order: external.order, comment_date: external.comment_date,
                 application_module_id: external.application_module_id
           )).not_to be_valid
  end
end
