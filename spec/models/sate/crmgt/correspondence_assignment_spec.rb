require 'rails_helper'

RSpec.describe Sate::Crmgt::CorrespondenceAssignment, type: :model do
  it 'has a valid factory for Internal Correspondence' do
    expect(create(:sate_crmgt_correspondence_assignment, :for_internal_correspondence)).to be_valid
  end

  it 'has a valid factory for External Incoming Correspondence' do
    expect(create(:sate_crmgt_correspondence_assignment, :for_external_incoming_correspondence)).to be_valid
  end

  it 'is invalid with no from user, to user, correspondence, assigned date, received date, and application module' do
    expect(build(:sate_crmgt_correspondence_assignment, from_user_id: nil)).not_to be_valid
    expect(build(:sate_crmgt_correspondence_assignment, to_user_id: nil)).not_to be_valid
    expect(build(:sate_crmgt_correspondence_assignment, correspondence: nil)).not_to be_valid
    expect(build(:sate_crmgt_correspondence_assignment, assigned_date: nil)).not_to be_valid
    expect(build(:sate_crmgt_correspondence_assignment, received_date: nil)).not_to be_valid
    expect(build(:sate_crmgt_correspondence_assignment, application_module_id: nil)).not_to be_valid
  end

  it 'is invalid with duplicate internal correspondence assignment information' do
    internal = create(:sate_crmgt_correspondence_assignment, :for_internal_correspondence)
    expect(build(:sate_crmgt_correspondence_assignment, from_user_id: internal.from_user_id,
                 to_user_id: internal.to_user_id, correspondence: internal.correspondence,
                 assigned_date: internal.assigned_date, received_date: internal.received_date,
                 application_module_id: internal.application_module_id
           )).not_to be_valid
  end

  it 'is invalid with duplicate external incoming correspondence assignment information' do
    external = create(:sate_crmgt_correspondence_assignment, :for_external_incoming_correspondence)
    expect(build(:sate_crmgt_correspondence_assignment, from_user_id: external.from_user_id,
                 to_user_id: external.to_user_id, correspondence: external.correspondence,
                 assigned_date: external.assigned_date, received_date: external.received_date,
                 application_module_id: external.application_module_id
           )).not_to be_valid
  end
end
