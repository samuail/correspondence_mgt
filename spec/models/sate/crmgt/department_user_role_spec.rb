require 'rails_helper'

RSpec.describe Sate::Crmgt::DepartmentUserRole, type: :model do
  it 'has a valid factory' do
    expect(create(:sate_crmgt_department_user_role)).to be_valid
  end

  it 'is invalid with no department, user role, and application module' do
    expect(build(:sate_crmgt_department_user_role, department_id: nil)).not_to be_valid
    expect(build(:sate_crmgt_department_user_role, user_role_id: nil)).not_to be_valid
    expect(build(:sate_crmgt_department_user_role, application_module_id: nil)).not_to be_valid
  end

  it 'is invalid with duplicate department user role information' do
    department_role = create(:sate_crmgt_department_user_role)
    expect(build(:sate_crmgt_department_user_role, department_id: department_role.department_id,
                 user_role_id: department_role.user_role_id,
                 application_module_id: department_role.application_module_id
           )).not_to be_valid
  end
end
