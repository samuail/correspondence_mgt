require 'rails_helper'

RSpec.describe Sate::Crmgt::ManuscriptComment, type: :model do
  it 'has a valid factory' do
    expect(create(:sate_crmgt_manuscript_comment)).to be_valid
  end

  it 'is invalid with no commented by user, manuscript, content, comment date, and application module' do
    expect(build(:sate_crmgt_manuscript_comment, commented_by_id: nil)).not_to be_valid
    expect(build(:sate_crmgt_manuscript_comment, manuscript: nil)).not_to be_valid
    expect(build(:sate_crmgt_manuscript_comment, comment_date: nil)).not_to be_valid
    expect(build(:sate_crmgt_manuscript_comment, application_module_id: nil)).not_to be_valid
  end

  it 'is invalid with duplicate manuscript comment information' do
    comment = create(:sate_crmgt_manuscript_comment)
    expect(build(:sate_crmgt_manuscript_comment, commented_by_id: comment.commented_by_id,
                 manuscript: comment.manuscript, content: comment.content,
                 comment_date: comment.comment_date,
                 application_module_id: comment.application_module_id
           )).not_to be_valid
  end
end
