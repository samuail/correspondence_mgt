require 'rails_helper'

RSpec.describe Sate::Crmgt::CorrespondenceCarbonCopy, type: :model do
  it 'has a valid factory for Internal Correspondence' do
    expect(create(:sate_crmgt_correspondence_carbon_copy, :for_internal_correspondence)).to be_valid
  end

  it 'has a valid factory for External Incoming Correspondence' do
    expect(create(:sate_crmgt_correspondence_carbon_copy, :for_external_incoming_correspondence)).to be_valid
  end

  it 'is invalid with no correspondence, destination department, and application module' do
    expect(build(:sate_crmgt_correspondence_carbon_copy, correspondence: nil)).not_to be_valid
    expect(build(:sate_crmgt_correspondence_carbon_copy, destination_id: nil)).not_to be_valid
    expect(build(:sate_crmgt_correspondence_carbon_copy, application_module_id: nil)).not_to be_valid
  end

  it 'is invalid with duplicate internal correspondence carbon copy information' do
    internal = create(:sate_crmgt_correspondence_carbon_copy, :for_internal_correspondence)
    expect(build(:sate_crmgt_correspondence_carbon_copy, correspondence: internal.correspondence,
                 destination_id: internal.destination_id,
                 application_module_id: internal.application_module_id)).not_to be_valid
  end

  it 'is invalid with duplicate external incoming correspondence carbon copy information' do
    external = create(:sate_crmgt_correspondence_carbon_copy, :for_external_incoming_correspondence)
    expect(build(:sate_crmgt_correspondence_carbon_copy, correspondence: external.correspondence,
                 destination_id: external.destination_id,
                 application_module_id: external.application_module_id)).not_to be_valid
  end
end