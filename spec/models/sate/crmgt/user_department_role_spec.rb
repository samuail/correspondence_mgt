require 'rails_helper'

RSpec.describe Sate::Crmgt::UserDepartmentRole, type: :model do
  it 'has a valid factory' do
    expect(create(:sate_crmgt_user_department_role)).to be_valid
  end

  it 'is invalid with no user, department, user role, and application module' do
    expect(build(:sate_crmgt_user_department_role, user_id: nil)).not_to be_valid
    expect(build(:sate_crmgt_user_department_role, department_user_role_id: nil)).not_to be_valid
    expect(build(:sate_crmgt_user_department_role, application_module_id: nil)).not_to be_valid
  end

  it 'is invalid with duplicate department user role information' do
    user_department_role = create(:sate_crmgt_user_department_role)
    expect(build(:sate_crmgt_user_department_role, user_id: user_department_role.user_id,
                 department_user_role_id: user_department_role.department_user_role_id,
                 application_module_id: user_department_role.application_module_id
           )).not_to be_valid
  end
end