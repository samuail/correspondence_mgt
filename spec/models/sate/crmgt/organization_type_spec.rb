require 'rails_helper'

RSpec.describe Sate::Crmgt::OrganizationType, type: :model do
  it 'has a valid factory' do
    expect(create(:sate_crmgt_organization_type)).to be_valid
  end
end
