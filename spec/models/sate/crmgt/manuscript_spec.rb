require 'rails_helper'

RSpec.describe Sate::Crmgt::Manuscript, type: :model do
  it 'has a valid factory' do
    expect(create(:sate_crmgt_manuscript)).to be_valid
  end

  it 'is invalid with no reference no, received date, title, destination, key in by, and application module' do
    expect(build(:sate_crmgt_manuscript, reference_no: nil)).not_to be_valid
    expect(build(:sate_crmgt_manuscript, received_date: nil)).not_to be_valid
    expect(build(:sate_crmgt_manuscript, title: nil)).not_to be_valid
    expect(build(:sate_crmgt_manuscript, destination_id: nil)).not_to be_valid
    expect(build(:sate_crmgt_manuscript, key_in_by_id: nil)).not_to be_valid
    expect(build(:sate_crmgt_manuscript, application_module_id: nil)).not_to be_valid
  end

  it 'is invalid with duplication reference no' do
    manuscript = create(:sate_crmgt_manuscript)
    expect(build(:sate_crmgt_manuscript,
                 reference_no: manuscript.reference_no)).not_to be_valid
  end

  it 'is invalid with duplication title' do
    manuscript = create(:sate_crmgt_manuscript)
    expect(build(:sate_crmgt_manuscript,
                 title: manuscript.title)).not_to be_valid
  end

  it 'is invalid with duplicate Manuscript information' do
    manuscript = create(:sate_crmgt_manuscript)
    expect(build(:sate_crmgt_manuscript, reference_no: manuscript.reference_no,
                 received_date: manuscript.received_date, title: manuscript.title,
                 destination_id: manuscript.destination_id,
                 application_module_id: manuscript.application_module_id
           )).not_to be_valid
  end
end
