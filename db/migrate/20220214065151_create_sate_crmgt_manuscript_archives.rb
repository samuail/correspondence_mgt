class CreateSateCrmgtManuscriptArchives < ActiveRecord::Migration[6.0]
  def change
    create_table :sate_crmgt_manuscript_archives do |t|
      t.references :manuscript, null: false, index: false
      t.string :remark
      t.integer :no_pages
      t.integer :no_copies, null: false
      t.boolean :has_archive_att, default: false
      t.datetime :archived_date
      t.integer :archived_by_id, null: false
      t.references :application_module, null: false, index: false

      t.timestamps
      t.index [:application_module_id], name: 'index_manuscript_arch_on_application_module_id'
    end
    add_index :sate_crmgt_manuscript_archives,
              [:manuscript_id, :application_module_id],
              { unique: true, name: 'unique_archive_details_for_a_manuscript_on_app_mod_idx'}
    add_foreign_key :sate_crmgt_manuscript_archives, :sate_crmgt_manuscripts, column: :manuscript_id
    add_foreign_key :sate_crmgt_manuscript_archives, :sate_auth_users, column: :archived_by_id
    add_foreign_key :sate_crmgt_manuscript_archives, :sate_auth_application_modules,
                    column: :application_module_id
  end
end
