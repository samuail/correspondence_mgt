class CreateSateCrmgtDepartments < ActiveRecord::Migration[6.0]
  def change
    create_table :sate_crmgt_departments do |t|
      t.string :code, null: false
      t.string :description, null: false
      t.references :department_type, index: true
      t.integer :parent_id, index: true
      t.references :application_module, index: true

      t.timestamps
    end
    add_index :sate_crmgt_departments,
              [:code, :description, :department_type_id, :application_module_id],
              { unique: true, name: 'unique_code_desc_dept_type_on_app_module_indx' }
    add_foreign_key :sate_crmgt_departments, :sate_crmgt_lookups, column: :department_type_id
    add_foreign_key :sate_crmgt_departments, :sate_crmgt_departments, column: :parent_id
    add_foreign_key :sate_crmgt_departments, :sate_auth_application_modules,
                    column: :application_module_id
  end
end
