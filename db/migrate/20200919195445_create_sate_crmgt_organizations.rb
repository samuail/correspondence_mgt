class CreateSateCrmgtOrganizations < ActiveRecord::Migration[6.0]
  def change
    create_table :sate_crmgt_organizations do |t|
      t.string :code, null: false
      t.string :description, null: false
      t.references :organization_type, index: true
      t.references :application_module, index: true

      t.timestamps
    end
    add_index :sate_crmgt_organizations,
              [:code, :description, :organization_type_id, :application_module_id],
              { unique: true, name: 'unique_code_desc_org_type_on_app_module_indx' }
    add_foreign_key :sate_crmgt_organizations, :sate_crmgt_lookups, column: :organization_type_id
    add_foreign_key :sate_crmgt_organizations, :sate_auth_application_modules,
                    column: :application_module_id
  end
end
