class AddRejectionRemarkToCorrespondenceAssignment < ActiveRecord::Migration[6.0]
  def change
    add_column :sate_crmgt_correspondence_assignments, :rejection_remark, :string
  end
end
