class CreateSateCrmgtCorrespondenceCarbonCopies < ActiveRecord::Migration[6.0]
  def change
    create_table :sate_crmgt_correspondence_carbon_copies do |t|
      t.references :correspondence, null: false, polymorphic: true, index: false
      t.integer :destination_id, null: false, index: true, foreign_key: true
      t.references :application_module, null: false, index: false

      t.timestamps
      t.index [:application_module_id], name: 'index_correspondence_ccs_on_application_module_id'
    end
    add_index :sate_crmgt_correspondence_carbon_copies,
              [:correspondence_id, :correspondence_type, :destination_id, :application_module_id],
              { unique: true, name: 'unique_correspondence_cc_on_dest_dept_app_module_indx'}
    add_foreign_key :sate_crmgt_correspondence_carbon_copies, :sate_crmgt_departments, column: :destination_id
    add_foreign_key :sate_crmgt_correspondence_carbon_copies, :sate_auth_application_modules,
                    column: :application_module_id
  end
end
