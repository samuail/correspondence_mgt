class AddHasCcToExternalOutgoingCorrespondence < ActiveRecord::Migration[6.0]
  def change
    add_column :sate_crmgt_external_outgoing_correspondences,
               :has_cc, :boolean, default: false
  end
end
