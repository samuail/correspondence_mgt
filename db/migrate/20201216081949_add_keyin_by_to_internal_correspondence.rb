class AddKeyinByToInternalCorrespondence < ActiveRecord::Migration[6.0]
  def change
    add_column :sate_crmgt_internal_correspondences, :key_in_by_id, :integer

    reversible do |change|
      change.up do
        first_User = Sate::Auth::User.where(first_name: "System", last_name: "Administrator").first
        Sate::Crmgt::InternalCorrespondence.find_each do |item|
          item.key_in_by_id = first_User.id
          item.save
        end

      end
    end

    change_column_null :sate_crmgt_internal_correspondences, :key_in_by_id, false

    add_foreign_key :sate_crmgt_internal_correspondences, :sate_auth_users, column: :key_in_by_id
  end
end
