class CreateSateCrmgtCorrespondenceComments < ActiveRecord::Migration[6.0]
  def change
    create_table :sate_crmgt_correspondence_comments do |t|
      t.integer :commented_by_id, null: false
      t.references :correspondence, null: false, polymorphic: true, index: false
      t.string :content, null: false
      t.integer :order, null: false
      t.datetime :comment_date, null: false
      t.references :application_module, null: false, index: false

      t.timestamps
      t.index [:application_module_id], name: 'index_correspondence_comments_on_application_module_id'
    end
    add_index :sate_crmgt_correspondence_comments,
              [:commented_by_id, :correspondence_id, :correspondence_type, :content, :application_module_id],
              { unique: true, name: 'unique_correspondence_comment_content_on_app_module_indx'}
    add_foreign_key :sate_crmgt_correspondence_comments, :sate_auth_users, column: :commented_by_id
    add_foreign_key :sate_crmgt_correspondence_comments, :sate_auth_application_modules,
                    column: :application_module_id
  end
end
