class RemoveArchiveDetailsFromManuscript < ActiveRecord::Migration[6.0]
  def change
    remove_column :sate_crmgt_manuscripts, :remark, :string
    remove_column :sate_crmgt_manuscripts, :no_documents, :integer
    remove_column :sate_crmgt_manuscripts, :has_archive_att, :boolean
    remove_column :sate_crmgt_manuscripts, :archived_date, :datetime
    remove_column :sate_crmgt_manuscripts, :archived_by_id, :integer
  end
end
