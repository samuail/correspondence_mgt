class AddBankNameToAuthor < ActiveRecord::Migration[6.0]
  def change
    add_column :sate_crmgt_authors,
               :bank_name, :string, default: 'Commercial Bank of Ethiopia'
  end
end
