class AddHasContactToAuthor < ActiveRecord::Migration[6.0]
  def change
    add_column :sate_crmgt_authors,
               :has_contact, :boolean, default: false
  end
end
