class CreateSateCrmgtExternalOutgoingCorrespondences < ActiveRecord::Migration[6.0]
  def change
    create_table :sate_crmgt_external_outgoing_correspondences do |t|
      t.string :reference_no, null: false, unique: true
      t.date :letter_date, null: false
      t.string :subject, null: false
      t.string :search_keywords, array: true
      t.integer :source_id, null: false, index: true, foreign_key: true
      t.integer :destination_id, null: false, index: false, foreign_key: true
      t.date :received_date, null: false
      t.string :status, default: "New"
      t.references :application_module, null: false, index: false

      t.timestamps
      t.index [:application_module_id], name: 'index_external_outgoing_corresp_on_app_module_id'
      t.index [:destination_id], name: 'index_external_outgoing_corresp_on_destination_id'
    end
    add_index :sate_crmgt_external_outgoing_correspondences,
              [:reference_no, :letter_date, :subject, :source_id, :destination_id, :application_module_id],
              { unique: true, name: 'unique_reference_no_letter_date_on_app_module_ext_outgoing_indx' }
    add_foreign_key :sate_crmgt_external_outgoing_correspondences, :sate_crmgt_departments, column: :source_id
    add_foreign_key :sate_crmgt_external_outgoing_correspondences, :sate_crmgt_organizations, column: :destination_id
    add_foreign_key :sate_crmgt_external_outgoing_correspondences, :sate_auth_application_modules,
                    column: :application_module_id
  end
end
