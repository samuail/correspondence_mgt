class AddToAssigneeReferenceToManuscriptAssignment < ActiveRecord::Migration[6.0]
  def change
    add_reference :sate_crmgt_manuscript_assignments,
                  :assignee, null: false, polymorphic: true, index: false
    add_index :sate_crmgt_manuscript_assignments,
              [:assignee_id], name: 'index_manuscript_assignments_on_assignee_id'
    add_index :sate_crmgt_manuscript_assignments,
              [:assignee_type], name: 'index_manuscript_assignments_on_assignee_type'
  end
end
