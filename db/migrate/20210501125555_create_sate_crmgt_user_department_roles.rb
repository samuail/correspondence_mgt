class CreateSateCrmgtUserDepartmentRoles < ActiveRecord::Migration[6.0]
  def change
    create_table :sate_crmgt_user_department_roles do |t|
      t.references :user, null: false
      t.references :department_user_role, null: false, index: false
      t.references :application_module, null: false

      t.timestamps

      t.index [:department_user_role_id], name: 'index_user_department_role_on_department_role_id'
    end

    add_index :sate_crmgt_user_department_roles,
              [:user_id, :department_user_role_id, :application_module_id],
              { unique: true, name: 'unique_user_department_user_role_on_app_module_indx' }
    add_foreign_key :sate_crmgt_user_department_roles, :sate_auth_users,
                    column: :user_id
    add_foreign_key :sate_crmgt_user_department_roles, :sate_crmgt_department_user_roles,
                    column: :department_user_role_id
    add_foreign_key :sate_crmgt_user_department_roles, :sate_auth_application_modules,
                    column: :application_module_id
  end
end