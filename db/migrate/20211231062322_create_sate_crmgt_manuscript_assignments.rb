class CreateSateCrmgtManuscriptAssignments < ActiveRecord::Migration[6.0]
  def change
    create_table :sate_crmgt_manuscript_assignments do |t|
      t.string :stage, null: false
      t.integer :from_user_id, null: false
      t.integer :to_internal_user_id
      t.string :to_external_user
      t.references :manuscript, null: false
      t.string :status
      t.datetime :assigned_date, null: false
      t.datetime :received_date, null: false
      t.references :application_module, null: false, index: false

      t.timestamps
      t.index [:application_module_id],
              name: 'index_manuscript_assignments_on_application_module_id'
    end
    add_index :sate_crmgt_manuscript_assignments,
              [:stage, :from_user_id, :to_internal_user_id,
               :to_external_user, :manuscript_id, :application_module_id],
              { unique: true, name: 'unique_manuscript_from_to_user_on_app_module_indx'}
    add_foreign_key :sate_crmgt_manuscript_assignments, :sate_auth_users, column: :from_user_id
    add_foreign_key :sate_crmgt_manuscript_assignments, :sate_auth_users, column: :to_internal_user_id
    add_foreign_key :sate_crmgt_manuscript_assignments, :sate_auth_application_modules,
                    column: :application_module_id
  end
end
