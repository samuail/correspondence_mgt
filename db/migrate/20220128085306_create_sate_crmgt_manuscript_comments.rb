class CreateSateCrmgtManuscriptComments < ActiveRecord::Migration[6.0]
  def change
    create_table :sate_crmgt_manuscript_comments do |t|
      t.integer :commented_by_id, null: false
      t.references :manuscript, null: false
      t.string :content
      t.datetime :comment_date, null: false
      t.references :application_module, null: false, index: false

      t.timestamps
      t.index [:application_module_id],
              name: 'index_manuscript_comments_on_application_module_id'
    end
    add_index :sate_crmgt_manuscript_comments,
              [:commented_by_id, :manuscript_id,
               :content, :comment_date, :application_module_id],
              { unique: true, name: 'unique_manuscript_comment_content_on_app_module_indx'}
    add_foreign_key :sate_crmgt_manuscript_comments, :sate_auth_users, column: :commented_by_id
    add_foreign_key :sate_crmgt_manuscript_comments, :sate_crmgt_manuscripts, column: :manuscript_id
    add_foreign_key :sate_crmgt_manuscript_comments, :sate_auth_application_modules,
                    column: :application_module_id
  end
end
