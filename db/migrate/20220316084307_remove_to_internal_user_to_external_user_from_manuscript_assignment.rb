class RemoveToInternalUserToExternalUserFromManuscriptAssignment < ActiveRecord::Migration[6.0]
  def change
    remove_column :sate_crmgt_manuscript_assignments,
                  :to_internal_user_id, :integer
    remove_column :sate_crmgt_manuscript_assignments,
                  :to_external_user, :string
  end
end
