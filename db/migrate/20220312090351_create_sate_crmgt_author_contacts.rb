class CreateSateCrmgtAuthorContacts < ActiveRecord::Migration[6.0]
  def change
    create_table :sate_crmgt_author_contacts do |t|
      t.references :author, null: false, index: false
      t.string :first_name, null: false
      t.string :father_name, null: false
      t.string :email, null: false
      t.string :telephone, null: false
      t.references :application_module, null: false, index: false

      t.timestamps
    end
    add_index :sate_crmgt_author_contacts,
              [:author_id, :first_name, :father_name, :email, :telephone, :application_module_id],
              { unique: true, name: 'unique_author_contact_detail_on_app_module_indx' }
    add_foreign_key :sate_crmgt_author_contacts, :sate_crmgt_authors, column: :author_id
    add_foreign_key :sate_crmgt_author_contacts, :sate_auth_application_modules,
                    column: :application_module_id
  end
end
