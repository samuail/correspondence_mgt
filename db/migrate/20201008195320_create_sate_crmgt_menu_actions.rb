class CreateSateCrmgtMenuActions < ActiveRecord::Migration[6.0]
  def change
    create_table :sate_crmgt_menu_actions do |t|
      t.references :menu, null: false, index: true
      t.references :action, null: false, index: true
      t.references :application_module, null: false, index: true

      t.timestamps
    end
    add_index :sate_crmgt_menu_actions,
              [:menu_id, :action_id, :application_module_id],
              { unique: true, name: 'unique_menu_action_on_app_module_indx' }
    add_foreign_key :sate_crmgt_menu_actions, :sate_auth_menus, column: :menu_id
    add_foreign_key :sate_crmgt_menu_actions, :sate_crmgt_lookups, column: :action_id
    add_foreign_key :sate_crmgt_menu_actions, :sate_auth_application_modules,
                    column: :application_module_id
  end
end
