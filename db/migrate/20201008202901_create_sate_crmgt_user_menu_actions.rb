class CreateSateCrmgtUserMenuActions < ActiveRecord::Migration[6.0]
  def change
    create_table :sate_crmgt_user_menu_actions do |t|
      t.references :user, null: false, index: true
      t.references :menu_action, null: false, index: true
      t.boolean :active, default: true
      t.references :application_module, null: false, index: true

      t.timestamps
    end
    add_index :sate_crmgt_user_menu_actions,
              [:user_id, :menu_action_id, :application_module_id],
              { unique: true, name: 'unique_user_menu_action_on_app_module_indx' }
    add_foreign_key :sate_crmgt_user_menu_actions, :sate_auth_users, column: :user_id
    add_foreign_key :sate_crmgt_user_menu_actions, :sate_crmgt_menu_actions, column: :menu_action_id
    add_foreign_key :sate_crmgt_user_menu_actions, :sate_auth_application_modules,
                    column: :application_module_id
  end
end
