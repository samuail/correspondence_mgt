class CreateSateCrmgtCorrespondenceAssignments < ActiveRecord::Migration[6.0]
  def change
    create_table :sate_crmgt_correspondence_assignments do |t|
      t.integer :from_user_id, null: false
      t.integer :to_user_id, null: false
      t.references :correspondence, null: false, polymorphic: true, index: false
      t.integer :order, null: false
      t.string :status, null: false
      t.datetime :assigned_date, null: false
      t.datetime :received_date, null: false
      t.references :application_module, null: false, index: false

      t.timestamps
      t.index [:application_module_id], name: 'index_correspondence_assignments_on_application_module_id'
    end
    add_index :sate_crmgt_correspondence_assignments,
              [:from_user_id, :to_user_id, :correspondence_id, :correspondence_type, :application_module_id],
              { unique: true, name: 'unique_correspondence_from_to_user_on_app_module_indx'}
    add_foreign_key :sate_crmgt_correspondence_assignments, :sate_auth_users, column: :from_user_id
    add_foreign_key :sate_crmgt_correspondence_assignments, :sate_auth_users, column: :to_user_id
    add_foreign_key :sate_crmgt_correspondence_assignments, :sate_auth_application_modules,
                    column: :application_module_id
  end
end
