class AddDocReferenceNoAmountRemarkToExternalIncomingCorrespondence < ActiveRecord::Migration[6.0]
  def change
    add_column :sate_crmgt_external_incoming_correspondences,
               :doc_reference_no, :string
    add_column :sate_crmgt_external_incoming_correspondences,
               :amount, :decimal, precision: 12, scale: 2
    add_column :sate_crmgt_external_incoming_correspondences,
               :remark, :string
    add_column :sate_crmgt_external_incoming_correspondences,
               :no_documents, :integer
    add_column :sate_crmgt_external_incoming_correspondences,
               :archived_date, :datetime
  end
end
