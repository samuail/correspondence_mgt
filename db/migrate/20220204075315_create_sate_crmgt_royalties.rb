class CreateSateCrmgtRoyalties < ActiveRecord::Migration[6.0]
  def change
    create_table :sate_crmgt_royalties do |t|
      t.references :manuscript, null: false, index: false
      t.references :author, null: false, index: false
      t.string :reference_no, null: false
      t.decimal :amount, null: false, precision: 12, scale: 2
      t.string :remark
      t.datetime :payment_date, null: false
      t.integer :key_in_by_id, null: false
      t.references :application_module, null: false, index: false

      t.timestamps
    end
    add_index :sate_crmgt_royalties,
              [:manuscript_id, :author_id, :reference_no, :amount, :remark, :payment_date, :application_module_id],
              {unique: true, name: 'unique_royalty_detail_double_save_indx'}
    add_foreign_key :sate_crmgt_royalties, :sate_crmgt_manuscripts, column: :manuscript_id
    add_foreign_key :sate_crmgt_royalties, :sate_crmgt_authors, column: :author_id
    add_foreign_key :sate_crmgt_royalties, :sate_auth_users, column: :key_in_by_id
    add_foreign_key :sate_crmgt_royalties, :sate_auth_application_modules,
                    column: :application_module_id
  end
end
