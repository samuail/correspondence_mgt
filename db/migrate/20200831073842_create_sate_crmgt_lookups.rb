class CreateSateCrmgtLookups < ActiveRecord::Migration[6.0]
  def change
    create_table :sate_crmgt_lookups do |t|
      t.string :code, null: false
      t.string :name, null: false
      t.string :type, null: false
      t.references :application_module, index: true

      t.timestamps
    end
    add_index :sate_crmgt_lookups, [:code, :name, :type], { :unique => true }
    add_foreign_key :sate_crmgt_lookups, :sate_auth_application_modules,
                    column: :application_module_id
  end
end
