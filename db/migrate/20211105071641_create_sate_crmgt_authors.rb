class CreateSateCrmgtAuthors < ActiveRecord::Migration[6.0]
  def change
    create_table :sate_crmgt_authors do |t|
      t.string :first_name, null: false
      t.string :father_name, null: false
      t.string :grand_father_name, null: false
      t.string :email, null: false
      t.string :telephone, null: false
      t.references :application_module, index: true

      t.timestamps
    end
    add_index :sate_crmgt_authors,
              [:first_name, :father_name, :grand_father_name, :application_module_id],
              { unique: true, name: 'unique_full_name_on_app_module_indx' }
    add_foreign_key :sate_crmgt_authors, :sate_auth_application_modules,
                    column: :application_module_id
  end
end
