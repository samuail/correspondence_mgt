class RemoveUniqueAndAddUniqueOnCorrespondenceAssignment < ActiveRecord::Migration[6.0]
  def change
    remove_index :sate_crmgt_correspondence_assignments,
                 name: 'unique_correspondence_from_to_user_on_app_module_indx'
    add_index :sate_crmgt_correspondence_assignments,
              [:from_user_id, :to_user_id, :order, :correspondence_id, :correspondence_type, :application_module_id],
              { unique: true, name: 'unique_correspondence_from_to_user_order_on_app_module_indx'}
  end
end
