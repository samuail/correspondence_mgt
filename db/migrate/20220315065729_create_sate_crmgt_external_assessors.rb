class CreateSateCrmgtExternalAssessors < ActiveRecord::Migration[6.0]
  def change
    create_table :sate_crmgt_external_assessors do |t|
      t.string :title
      t.string :first_name, null: false
      t.string :father_name, null: false
      t.string :grand_father_name, null: false
      t.string :email, null: false
      t.string :telephone, null: false
      t.string :area_of_expertise, null: false
      t.string :institution
      t.string :tin, null: false
      t.string :bank_acc, null: false
      t.references :application_module, null: false, index: false

      t.timestamps
    end
    add_index :sate_crmgt_external_assessors,
              [:first_name, :father_name, :grand_father_name, :email, :telephone, :application_module_id],
              { unique: true, name: 'unique_external_assessor_detail_on_app_module_indx' }
    add_foreign_key :sate_crmgt_external_assessors, :sate_auth_application_modules,
                    column: :application_module_id
  end
end
