class AddHasCcToInternalCorrespondence < ActiveRecord::Migration[6.0]
  def change
    add_column :sate_crmgt_internal_correspondences,
               :has_cc, :boolean, default: false
  end
end
