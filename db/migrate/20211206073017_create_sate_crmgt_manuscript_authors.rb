class CreateSateCrmgtManuscriptAuthors < ActiveRecord::Migration[6.0]
  def change
    create_table :sate_crmgt_manuscript_authors do |t|
      t.references :manuscript, null: false, index: false
      t.references :author, null: false, index: false
      t.references :application_module, null: false, index: false

      t.timestamps
    end
    add_index :sate_crmgt_manuscript_authors,
              [:manuscript_id, :author_id, :application_module_id],
              { unique: true, name: 'unique_manuscript_author_on_app_module_indx' }
    add_foreign_key :sate_crmgt_manuscript_authors, :sate_crmgt_manuscripts, column: :manuscript_id
    add_foreign_key :sate_crmgt_manuscript_authors, :sate_crmgt_authors, column: :author_id
    add_foreign_key :sate_crmgt_manuscript_authors, :sate_auth_application_modules,
                    column: :application_module_id
  end
end
