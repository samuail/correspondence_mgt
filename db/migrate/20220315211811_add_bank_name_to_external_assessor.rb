class AddBankNameToExternalAssessor < ActiveRecord::Migration[6.0]
  def change
    add_column :sate_crmgt_external_assessors,
               :bank_name, :string, :null => false
  end
end
