class CreateSateCrmgtManuscripts < ActiveRecord::Migration[6.0]
  def change
    create_table :sate_crmgt_manuscripts do |t|
      t.string :reference_no, null: false, unique: true
      t.datetime :received_date, null: false
      t.string :title, null: false, unique: true
      t.string :search_keywords, array: true
      t.integer :destination_id, null: false, index: true, foreign_key: true
      t.integer :key_in_by_id, null: false
      t.string :status, default: "New"
      t.references :application_module, null: false, index: false

      t.timestamps
      t.index [:application_module_id], name: 'index_manuscript_on_application_module_id'
    end
    add_index :sate_crmgt_manuscripts,
              [:reference_no, :title, :destination_id, :application_module_id],
              { unique: true, name: 'unique_reference_no_title_on_app_module_indx' }
    add_foreign_key :sate_crmgt_manuscripts, :sate_crmgt_departments, column: :destination_id
    add_foreign_key :sate_crmgt_manuscripts, :sate_auth_users, column: :key_in_by_id
    add_foreign_key :sate_crmgt_manuscripts, :sate_auth_application_modules,
                    column: :application_module_id
  end
end
