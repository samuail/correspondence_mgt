class AddCurrentStageToManuscript < ActiveRecord::Migration[6.0]
  def change
    add_column :sate_crmgt_manuscripts, :current_stage, :string
  end
end
