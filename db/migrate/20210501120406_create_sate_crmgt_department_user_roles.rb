class CreateSateCrmgtDepartmentUserRoles < ActiveRecord::Migration[6.0]
  def change
    create_table :sate_crmgt_department_user_roles do |t|
      t.references :department, null: false
      t.references :user_role, null: false
      t.boolean :is_head, default: false
      t.references :application_module, null: false

      t.timestamps
    end

    add_index :sate_crmgt_department_user_roles,
              [:department_id, :user_role_id, :application_module_id],
              { unique: true, name: 'unique_department_role_on_app_module_indx' }
    add_foreign_key :sate_crmgt_department_user_roles, :sate_auth_user_roles, column: :user_role_id
    add_foreign_key :sate_crmgt_department_user_roles, :sate_crmgt_departments, column: :department_id
    add_foreign_key :sate_crmgt_department_user_roles, :sate_auth_application_modules,
                    column: :application_module_id
  end
end
