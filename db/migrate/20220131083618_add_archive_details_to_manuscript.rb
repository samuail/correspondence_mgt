class AddArchiveDetailsToManuscript < ActiveRecord::Migration[6.0]
  def change
    add_column :sate_crmgt_manuscripts, :remark, :string
    add_column :sate_crmgt_manuscripts, :no_documents, :integer
    add_column :sate_crmgt_manuscripts, :has_archive_att, :boolean, default: false
    add_column :sate_crmgt_manuscripts, :archived_date, :datetime
    add_column :sate_crmgt_manuscripts, :archived_by_id, :integer, null: true

    add_foreign_key :sate_crmgt_manuscripts,
                    :sate_auth_users, column: :archived_by_id
  end
end
