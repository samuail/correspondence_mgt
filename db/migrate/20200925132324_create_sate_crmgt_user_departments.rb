class CreateSateCrmgtUserDepartments < ActiveRecord::Migration[6.0]
  def change
    create_table :sate_crmgt_user_departments do |t|
      t.references :user, null: false
      t.references :department, null: false
      t.references :user_role, null: false
      t.references :application_module, null: false

      t.timestamps
    end

    add_index :sate_crmgt_user_departments,
              [:user_id, :department_id, :user_role_id, :application_module_id],
              { unique: true, name: 'unique_user_department_role_on_app_module_indx' }
    add_foreign_key :sate_crmgt_user_departments, :sate_auth_users, column: :user_id
    add_foreign_key :sate_crmgt_user_departments, :sate_auth_user_roles, column: :user_role_id
    add_foreign_key :sate_crmgt_user_departments, :sate_crmgt_departments, column: :department_id
    add_foreign_key :sate_crmgt_user_departments, :sate_auth_application_modules,
                    column: :application_module_id
  end
end
