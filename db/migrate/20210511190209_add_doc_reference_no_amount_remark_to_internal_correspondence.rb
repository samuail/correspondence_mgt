class AddDocReferenceNoAmountRemarkToInternalCorrespondence < ActiveRecord::Migration[6.0]
  def change
    add_column :sate_crmgt_internal_correspondences,
               :doc_reference_no, :string
    add_column :sate_crmgt_internal_correspondences,
               :amount, :decimal, precision: 12, scale: 2
    add_column :sate_crmgt_internal_correspondences,
               :remark, :string
    add_column :sate_crmgt_internal_correspondences,
               :no_documents, :integer
    add_column :sate_crmgt_internal_correspondences,
               :archived_date, :datetime
  end
end
