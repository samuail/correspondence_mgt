class AddBankAccTinToAuthor < ActiveRecord::Migration[6.0]
  def change
    add_column :sate_crmgt_authors, :bank_acc, :string, :null => true
    add_column :sate_crmgt_authors, :tin, :string, :null => true
  end
end
