# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_03_16_084307) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "sate_auth_application_modules", force: :cascade do |t|
    t.string "code", limit: 6, null: false
    t.string "name", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["code"], name: "index_sate_auth_application_modules_on_code", unique: true
  end

  create_table "sate_auth_menus", force: :cascade do |t|
    t.string "text", limit: 50, null: false
    t.string "icon_cls"
    t.string "class_name"
    t.string "location"
    t.integer "parent_id"
    t.bigint "application_module_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["application_module_id"], name: "index_sate_auth_menus_on_application_module_id"
    t.index ["parent_id"], name: "index_sate_auth_menus_on_parent_id"
    t.index ["text", "application_module_id"], name: "index_sate_auth_menus_on_text_and_application_module_id", unique: true
  end

  create_table "sate_auth_user_menus", id: false, force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "menu_id"
    t.index ["menu_id"], name: "index_sate_auth_user_menus_on_menu_id"
    t.index ["user_id", "menu_id"], name: "index_user_menu_are_unique", unique: true
    t.index ["user_id"], name: "index_sate_auth_user_menus_on_user_id"
  end

  create_table "sate_auth_user_roles", force: :cascade do |t|
    t.string "name", limit: 50, null: false
    t.bigint "application_module_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["application_module_id"], name: "index_sate_auth_user_roles_on_application_module_id"
    t.index ["name", "application_module_id"], name: "index_sate_auth_user_roles_on_name_and_application_module_id", unique: true
  end

  create_table "sate_auth_user_user_roles", id: false, force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "user_role_id"
    t.index ["user_id", "user_role_id"], name: "index_user_user_role_are_unique", unique: true
    t.index ["user_id"], name: "index_sate_auth_user_user_roles_on_user_id"
    t.index ["user_role_id"], name: "index_sate_auth_user_user_roles_on_user_role_id"
  end

  create_table "sate_auth_users", force: :cascade do |t|
    t.string "first_name", limit: 50, null: false
    t.string "last_name", limit: 50, null: false
    t.string "email", limit: 50, null: false
    t.string "password_digest", null: false
    t.boolean "active", default: true, null: false
    t.bigint "application_module_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["application_module_id"], name: "index_sate_auth_users_on_application_module_id"
    t.index ["email"], name: "index_sate_auth_users_on_email", unique: true
  end

  create_table "sate_crmgt_author_contacts", force: :cascade do |t|
    t.bigint "author_id", null: false
    t.string "first_name", null: false
    t.string "father_name", null: false
    t.string "email", null: false
    t.string "telephone", null: false
    t.bigint "application_module_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["author_id", "first_name", "father_name", "email", "telephone", "application_module_id"], name: "unique_author_contact_detail_on_app_module_indx", unique: true
  end

  create_table "sate_crmgt_authors", force: :cascade do |t|
    t.string "first_name", null: false
    t.string "father_name", null: false
    t.string "grand_father_name", null: false
    t.string "email", null: false
    t.string "telephone", null: false
    t.bigint "application_module_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "bank_acc"
    t.string "tin"
    t.boolean "has_contact", default: false
    t.string "bank_name", default: "Commercial Bank of Ethiopia"
    t.index ["application_module_id"], name: "index_sate_crmgt_authors_on_application_module_id"
    t.index ["first_name", "father_name", "grand_father_name", "application_module_id"], name: "unique_full_name_on_app_module_indx", unique: true
  end

  create_table "sate_crmgt_correspondence_assignments", force: :cascade do |t|
    t.integer "from_user_id", null: false
    t.integer "to_user_id", null: false
    t.string "correspondence_type", null: false
    t.bigint "correspondence_id", null: false
    t.integer "order", null: false
    t.string "status", null: false
    t.datetime "assigned_date", null: false
    t.datetime "received_date", null: false
    t.bigint "application_module_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "rejection_remark"
    t.index ["application_module_id"], name: "index_correspondence_assignments_on_application_module_id"
    t.index ["from_user_id", "to_user_id", "order", "correspondence_id", "correspondence_type", "application_module_id"], name: "unique_correspondence_from_to_user_order_on_app_module_indx", unique: true
  end

  create_table "sate_crmgt_correspondence_carbon_copies", force: :cascade do |t|
    t.string "correspondence_type", null: false
    t.bigint "correspondence_id", null: false
    t.integer "destination_id", null: false
    t.bigint "application_module_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["application_module_id"], name: "index_correspondence_ccs_on_application_module_id"
    t.index ["correspondence_id", "correspondence_type", "destination_id", "application_module_id"], name: "unique_correspondence_cc_on_dest_dept_app_module_indx", unique: true
    t.index ["destination_id"], name: "index_sate_crmgt_correspondence_carbon_copies_on_destination_id"
  end

  create_table "sate_crmgt_correspondence_comments", force: :cascade do |t|
    t.integer "commented_by_id", null: false
    t.string "correspondence_type", null: false
    t.bigint "correspondence_id", null: false
    t.string "content", null: false
    t.integer "order", null: false
    t.datetime "comment_date", null: false
    t.bigint "application_module_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["application_module_id"], name: "index_correspondence_comments_on_application_module_id"
    t.index ["commented_by_id", "correspondence_id", "correspondence_type", "content", "application_module_id"], name: "unique_correspondence_comment_content_on_app_module_indx", unique: true
  end

  create_table "sate_crmgt_department_user_roles", force: :cascade do |t|
    t.bigint "department_id", null: false
    t.bigint "user_role_id", null: false
    t.boolean "is_head", default: false
    t.bigint "application_module_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["application_module_id"], name: "index_sate_crmgt_department_user_roles_on_application_module_id"
    t.index ["department_id", "user_role_id", "application_module_id"], name: "unique_department_role_on_app_module_indx", unique: true
    t.index ["department_id"], name: "index_sate_crmgt_department_user_roles_on_department_id"
    t.index ["user_role_id"], name: "index_sate_crmgt_department_user_roles_on_user_role_id"
  end

  create_table "sate_crmgt_departments", force: :cascade do |t|
    t.string "code", null: false
    t.string "description", null: false
    t.bigint "department_type_id"
    t.integer "parent_id"
    t.bigint "application_module_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["application_module_id"], name: "index_sate_crmgt_departments_on_application_module_id"
    t.index ["code", "description", "department_type_id", "application_module_id"], name: "unique_code_desc_dept_type_on_app_module_indx", unique: true
    t.index ["department_type_id"], name: "index_sate_crmgt_departments_on_department_type_id"
    t.index ["parent_id"], name: "index_sate_crmgt_departments_on_parent_id"
  end

  create_table "sate_crmgt_external_assessors", force: :cascade do |t|
    t.string "title"
    t.string "first_name", null: false
    t.string "father_name", null: false
    t.string "grand_father_name", null: false
    t.string "email", null: false
    t.string "telephone", null: false
    t.string "area_of_expertise", null: false
    t.string "institution"
    t.string "tin", null: false
    t.string "bank_acc", null: false
    t.bigint "application_module_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "bank_name", null: false
    t.index ["first_name", "father_name", "grand_father_name", "email", "telephone", "application_module_id"], name: "unique_external_assessor_detail_on_app_module_indx", unique: true
  end

  create_table "sate_crmgt_external_incoming_correspondences", force: :cascade do |t|
    t.string "reference_no", null: false
    t.date "letter_date", null: false
    t.string "subject", null: false
    t.string "search_keywords", array: true
    t.integer "source_id", null: false
    t.integer "destination_id", null: false
    t.date "received_date", null: false
    t.string "status", default: "New"
    t.bigint "application_module_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "key_in_by_id", null: false
    t.boolean "has_cc", default: false
    t.string "doc_reference_no"
    t.decimal "amount", precision: 12, scale: 2
    t.string "remark"
    t.integer "no_documents"
    t.datetime "archived_date"
    t.index ["application_module_id"], name: "index_external_incoming_corresp_on_app_module_id"
    t.index ["destination_id"], name: "index_external_incoming_corresp_on_destination_id"
    t.index ["reference_no", "letter_date", "subject", "source_id", "destination_id", "application_module_id"], name: "unique_reference_no_letter_date_on_app_module_ext_incoming_indx", unique: true
    t.index ["source_id"], name: "index_sate_crmgt_external_incoming_correspondences_on_source_id"
  end

  create_table "sate_crmgt_external_outgoing_correspondences", force: :cascade do |t|
    t.string "reference_no", null: false
    t.date "letter_date", null: false
    t.string "subject", null: false
    t.string "search_keywords", array: true
    t.integer "source_id", null: false
    t.integer "destination_id", null: false
    t.date "received_date", null: false
    t.string "status", default: "New"
    t.bigint "application_module_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "key_in_by_id", null: false
    t.boolean "has_cc", default: false
    t.index ["application_module_id"], name: "index_external_outgoing_corresp_on_app_module_id"
    t.index ["destination_id"], name: "index_external_outgoing_corresp_on_destination_id"
    t.index ["reference_no", "letter_date", "subject", "source_id", "destination_id", "application_module_id"], name: "unique_reference_no_letter_date_on_app_module_ext_outgoing_indx", unique: true
    t.index ["source_id"], name: "index_sate_crmgt_external_outgoing_correspondences_on_source_id"
  end

  create_table "sate_crmgt_internal_correspondences", force: :cascade do |t|
    t.string "reference_no", null: false
    t.date "letter_date", null: false
    t.string "subject", null: false
    t.string "search_keywords", array: true
    t.integer "source_id", null: false
    t.integer "destination_id", null: false
    t.date "received_date", null: false
    t.string "status", default: "New"
    t.bigint "application_module_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "key_in_by_id", null: false
    t.boolean "has_cc", default: false
    t.string "doc_reference_no"
    t.decimal "amount", precision: 12, scale: 2
    t.string "remark"
    t.integer "no_documents"
    t.datetime "archived_date"
    t.index ["application_module_id"], name: "index_internal_correspondences_on_application_module_id"
    t.index ["destination_id"], name: "index_sate_crmgt_internal_correspondences_on_destination_id"
    t.index ["reference_no", "letter_date", "subject", "source_id", "destination_id", "application_module_id"], name: "unique_reference_no_letter_date_on_app_module_indx", unique: true
    t.index ["source_id"], name: "index_sate_crmgt_internal_correspondences_on_source_id"
  end

  create_table "sate_crmgt_lookups", force: :cascade do |t|
    t.string "code", null: false
    t.string "name", null: false
    t.string "type", null: false
    t.bigint "application_module_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["application_module_id"], name: "index_sate_crmgt_lookups_on_application_module_id"
    t.index ["code", "name", "type"], name: "index_sate_crmgt_lookups_on_code_and_name_and_type", unique: true
  end

  create_table "sate_crmgt_manuscript_archives", force: :cascade do |t|
    t.bigint "manuscript_id", null: false
    t.string "remark"
    t.integer "no_pages"
    t.integer "no_copies", null: false
    t.boolean "has_archive_att", default: false
    t.datetime "archived_date"
    t.integer "archived_by_id", null: false
    t.bigint "application_module_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["application_module_id"], name: "index_manuscript_arch_on_application_module_id"
    t.index ["manuscript_id", "application_module_id"], name: "unique_archive_details_for_a_manuscript_on_app_mod_idx", unique: true
  end

  create_table "sate_crmgt_manuscript_assignments", force: :cascade do |t|
    t.string "stage", null: false
    t.integer "from_user_id", null: false
    t.bigint "manuscript_id", null: false
    t.string "status"
    t.datetime "assigned_date", null: false
    t.datetime "received_date", null: false
    t.bigint "application_module_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "assignee_type", null: false
    t.bigint "assignee_id", null: false
    t.index ["application_module_id"], name: "index_manuscript_assignments_on_application_module_id"
    t.index ["assignee_id"], name: "index_manuscript_assignments_on_assignee_id"
    t.index ["assignee_type"], name: "index_manuscript_assignments_on_assignee_type"
    t.index ["manuscript_id"], name: "index_sate_crmgt_manuscript_assignments_on_manuscript_id"
  end

  create_table "sate_crmgt_manuscript_authors", force: :cascade do |t|
    t.bigint "manuscript_id", null: false
    t.bigint "author_id", null: false
    t.bigint "application_module_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["manuscript_id", "author_id", "application_module_id"], name: "unique_manuscript_author_on_app_module_indx", unique: true
  end

  create_table "sate_crmgt_manuscript_comments", force: :cascade do |t|
    t.integer "commented_by_id", null: false
    t.bigint "manuscript_id", null: false
    t.string "content"
    t.datetime "comment_date", null: false
    t.bigint "application_module_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["application_module_id"], name: "index_manuscript_comments_on_application_module_id"
    t.index ["commented_by_id", "manuscript_id", "content", "comment_date", "application_module_id"], name: "unique_manuscript_comment_content_on_app_module_indx", unique: true
    t.index ["manuscript_id"], name: "index_sate_crmgt_manuscript_comments_on_manuscript_id"
  end

  create_table "sate_crmgt_manuscripts", force: :cascade do |t|
    t.string "reference_no", null: false
    t.datetime "received_date", null: false
    t.string "title", null: false
    t.string "search_keywords", array: true
    t.integer "destination_id", null: false
    t.integer "key_in_by_id", null: false
    t.string "status", default: "New"
    t.bigint "application_module_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "current_stage"
    t.index ["application_module_id"], name: "index_manuscript_on_application_module_id"
    t.index ["destination_id"], name: "index_sate_crmgt_manuscripts_on_destination_id"
    t.index ["reference_no", "title", "destination_id", "application_module_id"], name: "unique_reference_no_title_on_app_module_indx", unique: true
  end

  create_table "sate_crmgt_menu_actions", force: :cascade do |t|
    t.bigint "menu_id", null: false
    t.bigint "action_id", null: false
    t.bigint "application_module_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["action_id"], name: "index_sate_crmgt_menu_actions_on_action_id"
    t.index ["application_module_id"], name: "index_sate_crmgt_menu_actions_on_application_module_id"
    t.index ["menu_id", "action_id", "application_module_id"], name: "unique_menu_action_on_app_module_indx", unique: true
    t.index ["menu_id"], name: "index_sate_crmgt_menu_actions_on_menu_id"
  end

  create_table "sate_crmgt_notifications", force: :cascade do |t|
    t.string "correspondence_type", null: false
    t.bigint "correspondence_id", null: false
    t.bigint "user_id", null: false
    t.string "content", null: false
    t.datetime "sent_at", null: false
    t.bigint "application_module_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["application_module_id"], name: "index_sate_crmgt_notifications_on_application_module_id"
    t.index ["user_id"], name: "index_sate_crmgt_notifications_on_user_id"
  end

  create_table "sate_crmgt_organizations", force: :cascade do |t|
    t.string "code", null: false
    t.string "description", null: false
    t.bigint "organization_type_id"
    t.bigint "application_module_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["application_module_id"], name: "index_sate_crmgt_organizations_on_application_module_id"
    t.index ["code", "description", "organization_type_id", "application_module_id"], name: "unique_code_desc_org_type_on_app_module_indx", unique: true
    t.index ["organization_type_id"], name: "index_sate_crmgt_organizations_on_organization_type_id"
  end

  create_table "sate_crmgt_recipient_copies", force: :cascade do |t|
    t.bigint "notification_id", null: false
    t.integer "recipient_id", null: false
    t.boolean "checked", default: false
    t.datetime "read_at"
    t.bigint "application_module_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["application_module_id"], name: "index_sate_crmgt_recipient_copies_on_application_module_id"
    t.index ["notification_id"], name: "index_sate_crmgt_recipient_copies_on_notification_id"
    t.index ["recipient_id", "notification_id", "application_module_id"], name: "unique_notification_to_user_on_app_module_indx", unique: true
  end

  create_table "sate_crmgt_royalties", force: :cascade do |t|
    t.bigint "manuscript_id", null: false
    t.bigint "author_id", null: false
    t.string "reference_no", null: false
    t.decimal "amount", precision: 12, scale: 2, null: false
    t.string "remark"
    t.datetime "payment_date", null: false
    t.integer "key_in_by_id", null: false
    t.bigint "application_module_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["manuscript_id", "author_id", "reference_no", "amount", "remark", "payment_date", "application_module_id"], name: "unique_royalty_detail_double_save_indx", unique: true
  end

  create_table "sate_crmgt_user_department_roles", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "department_user_role_id", null: false
    t.bigint "application_module_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["application_module_id"], name: "index_sate_crmgt_user_department_roles_on_application_module_id"
    t.index ["department_user_role_id"], name: "index_user_department_role_on_department_role_id"
    t.index ["user_id", "department_user_role_id", "application_module_id"], name: "unique_user_department_user_role_on_app_module_indx", unique: true
    t.index ["user_id"], name: "index_sate_crmgt_user_department_roles_on_user_id"
  end

  create_table "sate_crmgt_user_departments", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "department_id", null: false
    t.bigint "user_role_id", null: false
    t.bigint "application_module_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["application_module_id"], name: "index_sate_crmgt_user_departments_on_application_module_id"
    t.index ["department_id"], name: "index_sate_crmgt_user_departments_on_department_id"
    t.index ["user_id", "department_id", "user_role_id", "application_module_id"], name: "unique_user_department_role_on_app_module_indx", unique: true
    t.index ["user_id"], name: "index_sate_crmgt_user_departments_on_user_id"
    t.index ["user_role_id"], name: "index_sate_crmgt_user_departments_on_user_role_id"
  end

  create_table "sate_crmgt_user_menu_actions", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "menu_action_id", null: false
    t.boolean "active", default: true
    t.bigint "application_module_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["application_module_id"], name: "index_sate_crmgt_user_menu_actions_on_application_module_id"
    t.index ["menu_action_id"], name: "index_sate_crmgt_user_menu_actions_on_menu_action_id"
    t.index ["user_id", "menu_action_id", "application_module_id"], name: "unique_user_menu_action_on_app_module_indx", unique: true
    t.index ["user_id"], name: "index_sate_crmgt_user_menu_actions_on_user_id"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "sate_auth_menus", "sate_auth_application_modules", column: "application_module_id"
  add_foreign_key "sate_auth_menus", "sate_auth_menus", column: "parent_id"
  add_foreign_key "sate_auth_user_menus", "sate_auth_menus", column: "menu_id"
  add_foreign_key "sate_auth_user_menus", "sate_auth_users", column: "user_id"
  add_foreign_key "sate_auth_user_roles", "sate_auth_application_modules", column: "application_module_id"
  add_foreign_key "sate_auth_user_user_roles", "sate_auth_user_roles", column: "user_role_id"
  add_foreign_key "sate_auth_user_user_roles", "sate_auth_users", column: "user_id"
  add_foreign_key "sate_auth_users", "sate_auth_application_modules", column: "application_module_id"
  add_foreign_key "sate_crmgt_author_contacts", "sate_auth_application_modules", column: "application_module_id"
  add_foreign_key "sate_crmgt_author_contacts", "sate_crmgt_authors", column: "author_id"
  add_foreign_key "sate_crmgt_authors", "sate_auth_application_modules", column: "application_module_id"
  add_foreign_key "sate_crmgt_correspondence_assignments", "sate_auth_application_modules", column: "application_module_id"
  add_foreign_key "sate_crmgt_correspondence_assignments", "sate_auth_users", column: "from_user_id"
  add_foreign_key "sate_crmgt_correspondence_assignments", "sate_auth_users", column: "to_user_id"
  add_foreign_key "sate_crmgt_correspondence_carbon_copies", "sate_auth_application_modules", column: "application_module_id"
  add_foreign_key "sate_crmgt_correspondence_carbon_copies", "sate_crmgt_departments", column: "destination_id"
  add_foreign_key "sate_crmgt_correspondence_comments", "sate_auth_application_modules", column: "application_module_id"
  add_foreign_key "sate_crmgt_correspondence_comments", "sate_auth_users", column: "commented_by_id"
  add_foreign_key "sate_crmgt_department_user_roles", "sate_auth_application_modules", column: "application_module_id"
  add_foreign_key "sate_crmgt_department_user_roles", "sate_auth_user_roles", column: "user_role_id"
  add_foreign_key "sate_crmgt_department_user_roles", "sate_crmgt_departments", column: "department_id"
  add_foreign_key "sate_crmgt_departments", "sate_auth_application_modules", column: "application_module_id"
  add_foreign_key "sate_crmgt_departments", "sate_crmgt_departments", column: "parent_id"
  add_foreign_key "sate_crmgt_departments", "sate_crmgt_lookups", column: "department_type_id"
  add_foreign_key "sate_crmgt_external_assessors", "sate_auth_application_modules", column: "application_module_id"
  add_foreign_key "sate_crmgt_external_incoming_correspondences", "sate_auth_application_modules", column: "application_module_id"
  add_foreign_key "sate_crmgt_external_incoming_correspondences", "sate_auth_users", column: "key_in_by_id"
  add_foreign_key "sate_crmgt_external_incoming_correspondences", "sate_crmgt_departments", column: "destination_id"
  add_foreign_key "sate_crmgt_external_incoming_correspondences", "sate_crmgt_organizations", column: "source_id"
  add_foreign_key "sate_crmgt_external_outgoing_correspondences", "sate_auth_application_modules", column: "application_module_id"
  add_foreign_key "sate_crmgt_external_outgoing_correspondences", "sate_auth_users", column: "key_in_by_id"
  add_foreign_key "sate_crmgt_external_outgoing_correspondences", "sate_crmgt_departments", column: "source_id"
  add_foreign_key "sate_crmgt_external_outgoing_correspondences", "sate_crmgt_organizations", column: "destination_id"
  add_foreign_key "sate_crmgt_internal_correspondences", "sate_auth_application_modules", column: "application_module_id"
  add_foreign_key "sate_crmgt_internal_correspondences", "sate_auth_users", column: "key_in_by_id"
  add_foreign_key "sate_crmgt_internal_correspondences", "sate_crmgt_departments", column: "destination_id"
  add_foreign_key "sate_crmgt_internal_correspondences", "sate_crmgt_departments", column: "source_id"
  add_foreign_key "sate_crmgt_lookups", "sate_auth_application_modules", column: "application_module_id"
  add_foreign_key "sate_crmgt_manuscript_archives", "sate_auth_application_modules", column: "application_module_id"
  add_foreign_key "sate_crmgt_manuscript_archives", "sate_auth_users", column: "archived_by_id"
  add_foreign_key "sate_crmgt_manuscript_archives", "sate_crmgt_manuscripts", column: "manuscript_id"
  add_foreign_key "sate_crmgt_manuscript_assignments", "sate_auth_application_modules", column: "application_module_id"
  add_foreign_key "sate_crmgt_manuscript_assignments", "sate_auth_users", column: "from_user_id"
  add_foreign_key "sate_crmgt_manuscript_authors", "sate_auth_application_modules", column: "application_module_id"
  add_foreign_key "sate_crmgt_manuscript_authors", "sate_crmgt_authors", column: "author_id"
  add_foreign_key "sate_crmgt_manuscript_authors", "sate_crmgt_manuscripts", column: "manuscript_id"
  add_foreign_key "sate_crmgt_manuscript_comments", "sate_auth_application_modules", column: "application_module_id"
  add_foreign_key "sate_crmgt_manuscript_comments", "sate_auth_users", column: "commented_by_id"
  add_foreign_key "sate_crmgt_manuscript_comments", "sate_crmgt_manuscripts", column: "manuscript_id"
  add_foreign_key "sate_crmgt_manuscripts", "sate_auth_application_modules", column: "application_module_id"
  add_foreign_key "sate_crmgt_manuscripts", "sate_auth_users", column: "key_in_by_id"
  add_foreign_key "sate_crmgt_manuscripts", "sate_crmgt_departments", column: "destination_id"
  add_foreign_key "sate_crmgt_menu_actions", "sate_auth_application_modules", column: "application_module_id"
  add_foreign_key "sate_crmgt_menu_actions", "sate_auth_menus", column: "menu_id"
  add_foreign_key "sate_crmgt_menu_actions", "sate_crmgt_lookups", column: "action_id"
  add_foreign_key "sate_crmgt_notifications", "sate_auth_application_modules", column: "application_module_id"
  add_foreign_key "sate_crmgt_notifications", "sate_auth_users", column: "user_id"
  add_foreign_key "sate_crmgt_organizations", "sate_auth_application_modules", column: "application_module_id"
  add_foreign_key "sate_crmgt_organizations", "sate_crmgt_lookups", column: "organization_type_id"
  add_foreign_key "sate_crmgt_recipient_copies", "sate_auth_application_modules", column: "application_module_id"
  add_foreign_key "sate_crmgt_recipient_copies", "sate_auth_users", column: "recipient_id"
  add_foreign_key "sate_crmgt_recipient_copies", "sate_crmgt_notifications", column: "notification_id"
  add_foreign_key "sate_crmgt_royalties", "sate_auth_application_modules", column: "application_module_id"
  add_foreign_key "sate_crmgt_royalties", "sate_auth_users", column: "key_in_by_id"
  add_foreign_key "sate_crmgt_royalties", "sate_crmgt_authors", column: "author_id"
  add_foreign_key "sate_crmgt_royalties", "sate_crmgt_manuscripts", column: "manuscript_id"
  add_foreign_key "sate_crmgt_user_department_roles", "sate_auth_application_modules", column: "application_module_id"
  add_foreign_key "sate_crmgt_user_department_roles", "sate_auth_users", column: "user_id"
  add_foreign_key "sate_crmgt_user_department_roles", "sate_crmgt_department_user_roles", column: "department_user_role_id"
  add_foreign_key "sate_crmgt_user_departments", "sate_auth_application_modules", column: "application_module_id"
  add_foreign_key "sate_crmgt_user_departments", "sate_auth_user_roles", column: "user_role_id"
  add_foreign_key "sate_crmgt_user_departments", "sate_auth_users", column: "user_id"
  add_foreign_key "sate_crmgt_user_departments", "sate_crmgt_departments", column: "department_id"
  add_foreign_key "sate_crmgt_user_menu_actions", "sate_auth_application_modules", column: "application_module_id"
  add_foreign_key "sate_crmgt_user_menu_actions", "sate_auth_users", column: "user_id"
  add_foreign_key "sate_crmgt_user_menu_actions", "sate_crmgt_menu_actions", column: "menu_action_id"
end
