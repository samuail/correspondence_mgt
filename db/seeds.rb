app_module = Sate::Auth::ApplicationModule.where(code: 'CRMT',
                                                 name: 'Correspondence Management').first_or_create

puts('** Loading Default User Data **')
user = Sate::Auth::User.where(first_name: 'System', last_name: 'Administrator').first
unless user
  user = Sate::Auth::User.create(first_name: 'System', last_name: 'Administrator', email: 'admin@local.com',
                                 password: '123456', application_module: app_module)
end
puts('** Default User loaded **')

puts('** Loading Default Departments **')
department_type = Sate::Crmgt::DepartmentType.where(code: 'CAM', name: 'Campus',application_module_id: app_module.id).first_or_create
Sate::Crmgt::Department.where(code: 'MAC', description: 'Main Campus', department_type_id: department_type.id, parent_id: nil, application_module_id: app_module.id).first_or_create
puts('** Default Departments Loaded')

puts('** Loading menu data **')
menu1 = Sate::Auth::Menu.where(text: 'System Settings', icon_cls: 'mdi-database-settings', application_module_id: app_module.id).first_or_create
menu2 = Sate::Auth::Menu.where(text: 'Business Settings', icon_cls: 'mdi-application-cog', application_module_id: app_module.id).first_or_create
menu3 = Sate::Auth::Menu.where(text: 'Incomings', icon_cls: 'mdi-file-import-outline', application_module_id: app_module.id).first_or_create
menu4 = Sate::Auth::Menu.where(text: 'Outgoings', icon_cls: 'mdi-file-export-outline', application_module_id: app_module.id).first_or_create
menu5 = Sate::Auth::Menu.where(text: 'Archives', icon_cls: 'mdi-archive-outline', application_module_id: app_module.id).first_or_create

Sate::Auth::Menu.where(text: 'Roles', parent_id: menu1.id, class_name: 'roles', icon_cls: 'mdi-account-multiple-outline', application_module_id: app_module.id).first_or_create
Sate::Auth::Menu.where(text: 'Menus', parent_id: menu1.id, class_name: 'menus', icon_cls: 'mdi-menu', application_module_id: app_module.id).first_or_create
Sate::Auth::Menu.where(text: 'Department Types', parent_id: menu1.id, class_name: 'department_types', icon_cls: 'mdi-group', application_module_id: app_module.id).first_or_create
Sate::Auth::Menu.where(text: 'Organization Types', parent_id: menu1.id, class_name: 'organization_types', icon_cls: 'mdi-shape-outline', application_module_id: app_module.id).first_or_create

Sate::Auth::Menu.where(text: 'Users', parent_id: menu2.id, class_name: 'users', icon_cls: 'mdi-account-group-outline', application_module_id: app_module.id).first_or_create
Sate::Auth::Menu.where(text: 'Departments', parent_id: menu2.id, class_name: 'departments', icon_cls: 'mdi-file-tree-outline', application_module_id: app_module.id).first_or_create
Sate::Auth::Menu.where(text: 'Organizations', parent_id: menu2.id, class_name: 'organizations', icon_cls: 'mdi-office-building-outline', application_module_id: app_module.id).first_or_create
Sate::Auth::Menu.where(text: 'Authors', parent_id: menu2.id, class_name: 'authors', icon_cls: 'mdi-head-lightbulb-outline', application_module_id: app_module.id).first_or_create
Sate::Auth::Menu.where(text: 'External Assessor', parent_id: menu2.id, class_name: 'external_assessor', icon_cls: 'mdi-book-education-outline', application_module_id: app_module.id).first_or_create

Sate::Auth::Menu.where(text: 'Centers', parent_id: menu3.id, class_name: 'incoming_centers', icon_cls: 'mdi-image-filter-center-focus-strong-outline', application_module_id: app_module.id).first_or_create
Sate::Auth::Menu.where(text: 'Colleges', parent_id: menu3.id, class_name: 'incoming_colleges', icon_cls: 'mdi-clipboard-arrow-right-outline', application_module_id: app_module.id).first_or_create
Sate::Auth::Menu.where(text: 'Outside AAU', parent_id: menu3.id, class_name: 'incoming_external', icon_cls: 'mdi-city-variant-outline', application_module_id: app_module.id).first_or_create
Sate::Auth::Menu.where(text: 'Manuscripts', parent_id: menu3.id, class_name: 'incoming_manuscripts', icon_cls: 'mdi-tray-full', application_module_id: app_module.id).first_or_create

Sate::Auth::Menu.where(text: 'Internal', parent_id: menu4.id, class_name: 'outgoing_internal', icon_cls: 'mdi-clipboard-arrow-right-outline', application_module_id: app_module.id).first_or_create
Sate::Auth::Menu.where(text: 'External', parent_id: menu4.id, class_name: 'outgoing_external', icon_cls: 'mdi-city-variant-outline', application_module_id: app_module.id).first_or_create

Sate::Auth::Menu.where(text: '[ Internal ]', parent_id: menu5.id, class_name: 'internal_archived', icon_cls: 'mdi-archive-arrow-down-outline', application_module_id: app_module.id).first_or_create
Sate::Auth::Menu.where(text: '[ External ]', parent_id: menu5.id, class_name: 'external_archived', icon_cls: 'mdi-archive-arrow-up-outline', application_module_id: app_module.id).first_or_create
Sate::Auth::Menu.where(text: '[ Manuscripts ]', parent_id: menu5.id, class_name: 'manuscript_archived', icon_cls: 'mdi-archive-arrow-down-outline', application_module_id: app_module.id).first_or_create

roles = Sate::Auth::Menu.where(text: 'Roles', application_module_id: app_module.id).first
menus = Sate::Auth::Menu.where(text: 'Menus', application_module_id: app_module.id).first
department_type = Sate::Auth::Menu.where(text: 'Department Types', application_module_id: app_module.id).first
organization_type = Sate::Auth::Menu.where(text: 'Organization Types', application_module_id: app_module.id).first

users = Sate::Auth::Menu.where(text: 'Users', application_module_id: app_module.id).first
departments = Sate::Auth::Menu.where(text: 'Departments', application_module_id: app_module.id).first
organizations = Sate::Auth::Menu.where(text: 'Organizations', application_module_id: app_module.id).first
authors = Sate::Auth::Menu.where(text: 'Authors', application_module_id: app_module.id).first
external_assessor = Sate::Auth::Menu.where(text: 'External Assessor', application_module_id: app_module.id).first

incoming_centers = Sate::Auth::Menu.where(text: 'Centers', application_module_id: app_module.id).first
incoming_colleges = Sate::Auth::Menu.where(text: 'Colleges', application_module_id: app_module.id).first
incoming_external = Sate::Auth::Menu.where(text: 'Outside AAU', application_module_id: app_module.id).first
incoming_manuscripts = Sate::Auth::Menu.where(text: 'Manuscripts', application_module_id: app_module.id).first

outgoing_internal = Sate::Auth::Menu.where(text: 'Internal', application_module_id: app_module.id).first
outgoing_external = Sate::Auth::Menu.where(text: 'External', application_module_id: app_module.id).first
puts('** Menu data loaded **')

puts('** Creating user roles **')
admin_role = Sate::Auth::UserRole.where(:name => 'Administrator', application_module_id: app_module.id).first_or_create
Sate::Auth::UserRole.where(:name => 'Secretary', application_module_id: app_module.id).first_or_create
Sate::Auth::UserRole.where(:name => 'Business Administrator', application_module_id: app_module.id).first_or_create
puts('** User roles created **')

puts('** Assigning roles to users **')
user_roles = user.roles.where(application_module_id: app_module.id)
user.roles.delete(user_roles)
user.roles << admin_role
puts('** Role assignment completed **')

puts('** Assigning menus to users **')
user_menus = user.menus.where(application_module_id: app_module.id)
user.menus.delete(user_menus)

user.menus << menu1
user.menus << menu1.children
user.menus << menu2
user.menus << menu2.children
user.menus << menu3
user.menus << menu3.children
user.menus << menu4
user.menus << menu4.children
user.menus << menu5
user.menus << menu5.children
puts('** Menu assignment completed **')

puts('** Creating Menu Action **')
Sate::Crmgt::Action.where(:code => 'ADD', :name => 'Add', application_module_id: app_module.id).first_or_create
Sate::Crmgt::Action.where(:code => 'EDT', :name => 'Edit', application_module_id: app_module.id).first_or_create
Sate::Crmgt::Action.where(:code => 'DEL', :name => 'Delete', application_module_id: app_module.id).first_or_create

Sate::Crmgt::Action.where(:code => 'UPD', :name => 'Upload', application_module_id: app_module.id).first_or_create
Sate::Crmgt::Action.where(:code => 'UPDAdd', :name => 'Upload Add', application_module_id: app_module.id).first_or_create
Sate::Crmgt::Action.where(:code => 'UPDEdt', :name => 'Upload Edit', application_module_id: app_module.id).first_or_create
Sate::Crmgt::Action.where(:code => 'UPDDel', :name => 'Upload Delete', application_module_id: app_module.id).first_or_create
Sate::Crmgt::Action.where(:code => 'UPDPre', :name => 'Upload Preview', application_module_id: app_module.id).first_or_create

Sate::Crmgt::Action.where(:code => 'CMT', :name => 'Comment', application_module_id: app_module.id).first_or_create
Sate::Crmgt::Action.where(:code => 'CMTAdd', :name => 'Comment Add', application_module_id: app_module.id).first_or_create
Sate::Crmgt::Action.where(:code => 'CMTEdt', :name => 'Comment Edit', application_module_id: app_module.id).first_or_create
Sate::Crmgt::Action.where(:code => 'CMTDel', :name => 'Comment Delete', application_module_id: app_module.id).first_or_create

Sate::Crmgt::Action.where(:code => 'PRV', :name => 'Preview', application_module_id: app_module.id).first_or_create
Sate::Crmgt::Action.where(:code => 'PRVCom', :name => 'Preview Comment', application_module_id: app_module.id).first_or_create

Sate::Crmgt::Action.where(:code => 'RUT', :name => 'Route', application_module_id: app_module.id).first_or_create
Sate::Crmgt::Action.where(:code => 'RUTAdd', :name => 'Route Add', application_module_id: app_module.id).first_or_create
Sate::Crmgt::Action.where(:code => 'RUTAddIn', :name => 'Route Add Within', application_module_id: app_module.id).first_or_create
Sate::Crmgt::Action.where(:code => 'RUTAddAll', :name => 'Route Add All', application_module_id: app_module.id).first_or_create
Sate::Crmgt::Action.where(:code => 'RUTEdt', :name => 'Route Edit', application_module_id: app_module.id).first_or_create
Sate::Crmgt::Action.where(:code => 'RUTDel', :name => 'Route Delete', application_module_id: app_module.id).first_or_create
Sate::Crmgt::Action.where(:code => 'RUTPre', :name => 'Route Preview', application_module_id: app_module.id).first_or_create
Sate::Crmgt::Action.where(:code => 'RUTAcp', :name => 'Route Accept', application_module_id: app_module.id).first_or_create
Sate::Crmgt::Action.where(:code => 'RUTRej', :name => 'Route Reject', application_module_id: app_module.id).first_or_create

Sate::Crmgt::Action.where(:code => 'ARC', :name => 'Archive', application_module_id: app_module.id).first_or_create

Sate::Crmgt::Action.where(:code => 'LSTUSR', :name => 'List User', application_module_id: app_module.id).first_or_create

Sate::Crmgt::Action.where(:code => 'ROLE', :name => 'Assign Roles', application_module_id: app_module.id).first_or_create
Sate::Crmgt::Action.where(:code => 'MENU', :name => 'Assign Menus', application_module_id: app_module.id).first_or_create
Sate::Crmgt::Action.where(:code => 'ACTION', :name => 'Menu Actions', application_module_id: app_module.id).first_or_create
Sate::Crmgt::Action.where(:code => 'STATUS', :name => 'Change Status', application_module_id: app_module.id).first_or_create
Sate::Crmgt::Action.where(:code => 'DEPT', :name => 'Assign Departments', application_module_id: app_module.id).first_or_create

Sate::Crmgt::Action.where(:code => 'ADDRT', :name => 'Add to Root', application_module_id: app_module.id).first_or_create

Sate::Crmgt::Action.where(:code => 'AXP', :name => 'Accept', application_module_id: app_module.id).first_or_create
Sate::Crmgt::Action.where(:code => 'REJ', :name => 'Reject', application_module_id: app_module.id).first_or_create


add = Sate::Crmgt::Action.where(:code => 'ADD', application_module_id: app_module.id).first
edit = Sate::Crmgt::Action.where(:code => 'EDT', application_module_id: app_module.id).first
delete = Sate::Crmgt::Action.where(:code => 'DEL', application_module_id: app_module.id).first
upload = Sate::Crmgt::Action.where(:code => 'UPD', application_module_id: app_module.id).first
upload_add = Sate::Crmgt::Action.where(:code => 'UPDAdd', application_module_id: app_module.id).first
upload_edit = Sate::Crmgt::Action.where(:code => 'UPDEdt', application_module_id: app_module.id).first
upload_delete = Sate::Crmgt::Action.where(:code => 'UPDDel', application_module_id: app_module.id).first
upload_preview = Sate::Crmgt::Action.where(:code => 'UPDPre', application_module_id: app_module.id).first
comment = Sate::Crmgt::Action.where(:code => 'CMT', application_module_id: app_module.id).first
comment_add = Sate::Crmgt::Action.where(:code => 'CMTAdd', application_module_id: app_module.id).first
comment_edit = Sate::Crmgt::Action.where(:code => 'CMTEdt', application_module_id: app_module.id).first
comment_delete = Sate::Crmgt::Action.where(:code => 'CMTDel', application_module_id: app_module.id).first
preview = Sate::Crmgt::Action.where(:code => 'PRV', application_module_id: app_module.id).first
preview_comment = Sate::Crmgt::Action.where(:code => 'PRVCom', application_module_id: app_module.id).first
route = Sate::Crmgt::Action.where(:code => 'RUT', application_module_id: app_module.id).first
route_add = Sate::Crmgt::Action.where(:code => 'RUTAdd', application_module_id: app_module.id).first
route_add_in = Sate::Crmgt::Action.where(:code => 'RUTAddIn', application_module_id: app_module.id).first
route_add_all = Sate::Crmgt::Action.where(:code => 'RUTAddAll', application_module_id: app_module.id).first
route_edit = Sate::Crmgt::Action.where(:code => 'RUTEdt', application_module_id: app_module.id).first
route_delete = Sate::Crmgt::Action.where(:code => 'RUTDel', application_module_id: app_module.id).first
route_preview = Sate::Crmgt::Action.where(:code => 'RUTPre', application_module_id: app_module.id).first
route_accept = Sate::Crmgt::Action.where(:code => 'RUTAcp', application_module_id: app_module.id).first
route_reject = Sate::Crmgt::Action.where(:code => 'RUTRej', application_module_id: app_module.id).first
archive = Sate::Crmgt::Action.where(:code => 'ARC', application_module_id: app_module.id).first

list_user = Sate::Crmgt::Action.where(:code => 'LSTUSR', application_module_id: app_module.id).first

user_assign_roles = Sate::Crmgt::Action.where(:code => 'ROLE', application_module_id: app_module.id).first
user_assign_menus = Sate::Crmgt::Action.where(:code => 'MENU', application_module_id: app_module.id).first
user_menu_actions = Sate::Crmgt::Action.where(:code => 'ACTION', application_module_id: app_module.id).first
user_change_status = Sate::Crmgt::Action.where(:code => 'STATUS', application_module_id: app_module.id).first
user_assign_department = Sate::Crmgt::Action.where(:code => 'DEPT', application_module_id: app_module.id).first

dept_add_root = Sate::Crmgt::Action.where(:code => 'ADDRT', application_module_id: app_module.id).first

accept = Sate::Crmgt::Action.where(:code => 'AXP', application_module_id: app_module.id).first
reject = Sate::Crmgt::Action.where(:code => 'REJ', application_module_id: app_module.id).first
puts('** Menu Action created **')

puts('** Assigning actions under a given menu **')
Sate::Crmgt::MenuAction.where(:menu_id => roles.id, :action_id => add.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => roles.id, :action_id => edit.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => roles.id, :action_id => delete.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => roles.id, :action_id => list_user.id, application_module_id: app_module.id).first_or_create

Sate::Crmgt::MenuAction.where(:menu_id => menus.id, :action_id => edit.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => menus.id, :action_id => list_user.id, application_module_id: app_module.id).first_or_create

Sate::Crmgt::MenuAction.where(:menu_id => department_type.id, :action_id => add.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => department_type.id, :action_id => edit.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => department_type.id, :action_id => delete.id, application_module_id: app_module.id).first_or_create

Sate::Crmgt::MenuAction.where(:menu_id => organization_type.id, :action_id => add.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => organization_type.id, :action_id => edit.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => organization_type.id, :action_id => delete.id, application_module_id: app_module.id).first_or_create

Sate::Crmgt::MenuAction.where(:menu_id => users.id, :action_id => add.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => users.id, :action_id => edit.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => users.id, :action_id => delete.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => users.id, :action_id => user_assign_roles.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => users.id, :action_id => user_assign_menus.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => users.id, :action_id => user_menu_actions.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => users.id, :action_id => user_change_status.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => users.id, :action_id => user_assign_department.id, application_module_id: app_module.id).first_or_create

Sate::Crmgt::MenuAction.where(:menu_id => departments.id, :action_id => add.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => departments.id, :action_id => edit.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => departments.id, :action_id => delete.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => departments.id, :action_id => dept_add_root.id, application_module_id: app_module.id).first_or_create

Sate::Crmgt::MenuAction.where(:menu_id => organizations.id, :action_id => add.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => organizations.id, :action_id => edit.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => organizations.id, :action_id => delete.id, application_module_id: app_module.id).first_or_create

Sate::Crmgt::MenuAction.where(:menu_id => authors.id, :action_id => add.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => authors.id, :action_id => edit.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => authors.id, :action_id => delete.id, application_module_id: app_module.id).first_or_create

Sate::Crmgt::MenuAction.where(:menu_id => external_assessor.id, :action_id => add.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => external_assessor.id, :action_id => edit.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => external_assessor.id, :action_id => delete.id, application_module_id: app_module.id).first_or_create

Sate::Crmgt::MenuAction.where(:menu_id => incoming_centers.id, :action_id => add.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_centers.id, :action_id => edit.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_centers.id, :action_id => delete.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_centers.id, :action_id => upload.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_centers.id, :action_id => upload_add.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_centers.id, :action_id => upload_edit.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_centers.id, :action_id => upload_delete.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_centers.id, :action_id => upload_preview.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_centers.id, :action_id => comment.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_centers.id, :action_id => comment_add.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_centers.id, :action_id => comment_edit.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_centers.id, :action_id => comment_delete.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_centers.id, :action_id => preview.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_centers.id, :action_id => preview_comment.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_centers.id, :action_id => route.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_centers.id, :action_id => route_add.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_centers.id, :action_id => route_add_in.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_centers.id, :action_id => route_add_all.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_centers.id, :action_id => route_edit.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_centers.id, :action_id => route_delete.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_centers.id, :action_id => route_preview.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_centers.id, :action_id => route_accept.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_centers.id, :action_id => route_reject.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_centers.id, :action_id => accept.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_centers.id, :action_id => reject.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_centers.id, :action_id => archive.id, application_module_id: app_module.id).first_or_create

Sate::Crmgt::MenuAction.where(:menu_id => incoming_colleges.id, :action_id => add.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_colleges.id, :action_id => edit.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_colleges.id, :action_id => delete.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_colleges.id, :action_id => upload.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_colleges.id, :action_id => upload_add.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_colleges.id, :action_id => upload_edit.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_colleges.id, :action_id => upload_delete.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_colleges.id, :action_id => upload_preview.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_colleges.id, :action_id => comment.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_colleges.id, :action_id => comment_add.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_colleges.id, :action_id => comment_edit.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_colleges.id, :action_id => comment_delete.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_colleges.id, :action_id => preview.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_colleges.id, :action_id => preview_comment.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_colleges.id, :action_id => route.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_colleges.id, :action_id => route_add.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_colleges.id, :action_id => route_add_in.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_colleges.id, :action_id => route_add_all.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_colleges.id, :action_id => route_edit.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_colleges.id, :action_id => route_delete.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_colleges.id, :action_id => route_preview.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_colleges.id, :action_id => route_accept.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_colleges.id, :action_id => route_reject.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_colleges.id, :action_id => accept.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_colleges.id, :action_id => reject.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_colleges.id, :action_id => archive.id, application_module_id: app_module.id).first_or_create

Sate::Crmgt::MenuAction.where(:menu_id => incoming_external.id, :action_id => add.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_external.id, :action_id => edit.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_external.id, :action_id => delete.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_external.id, :action_id => upload.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_external.id, :action_id => upload_add.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_external.id, :action_id => upload_edit.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_external.id, :action_id => upload_delete.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_external.id, :action_id => upload_preview.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_external.id, :action_id => comment.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_external.id, :action_id => comment_add.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_external.id, :action_id => comment_edit.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_external.id, :action_id => comment_delete.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_external.id, :action_id => preview.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_external.id, :action_id => preview_comment.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_external.id, :action_id => route.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_external.id, :action_id => route_add.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_external.id, :action_id => route_add_in.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_external.id, :action_id => route_add_all.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_external.id, :action_id => route_edit.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_external.id, :action_id => route_delete.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_external.id, :action_id => route_preview.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_external.id, :action_id => route_accept.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_external.id, :action_id => route_reject.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_external.id, :action_id => accept.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_external.id, :action_id => reject.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_external.id, :action_id => archive.id, application_module_id: app_module.id).first_or_create

Sate::Crmgt::MenuAction.where(:menu_id => incoming_manuscripts.id, :action_id => add.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_manuscripts.id, :action_id => edit.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_manuscripts.id, :action_id => delete.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_manuscripts.id, :action_id => upload.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_manuscripts.id, :action_id => upload_add.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_manuscripts.id, :action_id => upload_edit.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_manuscripts.id, :action_id => upload_delete.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_manuscripts.id, :action_id => upload_preview.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_manuscripts.id, :action_id => comment.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_manuscripts.id, :action_id => comment_add.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_manuscripts.id, :action_id => comment_edit.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_manuscripts.id, :action_id => comment_delete.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_manuscripts.id, :action_id => preview.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_manuscripts.id, :action_id => preview_comment.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_manuscripts.id, :action_id => route.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_manuscripts.id, :action_id => route_add.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_manuscripts.id, :action_id => route_add_in.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_manuscripts.id, :action_id => route_add_all.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_manuscripts.id, :action_id => route_edit.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_manuscripts.id, :action_id => route_delete.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_manuscripts.id, :action_id => route_preview.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_manuscripts.id, :action_id => route_accept.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_manuscripts.id, :action_id => route_reject.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => incoming_manuscripts.id, :action_id => archive.id, application_module_id: app_module.id).first_or_create


Sate::Crmgt::MenuAction.where(:menu_id => outgoing_internal.id, :action_id => add.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => outgoing_internal.id, :action_id => edit.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => outgoing_internal.id, :action_id => delete.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => outgoing_internal.id, :action_id => upload.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => outgoing_internal.id, :action_id => upload_add.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => outgoing_internal.id, :action_id => upload_edit.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => outgoing_internal.id, :action_id => upload_delete.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => outgoing_internal.id, :action_id => upload_preview.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => outgoing_internal.id, :action_id => comment.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => outgoing_internal.id, :action_id => preview.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => outgoing_internal.id, :action_id => preview_comment.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => outgoing_internal.id, :action_id => route.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => outgoing_internal.id, :action_id => route_preview.id, application_module_id: app_module.id).first_or_create

Sate::Crmgt::MenuAction.where(:menu_id => outgoing_external.id, :action_id => add.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => outgoing_external.id, :action_id => edit.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => outgoing_external.id, :action_id => delete.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => outgoing_external.id, :action_id => upload.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => outgoing_external.id, :action_id => upload_add.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => outgoing_external.id, :action_id => upload_edit.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => outgoing_external.id, :action_id => upload_delete.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => outgoing_external.id, :action_id => upload_preview.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::MenuAction.where(:menu_id => outgoing_external.id, :action_id => preview.id, application_module_id: app_module.id).first_or_create
puts('** Action assignment completed **')

puts('** Assigning User menu actions for the default user **')
user_add = Sate::Crmgt::MenuAction.where(:menu_id => users.id, :action_id => add.id, application_module_id: app_module.id).first
user_edit = Sate::Crmgt::MenuAction.where(:menu_id => users.id, :action_id => edit.id, application_module_id: app_module.id).first
user_delete = Sate::Crmgt::MenuAction.where(:menu_id => users.id, :action_id => delete.id, application_module_id: app_module.id).first
user_assigning_role = Sate::Crmgt::MenuAction.where(:menu_id => users.id, :action_id => user_assign_roles.id, application_module_id: app_module.id).first
user_assigning_menu = Sate::Crmgt::MenuAction.where(:menu_id => users.id, :action_id => user_assign_menus.id, application_module_id: app_module.id).first
user_assigning_menu_actions = Sate::Crmgt::MenuAction.where(:menu_id => users.id, :action_id => user_menu_actions.id, application_module_id: app_module.id).first
user_assigning_status = Sate::Crmgt::MenuAction.where(:menu_id => users.id, :action_id => user_change_status.id, application_module_id: app_module.id).first
user_assigning_department = Sate::Crmgt::MenuAction.where(:menu_id => users.id, :action_id => user_assign_department.id, application_module_id: app_module.id).first

Sate::Crmgt::UserMenuAction.where(user_id: user.id, menu_action_id: user_add.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::UserMenuAction.where(user_id: user.id, menu_action_id: user_edit.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::UserMenuAction.where(user_id: user.id, menu_action_id: user_delete.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::UserMenuAction.where(user_id: user.id, menu_action_id: user_assigning_role.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::UserMenuAction.where(user_id: user.id, menu_action_id: user_assigning_menu.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::UserMenuAction.where(user_id: user.id, menu_action_id: user_assigning_menu_actions.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::UserMenuAction.where(user_id: user.id, menu_action_id: user_assigning_status.id, application_module_id: app_module.id).first_or_create
Sate::Crmgt::UserMenuAction.where(user_id: user.id, menu_action_id: user_assigning_department.id, application_module_id: app_module.id).first_or_create
puts('** User menu action assignment completed **')