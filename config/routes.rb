Rails.application.routes.draw do
  mount Sate::Auth::Engine => '/crmgt/auth', as: 'sate_auth'

  if Rails.env.development?
    scope format: true, constraints: { format: /jpg|jpeg|png|gif|PNG|pdf/ } do
      get '/*anything', to: proc { [404, {}, ['']] }, constraints: lambda { |request| !request.path_parameters[:anything].start_with?('rails/') }
    end
  end

  post '/crmgt/auth/login', to: 'access#login'
  get '/crmgt/auth/logout', to: 'access#logout'

  get '/crmgt/dashboard', controller: 'dashboards', action: 'index'
  get '/crmgt/dashboard/user_status', controller: 'dashboards', action: 'get_department_user_status'
  get '/crmgt/dashboard/correspondence/by_type', controller: 'dashboards', action: 'correspondence_by_type'
  get '/crmgt/dashboard/correspondence/by_month', controller: 'dashboards', action: 'correspondence_by_month'
  get '/crmgt/dashboard/correspondence/by_status', controller: 'dashboards', action: 'correspondence_by_status'
  get '/crmgt/dashboard/correspondence/monthly_payment', controller: 'dashboards', action: 'correspondence_monthly_payment'
  get '/crmgt/dashboard/correspondence/center_departments', controller: 'dashboards', action: 'center_correspondence_details'
  get '/crmgt/dashboard/correspondence/college_departments', controller: 'dashboards', action: 'college_correspondence_details'
  get '/crmgt/dashboard/correspondence/department_outgoings', controller: 'dashboards', action: 'outgoing_correspondence_details'

  get '/crmgt/dashboard/manuscript/by_stage', controller: 'dashboards', action: 'manuscript_by_stage'
  get '/crmgt/dashboard/manuscript/by_month', controller: 'dashboards', action: 'manuscript_by_month'
  get '/crmgt/dashboard/manuscript/user_status', controller: 'dashboards', action: 'get_department_manuscript_user_status'

  get '/crmgt/auth/user/:user_id/department_role', controller: 'user_department_roles', action: 'index'
  post '/crmgt/auth/user/department_role', controller: 'user_department_roles', action: 'create'
  put '/crmgt/auth/user/department_role', controller: 'user_department_roles', action: 'update'
  patch '/crmgt/auth/user/department_role', controller: 'user_department_roles', action: 'update'
  post '/crmgt/auth/user/change_password', controller: 'user_department_roles', action: 'change_password'

  get '/crmgt/auth/user/:user_ids/assigned_document', controller: 'correspondence_assignments', action: 'get_assigned_docs'

  get '/crmgt/business_settings/department_type', controller: 'department_types', action: 'index'
  post '/crmgt/business_settings/department_type', controller: 'department_types', action: 'create'
  put '/crmgt/business_settings/department_type', controller: 'department_types', action: 'update'
  patch '/crmgt/business_settings/department_type', controller: 'department_types', action: 'update'

  get '/crmgt/business_settings/department', controller: 'departments', action: 'index'
  get '/crmgt/business_settings/department_by_type', controller: 'departments', action: 'get_department_by_type'
  get '/crmgt/business_settings/department_tree', controller: 'departments', action: 'fetch_departments'
  get '/crmgt/business_settings/department/:dept_id/users', controller: 'departments', action: 'get_users'
  get '/crmgt/business_settings/department/get_roles', controller: 'departments', action: 'get_assigned_roles'
  get '/crmgt/business_settings/department/get_heads', controller: 'departments', action: 'get_heads'
  get '/crmgt/business_settings/department/users', controller: 'departments', action: 'get_users_with_the_role'
  post '/crmgt/business_settings/department', controller: 'departments', action: 'create'
  post '/crmgt/business_settings/department/assign_roles', controller: 'departments', action: 'assign_roles'
  post '/crmgt/business_settings/department/assign_heads', controller: 'departments', action: 'assign_heads'
  put '/crmgt/business_settings/department', controller: 'departments', action: 'update'
  patch '/crmgt/business_settings/department', controller: 'departments', action: 'update'

  get '/crmgt/business_settings/organization_type', controller: 'organization_types', action: 'index'
  post '/crmgt/business_settings/organization_type', controller: 'organization_types', action: 'create'
  put '/crmgt/business_settings/organization_type', controller: 'organization_types', action: 'update'
  patch '/crmgt/business_settings/organization_type', controller: 'organization_types', action: 'update'

  get '/crmgt/business_settings/organization', controller: 'organizations', action: 'index'
  post '/crmgt/business_settings/organization', controller: 'organizations', action: 'create'
  put '/crmgt/business_settings/organization', controller: 'organizations', action: 'update'
  patch '/crmgt/business_settings/organization', controller: 'organizations', action: 'update'

  get '/crmgt/business_settings/author', controller: 'authors', action: 'index'
  post '/crmgt/business_settings/author', controller: 'authors', action: 'create'
  post '/crmgt/business_settings/author/contact', controller: 'authors', action: 'add_contact'
  put '/crmgt/business_settings/author/edit_contact', controller: 'authors', action: 'edit_contact'
  put '/crmgt/business_settings/author', controller: 'authors', action: 'update'
  patch '/crmgt/business_settings/author', controller: 'authors', action: 'update'

  get '/crmgt/business_settings/external_assessor', controller: 'external_assessors', action: 'index'
  post '/crmgt/business_settings/external_assessor', controller: 'external_assessors', action: 'create'
  put '/crmgt/business_settings/external_assessor', controller: 'external_assessors', action: 'update'
  patch '/crmgt/business_settings/external_assessor', controller: 'external_assessors', action: 'update'

  get '/crmgt/business_settings/author/royalty', controller: 'royalties', action: 'index'
  post '/crmgt/business_settings/author/royalty', controller: 'royalties', action: 'create'
  put '/crmgt/business_settings/author/royalty', controller: 'royalties', action: 'update'
  patch '/crmgt/business_settings/author/royalty', controller: 'royalties', action: 'update'

  get '/crmgt/correspondence/internal', controller: 'internal_correspondences', action: 'index'
  get '/crmgt/correspondence/internal/assigned', controller: 'internal_correspondences', action: 'get_assigned'
  post '/crmgt/correspondence/internal', controller: 'internal_correspondences', action: 'create'
  put '/crmgt/correspondence/internal', controller: 'internal_correspondences', action: 'update'
  put '/crmgt/correspondence/internal/update_status', controller: 'internal_correspondences', action: 'update_status'
  patch '/crmgt/correspondence/internal', controller: 'internal_correspondences', action: 'update'
  put '/crmgt/correspondence/internal/archive', controller: 'internal_correspondences', action: 'archive'
  get '/crmgt/correspondence/internal/archived', controller: 'internal_correspondences', action: 'archived'

  post '/crmgt/correspondence/internal/upload', controller: 'internal_correspondences', action: 'upload'
  post '/crmgt/correspondence/internal/change_uploads', controller: 'internal_correspondences', action: 'change_uploads'
  get '/crmgt/correspondence/internal/get_images', controller: 'internal_correspondences', action: 'get_images'

  get '/crmgt/correspondence/internal/assignment', controller: 'correspondence_assignments', action: 'index'
  post '/crmgt/correspondence/internal/assignment', controller: 'correspondence_assignments', action: 'create'
  put '/crmgt/correspondence/internal/assignment', controller: 'correspondence_assignments', action: 'update'

  get '/crmgt/correspondence/internal/comments', controller: 'correspondence_comments', action: 'index'
  post '/crmgt/correspondence/internal/comments', controller: 'correspondence_comments', action: 'create'
  put '/crmgt/correspondence/internal/comments', controller: 'correspondence_comments', action: 'update'

  get '/crmgt/correspondence/internal/ccs', controller: 'correspondence_carbon_copies', action: 'index'
  post '/crmgt/correspondence/internal/ccs', controller: 'correspondence_carbon_copies', action: 'create'
  put '/crmgt/correspondence/internal/ccs', controller: 'correspondence_carbon_copies', action: 'update'

  get '/crmgt/correspondence/external', controller: 'external_correspondences', action: 'index'
  get '/crmgt/correspondence/external/assigned', controller: 'external_correspondences', action: 'get_assigned'
  post '/crmgt/correspondence/external', controller: 'external_correspondences', action: 'create'
  put '/crmgt/correspondence/external', controller: 'external_correspondences', action: 'update'
  put '/crmgt/correspondence/external/update_status', controller: 'external_correspondences', action: 'update_status'
  patch '/crmgt/correspondence/external', controller: 'external_correspondences', action: 'update'
  put '/crmgt/correspondence/external/archive', controller: 'external_correspondences', action: 'archive'
  get '/crmgt/correspondence/external/archived', controller: 'external_correspondences', action: 'archived'

  post '/crmgt/correspondence/external/upload', controller: 'external_correspondences', action: 'upload'
  post '/crmgt/correspondence/external/change_uploads', controller: 'external_correspondences', action: 'change_uploads'
  get '/crmgt/correspondence/external/get_images', controller: 'external_correspondences', action: 'get_images'

  get '/crmgt/correspondence/external/assignment', controller: 'correspondence_assignments', action: 'index'
  post '/crmgt/correspondence/external/assignment', controller: 'correspondence_assignments', action: 'create'
  put '/crmgt/correspondence/external/assignment', controller: 'correspondence_assignments', action: 'update'

  get '/crmgt/correspondence/external/comments', controller: 'correspondence_comments', action: 'index'
  post '/crmgt/correspondence/external/comments', controller: 'correspondence_comments', action: 'create'
  put '/crmgt/correspondence/external/comments', controller: 'correspondence_comments', action: 'update'

  get '/crmgt/correspondence/external/ccs', controller: 'correspondence_carbon_copies', action: 'index'
  post '/crmgt/correspondence/external/ccs', controller: 'correspondence_carbon_copies', action: 'create'
  put '/crmgt/correspondence/external/ccs', controller: 'correspondence_carbon_copies', action: 'update'

  get '/crmgt/auth/menu/actions', controller: 'user_menu_actions', action: 'get_menu_action'
  get '/crmgt/auth/user/menu/actions', controller: 'user_menu_actions', action: 'get_assigned_menu_action'
  post '/crmgt/auth/user/menu/actions', controller: 'user_menu_actions', action: 'assign_menu_action'

  get '/crmgt/manuscript', controller: 'manuscripts', action: 'index'
  get '/crmgt/manuscript/assigned', controller: 'manuscripts', action: 'get_assigned'
  get '/crmgt/manuscript/archived', controller: 'manuscripts', action: 'archived'
  get '/crmgt/manuscript/get_next_no', controller: 'manuscripts', action: 'get_next_no'
  post '/crmgt/manuscript', controller: 'manuscripts', action: 'create'
  put '/crmgt/manuscript', controller: 'manuscripts', action: 'update'
  patch '/crmgt/manuscript', controller: 'manuscripts', action: 'update'
  put '/crmgt/manuscript/update_status', controller: 'manuscripts', action: 'update_status'
  put '/crmgt/manuscript/archive', controller: 'manuscripts', action: 'archive'

  post '/crmgt/manuscript/upload', controller: 'manuscripts', action: 'upload'
  post '/crmgt/manuscript/upload_archives', controller: 'manuscripts', action: 'upload_archive'
  post '/crmgt/manuscript/change_uploads', controller: 'manuscripts', action: 'change_uploads'
  post '/crmgt/manuscript/change_upload_archives', controller: 'manuscripts', action: 'change_upload_archives'
  get '/crmgt/manuscript/get_documents', controller: 'manuscripts', action: 'get_documents'
  get '/crmgt/manuscript/get_archive_att', controller: 'manuscripts', action: 'get_archive_att'

  get '/crmgt/manuscript/authors', controller: 'manuscript_authors', action: 'index'
  get '/crmgt/manuscript/author/manuscripts', controller: 'manuscript_authors', action: 'get_author_manuscripts'
  post '/crmgt/manuscript/authors', controller: 'manuscript_authors', action: 'create'
  put '/crmgt/manuscript/authors', controller: 'manuscript_authors', action: 'update'
  patch '/crmgt/manuscript/authors', controller: 'manuscript_authors', action: 'update'

  get '/crmgt/manuscript/assignments', controller: 'manuscript_assignments', action: 'index'
  post '/crmgt/manuscript/assignments', controller: 'manuscript_assignments', action: 'create'
  put '/crmgt/manuscript/assignments', controller: 'manuscript_assignments', action: 'update'
  patch '/crmgt/manuscript/assignments', controller: 'manuscript_assignments', action: 'update'

  get '/crmgt/manuscript/comments', controller: 'manuscript_comments', action: 'index'
  post '/crmgt/manuscript/comments', controller: 'manuscript_comments', action: 'create'
  put '/crmgt/manuscript/comments', controller: 'manuscript_comments', action: 'update'
  patch '/crmgt/manuscript/comments', controller: 'manuscript_comments', action: 'update'
end
