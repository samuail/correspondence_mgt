set :stage, :production
set :rails_env, :production

server "10.90.104.75", user: "deploy", roles: %w{app db web}
