lock "~> 3.14.1"

set :application, "correspondence_mgt"
set :repo_url, "git@bitbucket.org:samuail/correspondence_mgt.git"
# git@bitbucket.org:samuail/correspondence_mgt.git
# https://samuail@bitbucket.org/samuail/correspondence_mgt.git

set :deploy_to, "/home/deploy/Projects/API/#{fetch :application}"

append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "vendor/bundle", ".bundle", "public/system"
append :linked_files, "config/credentials/production.key"
set :linked_files, %w{config/credentials/production.key}

set :keep_releases, 5

set :pg_without_sudo, false
set :pg_host, 'localhost'
set :pg_database, 'correspondence_mgt_production'
set :pg_username, 'correspondence_mgt'

set :passenger_restart_with_touch, true

namespace :deploy do
  desc 'Copy database and secrets yml files'
  task :upload_yml do
    on roles :all do
      execute "mkdir -p #{shared_path}/config"
      upload! StringIO.new(File.read("config/database.yml")), "#{shared_path}/config/database.yml"
    end
  end

  namespace :check do
    before :linked_files, :set_master_key do
      on roles(:app), in: :sequence, wait: 10 do
        unless test("[ -f #{shared_path}/config/credentials/production.key ]")
          upload! 'config/credentials/production.key', "#{shared_path}/config/credentials/production.key"
        end
      end
    end
  end

  desc 'Seed the database'
  task :seed do
    puts "\n=== Seeding Database ===\n"
    on primary :db do
      within current_path do
        with rails_env: fetch(:stage) do
          execute :rake, 'db:seed'
        end
      end
    end
  end
end

before "deploy:check:linked_files", "deploy:upload_yml"
after 'deploy:symlink:release', 'deploy:seed'
