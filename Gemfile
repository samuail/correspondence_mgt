source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.7.0'

gem 'rails', '~> 6.0', '>= 6.0.3.2'

gem 'pg', '~> 1.2', '>= 1.2.3'

gem 'puma', '~> 4.3', '>= 4.3.5'

gem 'bootsnap', '~> 1.4', '>= 1.4.7', require: false

gem 'rack-cors', :require => 'rack/cors'

gem 'sate_common', :git => 'https://samuail@bitbucket.org/samuail/sate_common.git',
    :branch => 'version_6'

gem 'sate_auth', :git => 'https://samuail@bitbucket.org/samuail/sate_auth.git',
    :branch => 'version_6'

gem 'jwt', '~> 1.5', '>= 1.5.4'

gem 'simple_command', '~> 0.1.0'

gem 'active_model_serializers', '~> 0.10.10'

gem 'image_processing', '~> 1.12', '>= 1.12.1'

gem 'active_storage_validations', '~> 0.9.6'

group :development, :test do
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]

  gem 'rspec-rails', '~> 3.4', '>= 3.4.2'

  gem 'cucumber-rails', '~> 2.1', require: false

  gem 'shoulda-matchers', '~> 4.3'

  gem 'factory_bot_rails', '~> 6.1'

  gem 'ffaker', '~> 2.16'
end

group :development do
  gem 'listen', '~> 3.2', '>= 3.2.1'

  gem 'spring', '~> 2.1'

  gem 'spring-watcher-listen', '~> 2.0', '>= 2.0.1'

  gem 'capistrano', '~> 3.14', '>= 3.14.1'

  gem 'capistrano-postgresql', '~> 6.2'

  gem 'capistrano-rails', '~> 1.6', '>= 1.6.1'

  gem 'capistrano-passenger', '~> 0.2.0'

  gem 'capistrano-rbenv', '~> 2.2'
end

group :test do
  gem 'database_cleaner', '~> 1.8', '>= 1.8.5'

  gem 'rails-controller-testing', '~> 1.0', '>= 1.0.5'

  gem 'simplecov', '~> 0.18.5',  require: false
end

gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]