Feature: Add organization to the application.
  As a system user
  I want to add organization into the system
  So that i can access the system activities
  So that the system users can be able to create organization information.

  Background:
    Given I have the following application module information
      |     application_name      | code |
      | Correspondence Management | CRMT |
    And I have the following user information
      | first_name | last_name |    email    |  password  |     application_name      |
      | Samuel     | Teshome   | sam@abc.com | myPassword | Correspondence Management |

  @create_by_authorized_user
  Scenario: Adding organization by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following "organization type"
      | code |     name        |      application_name     |
      | GOV  | Government Body | Correspondence Management |
    When I want to add organization with the following details
      | code  |    description   | organization_type |
      | MOFED | Ministry of FED  |  Government Body  |
    Then I should have this organization information
      | code  |    description   | department_type |
      | MOFED | Ministry of FED  | Government Body |
    And I should see a success message "Organization Saved Successfully!"

  @create_with_error_message
  Scenario: Adding organization by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following "organization type"
      | code |     name        |      application_name     |
      | GOV  | Government Body | Correspondence Management |
    And I have the following organization
      | code  |    description   | organization_type |
      | MOFED | Ministry of FED  |  Government Body  |
    When I want to add organization with the following details
      | code  |    description   | organization_type |
      | MOFED | Ministry of FED  |  Government Body  |
    Then I should see an error message "Organization" "Code has already been taken" and "Description has already been taken"

  @create_by_Expired_Session
  Scenario: Adding organization with expired session
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following "organization type"
      | code |     name        |      application_name     |
      | GOV  | Government Body | Correspondence Management |
    When I want to add organization with the following details with expired user session
      | code  |    description   | organization_type |
      | MOFED | Ministry of FED  |  Government Body  |
    Then I should see an error message "Signature has expired"

  @Update_by_authorized_user
  Scenario: Editing organization  by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following "organization type"
      | code |     name        |      application_name     |
      | GOV  | Government Body | Correspondence Management |
    And I have the following organization
      | code  |    description   | organization_type |
      | MOFED | Ministry of FED  |  Government Body  |
    When I want to edit organization by the description "Ministry of FED" with the following details
      |                 description                  |
      | Ministry of Finance and Economic Development |
    Then  I should have this organization information
      | code  |                 description                  | organization_type |
      | MOFED | Ministry of Finance and Economic Development |  Government Body  |
    And I should see a success message "Organization Updated Successfully!"

  @Update_with_error_message
  Scenario: Editing organization  by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following "organization type"
      | code |     name        |      application_name     |
      | GOV  | Government Body | Correspondence Management |
    And I have the following organization
      | code  |    description   | organization_type |
      | MOFED | Ministry of FED  |  Government Body  |
    And I have the following organization
      |  code |          description         | organization_type |
      | MOFED | Ministry of Foreign Affairs  |  Government Body  |
    When I want to edit organization by the description "Ministry of FED" with the following details
      |        description          |
      | Ministry of Foreign Affairs |
    Then I should see an error message "Organization" "Code has already been taken" and "Description has already been taken"

  @update_by_Expired_Session
  Scenario: Editing organization  with expired session
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following "organization type"
      | code |     name        |      application_name     |
      | GOV  | Government Body | Correspondence Management |
    And I have the following organization
      | code  |    description   | organization_type |
      | MOFED | Ministry of FED  |  Government Body  |
    When I want to edit organization by the description "Ministry of FED" with the following details with expired user session
      |                 description                  |
      | Ministry of Finance and Economic Development |
    Then I should see an error message "Signature has expired"

  @list_all_organization
  Scenario: List all organization information
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following "organization type"
      | code |     name        |      application_name     |
      | GOV  | Government Body | Correspondence Management |
    And I have the following organization
      | code  |    description   | organization_type |
      | MOFED | Ministry of FED  |  Government Body  |
    And I have the following organization
      | code |          description         | organization_type |
      | MOFA | Ministry of Foreign Affairs  |  Government Body  |
    When I want to see all organization information
    Then I should have the following "2" organization information
      | code  |          description         | organization_type |
      | MOFED | Ministry of FED              |  Government Body  |
      | MOFA  | Ministry of Foreign Affairs  |  Government Body  |