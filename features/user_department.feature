Feature: Assign department and Role/Position to a specific user.
  As a system user
  I want to assign department and role to a given user
  So that i can associate the user to his/her department and role
  So that the user can be assigned with activities related to the specified department and role/position.

  Background:
    Given I have the following application module information
      |     application_name      | code |
      | Correspondence Management | CRMT |
    And I have the following user information
      | first_name | last_name |    email    |  password  |     application_name      |
      | Samuel     | Teshome   | sam@abc.com | myPassword | Correspondence Management |
    And I have the following "department type"
      | code |  name  |      application_name     |
      | CAM  | Campus | Correspondence Management |
    And I have the following "department type"
      | code |  name   |      application_name     |
      | UNT  |  Unit   | Correspondence Management |
    And I have the following department
      | code | description  | department_type | parent |
      | MAC  | Main Campus  |    Campus       |        |
    And I have the following department
      | code |    description    | department_type |    parent   |
      | PRO  | President Office  |      Unit       | Main Campus |
    And I have the following user role information
      |   name   |      application_name     |
      | Director | Correspondence Management |

  @create_by_authorized_user
  Scenario: Assigning department and role to a user by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    When I want to assign department and role to the current user with the following details
      |    department    |   role   |
      | President Office | Director |
    Then I should have this user department role information
      |     user    |    department    |   role   |
      | sam@abc.com | President Office | Director |
    And I should see a success message "Department and Role Assigned Successfully!"

  @create_with_error_message
  Scenario: Assigning department and role to a user by authorized user to get error
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following user department role information
      |     user    |    department    |   role   |
      | sam@abc.com | President Office | Director |
    When I want to assign department and role to the current user with the following details
      |    department    |   role   |
      | President Office | Director |
    Then I should see an error message "User Department Role User has already been taken"

  @create_by_Expired_Session
  Scenario: Assigning department and role to a user with expired session
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    When I want to assign department and role to the current user with the following details with expired user session
      |    department    |   role   |
      | President Office | Director |
    Then I should see an error message "Signature has expired"

  @Update_by_authorized_user
  Scenario: Editing department and role assigned to a user by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following user department role information
      |     user    |    department    |   role   |
      | sam@abc.com | President Office | Director |
    And I have the following user role information
      |     name    |      application_name     |
      | Team Leader | Correspondence Management |
    When I want to edit assigned department and role information of the current user with the following details
      |     role    |
      | Team Leader |
    Then  I should have this user department role information
      |     user    |    department    |     role    |
      | sam@abc.com | President Office | Team Leader |
    And I should see a success message "Department and Role Updated Successfully!"

  @Update_with_error_message
  Scenario: Editing department and role assigned to a user by authorized user to get error
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following user department role information
      |     user    |    department    |   role   |
      | sam@abc.com | President Office | Director |
    When I want to edit assigned department and role information of the current user with the following details
      | role |
      |      |
    Then I should see an error message "User Department Role User role must exist"

  @update_by_Expired_Session
  Scenario: Editing department and role assigned to a user with expired session
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following user department role information
      |     user    |    department    |   role   |
      | sam@abc.com | President Office | Director |
    When I want to edit assigned department and role information of the current user with the following details with expired user session
      |    department    |     role    |
      | President Office | Team Leader |
    Then I should see an error message "Signature has expired"

  @list_Department_Role_assigned_to_a_user
  Scenario: List Department and Role assigned to a user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following user department role information
      |     user    |    department    |   role   |
      | sam@abc.com | President Office | Director |
    When I want to get department and role assigned to the current user
    Then I should have the following "1" department and role information assigned to a current user
      |     user    |    department    |   role   |
      | sam@abc.com | President Office | Director |
