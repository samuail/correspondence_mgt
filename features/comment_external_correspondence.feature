Feature: Add Comment to External Correspondence by a given user.
  As a system user
  I want to add comment to external correspondence by a given user
  so that the user can attach additional information as comment to an external correspondence

  Background:
    Given I have the following application module information
      |     application_name      | code |
      | Correspondence Management | CRMT |
    And I have the following user information
      | first_name | last_name |    email    |  password  |     application_name      |
      | Samuel     | Teshome   | sam@abc.com | myPassword | Correspondence Management |
    And I have the following user information
      | first_name | last_name |    email     |  password  |     application_name      |
      | Teshome    | Samuel    | tesh@abc.com | myPassword | Correspondence Management |
    And I have the following "department type"
      | code |  name  |      application_name     |
      | CAM  | Campus | Correspondence Management |
    And I have the following "department type"
      | code |  name   |      application_name     |
      | UNT  |  Unit   | Correspondence Management |
    And I have the following department
      | code | description  | department_type | parent |
      | MAC  | Main Campus  |    Campus       |        |
    And I have the following department
      | code |    description    | department_type |    parent   |
      | PRO  | President Office  |      Unit       | Main Campus |
    And I have the following department
      | code |  description    | department_type |    parent   |
      | PRO  | Finance Office  |      Unit       | Main Campus |
    And I have the following user role information
      |   name   |      application_name     |
      | Director | Correspondence Management |
    And I have the following user department role information
      |     user    |  department    |   role   |
      | sam@abc.com | Finance Office | Director |
    And I have the following user department role information
      |     user     |  department    |   role   |
      | tesh@abc.com | Finance Office | Director |
    And I have the following "organization type"
      | code |     name        |      application_name     |
      | GOV  | Government Body | Correspondence Management |
    And I have the following organization
      | code  |    description   | organization_type |
      | MOFED | Ministry of FED  |  Government Body  |

  @add_comment_by_authorized_user
  Scenario: Add comment to an external correspondence by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following external "incoming" correspondence
      | reference_no | letter_date |     subject    |   key_word   |        from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1,word2  | Ministry of FED  |   04/16/2019  |
    When I want to add comment to "ABC/000/00" "external" "incoming" correspondence with the following details
      |     comment     |
      | For your action |
    Then I have "1" comment assigned to the "external" "incoming" correspondence "ABC/000/00" commented by "sam@abc.com"
    And I should see a success message "Correspondence Comment Saved Successfully!"

  @add_another_comment_by_authorized_user
  Scenario: Add comment to an external correspondence by authorized user
    And I am logged in to the system with email "tesh@abc.com" and password "myPassword"
    And I have the following external "incoming" correspondence
      | reference_no | letter_date |     subject    |   key_word   |        from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1,word2  | Ministry of FED  |   04/16/2019  |
    And I have the following "external" "incoming" correspondence comment
      | reference_no |    comment      |
      |  ABC/000/00  | For your action |
    When I want to add comment to "ABC/000/00" "external" "incoming" correspondence with the following details
      |       comment        |
      | For your Information |
    Then I have "2" comment assigned to the "external" "incoming" correspondence "ABC/000/00" commented by "tesh@abc.com"
    And I should see a success message "Correspondence Comment Saved Successfully!"

  @add_comment_with_error_message
  Scenario: Add comment to an external correspondence by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following external "incoming" correspondence
      | reference_no | letter_date |     subject    |   key_word   |        from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1,word2  | Ministry of FED  |   04/16/2019  |
    When I want to add comment to "ABC/000/00" "external" "incoming" correspondence with the following details
      | comment |
      |         |
    Then I should see an error message "Correspondence Comment Content can't be blank"

  @add_comment_by_Expired_Session
  Scenario: Add comment to an external correspondence with expired session
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following external "incoming" correspondence
      | reference_no | letter_date |     subject    |   key_word   |        from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1,word2  | Ministry of FED  |   04/16/2019  |
    When I want to add comment to "ABC/000/00" "external" "incoming" correspondence with the following details with expired session
      |     comment     |
      | For your action |
    Then I should see an error message "Signature has expired"

  @Update_comment_by_authorized_user
  Scenario: Update comment to an external correspondence by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following external "incoming" correspondence
      | reference_no | letter_date |     subject    |   key_word   |        from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1,word2  | Ministry of FED  |   04/16/2019  |
    And I have the following "external" "incoming" correspondence comment
      | reference_no |    comment      |
      |  ABC/000/00  | For your action |
    When I want to update "For your action" comment to "ABC/000/00" "external" "incoming" correspondence with the following details
      |       comment        |
      | For your information |
    Then I should be able to change "external" "incoming" correspondence "ABC/000/00" comment to "For your information"
    And I should see a success message "Correspondence Comment Updated Successfully!"

  @Update_comment_with_error_message
  Scenario: Update comment to an external correspondence by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following external "incoming" correspondence
      | reference_no | letter_date |     subject    |   key_word   |        from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1,word2  | Ministry of FED  |   04/16/2019  |
    And I have the following "external" "incoming" correspondence comment
      | reference_no |    comment      |
      |  ABC/000/00  | For your action |
    When I want to update "For your action" comment to "ABC/000/00" "external" "incoming" correspondence with the following details
      | comment |
      |         |
    Then I should see an error message "Correspondence Comment Content can't be blank"

  @Update_comment_by_Expired_Session
  Scenario: Update comment to an external correspondence with expired session
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following external "incoming" correspondence
      | reference_no | letter_date |     subject    |   key_word   |        from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1,word2  | Ministry of FED  |   04/16/2019  |
    And I have the following "external" "incoming" correspondence comment
      | reference_no |    comment      |
      |  ABC/000/00  | For your action |
    When I want to update "For your action" comment to "ABC/000/00" "external" "incoming" correspondence with the following details with expired session
      |       comment        |
      | For your information |
    Then I should see an error message "Signature has expired"

  @get_correspondence_comment_by_authorized_user
  Scenario: Get all external correspondence comment information with authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following external "incoming" correspondence
      | reference_no | letter_date |     subject    |   key_word   |        from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1,word2  | Ministry of FED  |   04/16/2019  |
    And I have the following "external" "incoming" correspondence comment
      | reference_no |    comment      |
      |  ABC/000/00  | For your action |
    When I want to get all comment information for "ABC/000/00" "external" "incoming" correspondence
    Then I should have "1" "external" "incoming" correspondence comment information with the following details
      | reference_no |  commented by  |      comment      |
      |  ABC/000/00  | Samuel Teshome |  For your action  |
