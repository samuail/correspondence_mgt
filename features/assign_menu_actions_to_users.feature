Feature: Assign Menu Action to a given user Information.
  As a system user
  I want to assign menu action information
  so that the user can perform activities associated with that menu action

  Background:
    Given I have the following application module information
      |     application_name      | code |
      | Correspondence Management | CRMT |
    And I have the following user information
      | first_name | last_name |    email    |  password  |     application_name      |
      | Samuel     | Teshome   | sam@abc.com | myPassword | Correspondence Management |
    And I have the following "action"
      | code | name |
      | ADD  |  Add |
    And I have the following "action"
      | code | name |
      | EDT  | Edit |
    And I have the following menu information
      |      text      | icon_cls |  class_name  | location | parent |     application_name      |
      | Correspondence |   xxx    |   abc.xyz    |   xyz    |   nil  | Correspondence Management |
    And I have the following menu information
      |   text   | icon_cls |  class_name  | location |      parent    |     application_name      |
      | Incoming |   xxx    |   abc.xyz    |   xyz    | Correspondence | Correspondence Management |
    And I have the following menu information
      |   text   | icon_cls |  class_name  | location |      parent    |     application_name      |
      | Outgoing |   zzz    |   xyz.abc    |   xyz    | Correspondence | Correspondence Management |
    And I have the following action under the menu "Incoming"
      | action |
      |  Add   |
      |  Edit  |
    And I have the following action under the menu "Outgoing"
      | action |
      |  Add   |
      |  Edit  |

  @assign_by_authorized_user
  Scenario: Assigning Menu Action by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    When I want to assign the following menu action information to the user "sam@abc.com"
      |   menu    | action |
      | Incoming  | Add    |
    Then I have the following "1" menu action assigned to the user "sam@abc.com"
      |      user      |   menu   | action | active |
      | Samuel Teshome | Incoming | Add    |  true  |
    And I should see a success message "Menu Action Assigned Successfully!"

  @assign_by_Expired_Session
  Scenario: Assigning Menu Action with expired session
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    When I want to assign the following menu action information to the user "sam@abc.com" with expired session
      |   menu    | action |
      | Incoming  | Add    |
    Then I should see an error message "Signature has expired"

  @get_menu_action
  Scenario: Get actions allowed under a menu
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    When I want to get action assigned to the menu items "Incoming" and "Outgoing"
    Then I should have the following menu action
      |   menu    | action |
      | Incoming  | Add    |
      | Incoming  | Edit   |
      | Outgoing  | Add    |
      | Outgoing  | Edit   |

  @get_user_menu_action
  Scenario: Get menu actions assigned to a user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following menu action assigned to the user "sam@abc.com"
      |   menu    | action |
      | Incoming  | Add    |
    And I have the following menu action assigned to the user "sam@abc.com"
      |   menu    | action |
      | Incoming  | Edit   |
    And I have the following menu action assigned to the user "sam@abc.com"
      |   menu    | action |
      | Outgoing  | Add    |
    And I have the following menu action assigned to the user "sam@abc.com"
      |   menu    | action |
      | Outgoing  | Edit   |
    And I have the following menu information assigned to the user "sam@abc.com"
      |     text       |
      | Correspondence |
    When I want to get menu action assigned to the user "sam@abc.com"
    Then I should have the following menu action
      |   menu    | action |
      | Incoming  | Add    |
      | Incoming  | Edit   |
      | Outgoing  | Add    |
      | Outgoing  | Edit   |
