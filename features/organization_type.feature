Feature: Add organization type to the application.
  As a system user
  I want to add organization type into the system
  So that i can access the system activities
  So that the system users can be able to create organization types and associate them with organization.

  Background:
    Given I have the following application module information
      |     application_name      | code |
      | Correspondence Management | CRMT |
    And I have the following user information
      | first_name | last_name |    email    |  password  |     application_name      |
      | Samuel     | Teshome   | sam@abc.com | myPassword | Correspondence Management |

  @create_by_authorized_user
  Scenario: Adding organization type by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    When I want to add "organization type" with the following details
      | code |       name      |    application_module     |
      | GOV  | Government Body | Correspondence Management |
    Then I should have this "organization type" information
      | code |       name      |
      | GOV  | Government Body |
    And I should see a success message "Organization type Saved Successfully!"

  @create_with_error_message
  Scenario: Adding organization type by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following "organization type"
      | code |       name      |
      | GOV  | Government Body |
    When I want to add "organization type" with the following details
      | code |       name      |
      | GOV  | Government Body |
    Then I should see an error message "Organization type" "Code has already been taken" "Name has already been taken" and "Type has already been taken"

  @create_by_Expired_Session
  Scenario: Adding organization type with expired session
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    When I want to add "organization type" with the following details with expired user session
      | code |       name      |
      | GOV  | Government Body |
    Then I should see an error message "Signature has expired"

  @Update_by_authorized_user
  Scenario: Editing organization type by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following "organization type"
      | code |       name      |
      | GOV  | Government Body |
    When I want to edit "organization type" by the name "Government Body" with the following details
      |   name     |
      | Government |
    Then I should have this "organization type" information
      | code |   name     |
      | GOV  | Government |
    And I should see a success message "Organization type Updated Successfully!"

  @Update_with_error_message
  Scenario: Editing organization type by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following "organization type"
      | code |       name      |
      | GOV  | Government Body |
    And I have the following "organization type"
      | code |   name     |
      | GOV  | Government |
    When I want to edit "organization type" by the name "Government Body" with the following details
      |   name     |
      | Government |
    Then I should see an error message "Organization type" "Code has already been taken" "Name has already been taken" and "Type has already been taken"

  @update_by_Expired_Session
  Scenario: Editing organization type with expired session
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following "organization type"
      | code |       name      |
      | GOV  | Government Body |
    When I want to edit "organization type" by the name "Government Body" with the following details with expired user session
      |   name     |
      | Government |
    Then I should see an error message "Signature has expired"

  @list_all_Organization_Type
  Scenario: List all Organization Type information
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following "organization type"
      | code |       name      |
      | GOV  | Government Body |
    And I have the following "organization type"
      | code |      name      |
      | NGO  | Non Government |
    When I want to see all "organization type" information
    Then I should have the following "2" "organization type" information
      | code |      name       |
      | GOV  | Government Body |
      | NGO  | Non Government  |
