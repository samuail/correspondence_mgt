Feature: Assign External Correspondence to a given user.
  As a system user
  I want to assign external correspondence to a given user
  so that the user can perform further activities related to the correspondence

  Background:
    Given I have the following application module information
      |     application_name      | code |
      | Correspondence Management | CRMT |
    And I have the following user information
      | first_name | last_name |    email    |  password  |     application_name      |
      | Samuel     | Teshome   | sam@abc.com | myPassword | Correspondence Management |
    And I have the following user information
      | first_name | last_name |    email     |  password  |     application_name      |
      | Teshome    | Samuel    | tesh@abc.com | myPassword | Correspondence Management |
    And I have the following user information
      | first_name | last_name |    email     |  password  |     application_name      |
      | Abebe      | Teshome   | abe@abc.com  | myPassword | Correspondence Management |
    And I have the following "department type"
      | code |  name  |      application_name     |
      | CAM  | Campus | Correspondence Management |
    And I have the following "department type"
      | code |  name   |      application_name     |
      | UNT  |  Unit   | Correspondence Management |
    And I have the following department
      | code | description  | department_type | parent |
      | MAC  | Main Campus  |    Campus       |        |
    And I have the following department
      | code |    description    | department_type |    parent   |
      | PRO  | President Office  |      Unit       | Main Campus |
    And I have the following department
      | code |  description    | department_type |    parent   |
      | PRO  | Finance Office  |      Unit       | Main Campus |
    And I have the following user role information
      |   name   |      application_name     |
      | Director | Correspondence Management |
    And I have the following user department role information
      |     user    |  department    |   role   |
      | sam@abc.com | Finance Office | Director |
    And I have the following user department role information
      |     user     |  department    |   role   |
      | tesh@abc.com | Finance Office | Director |
    And I have the following "organization type"
      | code |     name        |      application_name     |
      | GOV  | Government Body | Correspondence Management |
    And I have the following organization
      | code  |    description   | organization_type |
      | MOFED | Ministry of FED  |  Government Body  |

  @assign_by_authorized_user
  Scenario: Assigning external correspondence by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following external "incoming" correspondence
      | reference_no | letter_date |     subject    |   key_word    |       from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1, word2  | Ministry of FED |   04/16/2019  |
    When I want to assign "ABC/000/00" "external" "incoming" correspondence to the following user
      | first_name | last_name |    email     |
      | Teshome    | Samuel    | tesh@abc.com |
    Then I have the following "1" "external" "incoming" correspondence assigned to the user "tesh@abc.com"
    And I should see a success message "Correspondence Assignment Saved Successfully!"

  @assign_by_authorized_user
  Scenario: Assigning external correspondence by authorized user
    And I am logged in to the system with email "tesh@abc.com" and password "myPassword"
    And I have the following external "incoming" correspondence
      | reference_no | letter_date |     subject    |   key_word    |       from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1, word2  | Ministry of FED |   04/16/2019  |
    And I have the following "external" "incoming" correspondence assigned by the user "sam@abc.com"
      | reference_no | letter_date |     subject    |   key_word   |        from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1,word2  | President Office |   04/16/2019  |
    When I want to assign "ABC/000/00" "external" "incoming" correspondence to the following user
      | first_name | last_name |    email     |
      | Abebe      | Teshome   | abe@abc.com  |
    Then I have the following "1" "external" "incoming" correspondence assigned to the user "abe@abc.com"
    And I should see a success message "Correspondence Assignment Saved Successfully!"

  @assign_by_Expired_Session
  Scenario: Assigning external correspondence with expired session
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following external "incoming" correspondence
      | reference_no | letter_date |     subject    |   key_word    |       from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1, word2  | Ministry of FED |   04/16/2019  |
    When I want to assign "ABC/000/00" "external" "incoming" correspondence to the following user with expired session
      | first_name | last_name |    email     |
      | Teshome    | Samuel    | tesh@abc.com |
    Then I should see an error message "Signature has expired"

  @update_assigned_by_authorized_user
  Scenario: Update Assigned external correspondence by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following external "incoming" correspondence
      | reference_no | letter_date |     subject    |   key_word    |       from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1, word2  | Ministry of FED |   04/16/2019  |
    And I have the following "external" "incoming" correspondence assigned to the user "tesh@abc.com"
      | reference_no | letter_date |     subject    |   key_word    |       from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1, word2  | Ministry of FED |   04/16/2019  |
    When I want to update "ABC/000/00" "external" "incoming" correspondence assigned to the user "tesh@abc.com" to the following user
      | first_name | last_name |    email     |
      | Abebe      | Teshome   | abe@abc.com  |
    Then I have the following "1" "external" "incoming" correspondence assigned to the user "abe@abc.com"
    And I have the following "0" "external" "incoming" correspondence assigned to the user "tesh@abc.com"
    And I should see a success message "Correspondence Assignment Updated Successfully!"

  @update_assigned_by_authorized_user
  Scenario: Update Assigned external correspondence by authorized user
    And I am logged in to the system with email "tesh@abc.com" and password "myPassword"
    And I have the following external "incoming" correspondence
      | reference_no | letter_date |     subject    |   key_word    |       from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1, word2  | Ministry of FED |   04/16/2019  |
    And I have the following "external" "incoming" correspondence assigned by the user "sam@abc.com"
      | reference_no | letter_date |     subject    |   key_word    |       from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1, word2  | Ministry of FED |   04/16/2019  |
    When I want to received "external" "incoming" correspondence "ABC/000/00" assigned to the current user by the user "sam@abc.com"
    Then I should be able to change the status of the assignment to "Received"
    And I should see a success message "Correspondence Assignment Updated Successfully!"

  @update_assign_by_Expired_Session
  Scenario: Update Assigned external correspondence with expired session
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following external "incoming" correspondence
      | reference_no | letter_date |     subject    |   key_word    |       from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1, word2  | Ministry of FED |   04/16/2019  |
    And I have the following "external" "incoming" correspondence assigned to the user "tesh@abc.com"
      | reference_no | letter_date |     subject    |   key_word    |       from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1, word2  | Ministry of FED |   04/16/2019  |
    When I want to update "ABC/000/00" "external" "incoming" correspondence assigned to the user "tesh@abc.com" to the following user with expired session
      | first_name | last_name |    email     |
      | Abebe      | Teshome   | abe@abc.com  |
    Then I should see an error message "Signature has expired"

  @get_assigned_by_authorized_user
  Scenario: Get all external correspondence assignment information with authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following external "incoming" correspondence
      | reference_no | letter_date |     subject    |   key_word    |       from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1, word2  | Ministry of FED |   04/16/2019  |
    And I have the following "external" "incoming" correspondence assigned to the user "tesh@abc.com"
      | reference_no | letter_date |     subject    |   key_word    |       from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1, word2  | Ministry of FED |   04/16/2019  |
    When I want to get all assignment information for "ABC/000/00" "external" "incoming" correspondence
    Then I should have "1" correspondence assignment information with the following details
      |   from_user    |    to_user     | reference_no |
      | Samuel Teshome | Teshome Samuel |  ABC/000/00  |
