Feature: Upload attachment image related to a given correspondence.
  As a system user
  I want to upload one or more attachment images to a correspondence
  so that I can have soft copy entry of the documents related to a given correspondence

  Background:
    Given I have the following application module information
      |     application_name      | code |
      | Correspondence Management | CRMT |
    And I have the following user information
      | first_name | last_name |    email    |  password  |     application_name      |
      | Samuel     | Teshome   | sam@abc.com | myPassword | Correspondence Management |
    And I have the following "department type"
      | code |  name  |      application_name     |
      | CAM  | Campus | Correspondence Management |
    And I have the following "department type"
      | code |  name   |      application_name     |
      | UNT  |  Unit   | Correspondence Management |
    And I have the following department
      | code | description  | department_type | parent |
      | MAC  | Main Campus  |    Campus       |        |
    And I have the following department
      | code |    description    | department_type |    parent   |
      | PRO  | President Office  |      Unit       | Main Campus |
    And I have the following department
      | code |  description    | department_type |    parent   |
      | PRO  | Finance Office  |      Unit       | Main Campus |
    And I have the following user role information
      |   name   |      application_name     |
      | Director | Correspondence Management |
    And I have the following user department role information
      |     user    |  department    |   role   |
      | sam@abc.com | Finance Office | Director |
    And I have the following "organization type"
      | code |     name        |      application_name     |
      | GOV  | Government Body | Correspondence Management |
    And I have the following organization
      | code  |    description   | organization_type |
      | MOFED | Ministry of FED  |  Government Body  |

  @upload_one_attachment_image_by_authorized_user
  Scenario: Upload External Incoming Correspondence Image by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following external "incoming" correspondence
      | reference_no | letter_date |     subject    |   key_word    |       from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1, word2  | Ministry of FED |   04/16/2019  |
    When I want to upload "1" "Attachment" image for an external "incoming" correspondence with reference number "ABC/000/00"
    Then I should see a success message "Image Uploaded Successfully!"

  @upload_more_than_one_attachment_image_by_authorized_user
  Scenario: Upload External Incoming Correspondence Image by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following external "incoming" correspondence
      | reference_no | letter_date |     subject    |   key_word    |       from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1, word2  | Ministry of FED |   04/16/2019  |
    When I want to upload "2" "Attachment" image for an external "incoming" correspondence with reference number "ABC/000/00"
    Then I should see a success message "Image Uploaded Successfully!"

  @change_uploaded_attachment_image_by_authorized_user
  Scenario: Change Upload External Incoming Correspondence Image by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following external "incoming" correspondence
      | reference_no | letter_date |     subject    |   key_word    |       from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1, word2  | Ministry of FED |   04/16/2019  |
    And I have one Main and two Attachment images uploaded for the external "incoming" correspondence with reference number "ABC/000/00"
    When I want to change uploaded "Attachment" image for an external "incoming" correspondence with reference number "ABC/000/00"
    Then I should see a success message "Image Uploaded Successfully!"

  @upload_one_or_more_attachment_image_by_error_message_invalid_content
  Scenario: Upload External Incoming Correspondence Image by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following external "incoming" correspondence
      | reference_no | letter_date |     subject    |   key_word    |       from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1, word2  | Ministry of FED |   04/16/2019  |
    When I want to upload invalid "Attachment" file for an external "incoming" correspondence with reference number "ABC/000/00"
    Then I should see an error message "External Correspondence Attachment images must be an image"

  @upload_one_or_more_attachment_image_by_error_message_big_content
  Scenario: Upload External Incoming Correspondence Image by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following external "incoming" correspondence
      | reference_no | letter_date |     subject    |   key_word    |       from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1, word2  | Ministry of FED |   04/16/2019  |
    When I want to upload big "Attachment" file for an external "incoming" correspondence with reference number "ABC/000/00"
    Then I should see an error message "External Correspondence Attachment images is too big"

  @upload_one_or_more_attachment_image_by_Expired_Session
  Scenario: Upload External Incoming Correspondence Image by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following external "incoming" correspondence
      | reference_no | letter_date |     subject    |   key_word    |       from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1, word2  | Ministry of FED |   04/16/2019  |
    When I want to upload image for an external "incoming" correspondence with reference number "ABC/000/00" with expired user session
    Then I should see an error message "Signature has expired"

#  ========= Get External Correspondence Images ==============

  @get_external_incoming_correspondence_images_by_authorized_user
  Scenario: Get External Correspondence Image by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following external "incoming" correspondence
      | reference_no | letter_date |     subject    |   key_word    |       from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1, word2  | Ministry of FED |   04/16/2019  |
    And I have one Main and two Attachment images uploaded for the external "incoming" correspondence with reference number "ABC/000/00"
    When I want to get the soft copy images of the external "incoming" correspondence with reference number "ABC/000/00"
    Then I should have one Main and two Attachment images
