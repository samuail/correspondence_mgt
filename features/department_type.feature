Feature: Add department type to the application.
  As a system user
  I want to add department type into the system
  So that i can access the system activities
  So that the system users can be able to create department types and associate them with department.

  Background:
    Given I have the following application module information
      |     application_name      | code |
      | Correspondence Management | CRMT |
    And I have the following user information
      | first_name | last_name |    email    |  password  |     application_name      |
      | Samuel     | Teshome   | sam@abc.com | myPassword | Correspondence Management |

  @create_by_authorized_user
  Scenario: Adding department type by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    When I want to add "department type" with the following details
      | code |  name  |    application_module     |
      | CAM  | Campus | Correspondence Management |
      Then I should have this department type information
      | code |  name  |
      | CAM  | Campus |
    And I should see a success message "Department type Saved Successfully!"

  @create_with_error_message
  Scenario: Adding department type by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following "department type"
      | code |  name  |      application_name     |
      | CAM  | Campus | Correspondence Management |
    When I want to add "department type" with the following details
      | code |  name  |    application_module     |
      | CAM  | Campus | Correspondence Management |
    Then I should see an error message "Department type" "Code has already been taken" "Name has already been taken" and "Type has already been taken"

  @create_by_Expired_Session
  Scenario: Adding department type with expired session
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    When I want to add "department type" with the following details with expired user session
      | code |  name  |    application_module     |
      | CAM  | Campus | Correspondence Management |
    Then I should see an error message "Signature has expired"

  @Update_by_authorized_user
  Scenario: Editing department type by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following "department type"
      | code |  name  |      application_name     |
      | CAM  | Campus | Correspondence Management |
    When I want to edit "department type" by the name "Campus" with the following details
      |   name  |
      | College |
    Then I should have this department type information
      | code |   name  |
      | CAM  | College |
    And I should see a success message "Department type Updated Successfully!"

  @Update_with_error_message
  Scenario: Editing department type by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following "department type"
      | code |  name  |      application_name     |
      | CAM  | Campus | Correspondence Management |
    And I have the following "department type"
      | code |  name   |      application_name     |
      | CAM  | College | Correspondence Management |
    When I want to edit "department type" by the name "Campus" with the following details
      |   name  |
      | College |
    Then I should see an error message "Department type" "Code has already been taken" "Name has already been taken" and "Type has already been taken"

  @update_by_Expired_Session
  Scenario: Editing department type with expired session
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following "department type"
      | code |  name  |      application_name     |
      | CAM  | Campus | Correspondence Management |
    When I want to edit "department type" by the name "Campus" with the following details with expired user session
      |   name  |
      | College |
    Then I should see an error message "Signature has expired"

  @list_all_Department_Type
  Scenario: List all Department Type information
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following "department type"
      | code |  name  |      application_name     |
      | CAM  | Campus | Correspondence Management |
    And I have the following "department type"
      | code |  name   |      application_name     |
      | COL  | College | Correspondence Management |
    When I want to see all "department type" information
    Then I should have the following "2" "department type" information
      | code |  name   |      application_name     |
      | CAM  | Campus  | Correspondence Management |
      | COL  | College | Correspondence Management |
