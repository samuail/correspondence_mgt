Feature: Add department to the application.
  As a system user
  I want to add department into the system
  So that i can access the system activities
  So that the system users can be able to create department information.

  Background:
    Given I have the following application module information
      |     application_name      | code |
      | Correspondence Management | CRMT |
    And I have the following user information
      | first_name | last_name |    email    |  password  |     application_name      |
      | Samuel     | Teshome   | sam@abc.com | myPassword | Correspondence Management |

  @create_by_authorized_user
  Scenario: Adding department by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following "department type"
      | code |  name  |      application_name     |
      | CAM  | Campus | Correspondence Management |
    When I want to add department with the following details
      | code | description  | department_type | parent |
      | MAC  | Main Campus  |    Campus       |        |
    Then I should have this department information
      | code | description  | department_type | parent |
      | MAC  | Main Campus  |    Campus       |        |
    And I should see a success message "Department Saved Successfully!"

  @create_with_error_message
  Scenario: Adding department by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following "department type"
      | code |  name  |      application_name     |
      | CAM  | Campus | Correspondence Management |
    And I have the following department
      | code | description  | department_type | parent |
      | MAC  | Main Campus  |    Campus       |        |
    When I want to add department with the following details
      | code | description  | department_type | parent |
      | MAC  | Main Campus  |    Campus       |        |
    Then I should see an error message "Department" "Code has already been taken" and "Description has already been taken"

  @create_by_Expired_Session
  Scenario: Adding department with expired session
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following "department type"
      | code |  name  |      application_name     |
      | CAM  | Campus | Correspondence Management |
    When I want to add department with the following details with expired user session
      | code | description  | department_type | parent |
      | MAC  | Main Campus  |    Campus       |        |
    Then I should see an error message "Signature has expired"

  @Update_by_authorized_user
  Scenario: Editing department  by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following "department type"
      | code |  name  |      application_name     |
      | CAM  | Campus | Correspondence Management |
    And I have the following department
      | code | description  | department_type | parent |
      | MAC  | Main Campus  |    Campus       |        |
    When I want to edit department by the description "Main Campus" with the following details
      |           description             |
      | College of Business and Economics |
    Then  I should have this department information
      | code |           description              | department_type | parent |
      | MAC  | College of Business and Economics  |    Campus       |        |
    And I should see a success message "Department Updated Successfully!"

  @Update_with_error_message
  Scenario: Editing department  by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following "department type"
      | code |  name  |      application_name     |
      | CAM  | Campus | Correspondence Management |
    And I have the following department
      | code | description  | department_type | parent |
      | MAC  | Main Campus  |    Campus       |        |
    And I have the following department
      | code |           description              | department_type | parent |
      | MAC  | College of Business and Economics  |    Campus       |        |
    When I want to edit department by the description "Main Campus" with the following details
      |           description             |
      | College of Business and Economics |
    Then I should see an error message "Department" "Code has already been taken" and "Description has already been taken"

  @update_by_Expired_Session
  Scenario: Editing department  with expired session
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following "department type"
      | code |  name  |      application_name     |
      | CAM  | Campus | Correspondence Management |
    And I have the following department
      | code | description  | department_type | parent |
      | MAC  | Main Campus  |    Campus       |        |
    When I want to edit department by the description "Main Campus" with the following details with expired user session
      |           description             |
      | College of Business and Economics |
    Then I should see an error message "Signature has expired"

  @list_all_Department
  Scenario: List all Department information
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following "department type"
      | code |  name  |      application_name     |
      | CAM  | Campus | Correspondence Management |
    And I have the following department
      | code | description  | department_type | parent |
      | MAC  | Main Campus  |    Campus       |        |
    And I have the following department
      | code |           description              | department_type | parent |
      | MAC  | College of Business and Economics  |    Campus       |        |
    When I want to see all department information
    Then I should have the following "2" department information
      | code |           description              | department_type | parent |
      | MAC  |           Main Campus              |    Campus       |        |
      | MAC  | College of Business and Economics  |    Campus       |        |

  @list_all_Department_as_tree
  Scenario: Fetching department tree items
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following "department type"
      | code |  name  |      application_name     |
      | CAM  | Campus | Correspondence Management |
    And I have the following "department type"
      | code |  name   |      application_name     |
      | UNT  |  Unit   | Correspondence Management |
    And I have the following department
      | code | description  | department_type | parent |
      | MAC  | Main Campus  |    Campus       |        |
    And I have the following department
      | code |    description    | department_type |    parent   |
      | PRO  | President Office  |      Unit       | Main Campus |
    And I have the following department
      | code |           description              | department_type | parent |
      | CBE  | College of Business and Economics  |     Campus      |        |
    And I have the following department
      | code |           description               | department_type |              parent               |
      | ACC  | Department of Accounting & Finance  |      Unit       | College of Business and Economics |
    When I want to fetch department items
    Then I should have "2" parent department
    And I should have "1" department defined under the department "Main Campus"
    And I should have "1" department defined under the department "College of Business and Economics"