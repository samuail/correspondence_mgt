Feature: Capture Internal Correspondence
  As a system User
  I want to add internal correspondence into the system
  so that i can be able to make use of the correspondence information

  Background:
    Given I have the following application module information
      |     application_name      | code |
      | Correspondence Management | CRMT |
    And I have the following user information
      | first_name | last_name |    email    |  password  |     application_name      |
      | Samuel     | Teshome   | sam@abc.com | myPassword | Correspondence Management |
    And I have the following "department type"
      | code |  name  |      application_name     |
      | CAM  | Campus | Correspondence Management |
    And I have the following "department type"
      | code |  name   |      application_name     |
      | UNT  |  Unit   | Correspondence Management |
    And I have the following department
      | code | description  | department_type | parent |
      | MAC  | Main Campus  |    Campus       |        |
    And I have the following department
      | code |    description    | department_type |    parent   |
      | PRO  | President Office  |      Unit       | Main Campus |
    And I have the following department
      | code |  description    | department_type |    parent   |
      | PRO  | Finance Office  |      Unit       | Main Campus |
    And I have the following user role information
      |   name   |      application_name     |
      | Director | Correspondence Management |
    And I have the following user department role information
      |     user    |  department    |   role   |
      | sam@abc.com | Finance Office | Director |

  @create_internal_incoming_by_authorized_user
  Scenario: Adding Internal Incoming Correspondence by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    When I want to add an internal "incoming" correspondence with the following details
      | reference_no | letter_date |     subject    |   key_word   |        from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1,word2  | President Office |   04/16/2019  |
    Then I should have this internal "incoming" correspondence information
      | reference_no | letter_date |     subject    |   key_word   |        from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1,word2  | President Office |   04/16/2019  |
    And I should see a success message "Internal Correspondence Saved Successfully!"

  @create_internal_incoming_with_error_message
  Scenario: Adding Internal Correspondence by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following internal "incoming" correspondence
      | reference_no | letter_date |     subject    |   key_word   |        from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1,word2  | President Office |   04/16/2019  |
    When I want to add an internal "incoming" correspondence with the following details
      | reference_no | letter_date |     subject    |   key_word   |        from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1,word2  | President Office |   04/16/2019  |
    Then I should see an error message "Internal Correspondence Reference no has already been taken"

  @create_internal_incoming_by_Expired_Session
  Scenario: Adding Internal Correspondence with expired session
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    When I want to add an internal "incoming" correspondence with the following details with expired user session
      | reference_no | letter_date |     subject    |   key_word   |        from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1, word2 | President Office |   04/16/2019  |
    Then I should see an error message "Signature has expired"

  @Update_internal_incoming_by_authorized_user
  Scenario: Editing Internal Correspondence by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following internal "incoming" correspondence
      | reference_no | letter_date |     subject    |   key_word   |        from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1, word2 | President Office |   04/16/2019  |
    When I want to edit internal "incoming" correspondence by the reference no "ABC/000/00" with the following details
      | reference_no | letter_date |     subject    |   key_word   |        from      | received_date |
      |  ABC/001/13  |  04/16/2019 | Sample Subject | word1, word2 | President Office |   04/16/2019  |
    Then I should have this internal "incoming" correspondence information
      | reference_no |
      |  ABC/001/13  |
    And I should see a success message "Internal Correspondence Updated Successfully!"

  @Update_internal_incoming_with_error_message
  Scenario: Editing Internal Correspondence by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following internal "incoming" correspondence
      | reference_no | letter_date |     subject    |   key_word   |        from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1, word2 | President Office |   04/16/2019  |
    And I have the following internal "incoming" correspondence
      | reference_no | letter_date |     subject    |   key_word   |        from      | received_date |
      |  ABC/001/13  |  04/16/2019 | Sample Subject | word1, word2 | President Office |   04/16/2019  |
    When I want to edit internal "incoming" correspondence by the reference no "ABC/000/00" with the following details
      | reference_no |
      |  ABC/001/13  |
    Then I should see an error message "Internal Correspondence Reference no has already been taken"

  @update_internal_incoming_by_Expired_Session
  Scenario: Editing Internal Correspondence with expired session
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following internal "incoming" correspondence
      | reference_no | letter_date |     subject    |   key_word   |        from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1, word2 | President Office |   04/16/2019  |
    When I want to edit internal "incoming" correspondence by the reference no "ABC/000/00" with the following details with expired user session
      | reference_no |
      |  ABC/001/13  |
    Then I should see an error message "Signature has expired"

  # ======= External Incoming Correspondence ======= #

  @create_external_incoming_by_authorized_user
  Scenario: Adding Internal Incoming Correspondence by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    When I want to add an internal "outgoing" correspondence with the following details
      | reference_no | letter_date |     subject    |   key_word   |        to        | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1,word2  | President Office |   04/16/2019  |
    Then I should have this internal "outgoing" correspondence information
      | reference_no | letter_date |     subject    |   key_word   |        to        | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1,word2  | President Office |   04/16/2019  |
    And I should see a success message "Internal Correspondence Saved Successfully!"

  @create_external_incoming_with_error_message
  Scenario: Adding Internal Correspondence by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following internal "outgoing" correspondence
      | reference_no | letter_date |     subject    |   key_word   |        to        | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1,word2  | President Office |   04/16/2019  |
    When I want to add an internal "outgoing" correspondence with the following details
      | reference_no | letter_date |     subject    |   key_word   |        to        | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1,word2  | President Office |   04/16/2019  |
    Then I should see an error message "Internal Correspondence Reference no has already been taken"

  @create_external_incoming_by_Expired_Session
  Scenario: Adding Internal Correspondence with expired session
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    When I want to add an internal "outgoing" correspondence with the following details with expired user session
      | reference_no | letter_date |     subject    |   key_word   |        to        | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1, word2 | President Office |   04/16/2019  |
    Then I should see an error message "Signature has expired"

  @Update_external_incoming_by_authorized_user
  Scenario: Editing Internal Correspondence by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following internal "outgoing" correspondence
      | reference_no | letter_date |     subject    |   key_word   |        to        | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1, word2 | President Office |   04/16/2019  |
    When I want to edit internal "outgoing" correspondence by the reference no "ABC/000/00" with the following details
      | reference_no | letter_date |     subject    |   key_word   |        to        | received_date |
      |  ABC/001/13  |  04/16/2019 | Sample Subject | word1, word2 | President Office |   04/16/2019  |
    Then I should have this internal "outgoing" correspondence information
      | reference_no |
      |  ABC/001/13  |
    And I should see a success message "Internal Correspondence Updated Successfully!"

  @Update_external_incoming_with_error_message
  Scenario: Editing Internal Correspondence by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following internal "outgoing" correspondence
      | reference_no | letter_date |     subject    |   key_word   |        to        | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1, word2 | President Office |   04/16/2019  |
    And I have the following internal "outgoing" correspondence
      | reference_no | letter_date |     subject    |   key_word   |        to        | received_date |
      |  ABC/001/13  |  04/16/2019 | Sample Subject | word1, word2 | President Office |   04/16/2019  |
    When I want to edit internal "outgoing" correspondence by the reference no "ABC/000/00" with the following details
      | reference_no |
      |  ABC/001/13  |
    Then I should see an error message "Internal Correspondence Reference no has already been taken"

  @update_external_incoming_by_Expired_Session
  Scenario: Editing Internal Correspondence with expired session
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following internal "outgoing" correspondence
      | reference_no | letter_date |     subject    |   key_word   |        to        | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1, word2 | President Office |   04/16/2019  |
    When I want to edit internal "outgoing" correspondence by the reference no "ABC/000/00" with the following details with expired user session
      | reference_no |
      |  ABC/001/13  |
    Then I should see an error message "Signature has expired"

  @List_all_incoming_internal_correspondence_for_the_current_user
  Scenario: List all Internal Correspondence of the Logged in User
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following internal "incoming" correspondence
      | reference_no | letter_date |     subject    |   key_word   |        from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1,word2  | President Office |   04/16/2019  |
    And I have the following internal "incoming" correspondence
      | reference_no | letter_date |     subject    |   key_word   |        from      | received_date |
      |  ABC/001/13  |  04/16/2019 | Sample Subject | word1,word2  | President Office |   04/16/2019  |
    When I want to fetch all internal "incoming" correspondence for the current user
    Then I should have the "2" internal "incoming" correspondence details for the current user
      | reference_no | letter_date |     subject    |   key_word   |        from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1,word2  | President Office |   04/16/2019  |
      |  ABC/001/13  |  04/16/2019 | Sample Subject | word1,word2  | President Office |   04/16/2019  |

  @List_all_outgoing_internal_correspondence_for_the_current_user
  Scenario: List all Internal Correspondence of the Logged in User
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following internal "outgoing" correspondence
      | reference_no | letter_date |     subject    |   key_word   |        to        | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1,word2  | President Office |   04/16/2019  |
    And I have the following internal "outgoing" correspondence
      | reference_no | letter_date |     subject    |   key_word   |        to        | received_date |
      |  ABC/001/13  |  04/16/2019 | Sample Subject | word1,word2  | President Office |   04/16/2019  |
    When I want to fetch all internal "outgoing" correspondence for the current user
    Then I should have the "2" internal "outgoing" correspondence details for the current user
      | reference_no | letter_date |     subject    |   key_word   |        to        | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1,word2  | President Office |   04/16/2019  |
      |  ABC/001/13  |  04/16/2019 | Sample Subject | word1,word2  | President Office |   04/16/2019  |