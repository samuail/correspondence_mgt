When('I want to assign {string} {string} {string} correspondence to the following user') do
|reference_no, direction, request_type, user_detail|
  to_user = Sate::Auth::User.find_by email: user_detail.raw[1][2]
  from_user = @response["data"]["user"]
  if direction == "internal"
    correspondence = Sate::Crmgt::InternalCorrespondence.find_by_reference_no reference_no
    path = "/crmgt/correspondence/internal/assignment"
  elsif request_type == "incoming"
    correspondence = Sate::Crmgt::ExternalIncomingCorrespondence.find_by_reference_no reference_no
    path = "/crmgt/correspondence/external/assignment?request_type=" + request_type
  end
  token = @response["data"]["access_token"]
  header 'Authorization', "Bearer #{token}"

  @response = post path, { :correspondence_assignment => { from_user_id: from_user["id"],
                                                           to_user_id: to_user.id,
                                                           correspondence_id: correspondence.id } }
  @response = JSON(@response.body)
end

Then('I have the following {string} {string} {string} correspondence assigned to the user {string}') do
|count, direction, request_type, user_detail|
  if direction == "internal"
    correspondence_type = "Sate::Crmgt::InternalCorrespondence"
  elsif request_type == "incoming"
    correspondence_type = "Sate::Crmgt::ExternalIncomingCorrespondence"
  end
  to_user = Sate::Auth::User.find_by email: user_detail
  assignment = Sate::Crmgt::CorrespondenceAssignment.where(to_user_id:  to_user.id)

  expect(assignment.count).to eq count.to_i
  if count.to_i > 0
    expect(assignment.first.correspondence_type).to eq correspondence_type
  end
end

Given('I have the following {string} {string} correspondence assigned by the user {string}') do
|direction, request_type, email, correspondence|
  if direction == "internal"
    correspondence = Sate::Crmgt::InternalCorrespondence.find_by_reference_no correspondence.raw[1][0]
  elsif request_type == "incoming"
    correspondence = Sate::Crmgt::ExternalIncomingCorrespondence.find_by_reference_no correspondence.raw[1][0]
  end
  to_user = @response["data"]["user"]
  from_user = Sate::Auth::User.find_by email: email
  assignment = Sate::Crmgt::CorrespondenceAssignment.new(from_user_id: from_user.id, to_user_id: to_user["id"], correspondence: correspondence)
  assignment.application_module_id = @app_module.id
  assignment.assigned_date = Date.today
  assignment.received_date = Date.today + 4
  assignment.status = "Assigned"
  assignment.order = 1
  assignment.save
end

When('I want to assign {string} {string} {string} correspondence to the following user with expired session') do
|reference_no, direction, request_type, user_detail|
  to_user = Sate::Auth::User.find_by email: user_detail.raw[1][2]
  from_user = @response["data"]["user"]
  if direction == "internal"
    correspondence = Sate::Crmgt::InternalCorrespondence.find_by_reference_no reference_no
    path = "/crmgt/correspondence/internal/assignment"
  elsif request_type == "incoming"
    correspondence = Sate::Crmgt::ExternalIncomingCorrespondence.find_by_reference_no reference_no
    path = "/crmgt/correspondence/external/assignment?request_type=" + request_type
  end

  user_id = @response["data"]["user"]["id"]
  header 'Authorization', "Bearer #{ JsonWebToken.encode({ user_id: user_id }, (Time.now.to_i - 10)) }"

  @response = post path, { :correspondence_assignment => { from_user_id: from_user["id"],
                                                           to_user_id: to_user.id,
                                                           correspondence_id: correspondence.id } }
  @response = JSON(@response.body)
end

Given('I have the following {string} {string} correspondence assigned to the user {string}') do
|direction, request_type, email, correspondence|
  if direction == "internal"
    correspondence = Sate::Crmgt::InternalCorrespondence.find_by_reference_no correspondence.raw[1][0]
  elsif request_type == "incoming"
    correspondence = Sate::Crmgt::ExternalIncomingCorrespondence.find_by_reference_no correspondence.raw[1][0]
  end
  from_user = @response["data"]["user"]
  to_user = Sate::Auth::User.find_by email: email
  assignment = Sate::Crmgt::CorrespondenceAssignment.new(from_user_id: from_user["id"], to_user_id: to_user.id, correspondence: correspondence)
  assignment.application_module_id = @app_module.id
  assignment.assigned_date = Date.today
  assignment.received_date = Date.today + 4
  assignment.status = "Assigned"
  assignment.order = 1
  assignment.save
end

When('I want to update {string} {string} {string} correspondence assigned to the following user') do
 |reference_no, direction, request_type, user_detail|
      to_user = Sate::Auth::User.find_by email: user_detail.raw[1][2]
      from_user = @response["data"]["user"]
      if direction == "internal"
        correspondence = Sate::Crmgt::InternalCorrespondence.find_by_reference_no reference_no
        path = "/crmgt/correspondence/internal/assignment"
      elsif request_type == "incoming"
        correspondence = Sate::Crmgt::ExternalIncomingCorrespondence.find_by_reference_no reference_no
        path = "/crmgt/correspondence/external/assignment?request_type=" + request_type
      end
      token = @response["data"]["access_token"]
      header 'Authorization', "Bearer #{token}"

      existing_assignment = Sate::Crmgt::CorrespondenceAssignment.where(from_user_id: from_user["id"],
                                                                        to_user_id:  to_user.id,
                                                                        correspondence: correspondence)
      byebug

      @response = put path, { id: existing_assignment.to_param,
                              :correspondence_assignment => { to_user_id: to_user.id } }
      @response = JSON(@response.body)
end

When('I want to update {string} {string} {string} correspondence assigned to the user {string} to the following user') do
|reference_no, direction, request_type, old_user, user_detail|
  old_to_user = Sate::Auth::User.find_by email: old_user
  to_user = Sate::Auth::User.find_by email: user_detail.raw[1][2]
  from_user = @response["data"]["user"]
  if direction == "internal"
    correspondence = Sate::Crmgt::InternalCorrespondence.find_by_reference_no reference_no
    path = "/crmgt/correspondence/internal/assignment"
  elsif request_type == "incoming"
    correspondence = Sate::Crmgt::ExternalIncomingCorrespondence.find_by_reference_no reference_no
    path = "/crmgt/correspondence/external/assignment?request_type=" + request_type
  end
  token = @response["data"]["access_token"]
  header 'Authorization', "Bearer #{token}"

  existing_assignment = Sate::Crmgt::CorrespondenceAssignment.where(from_user_id: from_user["id"],
                                                                    to_user_id:  old_to_user.id,
                                                                    correspondence: correspondence)

  @response = put path, { id: existing_assignment.first.to_param,
                          :correspondence_assignment => { to_user_id: to_user.id } }
  @response = JSON(@response.body)
end

When('I want to received {string} {string} correspondence {string} assigned to the current user by the user {string}') do
|direction, request_type, reference_no, from_user_email|
  from_user = Sate::Auth::User.find_by email: from_user_email
  to_user = @response["data"]["user"]
  if direction == "internal"
    correspondence = Sate::Crmgt::InternalCorrespondence.find_by_reference_no reference_no
    path = "/crmgt/correspondence/internal/assignment"
  elsif request_type == "incoming"
    correspondence = Sate::Crmgt::ExternalIncomingCorrespondence.find_by_reference_no reference_no
    path = "/crmgt/correspondence/external/assignment?request_type=" + request_type
  end
  token = @response["data"]["access_token"]
  header 'Authorization', "Bearer #{token}"

  existing_assignment = Sate::Crmgt::CorrespondenceAssignment.where(from_user_id: from_user.id,
                                                                    to_user_id:  to_user["id"],
                                                                    correspondence: correspondence)

  @response = put path, { id: existing_assignment.first.to_param,
                          :correspondence_assignment => { status: "Received" } }
  @response = JSON(@response.body)
end

Then('I should be able to change the status of the assignment to {string}') do |status|
  expect(@response["data"]["status"]).to eq status
end

When('I want to update {string} {string} {string} correspondence assigned to the user {string} to the following user with expired session') do
|reference_no, direction, request_type, old_user, user_detail|
  old_to_user = Sate::Auth::User.find_by email: old_user
  to_user = Sate::Auth::User.find_by email: user_detail.raw[1][2]
  from_user = @response["data"]["user"]
  if direction == "internal"
    correspondence = Sate::Crmgt::InternalCorrespondence.find_by_reference_no reference_no
    path = "/crmgt/correspondence/internal/assignment"
  elsif request_type == "incoming"
    correspondence = Sate::Crmgt::ExternalIncomingCorrespondence.find_by_reference_no reference_no
    path = "/crmgt/correspondence/external/assignment?request_type=" + request_type
  end

  user_id = @response["data"]["user"]["id"]
  header 'Authorization', "Bearer #{ JsonWebToken.encode({ user_id: user_id }, (Time.now.to_i - 10)) }"

  existing_assignment = Sate::Crmgt::CorrespondenceAssignment.where(from_user_id: from_user["id"],
                                                                    to_user_id:  old_to_user.id,
                                                                    correspondence: correspondence)

  @response = put path, { id: existing_assignment.first.to_param,
                          :correspondence_assignment => { to_user_id: to_user.id } }
  @response = JSON(@response.body)
end

When('I want to get all assignment information for {string} {string} {string} correspondence') do
|reference_no, direction, request_type|
  if direction == "internal"
    correspondence = Sate::Crmgt::InternalCorrespondence.find_by_reference_no reference_no
    path = "/crmgt/correspondence/internal/assignment?correspondence_id=" + correspondence.id.to_s
  elsif request_type == "incoming"
    correspondence = Sate::Crmgt::ExternalIncomingCorrespondence.find_by_reference_no reference_no
    path = "/crmgt/correspondence/external/assignment?request_type=" + request_type + "&correspondence_id=" + correspondence.id.to_s
  end

  token = @response["data"]["access_token"]
  header 'Authorization', "Bearer #{token}"

  @response = get path
  @response = JSON(@response.body)
end

Then('I should have {string} correspondence assignment information with the following details') do |count, assignment|
  from_user = @response["data"][0]["from_user"]["first_name"] + " " + @response["data"][0]["from_user"]["last_name"]
  to_user = @response["data"][0]["to_user"]["first_name"] + " " + @response["data"][0]["to_user"]["last_name"]
  reference_no = @response["data"][0]["correspondence"]["reference_no"]

  expect(@response["total"]).to eq count.to_i
  expect(assignment.raw[1][0]).to eq from_user
  expect(assignment.raw[1][1]).to eq to_user
  expect(assignment.raw[1][2]).to eq reference_no
end