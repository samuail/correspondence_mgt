When('I want to add an internal {string} correspondence with the following details') do
|clazz, data|
  path = "/crmgt/correspondence/internal?request_type=" + clazz

  token = @response["data"]["access_token"]
  header 'Authorization', "Bearer #{token}"

  date = Date.strptime(data.raw[1][1], "%m/%d/%Y")

  from = Sate::Crmgt::Department.find_by_description data.raw[1][4]
  if clazz == "incoming"
    @response = post path, { :internal_correspondence => { reference_no: data.raw[1][0],
                                                           letter_date: date,
                                                           subject: data.raw[1][2],
                                                           search_keywords: data.raw[1][3].split(','),
                                                           source_id: from.id,
                                                           destination_id: nil } }
  else
    @response = post path, { :internal_correspondence => { reference_no: data.raw[1][0],
                                                           letter_date: date,
                                                           subject: data.raw[1][2],
                                                           search_keywords: data.raw[1][3].split(','),
                                                           source_id: nil,
                                                           destination_id: from.id } }
  end
  @response = JSON(@response.body)
end

Then('I should have this internal {string} correspondence information') do |clazz, data|
  expect(@response["data"]["reference_no"]).to eq data.raw[1][0]
  if data.raw[1].length > 1
    expect(@response["data"]["letter_date"].to_s).to eq Date.strptime(data.raw[1][1], "%m/%d/%Y").to_s
    expect(@response["data"]["subject"]).to eq data.raw[1][2]
    expect(@response["data"]["search_keywords"]).to eq data.raw[1][3].split(',')
    if clazz == "incoming"
      expect(@response["data"]["source"]["description"]).to eq data.raw[1][4]
    else
      expect(@response["data"]["destination"]["description"]).to eq data.raw[1][4]
    end
  end
end

Given('I have the following internal {string} correspondence') do |clazz, data|
  date = Date.strptime(data.raw[1][1], "%m/%d/%Y")
  from = Sate::Crmgt::Department.find_by_description data.raw[1][4]
  destination = Sate::Crmgt::UserDepartment.find_by_user_id @response["data"]["user"]["id"]
  user_id = @response["data"]["user"]["id"]
  if clazz == "incoming"
    Sate::Crmgt::InternalCorrespondence.create reference_no: data.raw[1][0],
                                               letter_date: date,
                                               subject: data.raw[1][2],
                                               search_keywords: data.raw[1][3].split(','),
                                               source_id: from.id,
                                               destination_id: destination.department_id,
                                               received_date: Date.today,
                                               key_in_by_id: user_id,
                                               application_module_id: @app_module.id
  else
    Sate::Crmgt::InternalCorrespondence.create reference_no: data.raw[1][0],
                                               letter_date: date,
                                               subject: data.raw[1][2],
                                               search_keywords: data.raw[1][3].split(','),
                                               source_id: destination.department_id,
                                               destination_id: from.id,
                                               received_date: Date.today,
                                               key_in_by_id: user_id,
                                               application_module_id: @app_module.id
  end
end

When('I want to add an internal {string} correspondence with the following details with expired user session') do
|clazz, data|
  path = "/crmgt/correspondence/internal?request_type=" + clazz

  user_id = @response["data"]["user"]["id"]
  header 'Authorization', "Bearer #{ JsonWebToken.encode({ user_id: user_id }, (Time.now.to_i - 10)) }"

  date = Date.strptime(data.raw[1][1], "%m/%d/%Y")

  from = Sate::Crmgt::Department.find_by_description data.raw[1][4]
  if clazz == "incoming"
    @response = post path, { :internal_correspondence => { reference_no: data.raw[1][0],
                                                           letter_date: date,
                                                           subject: data.raw[1][2],
                                                           search_keywords: data.raw[1][3].split(','),
                                                           source_id: from.id,
                                                           destination_id: nil } }
  else
    @response = post path, { :internal_correspondence => { reference_no: data.raw[1][0],
                                                           letter_date: date,
                                                           subject: data.raw[1][2],
                                                           search_keywords: data.raw[1][3].split(','),
                                                           source_id: nil,
                                                           destination_id: from.id } }
  end
  @response = JSON(@response.body)
end

When('I want to edit internal {string} correspondence by the reference no {string} with the following details') do
|clazz, oldData, newData|
  path = "/crmgt/correspondence/internal?request_type=" + clazz

  token = @response["data"]["access_token"]
  header 'Authorization', "Bearer #{token}"

  internal_correspondence = Sate::Crmgt::InternalCorrespondence.find_by_reference_no oldData

  @response = put path, { :id => internal_correspondence.id, :internal_correspondence => { reference_no: newData.raw[1][0] } }
  @response = JSON(@response.body)
end

When('I want to edit internal {string} correspondence by the reference no {string} with the following details with expired user session') do
|clazz, oldData, newData|
  path = "/crmgt/correspondence/internal?request_type=" + clazz

  user_id = @response["data"]["user"]["id"]
  header 'Authorization', "Bearer #{ JsonWebToken.encode({ user_id: user_id }, (Time.now.to_i - 10)) }"

  internal_correspondence = Sate::Crmgt::InternalCorrespondence.find_by_reference_no oldData

  @response = put path, { :id => internal_correspondence.id, :internal_correspondence => { reference_no: newData.raw[1][0] } }
  @response = JSON(@response.body)
end

When('I want to fetch all internal {string} correspondence for the current user') do |clazz|
  path = "/crmgt/correspondence/internal?request_type=" + clazz + "&department_type=center"

  token = @response["data"]["access_token"]
  header 'Authorization', "Bearer #{token}"

  @response = get path
  @response = JSON(@response.body)
end

Then('I should have the {string} internal {string} correspondence details for the current user') do
|count, clazz, data|
  expect(@response["total"]).to eq count.to_i
  expect(@response["data"][0]["reference_no"]).to eq data.raw[1][0]
  expect(@response["data"][0]["letter_date"].to_s).to eq Date.strptime(data.raw[1][1], "%m/%d/%Y").to_s
  expect(@response["data"][0]["subject"]).to eq data.raw[1][2]
  expect(@response["data"][0]["search_keywords"]).to eq data.raw[1][3].split(',')
  expect(@response["data"][1]["reference_no"]).to eq data.raw[2][0]
  expect(@response["data"][1]["letter_date"].to_s).to eq Date.strptime(data.raw[2][1], "%m/%d/%Y").to_s
  expect(@response["data"][1]["subject"]).to eq data.raw[2][2]
  expect(@response["data"][1]["search_keywords"]).to eq data.raw[2][3].split(',')
  if clazz == "incoming"
    expect(@response["data"][0]["source"]["description"]).to eq data.raw[1][4]
    expect(@response["data"][1]["source"]["description"]).to eq data.raw[2][4]
  else
    expect(@response["data"][0]["destination"]["description"]).to eq data.raw[1][4]
    expect(@response["data"][1]["destination"]["description"]).to eq data.raw[2][4]
  end
end