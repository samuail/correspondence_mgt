Given('I have the following user role information') do |role|
  Sate::Auth::UserRole.create name: role.raw[1][0],
                              application_module_id: @app_module.id
end

When('I want to assign department and role to the current user with the following details') do |user_department_role|
  path = "/crmgt/auth/user/department_role"

  token = @response["data"]["access_token"]
  header 'Authorization', "Bearer #{token}"

  department = Sate::Crmgt::Department.find_by_description user_department_role.raw[1][0]
  role = Sate::Auth::UserRole.find_by name: user_department_role.raw[1][1]

  @response = post path, { :user_department => { user_id: @response["data"]["user"]["id"],
                                                 department_id: department.id,
                                                 user_role_id: role.id } }
  @response = JSON(@response.body)
end

Then('I should have this user department role information') do |data|
  expect(@response["data"]["user"]["email"]).to eq data.raw[1][0]
  expect(@response["data"]["department"]["description"]).to eq data.raw[1][1]
  expect(@response["data"]["user_role"]["name"]).to eq data.raw[1][2]
end

Given('I have the following user department role information') do |user_department_role|
  user = Sate::Auth::User.find_by email: user_department_role.raw[1][0]
  department = Sate::Crmgt::Department.find_by_description user_department_role.raw[1][1]
  role = Sate::Auth::UserRole.find_by name: user_department_role.raw[1][2]
  Sate::Crmgt::UserDepartment.create user_id: user.id, department_id: department.id,
                                     user_role_id: role.id, application_module_id: @app_module.id
end

When('I want to assign department and role to the current user with the following details with expired user session') do |user_department_role|
  path = "/crmgt/auth/user/department_role"

  user_id = @response["data"]["user"]["id"]
  header 'Authorization', "Bearer #{ JsonWebToken.encode({ user_id: user_id }, (Time.now.to_i - 10)) }"

  department = Sate::Crmgt::Department.find_by_description user_department_role.raw[1][0]
  role = Sate::Auth::UserRole.find_by name: user_department_role.raw[1][1]

  @response = post path, { :user_department => { user_id: @response["data"]["user"]["id"],
                                                 department_id: department.id,
                                                 user_role_id: role.id } }
  @response = JSON(@response.body)
end

When('I want to edit assigned department and role information of the current user with the following details') do |newData|
  path = "/crmgt/auth/user/department_role"

  token = @response["data"]["access_token"]
  header 'Authorization', "Bearer #{token}"

  user_department_role = Sate::Crmgt::UserDepartment.find_by_user_id @response["data"]["user"]["id"]

  role = Sate::Auth::UserRole.find_by name: newData.raw[1][0]
  role_id = role != nil ? role.id : nil
  @response = put path, { :id => user_department_role.id, :user_department => { user_role_id: role_id } }
  @response = JSON(@response.body)
end

When('I want to edit assigned department and role information of the current user with the following details with expired user session') do |newData|
  path = "/crmgt/auth/user/department_role"

  user_id = @response["data"]["user"]["id"]
  header 'Authorization', "Bearer #{ JsonWebToken.encode({ user_id: user_id }, (Time.now.to_i - 10)) }"

  user_department_role = Sate::Crmgt::UserDepartment.find_by_user_id @response["data"]["user"]["id"]

  role = Sate::Auth::UserRole.find_by name: newData.raw[1][0]
  role_id = role != nil ? role.id : nil
  @response = put path, { :id => user_department_role.id, :user_department => { user_role_id: role_id } }
  @response = JSON(@response.body)
end

When('I want to get department and role assigned to the current user') do
  path = "/crmgt/auth/user/" + @response["data"]["user"]["id"].to_s + "/department_role"

  token = @response["data"]["access_token"]
  header 'Authorization', "Bearer #{token}"

  @response = get path
  @response = JSON(@response.body)
end

Then('I should have the following {string} department and role information assigned to a current user') do |count, data|
  expect(@response["total"]).to eq count.to_i
  expect(@response["data"]["user"]["email"]).to eq data.raw[1][0]
  expect(@response["data"]["department"]["description"]).to eq data.raw[1][1]
  expect(@response["data"]["user_role"]["name"]).to eq data.raw[1][2]
end