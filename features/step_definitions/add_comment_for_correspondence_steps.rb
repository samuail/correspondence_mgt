When('I want to add comment to {string} {string} {string} correspondence with the following details') do
|reference_no, direction, request_type, data|
  if direction == "internal"
    correspondence = Sate::Crmgt::InternalCorrespondence.find_by_reference_no reference_no
    path = "/crmgt/correspondence/internal/comments"
  elsif request_type == "incoming"
    correspondence = Sate::Crmgt::ExternalIncomingCorrespondence.find_by_reference_no reference_no
    path = "/crmgt/correspondence/external/comments?request_type=" + request_type
  end
  token = @response["data"]["access_token"]
  header 'Authorization', "Bearer #{token}"

  @response = post path, { :correspondence_comment => { correspondence_id: correspondence.id,
                                                        content: data.raw[1][0] } }
  @response = JSON(@response.body)
end
Then('I have {string} comment assigned to the {string} {string} correspondence {string} commented by {string}') do
|count, direction, request_type, reference_no, user_detail|
  if direction == "internal"
    correspondence_type = "Sate::Crmgt::InternalCorrespondence"
    correspondence = Sate::Crmgt::InternalCorrespondence.find_by_reference_no reference_no
  elsif request_type == "incoming"
    correspondence_type = "Sate::Crmgt::ExternalIncomingCorrespondence"
    correspondence = Sate::Crmgt::ExternalIncomingCorrespondence.find_by_reference_no reference_no
  end
  user = Sate::Auth::User.find_by email: user_detail
  comment = Sate::Crmgt::CorrespondenceComment.where(commented_by_id: user.id,
                                                     correspondence_id: correspondence.id)

  expect(comment.count).to eq count.to_i
  if count.to_i > 0
    expect(comment.first.correspondence_type).to eq correspondence_type
  end
end

When('I want to add comment to {string} {string} {string} correspondence with the following details with expired session') do
|reference_no, direction, request_type, data|
  if direction == "internal"
    correspondence = Sate::Crmgt::InternalCorrespondence.find_by_reference_no reference_no
    path = "/crmgt/correspondence/internal/comments"
  elsif request_type == "incoming"
    correspondence = Sate::Crmgt::ExternalIncomingCorrespondence.find_by_reference_no reference_no
    path = "/crmgt/correspondence/external/comments?request_type=" + request_type
  end

  user_id = @response["data"]["user"]["id"]
  header 'Authorization', "Bearer #{ JsonWebToken.encode({ user_id: user_id }, (Time.now.to_i - 10)) }"

  @response = post path, { :correspondence_comment => { correspondence_id: correspondence.id,
                                                        content: data.raw[1][0] } }
  @response = JSON(@response.body)
end

Given('I have the following {string} {string} correspondence comment') do
|direction, request_type, comment|
  if direction == "internal"
    correspondence = Sate::Crmgt::InternalCorrespondence.find_by_reference_no comment.raw[1][0]
  elsif request_type == "incoming"
    correspondence = Sate::Crmgt::ExternalIncomingCorrespondence.find_by_reference_no comment.raw[1][0]
  end

  user_id = @response["data"]["user"]["id"]
  comment = Sate::Crmgt::CorrespondenceComment.new(commented_by_id: user_id,
                                                   correspondence: correspondence,
                                                   content: comment.raw[1][1])
  comment.application_module_id = @app_module.id
  comment.comment_date = DateTime.now
  comment.order = 1
  comment.save
end

When('I want to update {string} comment to {string} {string} {string} correspondence with the following details') do
|old_comment, reference_no, direction, request_type, new_comment|
  user = @response["data"]["user"]
  if direction == "internal"
    correspondence = Sate::Crmgt::InternalCorrespondence.find_by_reference_no reference_no
    path = "/crmgt/correspondence/internal/comments"
  elsif request_type == "incoming"
    correspondence = Sate::Crmgt::ExternalIncomingCorrespondence.find_by_reference_no reference_no
    path = "/crmgt/correspondence/external/comments?request_type=" + request_type
  end
  token = @response["data"]["access_token"]
  header 'Authorization', "Bearer #{token}"

  existing_comment = Sate::Crmgt::CorrespondenceComment.find_by(commented_by_id: user["id"],
                                                                correspondence: correspondence,
                                                                content: old_comment)

  @response = put path, { id: existing_comment.to_param,
                          :correspondence_comment => { content: new_comment.raw[1][0] } }
  @response = JSON(@response.body)
end

Then('I should be able to change {string} {string} correspondence {string} comment to {string}') do
|direction, request_type, reference_no, new_comment|
  expect(@response["data"]["content"]).to eq new_comment
end

When('I want to update {string} comment to {string} {string} {string} correspondence with the following details with expired session') do
|old_comment, reference_no, direction, request_type, new_comment|
  user = @response["data"]["user"]
  if direction == "internal"
    correspondence = Sate::Crmgt::InternalCorrespondence.find_by_reference_no reference_no
    path = "/crmgt/correspondence/internal/comments"
  elsif request_type == "incoming"
    correspondence = Sate::Crmgt::ExternalIncomingCorrespondence.find_by_reference_no reference_no
    path = "/crmgt/correspondence/external/comments?request_type=" + request_type
  end

  header 'Authorization', "Bearer #{ JsonWebToken.encode({ user_id: user["id"] }, (Time.now.to_i - 10)) }"

  existing_comment = Sate::Crmgt::CorrespondenceComment.find_by(commented_by_id: user["id"],
                                                                correspondence: correspondence,
                                                                content: old_comment)

  @response = put path, { id: existing_comment.to_param,
                          :correspondence_comment => { content: new_comment.raw[1][0] } }
  @response = JSON(@response.body)
end

When('I want to get all comment information for {string} {string} {string} correspondence') do
|reference_no, direction, request_type|
  if direction == "internal"
    correspondence = Sate::Crmgt::InternalCorrespondence.find_by_reference_no reference_no
    path = "/crmgt/correspondence/internal/comments?correspondence_id=" + correspondence.id.to_s
  elsif request_type == "incoming"
    correspondence = Sate::Crmgt::ExternalIncomingCorrespondence.find_by_reference_no reference_no
    path = "/crmgt/correspondence/external/comments?request_type=" + request_type + "&correspondence_id=" + correspondence.id.to_s
  end

  token = @response["data"]["access_token"]
  header 'Authorization', "Bearer #{token}"

  @response = get path
  @response = JSON(@response.body)
end

Then('I should have {string} {string} {string} correspondence comment information with the following details') do
|count, direction, request_type, comment|
  commented_by = @response["data"][0]["commented_by"]["first_name"] + " " + @response["data"][0]["commented_by"]["last_name"]
  reference_no = @response["data"][0]["correspondence"]["reference_no"]
  content = @response["data"][0]["content"]

  expect(@response["total"]).to eq count.to_i
  expect(comment.raw[1][0]).to eq reference_no
  expect(comment.raw[1][1]).to eq commented_by
  expect(comment.raw[1][2]).to eq content
end