When('I want to add organization with the following details') do |organization|
  path = "/crmgt/business_settings/organization"
  token = @response["data"]["access_token"]

  header 'Authorization', "Bearer #{token}"

  organization_type = Sate::Crmgt::OrganizationType.find_by_name organization.raw[1][2]
  @response = post path, { :organization => { code: organization.raw[1][0],
                                              description: organization.raw[1][1],
                                              organization_type_id: organization_type.id } }

  @response = JSON(@response.body)
end

Then('I should have this organization information') do |data|
  expect(@response["data"]["code"]).to eq data.raw[1][0]
  expect(@response["data"]["description"]).to eq data.raw[1][1]
end

Given('I have the following organization') do |data|
  organization_type = Sate::Crmgt::OrganizationType.find_by_name data.raw[1][2]
  # byebug
  Sate::Crmgt::Organization.create code: data.raw[1][0],
                                   description: data.raw[1][1],
                                   organization_type_id: organization_type.id,
                                   application_module_id: @app_module.id
end

When('I want to add organization with the following details with expired user session') do |organization|
  path = "/crmgt/business_settings/organization"
  user_id = @response["data"]["user"]["id"]

  header 'Authorization', "Bearer #{ JsonWebToken.encode({ user_id: user_id }, (Time.now.to_i - 10)) }"

  organization_type = Sate::Crmgt::OrganizationType.find_by_name organization.raw[1][2]

  @response = post path, { :organization => { code: organization.raw[1][0],
                                            description: organization.raw[1][1],
                                            organization_type_id: organization_type.id } }

  @response = JSON(@response.body)
end

When('I want to edit organization by the description {string} with the following details') do
|oldData, newData|
  path = "/crmgt/business_settings/organization"
  token = @response["data"]["access_token"]

  header 'Authorization', "Bearer #{token}"

  update_data = Sate::Crmgt::Organization.find_by_description oldData

  @response = put path, { :id => update_data.id, :organization => { description: newData.raw[1][0] } }

  @response = JSON(@response.body)
end

When('I want to edit organization by the description {string} with the following details with expired user session') do
|oldData, newData|
  path = "/crmgt/business_settings/organization"
  user_id = @response["data"]["user"]["id"]

  header 'Authorization', "Bearer #{ JsonWebToken.encode({ user_id: user_id }, (Time.now.to_i - 10)) }"

  update_data = Sate::Crmgt::Organization.find_by_description oldData

  @response = put path, { :id => update_data.id, :organization => { description: newData.raw[1][0] } }

  @response = JSON(@response.body)
end

When('I want to see all organization information') do
  path = "/crmgt/business_settings/organization"
  token = @response["data"]["access_token"]

  header 'Authorization', "Bearer #{token}"

  @response = get path
  @response = JSON(@response.body)
end

Then('I should have the following {string} organization information') do |count, data|
  expect(@response["data"].count).to eq count.to_i
  expect(@response["data"][0]["code"]).to eq data.raw[1][0]
  expect(@response["data"][0]["description"]).to eq data.raw[1][1]
  expect(@response["data"][1]["code"]).to eq data.raw[2][0]
  expect(@response["data"][1]["description"]).to eq data.raw[2][1]
end