Given('I have the following menu information') do |menu_item|
  if menu_item.raw[1][4] == "nil"
    parentId = nil
  else
    parent = Sate::Auth::Menu.find_by text: menu_item.raw[1][4]
    parentId = parent.id
  end
  Sate::Auth::Menu.create text: menu_item.raw[1][0],
                          icon_cls: menu_item.raw[1][1],
                          class_name: menu_item.raw[1][2],
                          location: menu_item.raw[1][3],
                          parent_id: parentId,
                          application_module_id: @app_module.id
end

Given('I have the following action under the menu {string}') do |menu, action|
  menu = Sate::Auth::Menu.find_by text: menu
  action1 = Sate::Crmgt::Action.find_by_name action.raw[1][0]
  action2 = Sate::Crmgt::Action.find_by_name action.raw[2][0]
  Sate::Crmgt::MenuAction.create menu_id: menu.id,
                                 action_id: action1.id,
                                 application_module_id: @app_module.id
  Sate::Crmgt::MenuAction.create menu_id: menu.id,
                                 action_id: action2.id,
                                 application_module_id: @app_module.id
end

When('I want to assign the following menu action information to the user {string}') do |user, menu_action|
  path = "/crmgt/auth/user/menu/actions"

  token = @response["data"]["access_token"]
  header 'Authorization', "Bearer #{token}"

  user = Sate::Auth::User.find_by email: user
  menu = Sate::Auth::Menu.find_by text: menu_action.raw[1][0]
  action = Sate::Crmgt::Action.find_by_name menu_action.raw[1][1]
  menu_action = Sate::Crmgt::MenuAction.find_by menu_id: menu.id,
                                                action_id: action.id
  @response = post path, { user_id: user.id,
                           menu_action_ids: menu_action.id }
  @response = JSON(@response.body)
end

Then('I have the following {string} menu action assigned to the user {string}') do |count, email, data|
  expect(@response["total"]).to eq count.to_i
  user = Sate::Auth::User.find_by email: email
  menu = Sate::Auth::Menu.find_by text: data.raw[1][1]
  action = Sate::Crmgt::Action.find_by_name data.raw[1][2]
  menu_action = Sate::Crmgt::MenuAction.find_by menu_id: menu.id,
                                                action_id: action.id
  expect(@response["data"][0]["user_id"]).to eq user.id
  expect(@response["data"][0]["menu_action_id"]).to eq menu_action.id
  expect(@response["data"][0]["active"].to_s).to eq data.raw[1][3]
end

Given('I have the following menu action assigned to the user {string}') do |email, menu_action|
  user = Sate::Auth::User.find_by email: email
  menu = Sate::Auth::Menu.find_by text: menu_action.raw[1][0]
  action = Sate::Crmgt::Action.find_by_name menu_action.raw[1][1]
  menu_action = Sate::Crmgt::MenuAction.find_by menu_id: menu.id,
                                                action_id: action.id
  Sate::Crmgt::UserMenuAction.create user_id: user.id,
                                     menu_action_id: menu_action.id,
                                     application_module_id: @app_module.id
end

When('I want to assign the following menu action information to the user {string} with expired session') do |email, menu_action|
  path = "/crmgt/auth/user/menu/actions"

  user_id = @response["data"]["user"]["id"]
  header 'Authorization', "Bearer #{ JsonWebToken.encode({ user_id: user_id }, (Time.now.to_i - 10)) }"

  user = Sate::Auth::User.find_by email: email
  menu = Sate::Auth::Menu.find_by text: menu_action.raw[1][0]
  action = Sate::Crmgt::Action.find_by_name menu_action.raw[1][1]
  menu_action = Sate::Crmgt::MenuAction.find_by menu_id: menu.id,
                                                action_id: action.id
  @response = post path, { user_id: user.id,
                           menu_action_id: menu_action.id }
  @response = JSON(@response.body)
end

When('I want to get action assigned to the menu items {string} and {string}') do |menu_text1, menu_text2|
  path = "/crmgt/auth/menu/actions?menu_names[]=" + menu_text1 + "&menu_names[]=" + menu_text2

  token = @response["data"]["access_token"]
  header 'Authorization', "Bearer #{token}"

  @response = get path
  @response = JSON(@response.body)
end

Then('I should have the following menu action') do |menu_action|
  expect(@response["data"][0]["menus"][0]["menu_text"]).to eq menu_action.raw[1][0]
  expect(@response["data"][0]["menus"][0]["actions"][0]["action_name"]).to eq menu_action.raw[1][1]
  expect(@response["data"][0]["menus"][0]["actions"][1]["action_name"]).to eq menu_action.raw[2][1]
  expect(@response["data"][0]["menus"][1]["menu_text"]).to eq menu_action.raw[3][0]
  expect(@response["data"][0]["menus"][1]["actions"][0]["action_name"]).to eq menu_action.raw[3][1]
  expect(@response["data"][0]["menus"][1]["actions"][1]["action_name"]).to eq menu_action.raw[4][1]
end

Given('I have the following menu information assigned to the user {string}') do |email, menu|
  user = Sate::Auth::User.find_by email: email

  menu = Sate::Auth::Menu.find_by text: menu.raw[1][0]

  user.menus << menu
  user.menus << menu.children
end

When('I want to get menu action assigned to the user {string}') do |email|
  path = "/crmgt/auth/user/menu/actions"

  token = @response["data"]["access_token"]
  header 'Authorization', "Bearer #{token}"

  @response = get path
  @response = JSON(@response.body)
end
