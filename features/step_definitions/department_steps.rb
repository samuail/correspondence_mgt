When('I want to add department with the following details') do |department|
  path = "/crmgt/business_settings/department"
  token = @response["data"]["access_token"]

  header 'Authorization', "Bearer #{token}"

  department_type = Sate::Crmgt::DepartmentType.find_by_name department.raw[1][2]

  @response = post path, { :department => { code: department.raw[1][0],
                                            description: department.raw[1][1],
                                            department_type_id: department_type.id,
                                            parent_id: nil } }

  @response = JSON(@response.body)
end

Then('I should have this department information') do |data|
  expect(@response["data"]["code"]).to eq data.raw[1][0]
  expect(@response["data"]["description"]).to eq data.raw[1][1]
end

Given('I have the following department') do |data|
  department_type = Sate::Crmgt::DepartmentType.find_by_name data.raw[1][2]
  if data.raw[1][3] != ""
    parentDepartment = Sate::Crmgt::Department.find_by_description data.raw[1][3]
    parent_id = parentDepartment.id
  else
    parent_id = nil
  end
  Sate::Crmgt::Department.create code: data.raw[1][0],
                                 description: data.raw[1][1],
                                 department_type_id: department_type.id,
                                 parent_id: parent_id, application_module_id: @app_module.id
end

Then('I should see an error message {string} {string} and {string}') do |clazz, message1, message2|
  message = []
  message.push(clazz + " " + message1)
  message.push(clazz + " " + message2)
  expect(@response["errors"]).to eq message
end

When('I want to add department with the following details with expired user session') do |department|
  path = "/crmgt/business_settings/department"
  user_id = @response["data"]["user"]["id"]

  header 'Authorization', "Bearer #{ JsonWebToken.encode({ user_id: user_id }, (Time.now.to_i - 10)) }"

  department_type = Sate::Crmgt::DepartmentType.find_by_name department.raw[1][2]

  @response = post path, { :department => { code: department.raw[1][0],
                                            description: department.raw[1][1],
                                            department_type_id: department_type.id,
                                            parent_id: nil } }

  @response = JSON(@response.body)
end

When('I want to edit department by the description {string} with the following details') do
|oldData, newData|
  path = "/crmgt/business_settings/department"
  token = @response["data"]["access_token"]

  header 'Authorization', "Bearer #{token}"

  update_data = Sate::Crmgt::Department.find_by_description oldData

  @response = put path, { :id => update_data.id, :department => { description: newData.raw[1][0] } }

  @response = JSON(@response.body)
end

When('I want to edit department by the description {string} with the following details with expired user session') do
|oldData, newData|
  path = "/crmgt/business_settings/department"
  user_id = @response["data"]["user"]["id"]

  header 'Authorization', "Bearer #{ JsonWebToken.encode({ user_id: user_id }, (Time.now.to_i - 10)) }"

  update_data = Sate::Crmgt::Department.find_by_description oldData

  @response = put path, { :id => update_data.id, :department => { description: newData.raw[1][0] } }

  @response = JSON(@response.body)
end

When('I want to see all department information') do
  path = "/crmgt/business_settings/department"
  token = @response["data"]["access_token"]

  header 'Authorization', "Bearer #{token}"

  @response = get path
  @response = JSON(@response.body)
end

Then('I should have the following {string} department information') do |count, data|
  expect(@response["data"].count).to eq count.to_i
  expect(@response["data"][0]["code"]).to eq data.raw[1][0]
  expect(@response["data"][0]["description"]).to eq data.raw[1][1]
  expect(@response["data"][1]["code"]).to eq data.raw[2][0]
  expect(@response["data"][1]["description"]).to eq data.raw[2][1]
end

When('I want to fetch department items') do
  path = "/crmgt/business_settings/department_tree"
  token = @response["data"]["access_token"]

  header 'Authorization', "Bearer #{token}"

  @response = get path
  @response = JSON(@response.body)
end

Then('I should have {string} parent department') do |count|
  expect(@response["total"]).to eq count.to_i
end

Then('I should have {string} department defined under the department {string}') do |count, department|
  expect(@response["data"].select{|dep| dep["description"] == department}[0]["children"].length).to eq count.to_i
end