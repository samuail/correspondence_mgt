When('I want to add {string} with the following details') do |clazz, data|
  clazz = clazz.parameterize.underscore
  path = "/crmgt/business_settings/#{clazz}"
  token = @response["data"]["access_token"]

  header 'Authorization', "Bearer #{token}"

  @response = post path, { :lookup => { code: data.raw[1][0], name: data.raw[1][1] } }

  @response = JSON(@response.body)
end

Then('I should have this department type information') do |department_type|
  expect(@response["data"]["code"]).to eq department_type.raw[1][0]
  expect(@response["data"]["name"]).to eq department_type.raw[1][1]
end

Given('I have the following {string}') do |clazz, data|
  model = "Sate::Crmgt::#{clazz.split(' ').map(&:capitalize).join('')}".constantize
  model.create code: data.raw[1][0], name: data.raw[1][1], application_module_id: @app_module.id
end

Then('I should see an error message {string} {string} {string} and {string}') do
|clazz, message1, message2, message3|
  message = []
  message.push(clazz + " " + message1)
  message.push(clazz + " " + message2)
  message.push(clazz + " " + message3)
  expect(@response["errors"]).to eq message
end

When('I want to add {string} with the following details with expired user session') do
|clazz, data|
  clazz = clazz.parameterize.underscore
  path = "/crmgt/business_settings/#{clazz}"
  user_id = @response["data"]["user"]["id"]

  header 'Authorization', "Bearer #{ JsonWebToken.encode({ user_id: user_id }, (Time.now.to_i - 10)) }"

  @response = post path, { :lookup => { code: data.raw[1][0], name: data.raw[1][1] } }

  @response = JSON(@response.body)
end

When('I want to edit {string} by the name {string} with the following details') do
|clazz, oldData, newData|
  url = clazz.parameterize.underscore
  path = "/crmgt/business_settings/#{url}"
  token = @response["data"]["access_token"]

  header 'Authorization', "Bearer #{token}"

  model = "Sate::Crmgt::#{clazz.split(' ').map(&:capitalize).join('')}".constantize
  update_clazz = model.find_by_name oldData

  @response = put path, { :id => update_clazz.id, :lookup => { name: newData.raw[1][0] } }
  @response = JSON(@response.body)
end

When('I want to edit {string} by the name {string} with the following details with expired user session') do
|clazz, oldData, newData|
  url = clazz.parameterize.underscore
  path = "/crmgt/business_settings/#{url}"
  user_id = @response["data"]["user"]["id"]

  header 'Authorization', "Bearer #{ JsonWebToken.encode({ user_id: user_id }, (Time.now.to_i - 10)) }"

  model = "Sate::Crmgt::#{clazz.split(' ').map(&:capitalize).join('')}".constantize
  update_clazz = model.find_by_name oldData

  @response = put path, { :id => update_clazz.id, :lookup => { name: newData.raw[1][0] } }
  @response = JSON(@response.body)
end

When('I want to see all {string} information') do |clazz|
  clazz = clazz.parameterize.underscore
  path = "/crmgt/business_settings/#{clazz}"
  token = @response["data"]["access_token"]

  header 'Authorization', "Bearer #{token}"

  @response = get path
  @response = JSON(@response.body)
end

Then('I should have the following {string} {string} information') do |count, clazz, data|
  expect(@response["data"].count).to eq count.to_i
  expect(@response["data"][0]["code"]).to eq data.raw[1][0]
  expect(@response["data"][0]["name"]).to eq data.raw[1][1]
  expect(@response["data"][1]["code"]).to eq data.raw[2][0]
  expect(@response["data"][1]["name"]).to eq data.raw[2][1]
end

Then('I should have this {string} information') do |clazz, data|
  expect(@response["data"]["code"]).to eq data.raw[1][0]
  expect(@response["data"]["name"]).to eq data.raw[1][1]
end