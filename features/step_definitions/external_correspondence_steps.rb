When('I want to add an external {string} correspondence with the following details') do |clazz, data|
  path = "/crmgt/correspondence/external?request_type=" + clazz

  token = @response["data"]["access_token"]
  header 'Authorization', "Bearer #{token}"

  date = Date.strptime(data.raw[1][1], "%m/%d/%Y")
  if clazz == "incoming"
    from = Sate::Crmgt::Organization.find_by_description data.raw[1][4]
    @response = post path, { :external_correspondence => { reference_no: data.raw[1][0],
                                                           letter_date: date,
                                                           subject: data.raw[1][2],
                                                           search_keywords: data.raw[1][3].split(', '),
                                                           source_id: from.id,
                                                           destination_id: nil } }
  else
    to = Sate::Crmgt::Organization.find_by_description data.raw[1][4]
    @response = post path, { :external_correspondence => { reference_no: data.raw[1][0],
                                                           letter_date: date,
                                                           subject: data.raw[1][2],
                                                           search_keywords: data.raw[1][3].split(', '),
                                                           source_id: nil,
                                                           destination_id: to.id } }
  end
  @response = JSON(@response.body)
end

Then('I should have this external {string} correspondence information') do |clazz, data|
  expect(@response["data"]["reference_no"]).to eq data.raw[1][0]
  if data.raw[1].length > 1
    expect(@response["data"]["letter_date"].to_s).to eq Date.strptime(data.raw[1][1], "%m/%d/%Y").to_s
    expect(@response["data"]["subject"]).to eq data.raw[1][2]
    expect(@response["data"]["search_keywords"]).to eq data.raw[1][3].split(', ')
    if clazz == "incoming"
      expect(@response["data"]["source"]["description"]).to eq data.raw[1][4]
    else
      expect(@response["data"]["destination"]["description"]).to eq data.raw[1][4]
    end
  end
end

Given('I have the following external {string} correspondence') do |clazz, data|
  date = Date.strptime(data.raw[1][1], "%m/%d/%Y")
  user_id = @response["data"]["user"]["id"]
  if clazz == "incoming"
    from = Sate::Crmgt::Organization.find_by_description data.raw[1][4]
    destination = Sate::Crmgt::UserDepartment.find_by_user_id @response["data"]["user"]["id"]
    Sate::Crmgt::ExternalIncomingCorrespondence.create reference_no: data.raw[1][0],
                                                       letter_date: date,
                                                       subject: data.raw[1][2],
                                                       search_keywords: data.raw[1][3].split(','),
                                                       source_id: from.id,
                                                       destination_id: destination.department_id,
                                                       received_date: Date.today,
                                                       key_in_by_id: user_id,
                                                       application_module_id: @app_module.id
  else
    from = Sate::Crmgt::UserDepartment.find_by_user_id @response["data"]["user"]["id"]
    destination = Sate::Crmgt::Organization.find_by_description data.raw[1][4]
    Sate::Crmgt::ExternalOutgoingCorrespondence.create reference_no: data.raw[1][0],
                                                       letter_date: date,
                                                       subject: data.raw[1][2],
                                                       search_keywords: data.raw[1][3].split(','),
                                                       source_id: from.department_id,
                                                       destination_id: destination.id,
                                                       received_date: Date.today,
                                                       key_in_by_id: user_id,
                                                       application_module_id: @app_module.id
  end
end

When('I want to add an external {string} correspondence with the following details with expired user session') do |clazz, data|
  path = "/crmgt/correspondence/external?request_type=" + clazz

  user_id = @response["data"]["user"]["id"]
  header 'Authorization', "Bearer #{ JsonWebToken.encode({ user_id: user_id }, (Time.now.to_i - 10)) }"

  date = Date.strptime(data.raw[1][1], "%m/%d/%Y")
  if clazz == "incoming"
    from = Sate::Crmgt::Organization.find_by_description data.raw[1][4]
    @response = post path, { :external_correspondence => { reference_no: data.raw[1][0],
                                                           letter_date: date,
                                                           subject: data.raw[1][2],
                                                           search_keywords: data.raw[1][3].split(', '),
                                                           source_id: from.id,
                                                           destination_id: nil } }
  else
    to = Sate::Crmgt::Organization.find_by_description data.raw[1][4]
    @response = post path, { :external_correspondence => { reference_no: data.raw[1][0],
                                                           letter_date: date,
                                                           subject: data.raw[1][2],
                                                           search_keywords: data.raw[1][3].split(', '),
                                                           source_id: nil,
                                                           destination_id: to.id } }
  end
  @response = JSON(@response.body)
end

When('I want to edit external {string} correspondence by the reference no {string} with the following details') do
|clazz, oldData, newData|
  path = "/crmgt/correspondence/external?request_type=" + clazz

  token = @response["data"]["access_token"]
  header 'Authorization', "Bearer #{token}"

  if clazz == "incoming"
    external_incoming = Sate::Crmgt::ExternalIncomingCorrespondence.find_by_reference_no oldData
  else
    external_incoming = Sate::Crmgt::ExternalOutgoingCorrespondence.find_by_reference_no oldData
  end

  @response = put path, { :id => external_incoming.id, :external_correspondence => { reference_no: newData.raw[1][0] } }
  @response = JSON(@response.body)
end

When('I want to edit external {string} correspondence by the reference no {string} with the following details with expired user session') do
|clazz, oldData, newData|
  path = "/crmgt/correspondence/external?request_type=" + clazz

  user_id = @response["data"]["user"]["id"]
  header 'Authorization', "Bearer #{ JsonWebToken.encode({ user_id: user_id }, (Time.now.to_i - 10)) }"

  if clazz == "incoming"
    external_incoming = Sate::Crmgt::ExternalIncomingCorrespondence.find_by_reference_no oldData
  else
    external_incoming = Sate::Crmgt::ExternalOutgoingCorrespondence.find_by_reference_no oldData
  end

  @response = put path, { :id => external_incoming.id, :external_correspondence => { reference_no: newData.raw[1][0] } }
  @response = JSON(@response.body)
end

When('I want to fetch all external {string} correspondence for the current user') do |clazz|
  path = "/crmgt/correspondence/external?request_type=" + clazz

  token = @response["data"]["access_token"]
  header 'Authorization', "Bearer #{token}"

  @response = get path
  @response = JSON(@response.body)
end

Then('I should have the {string} external {string} correspondence details for the current user') do |count, clazz, data|
  expect(@response["total"]).to eq count.to_i
  expect(@response["data"][0]["reference_no"]).to eq data.raw[1][0]
  expect(@response["data"][0]["letter_date"].to_s).to eq Date.strptime(data.raw[1][1], "%m/%d/%Y").to_s
  expect(@response["data"][0]["subject"]).to eq data.raw[1][2]
  expect(@response["data"][0]["search_keywords"]).to eq data.raw[1][3].split(',')
  expect(@response["data"][1]["reference_no"]).to eq data.raw[2][0]
  expect(@response["data"][1]["letter_date"].to_s).to eq Date.strptime(data.raw[2][1], "%m/%d/%Y").to_s
  expect(@response["data"][1]["subject"]).to eq data.raw[2][2]
  expect(@response["data"][1]["search_keywords"]).to eq data.raw[2][3].split(',')
  if clazz == "incoming"
    expect(@response["data"][0]["source"]["description"]).to eq data.raw[1][4]
    expect(@response["data"][1]["source"]["description"]).to eq data.raw[2][4]
  else
    expect(@response["data"][0]["destination"]["description"]).to eq data.raw[1][4]
    expect(@response["data"][1]["destination"]["description"]).to eq data.raw[2][4]
  end
end