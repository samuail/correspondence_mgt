When('I want to upload {string} {string} image for an external {string} correspondence with reference number {string}') do
|number, type, direction, reference_no|
  if direction == "incoming"
    correspondence = Sate::Crmgt::ExternalIncomingCorrespondence.find_by_reference_no reference_no
  else
    correspondence = Sate::Crmgt::ExternalOutgoingCorrespondence.find_by_reference_no reference_no
  end
  @path_to_file = '/home/samuel/Desktop'
  path = "/crmgt/correspondence/external/upload"

  token = @response["data"]["access_token"]
  header 'Authorization', "Bearer #{token}"

  [0..number.to_i].each do |i|
    if i == 0
      test_image = @path_to_file + "/changes.png"
      content_type = "image/png"
    else
      test_image = @path_to_file + "/sate_auth_changes.png"
      content_type = "image/png"
    end
    file = Rack::Test::UploadedFile.new(test_image, content_type)
    @response = post path, { :id => correspondence.id, :request_type => direction, :type => type, :file => file }
    @response = JSON(@response.body)
  end
end

Given('I have one Main and two Attachment images uploaded for the external {string} correspondence with reference number {string}') do
|direction, reference_no|
  if direction == "incoming"
    correspondence = Sate::Crmgt::ExternalIncomingCorrespondence.find_by_reference_no reference_no
  else
    correspondence = Sate::Crmgt::ExternalOutgoingCorrespondence.find_by_reference_no reference_no
  end
  @path_to_file = '/home/samuel/Desktop'
  main = @path_to_file + "/changes.png"
  main_file = Rack::Test::UploadedFile.new(main, "image/png")
  att_file_1 = Rack::Test::UploadedFile.new(main, "image/png")
  att_file_2 = Rack::Test::UploadedFile.new(main, "image/png")

  correspondence.main_images.attach(main_file)
  @main = correspondence.main_images

  correspondence.attachment_images.attach(att_file_1)
  correspondence.attachment_images.attach(att_file_2)
end

When('I want to change uploaded {string} image for an external {string} correspondence with reference number {string}') do
|type, direction, reference_no|
  if direction == "incoming"
    correspondence = Sate::Crmgt::ExternalIncomingCorrespondence.find_by_reference_no reference_no
  else
    correspondence = Sate::Crmgt::ExternalOutgoingCorrespondence.find_by_reference_no reference_no
  end
  @path_to_file = '/home/samuel/Desktop'
  path = "/crmgt/correspondence/external/change_uploads"

  token = @response["data"]["access_token"]
  header 'Authorization', "Bearer #{token}"

  test_image = @path_to_file + "/changes.png"
  content_type = "image/png"
  file = Rack::Test::UploadedFile.new(test_image, content_type)

  @response = post path, { :id => correspondence.id, :request_type => direction, :type => type, :existing_image => @main.blobs[0].id, :file => file }
  @response = JSON(@response.body)
end

When('I want to upload invalid {string} file for an external {string} correspondence with reference number {string}') do
|type, direction, reference_no|
  if direction == "incoming"
    correspondence = Sate::Crmgt::ExternalIncomingCorrespondence.find_by_reference_no reference_no
  else
    correspondence = Sate::Crmgt::ExternalOutgoingCorrespondence.find_by_reference_no reference_no
  end
  @path_to_file = '/home/samuel/Desktop'
  path = "/crmgt/correspondence/external/upload"

  token = @response["data"]["access_token"]
  header 'Authorization', "Bearer #{token}"

  test_image = @path_to_file + "/registration_notice.txt"
  file = Rack::Test::UploadedFile.new(test_image, "application/json")
  @response = post path, { :id => correspondence.id, :request_type => direction, :type => type, :file => file }
  @response = JSON(@response.body)
end

When('I want to upload big {string} file for an external {string} correspondence with reference number {string}') do
|type, direction, reference_no|
  if direction == "incoming"
    correspondence = Sate::Crmgt::ExternalIncomingCorrespondence.find_by_reference_no reference_no
  else
    correspondence = Sate::Crmgt::ExternalOutgoingCorrespondence.find_by_reference_no reference_no
  end
  @path_to_file = '/home/samuel/Desktop'
  path = "/crmgt/correspondence/external/upload"

  token = @response["data"]["access_token"]
  header 'Authorization', "Bearer #{token}"

  test_image = @path_to_file + "/ancient-town-lake-china.jpg"
  file = Rack::Test::UploadedFile.new(test_image, "image/jpg")
  @response = post path, { :id => correspondence.id,  :request_type => direction, :type => type, :file => file }
  @response = JSON(@response.body)
end

When('I want to upload image for an external {string} correspondence with reference number {string} with expired user session') do
|direction, reference_no|
  if direction == "incoming"
    correspondence = Sate::Crmgt::ExternalIncomingCorrespondence.find_by_reference_no reference_no
  else
    correspondence = Sate::Crmgt::ExternalOutgoingCorrespondence.find_by_reference_no reference_no
  end
  @path_to_file = '/home/samuel/Desktop'
  path = "/crmgt/correspondence/external/upload"

  user_id = @response["data"]["user"]["id"]
  header 'Authorization', "Bearer #{ JsonWebToken.encode({ user_id: user_id }, (Time.now.to_i - 10)) }"

  test_image = @path_to_file + "/changes.png"
  file = Rack::Test::UploadedFile.new(test_image, "image/png")
  @response = post path, { :id => correspondence.id,  :request_type => direction, :image => { :type => "Main", :file => file } }
  @response = JSON(@response.body)
end

When('I want to get the soft copy images of the external {string} correspondence with reference number {string}') do
|direction, reference_no|
  if direction == "incoming"
    correspondence = Sate::Crmgt::ExternalIncomingCorrespondence.find_by_reference_no reference_no
  else
    correspondence = Sate::Crmgt::ExternalOutgoingCorrespondence.find_by_reference_no reference_no
  end
  @path_to_file = '/home/samuel/Desktop'
  path = "/crmgt/correspondence/external/get_images"

  token = @response["data"]["access_token"]
  header 'Authorization', "Bearer #{token}"

  @response = get path, { :id => correspondence.id,  :request_type => direction }
  @response = JSON(@response.body)
end