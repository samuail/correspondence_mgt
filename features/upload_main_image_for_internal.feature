Feature: Upload main image related to a given correspondence.
  As a system user
  I want to upload one or more main images to a correspondence
  so that I can have soft copy entry of the documents related to a given correspondence

  Background:
    Given I have the following application module information
      |     application_name      | code |
      | Correspondence Management | CRMT |
    And I have the following user information
      | first_name | last_name |    email    |  password  |     application_name      |
      | Samuel     | Teshome   | sam@abc.com | myPassword | Correspondence Management |
    And I have the following "department type"
      | code |  name  |      application_name     |
      | CAM  | Campus | Correspondence Management |
    And I have the following "department type"
      | code |  name   |      application_name     |
      | UNT  |  Unit   | Correspondence Management |
    And I have the following department
      | code | description  | department_type | parent |
      | MAC  | Main Campus  |    Campus       |        |
    And I have the following department
      | code |    description    | department_type |    parent   |
      | PRO  | President Office  |      Unit       | Main Campus |
    And I have the following department
      | code |  description    | department_type |    parent   |
      | PRO  | Finance Office  |      Unit       | Main Campus |
    And I have the following user role information
      |   name   |      application_name     |
      | Director | Correspondence Management |
    And I have the following user department role information
      |     user    |  department    |   role   |
      | sam@abc.com | Finance Office | Director |

  @upload_one_main_image_by_authorized_user
  Scenario: Upload Internal Incoming Correspondence Image by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following internal "incoming" correspondence
      | reference_no | letter_date |     subject    |   key_word   |        from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1,word2  | President Office |   04/16/2019  |
    When I want to upload "1" "Main" image for an internal "incoming" correspondence with reference number "ABC/000/00"
    Then I should see a success message "Image Uploaded Successfully!"

  @upload_more_than_one_main_image_by_authorized_user
  Scenario: Upload Internal Incoming Correspondence Image by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following internal "incoming" correspondence
      | reference_no | letter_date |     subject    |   key_word   |        from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1,word2  | President Office |   04/16/2019  |
    When I want to upload "2" "Main" image for an internal "incoming" correspondence with reference number "ABC/000/00"
    Then I should see a success message "Image Uploaded Successfully!"

  @change_uploaded_main_image_by_authorized_user
  Scenario: Change Upload Internal Incoming Correspondence Image by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following internal "incoming" correspondence
      | reference_no | letter_date |     subject    |   key_word   |        from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1,word2  | President Office |   04/16/2019  |
    And I have one Main and two Attachment images uploaded for the correspondence with reference number "ABC/000/00"
    When I want to change uploaded "Main" image for an internal "incoming" correspondence with reference number "ABC/000/00"
    Then I should see a success message "Image Uploaded Successfully!"

  @upload_one_or_more_main_image_by_error_message_invalid_content
  Scenario: Upload Internal Incoming Correspondence Image by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following internal "incoming" correspondence
      | reference_no | letter_date |     subject    |   key_word   |        from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1,word2  | President Office |   04/16/2019  |
    When I want to upload invalid "Main" file for an internal "incoming" correspondence with reference number "ABC/000/00"
    Then I should see an error message "Internal Correspondence Main images must be an image"

  @upload_one_or_more_main_image_by_error_message_big_content
  Scenario: Upload Internal Incoming Correspondence Image by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following internal "incoming" correspondence
      | reference_no | letter_date |     subject    |   key_word   |        from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1,word2  | President Office |   04/16/2019  |
    When I want to upload big "Main" file for an internal "incoming" correspondence with reference number "ABC/000/00"
    Then I should see an error message "Internal Correspondence Main images is too big"

  @upload_one_or_more_main_image_by_Expired_Session
  Scenario: Upload Internal Incoming Correspondence Image by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following internal "incoming" correspondence
      | reference_no | letter_date |     subject    |   key_word   |        from      | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1,word2  | President Office |   04/16/2019  |
    When I want to upload image for an internal "incoming" correspondence with reference number "ABC/000/00" with expired user session
    Then I should see an error message "Signature has expired"

# ========= Internal Outgoing ==============

  @upload_one_main_image_for_internal_outgoing_by_authorized_user
  Scenario: Upload Internal Incoming Correspondence Image by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following internal "outgoing" correspondence
      | reference_no | letter_date |     subject    |   key_word   |        to        | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1,word2  | President Office |   04/16/2019  |
    When I want to upload "1" "Main" image for an internal "outgoing" correspondence with reference number "ABC/000/00"
    Then I should see a success message "Image Uploaded Successfully!"

#  ========= Get Internal Correspondence Images ==============

  @get_internal_correspondence_images_by_authorized_user
  Scenario: Get Internal Correspondence Image by authorized user
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"
    And I have the following internal "incoming" correspondence
      | reference_no | letter_date |     subject    |   key_word   |        to        | received_date |
      |  ABC/000/00  |  04/16/2019 | Sample Subject | word1,word2  | President Office |   04/16/2019  |
    And I have one Main and two Attachment images uploaded for the correspondence with reference number "ABC/000/00"
    When I want to get the soft copy images of the correspondence with reference number "ABC/000/00"
    Then I should have one Main and two Attachment images
