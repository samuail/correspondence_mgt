module ApplicationHelper
  def app_module
    code = Rails.configuration.x.application_module_code
    Sate::Auth::ApplicationModule.find_by(:code => code)
  end
end
