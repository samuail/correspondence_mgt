class CorrespondenceAssignmentsController < ApplicationController
  before_action :set_correspondence_assignment, only: [:update]

  def index
    if params[:request_type] != "incoming"
      correspondence = Sate::Crmgt::InternalCorrespondence.find params[:correspondence_id]
    else
      correspondence = Sate::Crmgt::ExternalIncomingCorrespondence.find params[:correspondence_id]
    end
    @correspondence_assignments = Sate::Crmgt::CorrespondenceAssignment.where(application_module_id: app_module.id,
                                                                              correspondence: correspondence).order(:order)
    total = @correspondence_assignments.count
    @correspondence_assignments_json = ActiveModelSerializers::SerializableResource.new(@correspondence_assignments).as_json
    response = Sate::Common::MethodResponse.new(
        true, nil, @correspondence_assignments_json, nil, total)

    render json: response
  end

  def get_assigned_docs
    user_ids = params[:user_ids]
    @stats = []
    user_ids.split(',').each do |id|
      @stats << Sate::Crmgt::CorrespondenceAssignment.where('application_module_id = ? and to_user_id = ? and
                                                    extract(year from assigned_date) = ? and
                                                    extract(month from assigned_date) = ? and
                                                    extract(day from assigned_date) = ?',
                                                    app_module.id, id,
                                                    Date.today.year, Date.today.month, Date.today.day).count
    end
    total = @stats.count
    response = Sate::Common::MethodResponse.new(
      true, nil, @stats, nil, total)

    render json: response
  end

  def create
    if params[:request_type] != "incoming"
      correspondence_type = Sate::Crmgt::InternalCorrespondence
      correspondence = Sate::Crmgt::InternalCorrespondence.where(id: correspondence_assignment_params[:correspondence_id])
    else
      correspondence_type = Sate::Crmgt::ExternalIncomingCorrespondence
      correspondence = Sate::Crmgt::ExternalIncomingCorrespondence.where(id: correspondence_assignment_params[:correspondence_id])
    end
    @correspondence_assignment = Sate::Crmgt::CorrespondenceAssignment.new(correspondence_assignment_params)
    @correspondence_assignment.from_user_id = current_user.id
    @correspondence_assignment.correspondence_type = correspondence_type
    @correspondence_assignment.application_module_id = app_module.id
    @correspondence_assignment.assigned_date = DateTime.now
    @correspondence_assignment.received_date = DateTime.now
    @correspondence_assignment.status = "Assigned"
    existing_assignment = Sate::Crmgt::CorrespondenceAssignment.where(correspondence: correspondence).order(order: :desc )
    if existing_assignment.count > 0
      @correspondence_assignment.order = existing_assignment.first.order + 1
    else
      @correspondence_assignment.order = 1
    end
    if @correspondence_assignment.save
      correspondence.first.status = "Processing"
      correspondence.first.save
      @correspondence_assignment_json = ActiveModelSerializers::SerializableResource.new(@correspondence_assignment).as_json
      response = Sate::Common::MethodResponse.new(
          true, "Correspondence Assignment Saved Successfully!", @correspondence_assignment_json, nil, nil)
    else
      errors = Sate::Common::Util.error_messages @correspondence_assignment, 'Correspondence Assignment'
      response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
    end
    render json: response
  end

  def update
    if @correspondence_assignment.update(correspondence_assignment_params)
      @correspondence_assignment_json = ActiveModelSerializers::SerializableResource.new(@correspondence_assignment).as_json
      response = Sate::Common::MethodResponse.new(
          true, "Correspondence Assignment Updated Successfully!", @correspondence_assignment_json, nil, nil)
    else
      errors = Sate::Common::Util.error_messages @correspondence_assignment, 'Correspondence Assignment'
      response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
    end
    render json: response
  end

  private
    def set_correspondence_assignment
      @correspondence_assignment = Sate::Crmgt::CorrespondenceAssignment.find(params[:id])
    end

    def correspondence_assignment_params
      params.require(:correspondence_assignment).permit( :to_user_id, :correspondence_id, :received_date, :status, :rejection_remark)
    end
end
