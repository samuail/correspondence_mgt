class OrganizationsController < ApplicationController
  before_action :set_organization, only: [:update]

  def index
    @organizations = Sate::Crmgt::Organization.where application_module_id: app_module.id
    total = @organizations.count
    @organizations_json = ActiveModelSerializers::SerializableResource.new(@organizations).as_json
    response = Sate::Common::MethodResponse.new(true, nil, @organizations_json, nil, total)
    render json: response
  end

  def create
    @organization = Sate::Crmgt::Organization.new(organization_params)
    @organization.application_module_id = app_module.id
    if @organization.save
      @organizations_json = ActiveModelSerializers::SerializableResource.new(@organization).as_json
      response = Sate::Common::MethodResponse.new(
          true, "Organization Saved Successfully!", @organizations_json, nil, nil)
    else
      errors = Sate::Common::Util.error_messages @organization, 'Organization'
      response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
    end
    render json: response
  end

  def update
    if @organization.update(organization_params)
      @organizations_json = ActiveModelSerializers::SerializableResource.new(@organization).as_json
      response = Sate::Common::MethodResponse.new(
          true, "Organization Updated Successfully!", @organizations_json, nil, nil)
    else
      errors = Sate::Common::Util.error_messages @organization, 'Organization'
      response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
    end
    render json: response
  end

  private
    def set_organization
      @organization = Sate::Crmgt::Organization.find(params[:id])
    end

    def organization_params
      params.require(:organization).permit(:code, :description, :organization_type_id)
    end
end
