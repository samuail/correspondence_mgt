class UserMenuActionsController < ApplicationController

  def get_menu_action
    menu_texts = params[:menu_names]
    menus = Sate::Auth::Menu.where text: menu_texts
    @menu_actions = Sate::Crmgt::MenuAction.where menu_id: menus.ids,
                                                  application_module_id: app_module.id
    count = @menu_actions.count
    @menu_actions_json = ActiveModelSerializers::SerializableResource.new(@menu_actions).as_json
    menu_action_tree = []
    @menu_actions_json.each do |menu_action|
      parent = Sate::Auth::Menu.find_by id: menu_action[:menu][:parent_id]
      actions = []
      menus = []
      if menu_action_tree == []
        actions << { menu_action_id: menu_action[:id], action_id: menu_action[:action][:id],
                    action_name: menu_action[:action][:name], active: false }
        menus << { menu_id: menu_action[:menu][:id], menu_text: menu_action[:menu][:text], actions: actions}
        menu_action_tree << { parent_id: parent.id, parent_text: parent.text, menus: menus }
      else
        index = menu_action_tree.index { |parent_menu| parent_menu[:parent_id] == menu_action[:menu][:parent_id] }
        if index != nil
          idx = menu_action_tree[index][:menus].index { |menu| menu[:menu_id] == menu_action[:menu][:id] }
          if idx != nil
            action = { menu_action_id: menu_action[:id], action_id: menu_action[:action][:id],
                      action_name: menu_action[:action][:name], active: false }
            menu_action_tree[index][:menus][idx][:actions] << action
          else
            actions << { menu_action_id: menu_action[:id], action_id: menu_action[:action][:id],
                        action_name: menu_action[:action][:name], active: false }
            menu = { menu_id: menu_action[:menu][:id], menu_text: menu_action[:menu][:text], actions: actions }
            menu_action_tree[index][:menus] << menu
          end
        else
          actions << { menu_action_id: menu_action[:id], action_id: menu_action[:action][:id],
                      action_name: menu_action[:action][:name], active: false }
          menus << { menu_id: menu_action[:menu][:id], menu_text: menu_action[:menu][:text], actions: actions }
          menu_action_tree << { parent_id: parent.id, parent_text: parent.text, menus: menus }
        end
      end
    end
    response = Sate::Common::MethodResponse.new(
        true, nil, menu_action_tree, nil, count)
    render json: response
  end

  def get_assigned_menu_action
    user_id = params[:user_id] == nil ? current_user.id : params[:user_id]
    @menu_actions = Sate::Crmgt::UserMenuAction.where user_id: user_id,
                                                      application_module_id: app_module.id

    user = Sate::Auth::User.find user_id
    menu_list = user.menus.where(:parent => nil, :application_module => app_module).order(:text)

    @menus = []

    menu_list.each do |menu|
      children = []
      menu.children.order(:text).each do |child|
        if child.users.include? user
          actions = []
          menu_actions = Sate::Crmgt::MenuAction.where menu_id: child.id
          if menu_actions != nil
            menu_actions.each do |menu_action|
              m_act = @menu_actions.detect { |m_act| m_act.menu_action_id == menu_action.id }
              if m_act != nil
                actions << { menu_action_id: menu_action.id, action_id: menu_action.action_id,
                             action_name: menu_action.action.name, active: true }
              end
            end
          end
          children << { menu_id: child.id, menu_text: child.text, actions: actions }
        end
      end
      @menus << { parent_id: menu.id, parent_text: menu.text, menus: children }
    end
    response = Sate::Common::MethodResponse.new(
        true, nil, @menus, nil, 0)
    render json: response
  end

  def assign_menu_action
    user_id = params[:user_id]
    menu_action_ids = params[:menu_action_ids]
    Sate::Crmgt::UserMenuAction.where(user_id: user_id, application_module_id: app_module.id).delete_all
    menu_actions = Sate::Crmgt::MenuAction.where id: menu_action_ids
    menu_actions.each do |menu_action|
      Sate::Crmgt::UserMenuAction.create user_id: user_id,
                                         menu_action_id: menu_action.id,
                                         application_module_id: app_module.id
    end
    @user_menu_action = Sate::Crmgt::UserMenuAction.where user_id: user_id, application_module_id: app_module.id
    count = @user_menu_action.count
    @user_menu_action_json = ActiveModelSerializers::SerializableResource.new(@user_menu_action).as_json
    response = Sate::Common::MethodResponse.new(
        true, "Menu Action Assigned Successfully!", @user_menu_action_json, nil, count)
    render json: response
  end

end
