class ExternalCorrespondencesController < ApplicationController
  before_action :set_external_correspondence, only: [:update, :update_status, :archive,
                                                     :upload, :change_uploads, :get_images]

  def index
    request_type = params[:requestType]
    filter = params[:filter]
    sort_order = params[:sortOrder]
    page_number = params[:pageNumber].to_i
    page_size = params[:pageSize].to_i
    user_department_role = Sate::Crmgt::UserDepartmentRole.find_by_user_id current_user.id
    user_department = Sate::Crmgt::DepartmentUserRole.find user_department_role.department_user_role_id

    @external_correspondences = []
    @external_correspondences0 = []
    @external_correspondences2 = []
    @external_correspondences3 = []

    @correspondence_assignments = []
    @correspondence_ccs = []

    @direct = []
    @ccied = []
    @assigned = []

    @all_json = []

    if request_type == "incoming"
      @external_correspondences = Sate::Crmgt::ExternalIncomingCorrespondence.where(application_module_id: app_module.id,
                                                                                    destination_id: user_department.department_id)
                                                                             .where.not(status: 'Archived')
                                                                             .where.not(status: 'Rejected')
                                                                             .where("reference_no like ? or subject like ? or array_to_string(search_keywords, '||') like ?", "%#{filter}%", "%#{filter}%", "%#{filter}%")
      if @external_correspondences.count > 0
        @direct = ActiveModelSerializers::SerializableResource.new(@external_correspondences).as_json
        @direct.each do |one|
          one[:types] = "Direct"
        end
        @all_json = @direct
      end

      if current_user.roles.first.name == "Secretary" || current_user.roles.first.name == "Business Administrator"
        headRole = Sate::Crmgt::DepartmentUserRole.find_by department_id: user_department.department_id, is_head: true
        head = Sate::Crmgt::UserDepartmentRole.find_by department_user_role_id: headRole.id
        if head != nil
          @correspondence_assignments = Sate::Crmgt::CorrespondenceAssignment.where(application_module_id: app_module.id,
                                                                                 correspondence_type: 'Sate::Crmgt::ExternalIncomingCorrespondence',
                                                                                 to_user_id: head.user_id)
                                                                          .select("correspondence_id").map(&:correspondence_id)
          @external_correspondences0 = Sate::Crmgt::ExternalIncomingCorrespondence.where(id: @correspondence_assignments,
                                                                                       application_module_id: app_module.id)
                                                                                  .where.not(destination_id: user_department.department_id)
                                                                                  .where.not(status: 'Archived')
                                                                                  .where.not(status: 'Rejected')
                                                                                  .where("reference_no like ? or subject like ? or array_to_string(search_keywords, '||') like ?", "%#{filter}%", "%#{filter}%", "%#{filter}%")
          if @external_correspondences0.count > 0
            @assigned = ActiveModelSerializers::SerializableResource.new(@external_correspondences0).as_json
            @assigned.each do |one|
              one[:types] = "Assigned"
            end
            @all_json = @all_json + @assigned
          end
        end
      end

      @correspondence_ccs = Sate::Crmgt::CorrespondenceCarbonCopy.where(application_module_id: app_module.id,
                                                                       correspondence_type: 'Sate::Crmgt::ExternalIncomingCorrespondence',
                                                                       destination_id: user_department.department_id)
                                                                .select("correspondence_id").map(&:correspondence_id)
      if @correspondence_assignments.length > 0
        @correspondence_ccs = @correspondence_ccs - @correspondence_assignments
      end
      @external_correspondences2 = Sate::Crmgt::ExternalIncomingCorrespondence.where(id: @correspondence_ccs,
                                                                                     application_module_id: app_module.id)
                                                                              .where.not(status: 'Archived')
                                                                              .where.not(status: 'Rejected')
                                                                              .where("reference_no like ? or subject like ? or array_to_string(search_keywords, '||') like ?", "%#{filter}%", "%#{filter}%", "%#{filter}%")
      if @external_correspondences2.count > 0
        @ccied = ActiveModelSerializers::SerializableResource.new(@external_correspondences2).as_json
        @ccied.each do |one|
          one[:types] = "Copied"
        end
        @all_json = @all_json + @ccied
      end
    else
      @external_correspondences3 = Sate::Crmgt::ExternalOutgoingCorrespondence.where(application_module_id: app_module.id,
                                                                                    source_id: user_department.department_id)
                                                                              .where("reference_no like ? or subject like ? or array_to_string(search_keywords, '||') like ?", "%#{filter}%", "%#{filter}%", "%#{filter}%")
      if @external_correspondences3.count > 0
        @direct = ActiveModelSerializers::SerializableResource.new(
              @external_correspondences3,each_serializer: ExternalOutgoingCorrespondenceSerializer).as_json
        @direct.each do |one|
          one[:types] = "Direct"
        end
        @all_json = @direct
      end

      @correspondence_ccs = Sate::Crmgt::CorrespondenceCarbonCopy.where(application_module_id: app_module.id,
                                                                        correspondence_type: 'Sate::Crmgt::ExternalOutgoingCorrespondence',
                                                                        destination_id: user_department.department_id)
                                                                 .select("correspondence_id").map(&:correspondence_id)

      @external_correspondences2 = Sate::Crmgt::ExternalOutgoingCorrespondence.where(id: @correspondence_ccs,
                                                                                     application_module_id: app_module.id)
                                                                              .where("reference_no like ? or subject like ? or array_to_string(search_keywords, '||') like ?", "%#{filter}%", "%#{filter}%", "%#{filter}%")
      if @external_correspondences2.count > 0
        @ccied = ActiveModelSerializers::SerializableResource.new(
              @external_correspondences2, each_serializer: ExternalOutgoingCorrespondenceSerializer).as_json
        @ccied.each do |one|
          one[:types] = "Copied"
        end
        @all_json = @all_json + @ccied
      end
    end
    total = @all_json.count
    @all_json.sort_by! { |a| [-a[:received_date].to_time.to_i, -a[:letter_date].to_time.to_i ] }
    initialPos = page_number * page_size
    response = Sate::Common::MethodResponse.new(true, nil, @all_json.slice(initialPos, page_size), nil, total)
    render json: response
  end

  def get_assigned
    request_type = params[:requestType]
    filter = params[:filter]
    sort_order = params[:sortOrder]
    page_number = params[:pageNumber].to_i
    page_size = params[:pageSize].to_i
    user_department = Sate::Crmgt::UserDepartmentRole.find_by_user_id current_user.id
    assigned = Sate::Crmgt::CorrespondenceAssignment.where(application_module_id: app_module.id,
                                                           to_user_id: current_user.id,
                                                           correspondence_type: 'Sate::Crmgt::ExternalIncomingCorrespondence')
                                                    .select("correspondence_id").map(&:correspondence_id)
    @external_correspondences = Sate::Crmgt::ExternalIncomingCorrespondence.where(id: assigned,
                                                                                  application_module_id: app_module.id)
                                                                           .where.not(status: 'Archived')
                                                                           .where.not(status: 'Rejected')
                                                                           .where("reference_no like ? or subject like ? or array_to_string(search_keywords, '||') like ?", "%#{filter}%", "%#{filter}%", "%#{filter}%")
    total = @external_correspondences.count
    @external_correspondences_json = ActiveModelSerializers::SerializableResource.new(@external_correspondences).as_json
    @external_correspondences_json.sort_by! { |a| [-a[:received_date].to_time.to_i, -a[:letter_date].to_time.to_i ] }
    initialPos = page_number * page_size
    response = Sate::Common::MethodResponse.new(true, nil, @external_correspondences_json.slice(initialPos, page_size), nil, total)
    render json: response
  end

  def create
    request_type = params[:request_type]
    if request_type == "incoming"
      @external_correspondence = Sate::Crmgt::ExternalIncomingCorrespondence.new(external_correspondence_params)
      user_department = Sate::Crmgt::UserDepartmentRole.find_by_user_id current_user.id
      destination = Sate::Crmgt::DepartmentUserRole.find user_department.department_user_role_id
      @external_correspondence.destination_id = destination.department_id
      @internal_correspondence.status = "Sent"
    else
      @external_correspondence = Sate::Crmgt::ExternalOutgoingCorrespondence.new(external_correspondence_params)
      user_department = Sate::Crmgt::UserDepartmentRole.find_by_user_id current_user.id
      source = Sate::Crmgt::DepartmentUserRole.find user_department.department_user_role_id
      @external_correspondence.source_id = source.department_id
    end
    @external_correspondence.application_module_id = app_module.id
    @external_correspondence.search_keywords = params[:external_correspondence][:search_keywords]
    @external_correspondence.received_date = Date.today
    @external_correspondence.key_in_by_id = current_user.id
    if @external_correspondence.save
      if request_type == "incoming"
        @external_correspondence = Sate::Crmgt::ExternalIncomingCorrespondence.find_by application_module_id: app_module.id, id: @external_correspondence.id
      else
        @external_correspondence = Sate::Crmgt::ExternalOutgoingCorrespondence.find_by application_module_id: app_module.id, id: @external_correspondence.id
      end
      @external_correspondence_json = ActiveModelSerializers::SerializableResource.new(@external_correspondence).as_json
      response = Sate::Common::MethodResponse.new(
          true, "External Correspondence Saved Successfully!", @external_correspondence_json, nil, nil)
    else
      errors = Sate::Common::Util.error_messages @external_correspondence, 'External Correspondence'
      response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
    end
    render json: response
  end

  def update
    @external_correspondence.search_keywords = params[:external_correspondence][:search_keywords]
    if @external_correspondence.update(external_correspondence_params)
      if params[:request_type] == "incoming"
        @external_correspondence = Sate::Crmgt::ExternalIncomingCorrespondence.find_by application_module_id: app_module.id, id: @external_correspondence.id
      else
        @external_correspondence = Sate::Crmgt::ExternalOutgoingCorrespondence.find_by application_module_id: app_module.id, id: @external_correspondence.id
      end
      @external_correspondence_json = ActiveModelSerializers::SerializableResource.new(@external_correspondence).as_json
      response = Sate::Common::MethodResponse.new(
          true, "External Correspondence Updated Successfully!", @external_correspondence_json, nil, nil)
    else
      errors = Sate::Common::Util.error_messages @external_correspondence, 'External Correspondence'
      response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
    end
    render json: response
  end

  def update_status
    status = params[:status]
    if @external_correspondence.update(status: status, received_date: Date.today)
      @external_correspondence = Sate::Crmgt::ExternalIncomingCorrespondence.find_by application_module_id: app_module.id,
                                                                             id: @external_correspondence.id
      @external_correspondence_json = ActiveModelSerializers::SerializableResource.new(@external_correspondence).as_json
      response = Sate::Common::MethodResponse.new(
        true, "External Correspondence Status Updated Successfully!", @external_correspondence_json, nil, nil)
    end
    render json: response
  end

  def archive
    status = params[:status]
    doc_reference_no = params[:doc_reference_no]
    amount = params[:amount]
    remark = params[:remark]
    no_documents = params[:no_documents]
    if @external_correspondence.update(status: status, doc_reference_no: doc_reference_no,
                                       amount: amount, remark: remark, no_documents: no_documents,
                                       archived_date: DateTime.now)
      @external_correspondence = Sate::Crmgt::ExternalIncomingCorrespondence.find_by application_module_id: app_module.id,
                                                                                     id: @external_correspondence.id
      @external_correspondence_json = ActiveModelSerializers::SerializableResource.new(@external_correspondence).as_json
      response = Sate::Common::MethodResponse.new(
        true, "External Correspondence Archived Successfully!", @external_correspondence_json, nil, nil)
    end
    render json: response
  end

  def archived
    request_type = params[:requestType]
    department_type = params[:departmentType]
    filter = params[:filter]
    sort_order = params[:sortOrder]
    page_number = params[:pageNumber].to_i
    page_size = params[:pageSize].to_i
    status = "Archived"
    user_department_role = Sate::Crmgt::UserDepartmentRole.find_by_user_id current_user.id
    user_department = Sate::Crmgt::DepartmentUserRole.find user_department_role.department_user_role_id

    @external_correspondence = []
    @external_correspondence0 = []
    @external_correspondence2 = []
    @external_correspondence3 = []

    @correspondence_assignments = []
    @correspondence_ccs = []

    @direct = []
    @ccied = []
    @assigned = []

    @all_json = []

    @external_correspondence = Sate::Crmgt::ExternalIncomingCorrespondence.where(application_module_id: app_module.id,
                                                                                 status: status,
                                                                                 destination_id: user_department.department_id)
                                                                          .where("reference_no like ? or subject like ? or array_to_string(search_keywords, '||') like ?", "%#{filter}%", "%#{filter}%", "%#{filter}%")
    if @external_correspondence.count > 0
      @direct = ActiveModelSerializers::SerializableResource.new(@external_correspondence).as_json
      @direct.each do |one|
        one[:types] = "Direct"
      end
      @all_json = @direct
    end

    headRole = Sate::Crmgt::DepartmentUserRole.find_by department_id: user_department.department_id, is_head: true
    head = Sate::Crmgt::UserDepartmentRole.find_by department_user_role_id: headRole.id
    if head != nil
      @correspondence_assignments = Sate::Crmgt::CorrespondenceAssignment.where(application_module_id: app_module.id,
                                                                                correspondence_type: 'Sate::Crmgt::ExternalIncomingCorrespondence',
                                                                                to_user_id: head.user_id)
                                                                         .select("correspondence_id").map(&:correspondence_id)
      @external_correspondence0 = Sate::Crmgt::ExternalIncomingCorrespondence.where(id: @correspondence_assignments,
                                                                                    application_module_id: app_module.id,
                                                                                    status: status)
                                                                             .where.not(destination_id: user_department.department_id)
                                                                             .where("reference_no like ? or subject like ? or array_to_string(search_keywords, '||') like ?", "%#{filter}%", "%#{filter}%", "%#{filter}%")
      if @external_correspondence0.count > 0
        @assigned = ActiveModelSerializers::SerializableResource.new(@external_correspondence0).as_json
        @assigned.each do |one|
          one[:types] = "Assigned"
        end
        @all_json = @all_json + @assigned
      end
    end

    @correspondence_ccs = Sate::Crmgt::CorrespondenceCarbonCopy.where(application_module_id: app_module.id,
                                                                      correspondence_type: 'Sate::Crmgt::ExternalIncomingCorrespondence',
                                                                      destination_id: user_department.department_id)
                                                               .select("correspondence_id").map(&:correspondence_id)
    if @correspondence_assignments.length > 0
      @correspondence_ccs = @correspondence_ccs - @correspondence_assignments
    end

    @external_correspondence2 = Sate::Crmgt::ExternalIncomingCorrespondence.where(id: @correspondence_ccs,
                                                                                  application_module_id: app_module.id,
                                                                                  status: status)
                                                                           .where("reference_no like ? or subject like ? or array_to_string(search_keywords, '||') like ?", "%#{filter}%", "%#{filter}%", "%#{filter}%")
    if @external_correspondence2.count > 0
      @ccied = ActiveModelSerializers::SerializableResource.new(@external_correspondence2).as_json
      @ccied.each do |one|
        one[:types] = "Copied"
      end
      @all_json = @all_json + @ccied
    end

    total = @all_json.count
    @all_json.sort_by! { |a| [-a[:received_date].to_time.to_i, -a[:letter_date].to_time.to_i ] }
    initialPos = page_number * page_size
    response = Sate::Common::MethodResponse.new(true, nil, @all_json.slice(initialPos, page_size), nil, total)
    render json: response
  end

  def upload
    type = params[:type]
    file = params[:file]
    if type == "Main"
      if @external_correspondence.main_images.attach(file)
        response = Sate::Common::MethodResponse.new(
            true, "Image Uploaded Successfully!", @external_correspondence, nil, nil)
      else
        errors = Sate::Common::Util.error_messages @external_correspondence, 'External Correspondence'
        response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
      end
    else
      if @external_correspondence.attachment_images.attach(file)
        response = Sate::Common::MethodResponse.new(
            true, "Image Uploaded Successfully!", @external_correspondence, nil, nil)
      else
        errors = Sate::Common::Util.error_messages @external_correspondence, 'External Correspondence'
        response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
      end
    end
    render json: response
  end

  def change_uploads
    response = upload
    response = JSON(response)

    if response["success"]
      existing_image_id = params[:existing_image]
      existing_image = ActiveStorage::Blob.find(existing_image_id)
      existing_image.attachments.first.purge_later
    end
  end

  def get_images
    main_images = []
    attachment_images = []
    if @external_correspondence.main_images.attached?
      @external_correspondence.main_images.each do |main_image|
        meta = ActiveStorage::Analyzer::ImageAnalyzer.new(main_image).metadata
        original = Rails.application.routes.url_helpers.rails_blob_path(main_image, only_path: true)
        img_var = main_image.variant(resize: "150x180")
        variant = Rails.application.routes.url_helpers.rails_representation_url(img_var, only_path: true)
        main_images << { id: main_image.blob.id, type: "main_images", width: meta[:width], height: meta[:height], original: original, variant: variant }
      end
    end
    if @external_correspondence.attachment_images.attached?
      @external_correspondence.attachment_images.each do |attachment_image|
        meta = ActiveStorage::Analyzer::ImageAnalyzer.new(attachment_image).metadata
        original = Rails.application.routes.url_helpers.rails_blob_path(attachment_image, only_path: true)
        img_var = attachment_image.variant(resize: "150x180")
        variant = Rails.application.routes.url_helpers.rails_representation_url(img_var, only_path: true)
        attachment_images << { id: attachment_image.blob.id, type: "attachment_images", width: meta[:width], height: meta[:height], original: original, variant: variant }
      end
    end
    images = { main_images: main_images, attachment_images: attachment_images }
    render json: images
  end

  private
    def set_external_correspondence
      if params[:request_type] == "incoming"
        @external_correspondence = Sate::Crmgt::ExternalIncomingCorrespondence.find(params[:id])
      else
        @external_correspondence = Sate::Crmgt::ExternalOutgoingCorrespondence.find(params[:id])
      end
    end

    def external_correspondence_params
      params.require(:external_correspondence).permit(:reference_no, :letter_date, :subject, :search_keywords, :source_id, :destination_id, :has_cc)
    end
end
