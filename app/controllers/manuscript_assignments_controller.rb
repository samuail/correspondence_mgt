class ManuscriptAssignmentsController < ApplicationController
  before_action :set_manuscript_assignment, only: [:update]

  def index
    manuscript = Sate::Crmgt::Manuscript.find_by id: params[:manuscript_id]
    @manuscript_assignments = Sate::Crmgt::ManuscriptAssignment.where(application_module_id: app_module.id,
                                                                      manuscript: manuscript).order(:assigned_date)
    total = @manuscript_assignments.count
    @manuscript_assignments_json = ActiveModelSerializers::SerializableResource.new(@manuscript_assignments).as_json
    response = Sate::Common::MethodResponse.new(
      true, nil, @manuscript_assignments_json, nil, total)
    render json: response
  end

  def create
    @manuscript = Sate::Crmgt::Manuscript.where id: manuscript_assignment_params[:manuscript_id]
    if params[:assignee_type] == "Author"
      assignee_type = Sate::Crmgt::Author
    elsif params[:assignee_type] == "User"
      assignee_type = Sate::Auth::User
    else
      assignee_type = Sate::Crmgt::ExternalAssessor
    end
    @manuscript_assignment = Sate::Crmgt::ManuscriptAssignment.new(manuscript_assignment_params)
    @manuscript_assignment.from_user_id = current_user.id
    @manuscript_assignment.assignee_type = assignee_type
    @manuscript_assignment.application_module_id = app_module.id
    @manuscript_assignment.assigned_date = DateTime.now
    @manuscript_assignment.received_date = DateTime.now
    @manuscript_assignment.status = "Assigned"

    if @manuscript_assignment.save
      @manuscript.update(current_stage: manuscript_assignment_params[:stage])
      @manuscript_assignment_json = ActiveModelSerializers::SerializableResource.new(@manuscript_assignment).as_json
      response = Sate::Common::MethodResponse.new(
        true, "Manuscript Assignment Saved Successfully!", @manuscript_assignment_json, nil, nil)
    else
      errors = Sate::Common::Util.error_messages @manuscript_assignment, "Manuscript Assignment"
      response = Sate::Common::MethodResponse.new(
        false, nil, nil, errors, nil)
    end
    render json: response
  end

  def update
    @manuscript = Sate::Crmgt::Manuscript.where id: manuscript_assignment_params[:manuscript_id]
    if params[:assignee_type] == "Author"
      assignee_type = Sate::Crmgt::Author
    elsif params[:assignee_type] == "User"
      assignee_type = Sate::Auth::User
    else
      assignee_type = Sate::Crmgt::ExternalAssessor
    end
    if @manuscript_assignment.assignee_type != assignee_type
      @manuscript_assignment.assignee_type = assignee_type
    end
    if @manuscript_assignment.update(manuscript_assignment_params)
      @manuscript.update(current_stage: manuscript_assignment_params[:stage])
      @manuscript_assignment_json = ActiveModelSerializers::SerializableResource.new(@manuscript_assignment).as_json
      response = Sate::Common::MethodResponse.new(
        true, "Manuscript Assignment Updated Successfully!", @manuscript_assignment_json, nil, nil)
    else
      errors = Sate::Common::Util.error_messages @manuscript_assignment, "Manuscript Assignment"
      response = Sate::Common::MethodResponse.new(
        false, nil, nil, errors, nil)
    end
    render json: response
  end

  private
    def set_manuscript_assignment
      @manuscript_assignment = Sate::Crmgt::ManuscriptAssignment.find(params[:id])
    end

    def manuscript_assignment_params
      params.require(:manuscript_assignment).permit(:stage, :assignee_id, :manuscript_id)
    end
end
