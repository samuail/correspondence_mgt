class InternalCorrespondencesController < ApplicationController
  before_action :set_internal_correspondence, only: [:update, :update_status, :archive,
                                                     :upload, :change_uploads, :get_images]

  include Rails.application.routes.url_helpers

  def index
    request_type = params[:requestType]
    department_type = params[:departmentType]
    filter = params[:filter]
    sort_order = params[:sortOrder]
    page_number = params[:pageNumber].to_i
    page_size = params[:pageSize].to_i
    user_department_role = Sate::Crmgt::UserDepartmentRole.find_by_user_id current_user.id
    user_department = Sate::Crmgt::DepartmentUserRole.find user_department_role.department_user_role_id
    @internal_correspondences = []
    @internal_correspondences0 = []
    @internal_correspondences2 = []
    @internal_correspondences3 = []

    @correspondence_assignments = []
    @correspondence_ccs = []

    @direct = []
    @ccied = []
    @assigned = []

    @all_json = []

    if request_type == "incoming"
      source_departments = department_type == "center" ? Sate::Crmgt::Department.center : Sate::Crmgt::Department.college
      @internal_correspondences = Sate::Crmgt::InternalCorrespondence.where(application_module_id: app_module.id,
                                                                            destination_id: user_department.department_id,
                                                                            source_id: source_departments)
                                                                     .where.not(status: 'New')
                                                                     .where.not(status: 'Archived')
                                                                     .where.not(status: 'Rejected')
                                                                     .where("reference_no like ? or subject like ? or array_to_string(search_keywords, '||') like ?", "%#{filter}%", "%#{filter}%", "%#{filter}%")
      if @internal_correspondences.count > 0
        @direct = ActiveModelSerializers::SerializableResource.new(@internal_correspondences).as_json
        @direct.each do |one|
          one[:types] = "Direct"
        end
        @all_json = @direct
      end

      if current_user.roles.first.name == "Secretary" || current_user.roles.first.name == "Business Administrator"
        headRole = Sate::Crmgt::DepartmentUserRole.find_by department_id: user_department.department_id, is_head: true
        head = Sate::Crmgt::UserDepartmentRole.find_by department_user_role_id: headRole.id
        if head != nil
          @correspondence_assignments = Sate::Crmgt::CorrespondenceAssignment.where(application_module_id: app_module.id,
                                                                                    correspondence_type: 'Sate::Crmgt::InternalCorrespondence',
                                                                                    to_user_id: head.user_id)
                                                                             .select("correspondence_id").map(&:correspondence_id)
          @internal_correspondences0 = Sate::Crmgt::InternalCorrespondence.where(id: @correspondence_assignments,
                                                                                 application_module_id: app_module.id,
                                                                                 source_id: source_departments)
                                                                          .where.not(destination_id: user_department.department_id)
                                                                          .where.not(status: 'New')
                                                                          .where.not(status: 'Archived')
                                                                          .where.not(status: 'Rejected')
                                                                          .where("reference_no like ? or subject like ? or array_to_string(search_keywords, '||') like ?", "%#{filter}%", "%#{filter}%", "%#{filter}%")
          if @internal_correspondences0.count > 0
            @assigned = ActiveModelSerializers::SerializableResource.new(@internal_correspondences0).as_json
            @assigned.each do |one|
              one[:types] = "Assigned"
            end
            @all_json = @all_json + @assigned
          end
        end
      end

      @correspondence_ccs = Sate::Crmgt::CorrespondenceCarbonCopy.where(application_module_id: app_module.id,
                                                                        correspondence_type: 'Sate::Crmgt::InternalCorrespondence',
                                                                        destination_id: user_department.department_id)
                                                                 .select("correspondence_id").map(&:correspondence_id)
      if @correspondence_assignments.length > 0
        @correspondence_ccs = @correspondence_ccs - @correspondence_assignments
      end

      @internal_correspondences2 = Sate::Crmgt::InternalCorrespondence.where(id: @correspondence_ccs,
                                                                             application_module_id: app_module.id,
                                                                             source_id: source_departments)
                                                                      .where.not(status: 'New')
                                                                      .where.not(status: 'Archived')
                                                                      .where.not(status: 'Rejected')
                                                                      .where("reference_no like ? or subject like ? or array_to_string(search_keywords, '||') like ?", "%#{filter}%", "%#{filter}%", "%#{filter}%")
      if @internal_correspondences2.count > 0
        @ccied = ActiveModelSerializers::SerializableResource.new(@internal_correspondences2).as_json
        @ccied.each do |one|
          one[:types] = "Copied"
        end
        @all_json = @all_json + @ccied
      end


    else
      @internal_correspondences3 = Sate::Crmgt::InternalCorrespondence.where(application_module_id: app_module.id,
                                                                             source_id: user_department.department_id)
                                                                      .where("reference_no like ?", "%#{filter}%")
      @all_json = ActiveModelSerializers::SerializableResource.new(@internal_correspondences3).as_json
    end
    total = @all_json.count
    @all_json.sort_by! { |a| [-a[:received_date].to_time.to_i, -a[:letter_date].to_time.to_i ] }
    initialPos = page_number * page_size
    response = Sate::Common::MethodResponse.new(true, nil, @all_json.slice(initialPos, page_size), nil, total)
    render json: response
  end

  def get_assigned
    request_type = params[:requestType]
    department_type = params[:departmentType]
    filter = params[:filter]
    sort_order = params[:sortOrder]
    page_number = params[:pageNumber].to_i
    page_size = params[:pageSize].to_i
    source_departments = department_type == "center" ? Sate::Crmgt::Department.center : Sate::Crmgt::Department.college
    assigned = Sate::Crmgt::CorrespondenceAssignment.where(application_module_id: app_module.id,
                                                           to_user_id: current_user.id,
                                                           correspondence_type: 'Sate::Crmgt::InternalCorrespondence')
                                                    .select("correspondence_id").map(&:correspondence_id)
    @internal_correspondences = Sate::Crmgt::InternalCorrespondence.where(id: assigned,
                                                                          application_module_id: app_module.id,
                                                                          source_id: source_departments)
                                                                   .where.not(status: 'New')
                                                                   .where.not(status: 'Archived')
                                                                   .where.not(status: 'Rejected')
                                                                   .where("reference_no like ? or subject like ? or array_to_string(search_keywords, '||') like ?", "%#{filter}%", "%#{filter}%", "%#{filter}%")
    total = @internal_correspondences.count
    @internal_correspondences_json = ActiveModelSerializers::SerializableResource.new(@internal_correspondences).as_json
    @internal_correspondences_json.sort_by! { |a| [-a[:received_date].to_time.to_i, -a[:letter_date].to_time.to_i ] }
    initialPos = page_number * page_size
    response = Sate::Common::MethodResponse.new(true, nil, @internal_correspondences_json.slice(initialPos, page_size), nil, total)
    render json: response
  end

  def create
    request_type = params[:request_type]
    if request_type == "incoming"
      @internal_correspondence = Sate::Crmgt::InternalCorrespondence.new(internal_correspondence_params)
      user_department = Sate::Crmgt::UserDepartmentRole.find_by_user_id current_user.id
      destination = Sate::Crmgt::DepartmentUserRole.find user_department.department_user_role_id
      @internal_correspondence.destination_id = destination.department_id
      @internal_correspondence.status = "Sent"
    else
      @internal_correspondence = Sate::Crmgt::InternalCorrespondence.new(internal_correspondence_params)
      user_department = Sate::Crmgt::UserDepartmentRole.find_by_user_id current_user.id
      source = Sate::Crmgt::DepartmentUserRole.find user_department.department_user_role_id
      @internal_correspondence.source_id = source.department_id
    end
    @internal_correspondence.application_module_id = app_module.id
    @internal_correspondence.search_keywords = params[:internal_correspondence][:search_keywords]
    @internal_correspondence.received_date = Date.today
    @internal_correspondence.key_in_by_id = current_user.id
    if @internal_correspondence.save
      @internal_correspondence = Sate::Crmgt::InternalCorrespondence.find_by application_module_id: app_module.id, id: @internal_correspondence.id
      @internal_correspondence_json = ActiveModelSerializers::SerializableResource.new(@internal_correspondence).as_json
      response = Sate::Common::MethodResponse.new(
          true, "Internal Correspondence Saved Successfully!", @internal_correspondence_json, nil, nil)
    else
      errors = Sate::Common::Util.error_messages @internal_correspondence, 'Internal Correspondence'
      response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
    end
    render json: response
  end

  def update
    @internal_correspondence.search_keywords = params[:internal_correspondence][:search_keywords]
    if @internal_correspondence.update(internal_correspondence_params)
      @internal_correspondence = Sate::Crmgt::InternalCorrespondence.find_by application_module_id: app_module.id, id: @internal_correspondence.id
      @internal_correspondence_json = ActiveModelSerializers::SerializableResource.new(@internal_correspondence).as_json
      response = Sate::Common::MethodResponse.new(
          true, "Internal Correspondence Updated Successfully!", @internal_correspondence_json, nil, nil)
    else
      errors = Sate::Common::Util.error_messages @internal_correspondence, 'Internal Correspondence'
      response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
    end
    render json: response
  end

  def update_status
    status = params[:status]
    if @internal_correspondence.update(status: status, received_date: Date.today)
      @internal_correspondence = Sate::Crmgt::InternalCorrespondence.find_by application_module_id: app_module.id,
                                                                             id: @internal_correspondence.id
      @internal_correspondence_json = ActiveModelSerializers::SerializableResource.new(@internal_correspondence).as_json
      response = Sate::Common::MethodResponse.new(
        true, "Internal Correspondence Status Updated Successfully!", @internal_correspondence_json, nil, nil)
    end
    render json: response
  end

  def archive
    status = params[:status]
    doc_reference_no = params[:doc_reference_no]
    amount = params[:amount]
    remark = params[:remark]
    no_documents = params[:no_documents]
    if @internal_correspondence.update(status: status, doc_reference_no: doc_reference_no,
                                       amount: amount, remark: remark, no_documents: no_documents,
                                       archived_date: DateTime.now)
      @internal_correspondence = Sate::Crmgt::InternalCorrespondence.find_by application_module_id: app_module.id,
                                                                             id: @internal_correspondence.id
      @internal_correspondence_json = ActiveModelSerializers::SerializableResource.new(@internal_correspondence).as_json
      response = Sate::Common::MethodResponse.new(
        true, "Internal Correspondence Archived Successfully!", @internal_correspondence_json, nil, nil)
    end
    render json: response
  end

  def archived
    request_type = params[:requestType]
    department_type = params[:departmentType]
    filter = params[:filter]
    sort_order = params[:sortOrder]
    page_number = params[:pageNumber].to_i
    page_size = params[:pageSize].to_i
    status = "Archived"
    user_department_role = Sate::Crmgt::UserDepartmentRole.find_by_user_id current_user.id
    user_department = Sate::Crmgt::DepartmentUserRole.find user_department_role.department_user_role_id

    @internal_correspondences = []
    @internal_correspondences0 = []
    @internal_correspondences2 = []
    @internal_correspondences3 = []

    @correspondence_assignments = []
    @correspondence_ccs = []

    @direct = []
    @ccied = []
    @assigned = []

    @all_json = []

    @internal_correspondences = Sate::Crmgt::InternalCorrespondence.where(application_module_id: app_module.id,
                                                                          status: status,
                                                                          destination_id: user_department.department_id)
                                                                   .where("reference_no like ? or subject like ? or array_to_string(search_keywords, '||') like ?", "%#{filter}%", "%#{filter}%", "%#{filter}%")
    if @internal_correspondences.count > 0
      @direct = ActiveModelSerializers::SerializableResource.new(@internal_correspondences).as_json
      @direct.each do |one|
        one[:types] = "Direct"
      end
      @all_json = @direct
    end

    headRole = Sate::Crmgt::DepartmentUserRole.find_by department_id: user_department.department_id, is_head: true
    head = Sate::Crmgt::UserDepartmentRole.find_by department_user_role_id: headRole.id
    if head != nil
      @correspondence_assignments = Sate::Crmgt::CorrespondenceAssignment.where(application_module_id: app_module.id,
                                                                                correspondence_type: 'Sate::Crmgt::InternalCorrespondence',
                                                                                to_user_id: head.user_id)
                                                                         .select("correspondence_id").map(&:correspondence_id)
      @internal_correspondences0 = Sate::Crmgt::InternalCorrespondence.where(id: @correspondence_assignments,
                                                                             application_module_id: app_module.id,
                                                                             status: status)
                                                                      .where.not(destination_id: user_department.department_id)
                                                                      .where("reference_no like ? or subject like ? or array_to_string(search_keywords, '||') like ?", "%#{filter}%", "%#{filter}%", "%#{filter}%")
      if @internal_correspondences0.count > 0
        @assigned = ActiveModelSerializers::SerializableResource.new(@internal_correspondences0).as_json
        @assigned.each do |one|
          one[:types] = "Assigned"
        end
        @all_json = @all_json + @assigned
      end
    end

    @correspondence_ccs = Sate::Crmgt::CorrespondenceCarbonCopy.where(application_module_id: app_module.id,
                                                                      correspondence_type: 'Sate::Crmgt::InternalCorrespondence',
                                                                      destination_id: user_department.department_id)
                                                               .select("correspondence_id").map(&:correspondence_id)
    if @correspondence_assignments.length > 0
      @correspondence_ccs = @correspondence_ccs - @correspondence_assignments
    end

    @internal_correspondences2 = Sate::Crmgt::InternalCorrespondence.where(id: @correspondence_ccs,
                                                                           application_module_id: app_module.id,
                                                                           status: status)
                                                                    .where("reference_no like ? or subject like ? or array_to_string(search_keywords, '||') like ?", "%#{filter}%", "%#{filter}%", "%#{filter}%")
    if @internal_correspondences2.count > 0
      @ccied = ActiveModelSerializers::SerializableResource.new(@internal_correspondences2).as_json
      @ccied.each do |one|
        one[:types] = "Copied"
      end
      @all_json = @all_json + @ccied
    end

    total = @all_json.count
    @all_json.sort_by! { |a| [-a[:received_date].to_time.to_i, -a[:letter_date].to_time.to_i ] }
    initialPos = page_number * page_size
    response = Sate::Common::MethodResponse.new(true, nil, @all_json.slice(initialPos, page_size), nil, total)
    render json: response
  end

  def upload
    type = params[:type]
    file = params[:file]
    if type == "Main"
      if @internal_correspondence.main_images.attach(file)
        response = Sate::Common::MethodResponse.new(
            true, "Image Uploaded Successfully!", @internal_correspondence, nil, nil)
      else
        errors = Sate::Common::Util.error_messages @internal_correspondence, 'Internal Correspondence'
        response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
      end
    else
      if @internal_correspondence.attachment_images.attach(file)
        response = Sate::Common::MethodResponse.new(
            true, "Image Uploaded Successfully!", @internal_correspondence, nil, nil)
      else
        errors = Sate::Common::Util.error_messages @internal_correspondence, 'Internal Correspondence'
        response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
      end
    end
    render json: response
  end

  def change_uploads
    response = upload
    response = JSON(response)

    if response["success"]
      existing_image_id = params[:existing_image]
      existing_image = ActiveStorage::Blob.find(existing_image_id)
      existing_image.attachments.first.purge_later
    end
  end

  def get_images
    main_images = []
    attachment_images = []
    if @internal_correspondence.main_images.attached?
      @internal_correspondence.main_images.each do |main_image|
        meta = ActiveStorage::Analyzer::ImageAnalyzer.new(main_image).metadata
        original = Rails.application.routes.url_helpers.rails_blob_path(main_image, only_path: true)
        img_var = main_image.variant(resize: "150x180")
        variant = Rails.application.routes.url_helpers.rails_representation_url(img_var, only_path: true)
        main_images << { id: main_image.blob.id, type: "main_images", width: meta[:width], height: meta[:height], original: original, variant: variant }
      end
    end
    if @internal_correspondence.attachment_images.attached?
      @internal_correspondence.attachment_images.each do |attachment_image|
        meta = ActiveStorage::Analyzer::ImageAnalyzer.new(attachment_image).metadata
        original = Rails.application.routes.url_helpers.rails_blob_path(attachment_image, only_path: true)
        img_var = attachment_image.variant(resize: "150x180")
        variant = Rails.application.routes.url_helpers.rails_representation_url(img_var, only_path: true)
        attachment_images << { id: attachment_image.blob.id, type: "attachment_images", width: meta[:width], height: meta[:height], original: original, variant: variant }
      end
    end
    images = { main_images: main_images, attachment_images: attachment_images }
    render json: images
  end

  private
    def set_internal_correspondence
      @internal_correspondence = Sate::Crmgt::InternalCorrespondence.find(params[:id])
    end

    def internal_correspondence_params
      params.require(:internal_correspondence).permit(:reference_no, :letter_date, :subject, :search_keywords, :source_id, :destination_id, :has_cc)
    end
end
