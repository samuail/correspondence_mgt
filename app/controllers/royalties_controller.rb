class RoyaltiesController < ApplicationController
  before_action :set_royalty, only: [:update]

  def index
    author = Sate::Crmgt::Author.where id: params[:author_id]
    @royalties = Sate::Crmgt::Royalty.where author: author

    total = @royalties.count
    @royalties_json = ActiveModelSerializers::SerializableResource.new(@royalties).as_json
    response = Sate::Common::MethodResponse.new(
      true, nil, @royalties_json, nil, total)
    render json: response
  end

  def create
    @royalty = Sate::Crmgt::Royalty.new(royalty_params)
    @royalty.application_module_id = app_module.id
    @royalty.payment_date = DateTime.now
    @royalty.key_in_by_id = current_user.id
    if @royalty.save
      @royalty = Sate::Crmgt::Royalty.find_by application_module_id: app_module.id, id: @royalty.id
      @royalty_json = ActiveModelSerializers::SerializableResource.new(@royalty).as_json
      response = Sate::Common::MethodResponse.new(
        true, "Royalty Saved Successfully!", @royalty_json, nil, nil)
    else
      errors = Sate::Common::Util.error_messages @royalty, 'Royalty'
      response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
    end
    render json: response
  end

  def update
    if @royalty.update(royalty_params)
      @royalty = Sate::Crmgt::Royalty.find_by application_module_id: app_module.id, id: @royalty.id
      @royalty_json = ActiveModelSerializers::SerializableResource.new(@royalty).as_json
      response = Sate::Common::MethodResponse.new(
        true, "Royalty Updated Successfully!", @royalty_json, nil, nil)
    else
      errors = Sate::Common::Util.error_messages @royalty, 'Royalty'
      response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
    end
    render json: response
  end

  private
    def set_royalty
      @royalty = Sate::Crmgt::Royalty.find(params[:id])
    end

    def royalty_params
      params.require(:royalty).permit(:manuscript_id, :author_id, :reference_no, :amount, :remark)
    end
end
