class DashboardsController < ApplicationController

  def index
    user_department_role = Sate::Crmgt::UserDepartmentRole.find_by_user_id current_user.id
    user_department = Sate::Crmgt::DepartmentUserRole.find user_department_role.department_user_role_id

    center_departments = Sate::Crmgt::Department.center
    college_departments = Sate::Crmgt::Department.college

    @incoming_centers = []
    @incoming_colleges = []
    @incoming_externals = []
    @outgoing_internal = []
    @outgoing_external = []

    @c_assigned_internal = []
    @c_assigned_external = []
    @assigned_centers = []
    @assigned_colleges = []
    @assigned_externals = []

    @c_internal_cc = []
    @c_external_cc = []
    @c_external_outgoing_cc = []
    @cc_centers = []
    @cc_colleges = []
    @cc_externals = []
    @cc_external_outgoings = []

    @incoming_centers = Sate::Crmgt::InternalCorrespondence.where application_module_id: app_module.id,
                                                                  destination_id: user_department.department_id,
                                                                  source_id: center_departments
    @incoming_colleges = Sate::Crmgt::InternalCorrespondence.where application_module_id: app_module.id,
                                                                   destination_id: user_department.department_id,
                                                                    source_id: college_departments
    @incoming_externals = Sate::Crmgt::ExternalIncomingCorrespondence.where application_module_id: app_module.id,
                                                                            destination_id: user_department.department_id
    headRole = Sate::Crmgt::DepartmentUserRole.find_by department_id: user_department.department_id, is_head: true
    head = Sate::Crmgt::UserDepartmentRole.find_by department_user_role_id: headRole.id
    if head != nil
      @c_assigned_internal = Sate::Crmgt::CorrespondenceAssignment.where(application_module_id: app_module.id,
                                                               correspondence_type: 'Sate::Crmgt::InternalCorrespondence',
                                                               to_user_id: head.user_id)
                                                        .select("correspondence_id").map(&:correspondence_id)
      @assigned_centers =  Sate::Crmgt::InternalCorrespondence.where(id: @c_assigned_internal,
                                                                   application_module_id: app_module.id,
                                                                   source_id: center_departments)
                                                            .where.not(destination_id: user_department.department_id)
      @assigned_colleges =  Sate::Crmgt::InternalCorrespondence.where(id: @c_assigned_internal,
                                                                   application_module_id: app_module.id,
                                                                   source_id: college_departments)
                                                            .where.not(destination_id: user_department.department_id)
      @c_assigned_external = Sate::Crmgt::CorrespondenceAssignment.where(application_module_id: app_module.id,
                                                                        correspondence_type: 'Sate::Crmgt::ExternalIncomingCorrespondence',
                                                                        to_user_id: head.user_id)
                                                                 .select("correspondence_id").map(&:correspondence_id)
      @assigned_externals = Sate::Crmgt::ExternalIncomingCorrespondence.where(id: @c_assigned_external,
                                                                             application_module_id: app_module.id)
                                                                      .where.not(destination_id: user_department.department_id)

      @incoming_centers = @incoming_centers + @assigned_centers
      @incoming_colleges = @incoming_colleges + @assigned_colleges
      @incoming_externals = @incoming_externals + @assigned_externals
    end

    @c_internal_cc = Sate::Crmgt::CorrespondenceCarbonCopy.where(application_module_id: app_module.id,
                                                                 correspondence_type: 'Sate::Crmgt::InternalCorrespondence',
                                                                 destination_id: user_department.department_id)
                                                          .select("correspondence_id").map(&:correspondence_id)
    @c_external_cc = Sate::Crmgt::CorrespondenceCarbonCopy.where(application_module_id: app_module.id,
                                                                 correspondence_type: 'Sate::Crmgt::ExternalIncomingCorrespondence',
                                                                 destination_id: user_department.department_id)
                                                          .select("correspondence_id").map(&:correspondence_id)
    @c_external_outgoing_cc = Sate::Crmgt::CorrespondenceCarbonCopy.where(application_module_id: app_module.id,
                                                                          correspondence_type: 'Sate::Crmgt::ExternalOutgoingCorrespondence',
                                                                          destination_id: user_department.department_id)
                                                                   .select("correspondence_id").map(&:correspondence_id)
    if @c_assigned_internal.length > 0
      @c_internal_cc = @c_internal_cc - @c_assigned_internal
    end
    if @c_assigned_external.length > 0
      @c_external_cc = @c_external_cc - @c_assigned_external
    end

    @cc_centers = Sate::Crmgt::InternalCorrespondence.where id: @c_internal_cc,
                                                            application_module_id: app_module.id,
                                                            source_id: center_departments
    @cc_colleges = Sate::Crmgt::InternalCorrespondence.where id: @c_internal_cc,
                                                             application_module_id: app_module.id,
                                                             source_id: college_departments
    @cc_externals = Sate::Crmgt::ExternalIncomingCorrespondence.where id: @c_external_cc,
                                                                      application_module_id: app_module.id
    @cc_external_outgoings = Sate::Crmgt::ExternalOutgoingCorrespondence.where id: @c_external_outgoing_cc,
                                                                               application_module_id: app_module.id

    @incoming_centers = @incoming_centers + @cc_centers
    @incoming_colleges = @incoming_colleges + @cc_colleges
    @incoming_externals = @incoming_externals + @cc_externals

    @outgoing_internals = Sate::Crmgt::InternalCorrespondence.where application_module_id: app_module.id,
                                                                    source_id: user_department.department_id
    @outgoing_externals = Sate::Crmgt::ExternalOutgoingCorrespondence.where application_module_id: app_module.id,
                                                                            source_id: user_department.department_id
    @outgoing_externals = @outgoing_externals + @cc_external_outgoings
    @incoming_centers = ActiveModelSerializers::SerializableResource.new(@incoming_centers).as_json
    @incoming_colleges = ActiveModelSerializers::SerializableResource.new(@incoming_colleges).as_json

    @outgoing_internals = ActiveModelSerializers::SerializableResource.new(@outgoing_internals).as_json

    data = { incoming_centers: @incoming_centers, incoming_colleges: @incoming_colleges, incoming_externals: @incoming_externals,
             outgoing_internals: @outgoing_internals, outgoing_externals: @outgoing_externals}
    response = Sate::Common::MethodResponse.new(true, nil, data, nil, 0)
    render json: response
  end

  def get_user_status
    user_department_role = Sate::Crmgt::UserDepartmentRole.find_by_user_id current_user.id
    user_department = Sate::Crmgt::DepartmentUserRole.find user_department_role.department_user_role_id
    department_user_role_ids = Sate::Crmgt::DepartmentUserRole.where(application_module_id: app_module.id,
                                                                     department_id: user_department.department_id)
                                                              .select("id").map(&:id)
    user_ids = Sate::Crmgt::UserDepartmentRole.where(application_module_id: app_module.id,
                                                  department_user_role_id: department_user_role_ids)
                                           .select("user_id").map(&:user_id)
    assignments = Sate::Crmgt::CorrespondenceAssignment.where(to_user_id: user_ids).
                      or(Sate::Crmgt::CorrespondenceAssignment.where(from_user_id: user_ids)).group(:id, :to_user_id)
    @assignments = ActiveModelSerializers::SerializableResource.new(assignments).as_json
    response = Sate::Common::MethodResponse.new(true, nil, @assignments, nil, nil )
    render json: response
  end

  def all_correspondence
    user_department_role = Sate::Crmgt::UserDepartmentRole.find_by_user_id current_user.id
    user_department = Sate::Crmgt::DepartmentUserRole.find user_department_role.department_user_role_id

    center_departments = Sate::Crmgt::Department.center
    college_departments = Sate::Crmgt::Department.college

    headRole = Sate::Crmgt::DepartmentUserRole.find_by department_id: user_department.department_id, is_head: true
    head = Sate::Crmgt::UserDepartmentRole.find_by department_user_role_id: headRole.id

    @center_incomings = Sate::Crmgt::InternalCorrespondence.from_internal "Center", user_department.department_id
    @college_incomings = Sate::Crmgt::InternalCorrespondence.from_internal "College", user_department.department_id
    @external_incomings = Sate::Crmgt::ExternalIncomingCorrespondence.from_external user_department.department_id

    @center_assigned = []
    @college_assigned = []
    @external_assigned = []

    unless head == nil
      @center_assigned = self.assigned_incoming(user_department.department_id, center_departments, head, "Internal", 'Sate::Crmgt::InternalCorrespondence')
      @college_assigned = self.assigned_incoming(user_department.department_id, college_departments, head, "Internal", 'Sate::Crmgt::InternalCorrespondence')
      @external_assigned = self.assigned_incoming(user_department.department_id, college_departments, head, "External", 'Sate::Crmgt::ExternalIncomingCorrespondence')
    end

    @center_cc = []
    @college_cc = []
    @external_incoming_cc = []
    @external_outgoing_center_cc = []
    @external_outgoing_college_cc = []

    @center_cc = self.ccied_incoming(user_department.department_id, center_departments, "Internal", 'Sate::Crmgt::InternalCorrespondence')
    @college_cc = self.ccied_incoming(user_department.department_id, college_departments, "Internal", 'Sate::Crmgt::InternalCorrespondence')
    @external_incoming_cc = self.ccied_incoming(user_department.department_id, center_departments, "External In", 'Sate::Crmgt::ExternalIncomingCorrespondence')
    @external_outgoing_center_cc = self.ccied_incoming(user_department.department_id, center_departments, "External Out", 'Sate::Crmgt::ExternalOutgoingCorrespondence')
    @external_outgoing_college_cc = self.ccied_incoming(user_department.department_id, college_departments, "External Out", 'Sate::Crmgt::ExternalOutgoingCorrespondence')

    @internal_outgoings = []
    @external_outgoings = []

    @internal_outgoings = Sate::Crmgt::InternalCorrespondence.to_internal user_department.department_id
    @external_outgoings = Sate::Crmgt::ExternalOutgoingCorrespondence.to_external user_department.department_id

    @center_assigned = @center_assigned - @center_cc
    @center_incomings = @center_incomings + @center_assigned + @center_cc

    @college_assigned = @college_assigned - @college_cc
    @college_incomings = @college_incomings + @college_assigned + @college_cc

    @external_assigned = @external_assigned - @external_incoming_cc
    @external_incomings = @external_incomings + @external_assigned + @external_incoming_cc
  end

  def group_by_received_date(objs)
    objs.group_by { |center| center[:received_date].strftime("%Y-%m") }.values
  end

  def group_by_status(objs)
    objs.group_by { |obj| obj[:status] }.values
  end

  def group_by_source(objs)
    objs.group_by { |obj| obj[:source][:id] }.values
  end

  def group_by_destination(objs)
    objs.group_by { |obj| obj[:destination][:id] }.values
  end

  def assign_status(objs)
    status = []
    objs.each do |obj| status << { "Received Date" => obj[0]['received_date'].strftime("%Y-%m"),
                                   "Month" => obj[0]['received_date'].strftime("%b"), "Count" => obj.count }
    end
    status.sort_by! { |obj| obj["Received Date"] }.reverse!
  end

  def return_count(status, month_name)
    current = status.select { |stat| stat["Month"] == month_name }
    count = current == [] ? 0 : current[0]["Count"]
    count
  end

  def return_status_count(objs)
    stats = []
    objs.each { |obj| stats << { "status" => obj[0]['status'], "count" => obj.count } }
    stats
  end

  def department_by_status(objs, dept)
    department_stats = []
    objs.each do |obj|
      obj_stats = self.group_by_status obj
      dept_status = []
      obj_stats.each do |obj_stat|
        dept_status << { "status" => obj_stat[0][:status], "count" => obj_stat.count }
      end
      department_stats << { "department" => obj[0][:"#{dept}"] , "total" => obj.count,
                            "by_status" => dept_status }
    end
    department_stats.sort_by! { |dept| dept["total"] }.reverse!
  end

  def correspondence_by_type
    self.all_correspondence
    total_count = []
    total_count << { "center" => @center_incomings.count + @external_outgoing_center_cc.count }
    total_count << { "college" => @college_incomings.count + @external_outgoing_college_cc.count }
    total_count << { "external" => @external_incomings.count }
    total_count << { "internal" => @internal_outgoings.count }
    total_count << { "external_out" => @external_outgoings.count }

    response = Sate::Common::MethodResponse.new(true, nil, total_count, nil, nil )
    render json: response
  end

  def correspondence_by_status
    self.all_correspondence
    centers = self.group_by_status @center_incomings
    center_status = self.return_status_count centers

    colleges = self.group_by_status @college_incomings
    college_status = self.return_status_count colleges

    externals = self.group_by_status @external_incomings
    external_status = self.return_status_count externals

    internals = self.group_by_status @internal_outgoings
    internal_status = self.return_status_count internals

    external_outgoings = self.group_by_status @external_outgoings
    external_outgoing_status = self.return_status_count external_outgoings

    correspondence_status = []
    correspondence_status << { "center" => center_status }
    correspondence_status << { "college" => college_status }
    correspondence_status << { "external" => external_status }
    correspondence_status << { "internal" => internal_status }
    correspondence_status << { "external_out" => external_outgoing_status }
    response = Sate::Common::MethodResponse.new(true, nil, correspondence_status, nil, nil )
    render json: response
  end

  def correspondence_by_month
    self.all_correspondence
    centers = self.group_by_received_date @center_incomings
    center_status = self.assign_status centers

    colleges = self.group_by_received_date @college_incomings
    college_status = self.assign_status colleges

    externals = self.group_by_received_date @external_incomings
    external_status = self.assign_status externals

    internals = self.group_by_received_date @internal_outgoings
    internal_status = self.assign_status internals

    external_outgoings = self.group_by_received_date @external_outgoings
    external_outgoing_status = self.assign_status external_outgoings

    external_outgoing_center_cc = self.group_by_received_date @external_outgoing_center_cc
    external_outgoing_center_status = self.assign_status external_outgoing_center_cc

    external_outgoing_college_cc = self.group_by_received_date @external_outgoing_college_cc
    external_outgoing_college_status = self.assign_status external_outgoing_college_cc

    correspondence_by_month = []

    5.downto(0).collect do |n|
      month_name = Date.parse(Date::MONTHNAMES[n.months.ago.month]).strftime('%b')
      center_count = self.return_count center_status, month_name
      college_count = self.return_count college_status, month_name
      external_count = self.return_count external_status, month_name
      internal_count = self.return_count internal_status, month_name
      external_outgoing_count = self.return_count external_outgoing_status, month_name
      external_out_center_cc_count = self.return_count external_outgoing_center_status, month_name
      external_out_college_cc_count = self.return_count external_outgoing_college_status, month_name

      correspondence_by_month << { "month" => month_name,
                                   "center" => center_count + external_out_center_cc_count,
                                   "college" => college_count + external_out_college_cc_count,
                                   "external" => external_count,
                                   "internal" => internal_count,
                                   "external_out" => external_outgoing_count,
                                   "outgoing" => internal_count + external_outgoing_count }
    end
    response = Sate::Common::MethodResponse.new(true, nil, correspondence_by_month, nil, nil )
    render json: response
  end

  def get_monthly_payment(objs)
    objs = objs.group_by { |obj| obj[:status] }
    stats = []
    unless objs["Archived"] == nil
      objs = objs["Archived"].group_by { |obj| obj[:archived_date].strftime("%Y-%m") }
      objs.each do |obj|
        amt = 0
        obj[1].each do |obj|
          amt += obj[:amount] == nil ? 0 : obj[:amount]
        end
        stats << { "year_month" => obj[0], "month" => obj[1][0][:archived_date].strftime("%b"), "amount" => amt }
      end
    end
    stats
  end

  def return_amount(objs, month_name)
    current = objs.select { |obj| obj["month"] == month_name }
    amt = current == [] ? 0 : current[0]["amount"]
    amt
  end

  def correspondence_monthly_payment
    self.all_correspondence
    center_monthly = self.get_monthly_payment @center_incomings
    college_monthly = self.get_monthly_payment @college_incomings
    external_monthly = self.get_monthly_payment @external_incomings
    outgoing = @internal_outgoings - @center_incomings
    outgoing_monthly = self.get_monthly_payment outgoing

    correspondence_by_monthly_paid = []

    5.downto(0).collect do |n|
      month_name = Date.parse(Date::MONTHNAMES[n.months.ago.month]).strftime("%b")
      center_amount = self.return_amount center_monthly, month_name
      college_amount = self.return_amount college_monthly, month_name
      external_amount = self.return_amount external_monthly, month_name
      outgoing_amount = self.return_amount outgoing_monthly, month_name
      amount = center_amount + college_amount + external_amount + outgoing_amount
      correspondence_by_monthly_paid << { "month" => month_name,
                                          "amount" => amount }
    end
    response = Sate::Common::MethodResponse.new(true, nil, correspondence_by_monthly_paid, nil, nil )
    render json: response
  end

  def center_correspondence_details
    self.all_correspondence
    center_incomings = ActiveModelSerializers::SerializableResource.new(@center_incomings).as_json
    centers = self.group_by_source center_incomings
    center_departments = self.department_by_status centers, "source"
    response = Sate::Common::MethodResponse.new(true, nil, center_departments, nil, nil )
    render json: response
  end

  def college_correspondence_details
    self.all_correspondence
    college_incomings = ActiveModelSerializers::SerializableResource.new(@college_incomings).as_json
    colleges = self.group_by_source college_incomings
    college_departments = self.department_by_status colleges, "source"
    response = Sate::Common::MethodResponse.new(true, nil, college_departments, nil, nil )
    render json: response
  end

  def outgoing_correspondence_details
    self.all_correspondence
    internal_outgoings = ActiveModelSerializers::SerializableResource.new(@internal_outgoings).as_json
    internals = self.group_by_destination internal_outgoings
    internal_departments = self.department_by_status internals, "destination"
    response = Sate::Common::MethodResponse.new(true, nil, internal_departments, nil, nil )
    render json: response
  end

  def get_department_user_status
    user_department_role = Sate::Crmgt::UserDepartmentRole.find_by_user_id current_user.id
    user_department = Sate::Crmgt::DepartmentUserRole.find user_department_role.department_user_role_id
    department_user_role_ids = Sate::Crmgt::DepartmentUserRole.where(application_module_id: app_module.id,
                                                                     department_id: user_department.department_id)
                                                              .select("id").map(&:id)
    user_ids = Sate::Crmgt::UserDepartmentRole.where(application_module_id: app_module.id,
                                                     department_user_role_id: department_user_role_ids)
                                              .select("user_id").map(&:user_id)
    assigned_froms = Sate::Crmgt::CorrespondenceAssignment.where(to_user_id: user_ids)
    assigned_froms = ActiveModelSerializers::SerializableResource.new(assigned_froms).as_json
    assigned_tos = Sate::Crmgt::CorrespondenceAssignment.where(from_user_id: user_ids)
    assigned_tos = ActiveModelSerializers::SerializableResource.new(assigned_tos).as_json
    assigned_froms = assigned_froms.group_by { |from| from[:to_user][:id] }.values
    assigned_tos = assigned_tos.group_by { |to| to[:from_user][:id] }.values
    assignment = []
    assigned_froms.each do |user_routes|
      routes = user_routes.group_by { |route| route[:status] }.values
      by_status = []
      routes.each do |route|
        by_status << { "status" => route[0][:status],
                       "count" => route.count }
      end
      current = assigned_tos.detect { |to| to[0][:from_user] == routes[0][0][:to_user] }
      from_count = current == nil ? 0 : current.count
      assignment << { "user" => routes[0][0][:to_user],
                      "to_user" => by_status,
                      "from_user" => from_count }
    end
    response = Sate::Common::MethodResponse.new(true, nil, assignment, nil, nil )
    render json: response
  end

  def ccied_incoming(dest_department_id, source_departments, type, correspondence_type)
    cc_correspondence = Sate::Crmgt::CorrespondenceCarbonCopy.where(application_module_id: app_module.id,
                                                                    correspondence_type: correspondence_type,
                                                                    destination_id: dest_department_id)
                                                             .select("correspondence_id").map(&:correspondence_id)
    if type == "Internal"
      ccied_incoming = Sate::Crmgt::InternalCorrespondence.where id: cc_correspondence,
                                                                 application_module_id: app_module.id,
                                                                 source_id: source_departments
    elsif type == "External In"
      ccied_incoming = Sate::Crmgt::ExternalIncomingCorrespondence.where id: cc_correspondence,
                                                                         application_module_id: app_module.id
    elsif type == "External Out"
      ccied_incoming = Sate::Crmgt::ExternalOutgoingCorrespondence.where id: cc_correspondence,
                                                                         source_id: source_departments,
                                                                         application_module_id: app_module.id
    end
    return ccied_incoming
  end

  def assigned_incoming(dest_department_id, source_departments, head, type, correspondence_type)
    assigned_correspondence = Sate::Crmgt::CorrespondenceAssignment.where(application_module_id: app_module.id,
                                                                          correspondence_type: correspondence_type,
                                                                          to_user_id: head.user_id)
                                                                   .select("correspondence_id").map(&:correspondence_id)
    if type == "Internal"
      assigned_incoming = Sate::Crmgt::InternalCorrespondence.where(id: assigned_correspondence,
                                                                    application_module_id: app_module.id,
                                                                    source_id: source_departments)
                                                             .where.not(destination_id: dest_department_id)
    elsif type == "External"
      assigned_incoming = Sate::Crmgt::ExternalIncomingCorrespondence.where(id: assigned_correspondence,
                                                                            application_module_id: app_module.id)
                                                                     .where.not(destination_id: dest_department_id)
    end
    return assigned_incoming
  end

  def all_manuscript
    @manuscripts = []
    user_department_role = Sate::Crmgt::UserDepartmentRole.find_by_user_id current_user.id
    user_department = Sate::Crmgt::DepartmentUserRole.find user_department_role.department_user_role_id
    @manuscripts = Sate::Crmgt::Manuscript.where(application_module_id: app_module.id,
                                                 destination_id: user_department.department_id)
  end

  def group_by_stage manuscripts
    manuscripts.group_by { |obj| obj[:current_stage] }.values
  end

  def return_stage_count(objs)
    stats = []
    objs.each { |obj| stats << { "stage" => obj[0]['current_stage'], "count" => obj.count } }
    stats
  end

  def manuscript_by_stage
    self.all_manuscript
    manuscripts = self.group_by_stage @manuscripts
    manuscripts_status = self.group_by_status @manuscripts
    manuscript_stages = self.return_stage_count manuscripts
    manuscript_status = self.return_status_count manuscripts_status
    total_count = []
    new_manuscript = manuscript_status.detect { |status| status["status"] == "Accepted" }
    archived = manuscript_status.detect { |status| status["status"] == "Archived" }
    rejected = manuscript_status.detect { |status| status["status"] == "Rejected" }
    pre_assessment = manuscript_stages.detect { |stage| stage["stage"] == "pre_assessment" }
    main_assessment = manuscript_stages.detect { |stage| stage["stage"] == "main_assessment" }
    incorporate_comments = manuscript_stages.detect { |stage| stage["stage"] == "incorporate_comments" }
    editorial = manuscript_stages.detect { |stage| stage["stage"] == "editorial" }
    typesetter = manuscript_stages.detect { |stage| stage["stage"] == "typesetter" }
    printing = manuscript_stages.detect { |stage| stage["stage"] == "printing" }
    total_count << { "manuscript" => @manuscripts.count }
    total_count << { "New" => new_manuscript == nil ? 0 : new_manuscript["count"] }
    total_count << { "stage_1" => pre_assessment == nil ? 0 : pre_assessment["count"]  }
    total_count << { "stage_2" => main_assessment == nil ? 0 : main_assessment["count"] }
    total_count << { "stage_3" => incorporate_comments == nil ? 0 : incorporate_comments["count"] }
    total_count << { "stage_4" => editorial == nil ? 0 : editorial["count"] }
    total_count << { "stage_5" => typesetter == nil ? 0 : typesetter["count"] }
    total_count << { "stage_6" => printing == nil ? 0 : printing["count"] }
    total_count << { "Archived" => archived == nil ? 0 : archived["count"] }
    total_count << { "Rejected" => rejected == nil ? 0 : rejected["count"] }

    response = Sate::Common::MethodResponse.new(true, nil, total_count, nil, nil )
    render json: response
  end

  def manuscript_by_month
    self.all_manuscript
    manuscripts = self.group_by_received_date @manuscripts
    manuscript_status = self.assign_status manuscripts

    manuscript_by_month = []

    5.downto(0).collect do |n|
      month_name = Date.parse(Date::MONTHNAMES[n.months.ago.month]).strftime('%b')
      manuscript_count = self.return_count manuscript_status, month_name

      manuscript_by_month << { "month" => month_name,
                               "manuscript" => manuscript_count }
    end
    response = Sate::Common::MethodResponse.new(true, nil, manuscript_by_month, nil, nil )
    render json: response
  end

  def get_department_manuscript_user_status
    user_department_role = Sate::Crmgt::UserDepartmentRole.find_by_user_id current_user.id
    user_department = Sate::Crmgt::DepartmentUserRole.find user_department_role.department_user_role_id
    department_user_role_ids = Sate::Crmgt::DepartmentUserRole.where(application_module_id: app_module.id,
                                                                     department_id: user_department.department_id)
                                                              .select("id").map(&:id)
    user_ids = Sate::Crmgt::UserDepartmentRole.where(application_module_id: app_module.id,
                                                     department_user_role_id: department_user_role_ids)
                                              .select("user_id").map(&:user_id)
    assigned_froms = Sate::Crmgt::ManuscriptAssignment.where(assignee_id: user_ids, assignee_type: 'Sate::Auth::User')
    assigned_froms = ActiveModelSerializers::SerializableResource.new(assigned_froms).as_json
    assigned_tos = Sate::Crmgt::ManuscriptAssignment.where(from_user_id: user_ids)
    assigned_tos = ActiveModelSerializers::SerializableResource.new(assigned_tos).as_json
    assigned_froms = assigned_froms.group_by { |from| from[:assignee][:id] }.values
    assigned_tos = assigned_tos.group_by { |to| to[:from_user][:id] }.values
    assignment = []
    assigned_froms.each do |user_routes|
      routes = user_routes.group_by { |route| route[:status] }.values
      by_status = []
      routes.each do |route|
        by_status << { "status" => route[0][:status],
                       "count" => route.count }
      end
      current = assigned_tos.detect { |to| to[0][:from_user] == routes[0][0][:assignee] }
      from_count = current == nil ? 0 : current.count
      assignment << { "user" => routes[0][0][:assignee],
                      "to_user" => by_status,
                      "from_user" => from_count }
    end
    response = Sate::Common::MethodResponse.new(true, nil, assignment, nil, nil )
    render json: response
  end
end
