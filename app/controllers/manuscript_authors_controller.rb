class ManuscriptAuthorsController < ApplicationController
  before_action :set_manuscript_author, only: [:update]

  def index
    manuscript = Sate::Crmgt::Manuscript.where(id: params[:manuscript_id])
    @manuscript_authors = Sate::Crmgt::ManuscriptAuthor.where(application_module_id: app_module.id,
                                                              manuscript: manuscript)
    total = @manuscript_authors.count
    @manuscript_authors_json = ActiveModelSerializers::SerializableResource.new(@manuscript_authors).as_json
    response = Sate::Common::MethodResponse.new(
      true, nil, @manuscript_authors_json, nil, total)

    render json: response
  end

  def get_author_manuscripts
    author = Sate::Crmgt::Author.where id: params[:author_id]
    manuscripts = Sate::Crmgt::ManuscriptAuthor.where(application_module_id: app_module.id,
                                                      author: author)
                                               .select("manuscript_id").map(&:manuscript_id)
    @manuscripts = Sate::Crmgt::Manuscript.where(id: manuscripts,
                                                 application_module_id: app_module.id)
    total = @manuscripts.count
    @manuscripts_json = ActiveModelSerializers::SerializableResource.new(@manuscripts).as_json
    response = Sate::Common::MethodResponse.new(
      true, nil, @manuscripts_json, nil, total)

    render json: response
  end

  def create
    @manuscript_author = Sate::Crmgt::ManuscriptAuthor.new(manuscript_author_params)
    @manuscript_author.application_module_id = app_module.id

    if @manuscript_author.save
      @manuscript_author_json  = ActiveModelSerializers::SerializableResource.new(@manuscript_author).as_json
      response = Sate::Common::MethodResponse.new(
        true, "Manuscript Author Saved Successfully!", @manuscript_author_json, nil, nil)
    else
      errors = Sate::Common::Util.error_messages @manuscript_author, 'Manuscript Author'
      response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
    end
    render json: response
  end

  def update
    if @manuscript_author.update(manuscript_author_params)
      @manuscript_author_json  = ActiveModelSerializers::SerializableResource.new(@manuscript_author).as_json
      response = Sate::Common::MethodResponse.new(
        true, "Manuscript Author Updated Successfully!", @manuscript_author_json, nil, nil)
    else
      errors = Sate::Common::Util.error_messages @manuscript_author, 'Manuscript Author'
      response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
    end
    render json: response
  end

  private
    def set_manuscript_author
      @manuscript_author = Sate::Crmgt::ManuscriptAuthor.find(params[:id])
    end

    def manuscript_author_params
      params.require(:manuscript_author).permit( :manuscript_id, :author_id)
    end
end
