class ExternalAssessorsController < ApplicationController
  before_action :set_external_assessor, only: [:update]

  def index
    @external_assessors = Sate::Crmgt::ExternalAssessor.where application_module_id: app_module.id
    total = @external_assessors.count
    @external_assessors_json = ActiveModelSerializers::SerializableResource.new(@external_assessors).as_json
    response = Sate::Common::MethodResponse.new(true, nil, @external_assessors_json, nil, total)
    render json: response
  end

  def create
    @external_assessor = Sate::Crmgt::ExternalAssessor.new(external_assessor_params)
    @external_assessor.application_module_id = app_module.id
    if @external_assessor.save
      @external_assessors_json = ActiveModelSerializers::SerializableResource.new(@external_assessor).as_json
      response = Sate::Common::MethodResponse.new(
        true, "Assessor Saved Successfully!", @external_assessors_json, nil, nil)
    else
      errors = Sate::Common::Util.error_messages @external_assessor, 'External Assessor'
      response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
    end
    render json: response
  end

  def update
    if @external_assessor.update(external_assessor_params)
      @external_assessors_json = ActiveModelSerializers::SerializableResource.new(@external_assessor).as_json
      response = Sate::Common::MethodResponse.new(
        true, "Assessor Updated Successfully!", @external_assessors_json, nil, nil)
    else
      errors = Sate::Common::Util.error_messages @external_assessor, 'External Assessor'
      response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
    end
    render json: response
  end

  private
    def set_external_assessor
      @external_assessor = Sate::Crmgt::ExternalAssessor.find(params[:id])
    end

    def external_assessor_params
      params.require(:external_assessor).permit(:title, :first_name, :father_name, :grand_father_name, :email, :telephone, :area_of_expertise, :institution, :bank_name, :bank_acc, :tin)
    end
end
