class ManuscriptCommentsController < ApplicationController
  before_action :set_manuscript_comment, only: [ :update ]

  def index
    manuscript = Sate::Crmgt::Manuscript.find params[:manuscript_id]
    @manuscript_comments = Sate::Crmgt::ManuscriptComment.where(application_module_id: app_module.id,
                                                                manuscript: manuscript)
    total = @manuscript_comments.count
    @manuscript_comments_json = ActiveModelSerializers::SerializableResource.new(@manuscript_comments).as_json
    response = Sate::Common::MethodResponse.new(
      true, nil, @manuscript_comments_json, nil, total)

    render json: response
  end

  def create
    @manuscript_comment = Sate::Crmgt::ManuscriptComment.new(manuscript_comment_params)
    @manuscript_comment.commented_by_id = current_user.id
    @manuscript_comment.application_module_id = app_module.id
    @manuscript_comment.comment_date = DateTime.now

    if @manuscript_comment.save
      @manuscript_comment_json = ActiveModelSerializers::SerializableResource.new(@manuscript_comment).as_json
      response = Sate::Common::MethodResponse.new(
        true, "Manuscript Comment Saved Successfully!", @manuscript_comment_json, nil, nil)
    else
      errors = Sate::Common::Util.error_messages @manuscript_comment, 'Manuscript Comment'
      response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
    end
    render json: response
  end

  def update
    if @manuscript_comment.update(manuscript_comment_params)
      @manuscript_comment_json = ActiveModelSerializers::SerializableResource.new(@manuscript_comment).as_json
      response = Sate::Common::MethodResponse.new(
        true, "Manuscript Comment Updated Successfully!", @manuscript_comment_json, nil, nil)
    else
      errors = Sate::Common::Util.error_messages @manuscript_comment, 'Manuscript Comment'
      response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
    end
    render json: response
  end

  private
    def set_manuscript_comment
      @manuscript_comment = Sate::Crmgt::ManuscriptComment.find(params[:id])
    end

    def manuscript_comment_params
      params.require(:manuscript_comment).permit(:manuscript_id, :content)
    end
end
