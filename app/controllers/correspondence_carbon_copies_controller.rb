class CorrespondenceCarbonCopiesController < ApplicationController
  before_action :set_correspondence_carbon_copy, only: [:update]

  def index
    if params[:direction] != "external"
      correspondence = Sate::Crmgt::InternalCorrespondence.find params[:correspondence_id]
    else
      if params[:request_type] == "incoming"
        correspondence = Sate::Crmgt::ExternalIncomingCorrespondence.find params[:correspondence_id]
      else
        correspondence = Sate::Crmgt::ExternalOutgoingCorrespondence.find params[:correspondence_id]
      end
    end
    @correspondence_carbon_copies = Sate::Crmgt::CorrespondenceCarbonCopy.where(application_module_id: app_module.id,
                                                                                correspondence: correspondence)
    total = @correspondence_carbon_copies.count
    @correspondence_carbon_copies_json = ActiveModelSerializers::SerializableResource.new(@correspondence_carbon_copies).as_json
    response = Sate::Common::MethodResponse.new(
      true, nil, @correspondence_carbon_copies_json, nil, total)

    render json: response
  end

  def create
    if params[:direction] != "external"
      correspondence_type = Sate::Crmgt::InternalCorrespondence
      correspondence = Sate::Crmgt::InternalCorrespondence
                         .where(id: correspondence_carbon_copy_params[:correspondence_id])
    else
      if params[:request_type] == "incoming"
        correspondence_type = Sate::Crmgt::ExternalIncomingCorrespondence
        correspondence = Sate::Crmgt::ExternalIncomingCorrespondence
                           .where(id: correspondence_carbon_copy_params[:correspondence_id])
      else
        correspondence_type = Sate::Crmgt::ExternalOutgoingCorrespondence
        correspondence = Sate::Crmgt::ExternalOutgoingCorrespondence
                           .where(id: correspondence_carbon_copy_params[:correspondence_id])
      end
    end
    @correspondence_carbon_copy = Sate::Crmgt::CorrespondenceCarbonCopy.new(correspondence_carbon_copy_params)
    @correspondence_carbon_copy.correspondence_type = correspondence_type
    @correspondence_carbon_copy.application_module_id = app_module.id

    if @correspondence_carbon_copy.save
      @correspondence_carbon_copy_json = ActiveModelSerializers::SerializableResource.new(@correspondence_carbon_copy).as_json
      response = Sate::Common::MethodResponse.new(
        true, "Correspondence Cc Saved Successfully!",
        @correspondence_carbon_copy_json, nil, nil)
    else
      errors = Sate::Common::Util.error_messages @correspondence_carbon_copy, 'Correspondence Cc'
      response = Sate::Common::MethodResponse.new(false, nil,
                                                  nil, errors, nil)
    end
    render json: response
  end

  def update
    if @correspondence_carbon_copy.update(correspondence_carbon_copy_params)
      @correspondence_carbon_copy_json = ActiveModelSerializers::SerializableResource.new(@correspondence_carbon_copy).as_json
      response = Sate::Common::MethodResponse.new(
        true, "Correspondence Cc Updated Successfully!",
        @correspondence_carbon_copy_json, nil, nil)
    else
      errors = Sate::Common::Util.error_messages @correspondence_carbon_copy, 'Correspondence Cc'
      response = Sate::Common::MethodResponse.new(false, nil,
                                                  nil, errors, nil)
    end
    render json: response
  end

  private
    def set_correspondence_carbon_copy
      @correspondence_carbon_copy = Sate::Crmgt::CorrespondenceCarbonCopy.find(params[:id])
    end

    def correspondence_carbon_copy_params
      params.require(:correspondence_carbon_copy).permit(:correspondence_id, :destination_id)
    end
end
