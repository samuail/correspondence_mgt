class UserDepartmentsController < ApplicationController
  before_action :set_user_department, only: [:update]

  def index
    @user_department = Sate::Crmgt::UserDepartment.find_by_user_id params[:user_id]
    @user_department_json = ActiveModelSerializers::SerializableResource.new(@user_department).as_json
    response = Sate::Common::MethodResponse.new(
        true, nil, @user_department_json, nil, 1)
    render json: response
  end

  def create
    @user_department = Sate::Crmgt::UserDepartment.new(user_department_params)
    @user_department.application_module_id = app_module.id
    if @user_department.save
      @user_department = Sate::Crmgt::UserDepartment.find_by application_module_id: app_module.id, id: @user_department.id
      @user_department_json = ActiveModelSerializers::SerializableResource.new(@user_department).as_json
      response = Sate::Common::MethodResponse.new(
          true, "Department and Role Assigned Successfully!", @user_department_json, nil, nil)
    else
      errors = Sate::Common::Util.error_messages @user_department, 'User Department Role'
      response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
    end
    render json: response
  end

  def update
    if @user_department.update(user_department_params)
      @user_department = Sate::Crmgt::UserDepartment.find_by application_module_id: app_module.id, id: @user_department.id
      @user_department_json = ActiveModelSerializers::SerializableResource.new(@user_department).as_json
      response = Sate::Common::MethodResponse.new(
          true, "Department and Role Updated Successfully!", @user_department_json, nil, nil)
    else
      errors = Sate::Common::Util.error_messages @user_department, 'User Department Role'
      response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
    end
    render json: response
  end

  private
    def set_user_department
      @user_department = Sate::Crmgt::UserDepartment.find(params[:id])
    end

    def user_department_params
      params.require(:user_department).permit(:user_id, :department_id, :user_role_id)
    end
end
