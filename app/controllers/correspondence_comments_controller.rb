class CorrespondenceCommentsController < ApplicationController
  before_action :set_correspondence_comment, only: [:update]

  def index
    if params[:request_type] != "incoming"
      correspondence = Sate::Crmgt::InternalCorrespondence.find params[:correspondence_id]
    else
      correspondence = Sate::Crmgt::ExternalIncomingCorrespondence.find params[:correspondence_id]
    end
    @correspondence_comments = Sate::Crmgt::CorrespondenceComment.where(application_module_id: app_module.id,
                                                                        correspondence: correspondence).order(:order)
    total = @correspondence_comments.count
    @correspondence_comments_json = ActiveModelSerializers::SerializableResource.new(@correspondence_comments).as_json
    response = Sate::Common::MethodResponse.new(
        true, nil, @correspondence_comments_json, nil, total)

    render json: response
  end

  def create
    if params[:request_type] != "incoming"
      correspondence_type = Sate::Crmgt::InternalCorrespondence
      correspondence = Sate::Crmgt::InternalCorrespondence.where(id: correspondence_comment_params[:correspondence_id])
    else
      correspondence_type = Sate::Crmgt::ExternalIncomingCorrespondence
      correspondence = Sate::Crmgt::ExternalIncomingCorrespondence.where(id: correspondence_comment_params[:correspondence_id])
    end
    @correspondence_comment = Sate::Crmgt::CorrespondenceComment.new(correspondence_comment_params)
    @correspondence_comment.commented_by_id = current_user.id
    @correspondence_comment.correspondence_type = correspondence_type
    @correspondence_comment.application_module_id = app_module.id
    @correspondence_comment.comment_date = DateTime.now
    existing_comment = Sate::Crmgt::CorrespondenceComment.where(correspondence: correspondence).order(order: :desc )
    if existing_comment.count > 0
      @correspondence_comment.order = existing_comment.first.order + 1
    else
      @correspondence_comment.order = 1
    end
    if @correspondence_comment.save
      @correspondence_comment_json = ActiveModelSerializers::SerializableResource.new(@correspondence_comment).as_json
      response = Sate::Common::MethodResponse.new(
          true, "Correspondence Comment Saved Successfully!", @correspondence_comment_json, nil, nil)
    else
      errors = Sate::Common::Util.error_messages @correspondence_comment, 'Correspondence Comment'
      response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
    end
    render json: response
  end

  def update
    if @correspondence_comment.update(correspondence_comment_params)
      @correspondence_comment_json = ActiveModelSerializers::SerializableResource.new(@correspondence_comment).as_json
      response = Sate::Common::MethodResponse.new(
          true, "Correspondence Comment Updated Successfully!", @correspondence_comment_json, nil, nil)
    else
      errors = Sate::Common::Util.error_messages @correspondence_comment, 'Correspondence Comment'
      response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
    end
    render json: response
  end

  private
    def set_correspondence_comment
      @correspondence_comment = Sate::Crmgt::CorrespondenceComment.find(params[:id])
    end

    def correspondence_comment_params
      params.require(:correspondence_comment).permit( :correspondence_id, :content)
    end
end
