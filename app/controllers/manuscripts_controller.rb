class ManuscriptsController < ApplicationController
  before_action :set_manuscript, only: [:update, :update_status, :archive, :upload, :upload_archive,
                                        :change_uploads, :get_documents, :get_archive_att ]

  def index
    filter = params[:filter]
    sort_order = params[:sortOrder]
    page_number = params[:pageNumber].to_i
    page_size = params[:pageSize].to_i
    user_department_role = Sate::Crmgt::UserDepartmentRole.find_by_user_id current_user.id
    user_department = Sate::Crmgt::DepartmentUserRole.find user_department_role.department_user_role_id
    @manuscripts = Sate::Crmgt::Manuscript.where(application_module_id: app_module.id,
                                                 destination_id: user_department.department_id)
                                          .where.not(status: 'Archived')
                                          .where.not(status: 'Rejected')
                                          .where("reference_no like ? or title like ? or array_to_string(search_keywords, '||') like ?",
                                                 "%#{filter}%", "%#{filter}%", "%#{filter}%")
    total = @manuscripts.count
    @manuscripts_json = ActiveModelSerializers::SerializableResource.new(@manuscripts).as_json
    @manuscripts_json.sort_by! { |a| [-a[:received_date].to_time.to_i ] }
    initialPos = page_number * page_size
    response = Sate::Common::MethodResponse.new(true, nil, @manuscripts_json.slice(initialPos, page_size), nil, total)
    render json: response
  end

  def get_assigned
    filter = params[:filter]
    sort_order = params[:sortOrder]
    page_number = params[:pageNumber].to_i
    page_size = params[:pageSize].to_i
    assigned = Sate::Crmgt::ManuscriptAssignment.where(application_module_id: app_module.id,
                                                       to_internal_user_id: current_user.id)
                                                .select("manuscript_id").map(&:manuscript_id)
    @manuscripts = Sate::Crmgt::Manuscript.where(id: assigned,
                                                 application_module_id: app_module.id)
                                          .where.not(status: 'Archived')
                                          .where.not(status: 'Rejected')
                                          .where("reference_no like ? or title like ? or array_to_string(search_keywords, '||') like ?", "%#{filter}%", "%#{filter}%", "%#{filter}%")
    total = @manuscripts.count
    @manuscripts_json = ActiveModelSerializers::SerializableResource.new(@manuscripts).as_json
    @manuscripts_json.sort_by! { |a| [-a[:received_date].to_time.to_i ] }
    initialPos = page_number * page_size
    response = Sate::Common::MethodResponse.new(true, nil, @manuscripts_json.slice(initialPos, page_size), nil, total)
    render json: response
  end

  def archived
    filter = params[:filter]
    sort_order = params[:sortOrder]
    page_number = params[:pageNumber].to_i
    page_size = params[:pageSize].to_i
    user_department_role = Sate::Crmgt::UserDepartmentRole.find_by_user_id current_user.id
    user_department = Sate::Crmgt::DepartmentUserRole.find user_department_role.department_user_role_id
    @manuscripts = Sate::Crmgt::Manuscript.where(application_module_id: app_module.id,
                                                 destination_id: user_department.department_id)
                                          .where(status: 'Archived')
                                          .where("reference_no like ? or title like ? or array_to_string(search_keywords, '||') like ?", "%#{filter}%", "%#{filter}%", "%#{filter}%")
    total = @manuscripts.count
    @manuscripts_json = ActiveModelSerializers::SerializableResource.new(@manuscripts).as_json
    @manuscripts_json.sort_by! { |a| [-a[:received_date].to_time.to_i ] }
    initialPos = page_number * page_size
    response = Sate::Common::MethodResponse.new(true, nil, @manuscripts_json.slice(initialPos, page_size), nil, total)
    render json: response
  end

  def get_next_no
    @manuscripts = Sate::Crmgt::Manuscript.where("reference_no like ?", "%#{Date.today.year}").order(reference_no: :desc)
    if @manuscripts.length == 0
      next_no = 1
    else
      reference_no = @manuscripts.first["reference_no"]
      next_no = reference_no[6..10].to_i + 1
    end
    response = Sate::Common::MethodResponse.new(true, nil, next_no, nil, nil)
    render json: response
  end

  def create
    @manuscript = Sate::Crmgt::Manuscript.new(manuscript_params)
    @manuscript.application_module_id = app_module.id
    user_department = Sate::Crmgt::UserDepartmentRole.find_by_user_id current_user.id
    destination = Sate::Crmgt::DepartmentUserRole.find user_department.department_user_role_id
    @manuscript.destination_id = destination.department_id
    @manuscript.received_date = DateTime.now
    @manuscript.search_keywords = params[:manuscript][:search_keywords]
    @manuscript.key_in_by_id = current_user.id
    if @manuscript.save
      @manuscript = Sate::Crmgt::Manuscript.find_by application_module_id: app_module.id, id: @manuscript.id
      @manuscript_json = ActiveModelSerializers::SerializableResource.new(@manuscript).as_json
      response = Sate::Common::MethodResponse.new(
        true, "Manuscript Saved Successfully!", @manuscript_json, nil, nil)
    else
      errors = Sate::Common::Util.error_messages @manuscript, 'Manuscript'
      response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
    end
    render json: response
  end

  def update
    @manuscript.search_keywords = params[:manuscript][:search_keywords]
    if @manuscript.update(manuscript_params)
      @manuscript = Sate::Crmgt::Manuscript.find_by application_module_id: app_module.id, id: @manuscript.id
      @manuscript_json = ActiveModelSerializers::SerializableResource.new(@manuscript).as_json
      response = Sate::Common::MethodResponse.new(
        true, "Manuscript Updated Successfully!", @manuscript_json, nil, nil)
    else
      errors = Sate::Common::Util.error_messages @manuscript, 'Manuscript'
      response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
    end
    render json: response
  end

  def update_status
    status = params[:status]
    if @manuscript.update(status: status)
      @manuscript = Sate::Crmgt::Manuscript.find_by application_module_id: app_module.id,
                                                    id: @manuscript.id
      @manuscript_json = ActiveModelSerializers::SerializableResource.new(@manuscript).as_json
      response = Sate::Common::MethodResponse.new(
        true, "Manuscript " + status + " Successfully!", @manuscript_json, nil, nil)
    end
    render json: response
  end

  def upload
    file = params[:file]
    if @manuscript.documents.attach(file)
      response = Sate::Common::MethodResponse.new(
        true, "Manuscript Uploaded Successfully!", @manuscript, nil, nil)
    else
      errors = Sate::Common::Util.error_messages @manuscript, 'Manuscript'
      response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
    end
    render json: response
  end

  def upload_archive
    file = params[:file]
    @manuscript_archive = Sate::Crmgt::ManuscriptArchive.find_by manuscript_id: @manuscript.id,
                                                                 application_module_id: app_module.id
    if @manuscript_archive.attachments.attach(file)
      response = Sate::Common::MethodResponse.new(
        true, "Manuscript Archive Uploaded Successfully!", @manuscript_archive, nil, nil)
    else
      errors = Sate::Common::Util.error_messages @manuscript_archive, 'Manuscript Archive'
      response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
    end
    render json: response
  end

  def change_uploads
    response = upload
    response = JSON(response)

    if response["success"]
      existing_image_id = params[:existing_image]
      existing_image = ActiveStorage::Blob.find(existing_image_id)
      existing_image.attachments.first.purge_later
    end
  end

  def get_documents
    documents = []
    if @manuscript.documents.attached?
      @manuscript.documents.each do |document|
        original = Rails.application.routes.url_helpers.rails_blob_path(document, only_path: true)
        img_var = document.preview(resize: "150x180")
        blob = document.blob
        variant = Rails.application.routes.url_helpers.rails_representation_url(img_var, only_path: true)
        documents << { id: document.blob.id, blob: blob, type: "documents", original: original, variant: variant }
      end
    end
    images = { documents: documents }
    render json: images
  end

  def get_archive_att
    archive_atts = []
    @manuscript_archive = Sate::Crmgt::ManuscriptArchive.find_by manuscript_id: @manuscript.id,
                                                                 application_module_id: app_module.id
    if @manuscript_archive.attachments.attached?
      @manuscript_archive.attachments.each do |archive|
        original = Rails.application.routes.url_helpers.rails_blob_path(archive, only_path: true)
        img_var = archive.previewable? ? archive.preview(resize: "150x180") : archive.variant(resize: "150x180")
        blob = archive.blob
        variant = Rails.application.routes.url_helpers.rails_representation_url(img_var, only_path: true)
        archive_atts << { id: archive.blob.id, blob: blob, type: "archives", original: original, variant: variant }
      end
    end
    archives = { documents: archive_atts }
    render json: archives
  end

  def archive
    status = params[:status]
    @manuscript_archive = Sate::Crmgt::ManuscriptArchive.new(manuscript: @manuscript, remark: params[:remark],
                                                             no_pages: params[:no_pages], no_copies: params[:no_copies],
                                                             has_archive_att: params[:has_archive_att])
    @manuscript_archive.archived_date = DateTime.now
    @manuscript_archive.archived_by_id = current_user.id
    @manuscript_archive.application_module_id = app_module.id
    if @manuscript_archive.save
      @manuscript.update(status: status)
      response = Sate::Common::MethodResponse.new(
        true, "Manuscript Archived Successfully!", nil, nil, nil)
    else
      errors = Sate::Common::Util.error_messages @manuscript_archive, 'Manuscript Archive'
      response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
    end
    render json: response
  end

  private
    def set_manuscript
      @manuscript = Sate::Crmgt::Manuscript.find(params[:id])
    end

    def manuscript_params
      params.require(:manuscript).permit(:reference_no, :title, :search_keywords)
    end
end
