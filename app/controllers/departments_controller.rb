class DepartmentsController < ApplicationController
  before_action :set_department, only: [:update,
                                        :assign_roles, :get_assigned_roles,
                                        :assign_heads, :get_heads]

  def index
    @departments = Sate::Crmgt::Department.where application_module_id: app_module.id
    total = @departments.count
    @departments_json = ActiveModelSerializers::SerializableResource.new(@departments).as_json
    response = Sate::Common::MethodResponse.new(true, nil, @departments_json, nil, total)
    render json: response
  end

  def get_department_by_type
    department_type = params[:department_type]
    user_department = Sate::Crmgt::UserDepartment.find_by_user_id current_user.id
    if department_type != "Outgoing Internal"
      ids = department_type == "Center" ? Sate::Crmgt::Department.center : Sate::Crmgt::Department.college
      @departments = Sate::Crmgt::Department.where(application_module_id: app_module.id, id: ids)
                                            .where.not(id: user_department.department_id)
                                            .order(:description)
    else
      @departments = Sate::Crmgt::Department.where(application_module_id: app_module.id)
                                            .where.not(id: user_department.department_id)
                                            .where.not(description: "Main Campus")
                                            .order(:description)
    end
    total = @departments.count
    @departments_json = ActiveModelSerializers::SerializableResource.new(@departments).as_json
    response = Sate::Common::MethodResponse.new(true, nil, @departments_json, nil, total)
    render json: response
  end

  def fetch_departments
    count = 0
    if @departments.nil?
      department_list = Sate::Crmgt::Department.where(:parent => nil, application_module_id: app_module.id)
      count = department_list.length
      @departments = []
      department_list.each do |department|
        @departments << department.to_node
      end
    end
    response = Sate::Common::MethodResponse.new(
        true, nil, @departments, nil, count)
    render json: response
  end

  def create
    @department = Sate::Crmgt::Department.new(department_params)
    @department.application_module_id = app_module.id
    if @department.save
      @department = Sate::Crmgt::Department.find_by application_module_id: app_module.id, id: @department.id
      @department_json = ActiveModelSerializers::SerializableResource.new(@department).as_json
      response = Sate::Common::MethodResponse.new(
          true, "Department Saved Successfully!", @department_json, nil, nil)
    else
      errors = Sate::Common::Util.error_messages @department, 'Department'
      response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
    end
    render json: response
  end

  def update
    if @department.update(department_params)
      @department = Sate::Crmgt::Department.find_by application_module_id: app_module.id, id: @department.id
      @department_json = ActiveModelSerializers::SerializableResource.new(@department).as_json
      response = Sate::Common::MethodResponse.new(
          true, 'Department Updated Successfully!', @department_json, nil, 0)
    else
      errors = Sate::Common::Util.error_messages @department,
                                                 'Department'
      response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
    end
    render json: response
  end

  def assign_roles
    roles = Sate::Auth::UserRole.find params[:role_ids]

    for role in roles
      @department.department_user_roles.create(user_role: role, application_module_id: app_module.id)
    end
    response = Sate::Common::MethodResponse.new(
      true, 'User Role was successfully assigned to the Department.',
      nil, nil, 0)
    render json: response
  end

  def get_assigned_roles
    @user_roles = @department.user_roles.where(:application_module_id => app_module.id)
    total = @user_roles.count
    response = Sate::Common::MethodResponse.new(true, nil,
      @user_roles, nil, total)
    render json: response
  end

  def assign_heads
    roles = Sate::Auth::UserRole.find params[:role_ids]
    @department_user_role = @department.department_user_roles
                                      .where(application_module_id: app_module.id, user_role: roles)
    @department_user_role.first.is_head = true
    @department_user_role.first.save
    response = Sate::Common::MethodResponse.new(true, "Department Head was Successfully Saved.",
                                                nil, nil, 1)
    render json: response
  end

  def get_heads
    @department_user_role = @department.department_user_roles
                                       .where(application_module_id: app_module.id,
                                              is_head: true)
    total = @department_user_role.count
    response = Sate::Common::MethodResponse.new(true, nil,
                                                @department_user_role, nil, total)
    render json: response
  end

  def get_users
    user_ids = []
    user_departments = Sate::Crmgt::UserDepartment.where department_id: params[:dept_id]
    user_departments.each do |user_department|
      if user_department.user_id != current_user.id
        user_ids << user_department.user_id
      end
    end
    @users = Sate::Auth::User.find user_ids
    total = @users.count
    response = Sate::Common::MethodResponse.new(true, nil, @users, nil, total)
    render json: response
  end

  def get_users_with_the_role
    @users = []
    department_id = params[:department_id]
    role_id = params[:role_id]
    department_role = Sate::Crmgt::DepartmentUserRole.find_by department_id: department_id, user_role_id: role_id,
                                                              application_module_id: app_module.id
    user_departments = Sate::Crmgt::UserDepartmentRole.where department_user_role_id: department_role.id
    role = Sate::Auth::UserRole.find role_id
    role_users = role.users
    user_departments.each do |user_department|
      if user_department.user_id != current_user.id
        @users << role_users.detect { |role_user| role_user[:id] == user_department.user_id }
      end
    end
    total = @users.count
    response = Sate::Common::MethodResponse.new(true, nil, @users, nil, total)
    render json: response
  end

  private
    def set_department
      @department = Sate::Crmgt::Department.find(params[:id])
    end

    def department_params
      params.require(:department).permit(:code, :description, :department_type_id, :parent_id)
    end
end
