require 'sate/common/methodresponse'
require 'sate/common/util'
class ApplicationController < ActionController::API
  include ExceptionHandler
  include ApplicationHelper

  before_action :authenticate_request
  attr_reader :current_user

  private

  def authenticate_request
    @current_user = AuthorizeApiRequest.call(request.headers).result
  end
end
