class AuthorsController < ApplicationController
  before_action :set_author, only: [:update, :add_contact]

  def index
    @authors = Sate::Crmgt::Author.where application_module_id: app_module.id
    total = @authors.count
    @authors_json = ActiveModelSerializers::SerializableResource.new(@authors).as_json
    response = Sate::Common::MethodResponse.new(true, nil, @authors_json, nil, total)
    render json: response
  end

  def create
    @author = Sate::Crmgt::Author.new(author_params)
    @author.application_module_id = app_module.id
    if @author.save
      @authors_json = ActiveModelSerializers::SerializableResource.new(@author).as_json
      response = Sate::Common::MethodResponse.new(
        true, "Author Saved Successfully!", @authors_json, nil, nil)
    else
      errors = Sate::Common::Util.error_messages @author, 'Author'
      response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
    end
    render json: response
  end

  def add_contact
    @author_contact = Sate::Crmgt::AuthorContact.new(author_contact_params)
    @author_contact.author = @author
    @author_contact.application_module_id = app_module.id
    if @author_contact.save
      @author_contacts_json = ActiveModelSerializers::SerializableResource.new(@author_contact).as_json
      response = Sate::Common::MethodResponse.new(
        true, "Author Contact Saved Successfully!", @author_contacts_json, nil, nil)
    else
      errors = Sate::Common::Util.error_messages @author, 'Author Contact'
      response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
    end
    render json: response
  end

  def edit_contact
    @author_contact = Sate::Crmgt::AuthorContact.find(params[:id])
    if @author_contact.update(author_contact_params)
      @author_contacts_json = ActiveModelSerializers::SerializableResource.new(@author_contact).as_json
      response = Sate::Common::MethodResponse.new(
        true, "Author Contact Updated Successfully!", @author_contacts_json, nil, nil)
    else
      errors = Sate::Common::Util.error_messages @author, 'Author Contact'
      response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
    end
    render json: response
  end

  def update
    if @author.update(author_params)
      @authors_json = ActiveModelSerializers::SerializableResource.new(@author).as_json
      response = Sate::Common::MethodResponse.new(
        true, "Author Updated Successfully!", @authors_json, nil, nil)
    else
      errors = Sate::Common::Util.error_messages @author, 'Author'
      response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
    end
    render json: response
  end

  private
    def set_author
      @author = Sate::Crmgt::Author.find(params[:id])
    end

    def author_params
      params.require(:author).permit(:first_name, :father_name, :grand_father_name, :email, :telephone, :bank_name, :bank_acc, :tin, :has_contact)
    end

    def author_contact_params
      params.require(:author_contact).permit(:first_name, :father_name, :email, :telephone)
    end
end
