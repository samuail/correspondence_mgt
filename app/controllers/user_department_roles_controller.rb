class UserDepartmentRolesController < ApplicationController
  before_action :set_user_department_role, only: [:update]

  def index
    @user_department_roles = Sate::Crmgt::UserDepartmentRole.find_by_user_id params[:user_id]
    @user_department_roles_json = ActiveModelSerializers::SerializableResource.new(@user_department_roles).as_json
    response = Sate::Common::MethodResponse.new(
      true, nil, @user_department_roles_json, nil, 1)
    render json: response
  end

  def create
    @user_department_role = Sate::Crmgt::UserDepartmentRole.new(user_department_role_params)
    department_id = params[:user_department_role][:department_id]
    role_id = params[:user_department_role][:user_role_id]
    department = Sate::Crmgt::DepartmentUserRole.where department_id: department_id,
                                                       user_role_id: role_id,
                                                       application_module_id: app_module.id
    @user_department_role.department_user_role_id = department.first.id
    @user_department_role.application_module_id = app_module.id
    if @user_department_role.save
      user = Sate::Auth::User.find params[:user_department_role][:user_id]
      user_roles = user.roles
      user.roles.delete(user_roles)
      new_role = Sate::Auth::UserRole.find role_id
      user.roles << new_role
      @user_department_role = Sate::Crmgt::UserDepartmentRole.find_by application_module_id: app_module.id, id: @user_department_role.id
      @user_department_role_json = ActiveModelSerializers::SerializableResource.new(@user_department_role).as_json
      response = Sate::Common::MethodResponse.new(
        true, "Department and Role Assigned Successfully!", @user_department_role_json, nil, nil)
    else
      errors = Sate::Common::Util.error_messages @user_department_role, 'User Department Role'
      response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
    end
    render json: response
  end

  def update
    department_id = params[:user_department_role][:department_id]
    role_id = params[:user_department_role][:user_role_id]
    department = Sate::Crmgt::DepartmentUserRole.where department_id: department_id, user_role_id: role_id, application_module_id: app_module.id
    @user_department_role.department_user_role_id = department.first.id
    if @user_department_role.save
      user = Sate::Auth::User.find params[:user_department_role][:user_id]
      user_roles = user.roles
      user.roles.delete(user_roles)
      new_role = Sate::Auth::UserRole.find role_id
      user.roles << new_role
      @user_department_role = Sate::Crmgt::UserDepartmentRole.find_by application_module_id: app_module.id, id: @user_department_role.id
      @user_department_role_json = ActiveModelSerializers::SerializableResource.new(@user_department_role).as_json
      response = Sate::Common::MethodResponse.new(
        true, "Department and Role Updated Successfully!", @user_department_role_json, nil, nil)
    else
      errors = Sate::Common::Util.error_messages @user_department_role, 'User Department Role'
      response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
    end
    render json: response
  end

  def change_password
    @user = Sate::Auth::User.find params[:id]
    new_password = params[:password]
    @user.password = new_password
    if @user.save
      response = Sate::Common::MethodResponse.new(
        true, "Password Changed Successfully!", nil, nil, nil)
    else
      errors = Sate::Common::Util.error_messages @user, 'User'
      response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
    end
    render json: response
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_department_role
      @user_department_role = Sate::Crmgt::UserDepartmentRole.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def user_department_role_params
      params.require(:user_department_role).permit(:user_id, :department_user_role_id)
    end
end
