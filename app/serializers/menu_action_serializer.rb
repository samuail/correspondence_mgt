class MenuActionSerializer < ActiveModel::Serializer
  attributes :id
  has_one :menu
  has_one :action
  has_one :application_module
end
