class CorrespondenceCommentSerializer < ActiveModel::Serializer
  attributes :id, :content, :order, :comment_date
  has_one :commented_by
  has_one :correspondence
  has_one :application_module
end
