class ManuscriptSerializer < ActiveModel::Serializer
  attributes :id, :reference_no, :title, :search_keywords,
             :received_date, :status, :current_stage
  has_one :destination
  has_one :key_in_by
  has_one :manuscript_archive
  has_one :application_module
  has_many :authors

  def current_stage
    Sate::Crmgt::Manuscript.current_stages[object.current_stage]
  end
end
