class ExternalIncomingCorrespondenceSerializer < ActiveModel::Serializer
  attributes :id, :reference_no, :letter_date, :subject, :search_keywords,
             :received_date, :status, :has_cc, :types,
             :doc_reference_no, :amount, :remark, :no_documents, :archived_date
  has_one :source
  has_one :destination
  has_one :key_in_by
  has_many :correspondence_ccs
  has_one :application_module

  def correspondence_ccs
    correspondence_ccs = []
    self.object.correspondence_carbon_copies.each do |cc|
      correspondence_ccs.push({id: cc.id, correspondence: cc.correspondence, destination: cc.destination })
    end
    correspondence_ccs
  end

  def types
    ""
  end
end
