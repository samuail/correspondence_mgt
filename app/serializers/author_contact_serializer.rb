class AuthorContactSerializer < ActiveModel::Serializer
  attributes :id, :first_name, :father_name, :email, :telephone
end
