class CorrespondenceCarbonCopySerializer < ActiveModel::Serializer
  attributes :id
  has_one :correspondence
  has_one :destination
  has_one :application_module
end
