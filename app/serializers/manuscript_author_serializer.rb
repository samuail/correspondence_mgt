class ManuscriptAuthorSerializer < ActiveModel::Serializer
  attributes :id
  has_one :manuscript
  has_one :author
end
