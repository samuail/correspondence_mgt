class CorrespondenceAssignmentSerializer < ActiveModel::Serializer
  attributes :id, :assigned_date, :received_date, :status, :order, :rejection_remark, :correspondence_type
  has_one :from_user
  has_one :to_user
  has_one :correspondence
  has_one :application_module
end
