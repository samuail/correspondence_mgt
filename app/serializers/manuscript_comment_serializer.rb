class ManuscriptCommentSerializer < ActiveModel::Serializer
  attributes :id, :content, :comment_date
  has_one :commented_by
  has_one :manuscript
  has_one :application_module
end
