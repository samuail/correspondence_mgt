class UserMenuActionSerializer < ActiveModel::Serializer
  attributes :id, :user_id, :menu_action_id, :active
end
