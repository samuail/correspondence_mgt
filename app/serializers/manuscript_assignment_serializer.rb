class ManuscriptAssignmentSerializer < ActiveModel::Serializer
  attributes :id, :stage, :assignee_type, :assigned_date, :received_date, :status
  has_one :from_user
  has_one :assignee
  has_one :manuscript
  has_one :application_module

  def stage
    Sate::Crmgt::ManuscriptAssignment.stages[object.stage]
  end
end
