class DepartmentTypeSerializer < ActiveModel::Serializer
  attributes :id, :code, :name
end
