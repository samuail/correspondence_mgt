class DepartmentUserRoleSerializer < ActiveModel::Serializer
  attributes :id, :is_head, :department_id, :user_role_id

  attribute :department_name do
    self.object.department.description
  end

  attribute :department_code do
    self.object.department.code
  end

  attribute :user_role_name do
    self.object.user_role.name
  end
end
