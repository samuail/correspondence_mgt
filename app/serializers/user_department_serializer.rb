class UserDepartmentSerializer < ActiveModel::Serializer
  attributes :id
  has_one :user
  has_one :department
  has_one :user_role
  has_one :application_module
end
