class ManuscriptArchiveSerializer < ActiveModel::Serializer
  attributes :id, :no_copies, :no_pages, :remark, :has_archive_att,
             :archived_date, :archived_by, :already_attached

  def archived_by
    Sate::Auth::User.find_by id: object.archived_by_id
  end

  def already_attached
    object.attachments.attached?
  end
end
