class RoyaltySerializer < ActiveModel::Serializer
  attributes :id, :reference_no, :amount, :remark, :payment_date
  has_one :manuscript
  has_one :author
  has_one :key_in_by
  has_one :application_module
end
