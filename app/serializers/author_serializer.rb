class AuthorSerializer < ActiveModel::Serializer
  attributes :id, :first_name, :father_name, :grand_father_name, :email,
             :telephone, :bank_name, :bank_acc, :tin, :full_name, :has_contact
  has_one :application_module
  has_one :author_contact

  def full_name
    object.first_name + " " + object.father_name + " " + object.grand_father_name
  end
end
