class ExternalAssessorSerializer < ActiveModel::Serializer
  attributes :id, :title, :first_name, :father_name, :grand_father_name, :email,
             :telephone, :bank_name, :bank_acc, :tin, :full_name, :area_of_expertise, :institution
  has_one :application_module

  def full_name
    object.first_name + " " + object.father_name + " " + object.grand_father_name
  end
end
