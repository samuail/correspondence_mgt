class DepartmentSerializer < ActiveModel::Serializer
  attributes :id, :code, :description
  has_one :department_type
  has_one :parent
  has_many :children
  has_one :application_module
end
