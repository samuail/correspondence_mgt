class OrganizationTypeSerializer < ActiveModel::Serializer
  attributes :id, :code, :name
end
