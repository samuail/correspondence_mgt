class UserDepartmentRoleSerializer < ActiveModel::Serializer
  attributes :id
  has_one :user
  has_one :department_user_role
  has_one :application_module
end
