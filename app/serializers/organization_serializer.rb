class OrganizationSerializer < ActiveModel::Serializer
  attributes :id, :code, :description
  has_one :organization_type
  has_one :application_module
end
