class Sate::Crmgt::Author < ApplicationRecord
  belongs_to :application_module, class_name: 'Sate::Auth::ApplicationModule'
  has_one :author_contact, :class_name => 'Sate::Crmgt::AuthorContact'

  validates :first_name, :father_name, :grand_father_name,
            presence: true,
            uniqueness: {
              scope: [:first_name, :father_name, :grand_father_name, :application_module_id]
            }

  validates :telephone, presence: true

  VALID_EMAIL_REGEX = /\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
  validates :email, presence: true,
            format: { with: VALID_EMAIL_REGEX }

  before_save { email.downcase }
end
