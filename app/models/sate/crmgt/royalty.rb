class Sate::Crmgt::Royalty < ApplicationRecord
  belongs_to :manuscript, class_name: 'Sate::Crmgt::Manuscript'
  belongs_to :author, class_name: 'Sate::Crmgt::Author'
  belongs_to :key_in_by, class_name: 'Sate::Auth::User'
  belongs_to :application_module, class_name: 'Sate::Auth::ApplicationModule'

  validates :manuscript_id, :author_id, :reference_no, :amount, :application_module_id,
            presence: true,
            uniqueness: {
              scope: [:manuscript_id, :author_id,
                      :reference_no, :amount, :remark,
                      :payment_date, :application_module_id]
            }
end
