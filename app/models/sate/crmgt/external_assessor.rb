class Sate::Crmgt::ExternalAssessor < ApplicationRecord
  belongs_to :application_module, class_name: 'Sate::Auth::ApplicationModule'

  validates :first_name, :father_name, :grand_father_name,
            :area_of_expertise, :tin, :bank_acc, :bank_name,
            presence: true,
            uniqueness: {
              scope: [:first_name, :father_name, :grand_father_name,
                      :email, :telephone, :application_module_id]
            }

  validates :telephone, presence: true

  VALID_EMAIL_REGEX = /\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
  validates :email, presence: true,
            format: { with: VALID_EMAIL_REGEX }

  before_save { email.downcase }
end
