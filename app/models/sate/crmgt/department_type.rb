class Sate::Crmgt::DepartmentType < Sate::Crmgt::Lookup
  has_many :departments, :class_name => 'Sate::Crmgt::Department'
end
