class Sate::Crmgt::UserDepartmentRole < ApplicationRecord
  belongs_to :user, class_name: 'Sate::Auth::User'
  belongs_to :department_user_role, class_name: 'Sate::Crmgt::DepartmentUserRole'
  belongs_to :application_module, class_name: 'Sate::Auth::ApplicationModule'

  validates :user_id, :department_user_role_id, :application_module_id,
            presence: true,
            uniqueness: {
              scope: [:user_id, :department_user_role_id, :application_module_id]
            }
end