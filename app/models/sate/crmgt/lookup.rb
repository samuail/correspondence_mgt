class Sate::Crmgt::Lookup < ApplicationRecord
  validates :code, :name, :type, presence: true,
            uniqueness: { scope: [:code, :name, :type, :application_module_id] }

  belongs_to :application_module, class_name: 'Sate::Auth::ApplicationModule'
end
