class Sate::Crmgt::CorrespondenceAssignment < ApplicationRecord
  belongs_to :from_user, class_name: 'Sate::Auth::User'
  belongs_to :to_user, class_name: 'Sate::Auth::User'
  belongs_to :correspondence, polymorphic: true
  belongs_to :application_module, class_name: 'Sate::Auth::ApplicationModule'

  validates :from_user_id, :to_user_id, :correspondence_id, :correspondence_type, :assigned_date, :order, :status,
            :received_date, :application_module_id,
            presence: true,
            uniqueness: {
                scope: [:from_user_id, :to_user_id, :order, :correspondence_id,
                        :correspondence_type, :application_module_id]
            }
end