class Sate::Crmgt::MenuAction < ApplicationRecord
  belongs_to :menu, class_name: 'Sate::Auth::Menu'
  belongs_to :action, class_name: 'Sate::Crmgt::Action'
  belongs_to :application_module, class_name: 'Sate::Auth::ApplicationModule'

  validates :menu_id, :action_id,
            presence: true,
            uniqueness: {
                scope: [:menu_id, :action_id, :application_module_id]
            }
end
