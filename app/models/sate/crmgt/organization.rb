class Sate::Crmgt::Organization < ApplicationRecord
  belongs_to :organization_type, class_name: 'Sate::Crmgt::OrganizationType'
  belongs_to :application_module, class_name: 'Sate::Auth::ApplicationModule'

  validates :code, :description,
            presence: true,
            uniqueness: {
                scope: [:code, :description, :organization_type_id, :application_module_id]
            }
end
