class Sate::Crmgt::AuthorContact < ApplicationRecord
  belongs_to :author, class_name: 'Sate::Crmgt::Author'
  belongs_to :application_module, class_name: 'Sate::Auth::ApplicationModule'

  validates :author_id, :first_name, :father_name, :telephone, :application_module_id,
            presence: true,
            uniqueness: {
              scope: [:author_id, :first_name, :father_name,
                      :email, :telephone, :application_module_id]
            }

  VALID_EMAIL_REGEX = /\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
  validates :email, presence: true,
            format: { with: VALID_EMAIL_REGEX }

  before_save { email.downcase }
end
