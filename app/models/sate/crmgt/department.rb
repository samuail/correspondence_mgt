class Sate::Crmgt::Department < ApplicationRecord
  belongs_to :department_type, class_name: 'Sate::Crmgt::DepartmentType'
  belongs_to :parent, class_name: 'Sate::Crmgt::Department', optional: true
  has_many :children, class_name: 'Sate::Crmgt::Department', foreign_key: 'parent_id'
  belongs_to :application_module, class_name: 'Sate::Auth::ApplicationModule'

  has_many :department_user_roles, class_name: 'Sate::Crmgt::DepartmentUserRole'
  has_many :user_roles, through: :department_user_roles

  validates :code, :description,
            presence: true,
            uniqueness: {
                scope: [:code, :description, :department_type_id, :application_module_id]
            }

  def to_node
    if self.parent
      parent_id = self.parent.id
    else
      parent_id = 0
    end
    {
        "id" => self.id,
        "code" => self.code,
        "description" => self.description,
        "department_type_id" => self.department_type.id,
        "parent_id" => parent_id,
        "children"   => self.children.map { |c| c.to_node }
    }
  end

  def descendants
    self.children | self.children.map(&:descendants).flatten
  end

  def self.center
    dept = Sate::Crmgt::Department.find_by(description: "Main Campus")
    ids = []
    dept.descendants.each do |descendant|
      ids << descendant.id
    end
    ids
  end

  def self.college
    depts = Sate::Crmgt::Department.where.not(description: "Main Campus")
    center_ids = Sate::Crmgt::Department.center
    ids = []
    depts.each do |dept|
      if !(center_ids.include? dept.id)
        ids << dept.id
      end
    end
    ids
  end
end
