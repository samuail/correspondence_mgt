class Sate::Crmgt::Manuscript < ApplicationRecord
  belongs_to :destination, class_name: 'Sate::Crmgt::Department'
  belongs_to :key_in_by, class_name: 'Sate::Auth::User'
  belongs_to :application_module, class_name: 'Sate::Auth::ApplicationModule'

  has_many :manuscript_authors, class_name: 'Sate::Crmgt::ManuscriptAuthor'
  has_many :authors, through: :manuscript_authors

  has_one :manuscript_archive, class_name: 'Sate::Crmgt::ManuscriptArchive'

  has_many_attached :documents

  enum current_stage: { pre_assessment: "Preliminary Assessment", main_assessment: "Main Assessment",
                incorporate_comments: "Incorporate Comments", editorial: "Editorial",
                typesetter: "Typesetter", printing: "Printing" }

  validates :reference_no, uniqueness: true
  validates :title, uniqueness: true
  validates :reference_no, :title, :destination_id,
            :received_date, :key_in_by_id, :application_module_id,
            presence: true,
            uniqueness: {
              scope: [:reference_no, :title, :destination_id,
                      :application_module_id]
            }
  validates :documents, content_type: { in: [:jpg, :jpeg, :png, :pdf], message: 'is not a PDF' }
  validates :documents, size: { less_than: 5.megabytes , message: 'is not given between size' }

end
