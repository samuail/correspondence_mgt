class Sate::Crmgt::UserMenuAction < ApplicationRecord
  belongs_to :user, class_name: 'Sate::Auth::User'
  belongs_to :menu_action, class_name: 'Sate::Crmgt::MenuAction'
  belongs_to :application_module, class_name: 'Sate::Auth::ApplicationModule'

  validates :user_id, :menu_action_id,
            presence: true,
            uniqueness: {
                scope: [:user_id, :menu_action_id, :application_module_id]
            }
end
