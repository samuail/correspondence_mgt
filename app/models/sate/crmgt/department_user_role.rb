class Sate::Crmgt::DepartmentUserRole < ApplicationRecord
  belongs_to :department, class_name: 'Sate::Crmgt::Department'
  belongs_to :user_role, class_name: 'Sate::Auth::UserRole'
  belongs_to :application_module, class_name: 'Sate::Auth::ApplicationModule'

  validates :department_id, :user_role_id, :application_module_id,
            presence: true,
            uniqueness: {
              scope: [:department_id, :user_role_id, :application_module_id]
            }
end
