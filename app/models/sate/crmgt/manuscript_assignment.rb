class Sate::Crmgt::ManuscriptAssignment < ApplicationRecord
  belongs_to :from_user, class_name: 'Sate::Auth::User'
  belongs_to :manuscript, class_name: 'Sate::Crmgt::Manuscript'
  belongs_to :application_module, class_name: 'Sate::Auth::ApplicationModule'
  belongs_to :assignee, polymorphic: true

  enum stage: { pre_assessment: "Preliminary Assessment", main_assessment: "Main Assessment",
                incorporate_comments: "Incorporate Comments", editorial: "Editorial",
                typesetter: "Typesetter", printing: "Printing" }

  validates :stage, :from_user_id, :manuscript_id, :assigned_date,
            :received_date, :application_module_id,
            presence: true,
            uniqueness: {
              scope: [:stage, :from_user_id, :assignee,
                      :manuscript_id, :application_module_id]
            }
end
