class Sate::Crmgt::CorrespondenceCarbonCopy < ApplicationRecord
  belongs_to :correspondence, polymorphic: true
  belongs_to :destination, class_name: 'Sate::Crmgt::Department'
  belongs_to :application_module, class_name: 'Sate::Auth::ApplicationModule'

  validates :correspondence_id, :correspondence_type, :destination_id,
            :application_module_id,
            presence: true,
            uniqueness: {
              scope: [:correspondence_id, :correspondence_type,
                      :destination_id, :application_module_id]
            }
end
