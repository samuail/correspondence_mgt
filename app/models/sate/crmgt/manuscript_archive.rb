class Sate::Crmgt::ManuscriptArchive < ApplicationRecord
  belongs_to :manuscript, class_name: 'Sate::Crmgt::Manuscript'
  belongs_to :archived_by, class_name: 'Sate::Auth::User'
  belongs_to :application_module, class_name: 'Sate::Auth::ApplicationModule'

  has_many_attached :attachments

  validates :manuscript_id, :no_copies,
            :archived_by_id, :application_module_id,
            presence: true,
            uniqueness: {
              scope: [:manuscript_id, :application_module_id]
            }

  validates :attachments, content_type: { in: [:jpg, :jpeg, :png, :pdf], message: 'is not an Image and/or a PDF' }
  validates :attachments, size: { less_than: 5.megabytes , message: 'is not given between size' }
end
