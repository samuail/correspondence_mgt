class Sate::Crmgt::ManuscriptComment < ApplicationRecord
  belongs_to :commented_by, class_name: 'Sate::Auth::User'
  belongs_to :manuscript, class_name: 'Sate::Crmgt::Manuscript'
  belongs_to :application_module, class_name: 'Sate::Auth::ApplicationModule'

  validates :commented_by_id, :manuscript_id,
            :comment_date, :application_module_id,
            presence: true,
            uniqueness: {
              scope: [:commented_by_id, :manuscript_id, :comment_date,
                      :content, :application_module_id]
            }
end
