class Sate::Crmgt::CorrespondenceComment < ApplicationRecord
  belongs_to :commented_by, class_name: 'Sate::Auth::User'
  belongs_to :correspondence, polymorphic: true
  belongs_to :application_module, class_name: 'Sate::Auth::ApplicationModule'

  validates :commented_by_id, :correspondence_id, :correspondence_type, :content, :order,
            :comment_date, :application_module_id,
            presence: true,
            uniqueness: {
                scope: [:commented_by_id, :correspondence_id, :correspondence_type,
                        :content, :application_module_id]
            }
end
