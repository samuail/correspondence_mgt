class Sate::Crmgt::ExternalOutgoingCorrespondence < ApplicationRecord
  belongs_to :source, class_name: 'Sate::Crmgt::Department'
  belongs_to :destination, class_name: 'Sate::Crmgt::Organization'
  belongs_to :key_in_by, class_name: 'Sate::Auth::User'
  belongs_to :application_module, class_name: 'Sate::Auth::ApplicationModule'

  has_many :correspondence_carbon_copies, as: :correspondence

  has_many_attached :main_images
  has_many_attached :attachment_images

  validates :reference_no, uniqueness: true
  validates :reference_no, :letter_date, :subject, :source_id, :destination_id,
            :received_date, :application_module_id,
            presence: true,
            uniqueness: {
                scope: [:reference_no, :letter_date, :subject, :source_id, :destination_id,
                        :application_module_id]
            }

  validate :acceptable_main_image
  validate :acceptable_attachment_image

  private

  def acceptable_main_image
    return unless main_images.attached?
    main_image_size_err = ''
    main_image_valid = true
    main_image_cnt_type_err = ''
    main_images.each do |main_image|
      if main_image.byte_size >= 2.megabyte
        main_image_size_err = "is too big"
        main_image_valid = false
      end
      unless main_image.blob.content_type.start_with? 'image/'
        main_image_cnt_type_err = "must be an image"
        main_image_valid = false
      end
    end

    unless main_image_valid
      unless main_image_size_err.empty?
        errors.add(:main_images, main_image_size_err)
      end
      unless main_image_cnt_type_err.empty?
        errors.add(:main_images, main_image_cnt_type_err)
      end
    end
  end

  def acceptable_attachment_image
    return unless attachment_images.attached?
    attachment_image_size_err = ''
    attachment_image_valid = true
    attachment_image_cnt_type_err = ''
    attachment_images.each do |attachment_image|
      if attachment_image.byte_size >= 2.megabyte
        attachment_image_size_err = "is too big"
        attachment_image_valid = false
      end
      unless attachment_image.blob.content_type.start_with? 'image/'
        attachment_image_cnt_type_err = "must be an image"
        attachment_image_valid = false
      end
    end

    unless attachment_image_valid
      unless attachment_image_size_err.empty?
        errors.add(:attachment_images, attachment_image_size_err)
      end
      unless attachment_image_cnt_type_err.empty?
        errors.add(:attachment_images, attachment_image_cnt_type_err)
      end
    end
  end

  def self.to_external (source_department_id)
    Sate::Crmgt::ExternalOutgoingCorrespondence.where source_id: source_department_id
  end
end
