class AuthenticateUser
  prepend SimpleCommand
  attr_accessor :email, :password

  #this is where parameters are taken when the command is called
  def initialize(email, password)
    @email = email
    @password = password
  end

  #this is where the result gets returned
  def call
    JsonWebToken.encode(user_id: user.id) if user
  end

  private

  def user
    user = Sate::Auth::User.find_by_email(email)
    if user
      if user.active
        if user.authenticate(password)
          return user
        else
          raise ExceptionHandler::AuthenticationError, 'Invalid Credentials.'
          # errors.add :user_authentication, 'Invalid Credentials.'
          # nil
        end
      else
        raise ExceptionHandler::AuthenticationError, 'Inactive Credentials.'
        # errors.add :user_authentication, 'Inactive Credentials.'
        # nil
      end
    else
      raise ExceptionHandler::AuthenticationError, "Credentials doesn't Exist."
    end
  end
end